/*
 * File name: unit.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef QAMER_UNIT_HPP
#define QAMER_UNIT_HPP

#include "qamer_global.hpp"
#include "sceneplacableobject.hpp"
#include <QPoint>
#include <QMap>

namespace Qamer {

class UnitPrivate;

class QAMERSHARED_EXPORT Unit : public ScenePlacableObject
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(Unit)
    Q_PROPERTY(int unitSide READ unitSide WRITE setUnitSide)
    Q_PROPERTY(int movepoint READ movepoint WRITE setMovepoint)
    Q_PROPERTY(int facing READ facing WRITE setFacing NOTIFY facing_changed SCRIPTABLE scriptable)
    Q_PROPERTY(bool unique READ unique SCRIPTABLE loadable)

public:
    friend class IndirectAccessor;

    Q_INVOKABLE explicit Unit(QObject *parent = 0);
    ~Unit();
    
    qUnitSide unitSide() const;
    void setUnitSide(qUnitSide unitside);
    //should repalce by statValue?
    int movepoint() const;
    void setMovepoint(int movepoint);
    //stat value
    int statValueCount() const;
    Q_INVOKABLE bool hasStatValue(const QByteArray &name) const;
    Q_INVOKABLE int addStatValue(const QByteArray &name,
                                  int max = 255, int min = 0,
                                  int flags = Enum::StatValueZeroAtBegin,
                                  int val = 0);
    Q_INVOKABLE int statCurrentValue(const QByteArray& name) const;
    Q_INVOKABLE int statMaxValue(const QByteArray& name) const;
    Q_INVOKABLE int statMinValue(const QByteArray& name) const;
    Q_INVOKABLE bool increaseStatValue(const QByteArray& name, int val);
    Q_INVOKABLE void resetStatValue(const QByteArray& name);
    Q_INVOKABLE void setStatCurrentValue(const QByteArray &name, int cur);
    Q_INVOKABLE void setStatMaxValue(const QByteArray &name, int max);
    Q_INVOKABLE void setStatMinValue(const QByteArray &name, int min);
    //logically facing, also change drawing
    int facing() const;
    void setFacing(int facing);
    //is unique
    bool unique() const;
    Q_INVOKABLE void setUnique(bool yes = true);
    //skill
        //need update in ui
    Q_INVOKABLE void addSkill(qObjectId id);
        //for script support
    Q_INVOKABLE QVariantList skillList(int filter = Enum::ObjectSubTypeMask) const;
    QList<qObjectId> skillIdList(int filter = Enum::ObjectSubTypeMask) const;
    //resistance to statuses and elements
    Q_INVOKABLE int resistance(int id) const;
    void setResistance(int id, int val);//all non-setted view as 0(normal effect)

    void painted(QPainter *painter, const QRect &updateRect);
    //prototype pattern
    GameObject *clone() const;
    bool isClonable() const;
signals:
    void statValue_changed(const QByteArray &name, int which, int value);
    void action_ended();
    void facing_changed();
    void skill_added();

protected:
    void updatePosition();

    Unit(UnitPrivate *pdd, QObject *parent);
    Unit(const Unit &that);
private:

};

} // end namespace Qamer

#endif // QAMER_UNIT_HPP
