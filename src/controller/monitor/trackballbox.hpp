/*
 *File name: trackballbox.hpp
 *Author: kwueron
 *E-mail: %EMAIL%
 *Last revised: 2014/1/30
 *Published under Public Domain
 */

#ifndef TRACKBALLBOX_HPP
#define TRACKBALLBOX_HPP

#include <QGroupBox>

class TrackballBox : public QGroupBox
{
    Q_OBJECT
public:
    explicit TrackballBox(QWidget *parent = 0);

signals:

public slots:

};

#endif // TRACKBALLBOX_HPP
