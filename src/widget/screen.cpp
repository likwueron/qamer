/*
 * File name: screen.cpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "screen.hpp"
#include <QVariant>
#include <QMetaType>
#include <QDebug>

namespace Qamer {

Screen::Screen(QWidget *parent)
    : QWidget(parent)
{

}

void Screen::when_widgetToScreen_required()
{
    QVariant v = sender()->property("toScreen");
    if(v.canConvert(QMetaType::Int)) {
        emit changeScreen_required(v.toInt());
    }
}

void Screen::when_widgetPerformScript_required()
{

}

} // namespace Qamer

