/*
 * File name: qpainterprototype.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "qpainterprototype.hpp"
#include "scriptgeneral.hpp"

Q_DECLARE_METATYPE(QPainter*)

namespace Qamer {

QPainterPrototype::QPainterPrototype(QObject *parent) :
    QObject(parent)
{
}

void QPainterPrototype::drawArc(const QRectF &rectangle, int startAngle, int spanAngle)
{
    QPainter *p = qscriptvalue_cast<QPainter*>(thisObject());
    if(p) {
        p->drawArc(rectangle, startAngle, spanAngle);
    }
    else {
        throwTypeError(context(), "QPainter");
    }
}

void QPainterPrototype::drawArc(const QRect &rectangle, int startAngle, int spanAngle)
{
    drawArc(QRectF(rectangle), startAngle, spanAngle);
}

void QPainterPrototype::drawArc(int x, int y, int width, int height, int startAngle, int spanAngle)
{
    drawArc(QRectF(x, y, width, height), startAngle, spanAngle);
}

} // end namespace Qamer
