/*
 * File name: parameter.hpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PARAMETER_HPP
#define PARAMETER_HPP

#include "../qamer_export.hpp"
#include <QMetaType>
#include <QByteArray>
#include <QVariant>
#include <QDebug>

//if it stored a string(QByteArray)
//the first char will stored length of the string

//if stored int
//first byte become 0; int will be stored at 1st-4th bytes

class QAMERSHARED_EXPORT Parameter
{
public:
    //max is 256 because unsigned char is from 0 to 255
    static const short defaultSize = 64;
    //if defaultLen within Char's range, Parameter can store int array
    //this value's unit: sizeof(int)
    //but the rest of this class use byte as unit
    static const char defaultIntsMax;
    //for qmetatype
    static const int defaultParameterMID;
    static const int defaultParaListMID;

    static Parameter fromRawData(const char *bytes);

    Parameter();
    Parameter(int number);
    Parameter(const int *numArray, char size);
    Parameter(char c);
    Parameter(const char* cstr);
    Parameter(const QByteArray &text);
    Parameter(const QVariant &that);
    Parameter(const QVariantList &that);
    Parameter(const Parameter &that);
    Parameter(const Parameter &&that);
    ~Parameter();

    QVariant::Type type() const;
    //test
    bool isInt() const;
    bool isIntArray() const;
    bool isChar() const;
    bool isByteArray() const;
    //special test
    bool isBaseOperator() const;
    bool isMathBracket() const;
    bool isBaseMathSign() const;
    //getter
    int toInt(bool *ok = nullptr) const;
    int* toIntArray(int &len) const;
    const int* toCIntArray(int &len) const;
    char toChar() const;
    QByteArray toByteArray() const;
    //setter
    void setValue(int number);
    void setValue(const int *numArray, char size);
    void setValue(char c);
    void setValue(const char* cstr);
    void setValue(const QByteArray &text);
    void setValue(const Parameter &that);
    //unit: byte
    unsigned char count() const;
    inline unsigned char length() const;

    Parameter& operator = (int that);
    Parameter& operator = (char that);
    Parameter& operator = (const char *that);
    Parameter& operator = (const QByteArray &that);
    Parameter& operator = (const Parameter &that);
    Parameter& operator = (const Parameter &&that);

    bool operator == (const Parameter &that) const;

    inline const int* cIntPtr() const;
    inline const char* cCharPtr() const;
private:
    void setup();

    int* intPtr();
    char* charPtr();
    //unit: byte
    //add minus if you want to set as int or int array
    void setLength(short len);

    char mp_array[defaultSize];
};

unsigned char Parameter::length() const
{
    return count();
}

const int *Parameter::cIntPtr() const
{
    return (int*)(mp_array+1);
}

const char *Parameter::cCharPtr() const
{
    return (mp_array+1);
}
//operator BEGIN
QAMERSHARED_EXPORT QDebug operator<<(QDebug dbg, const Parameter &para);

inline bool operator==(const Parameter &a, int b)
{
    return (a.isInt()) && (*(a.cIntPtr()) == b);
}

inline bool operator==(int b, const Parameter &a)
{
    return (a.isInt()) && (*(a.cIntPtr()) == b);
}

inline bool operator==(const Parameter &a, char b)
{
    return (a.isChar()) && (*(a.cCharPtr()) == b);
}

inline bool operator==(char b, const Parameter &a)
{
    return (a.isChar()) && (*(a.cCharPtr()) == b);
}

inline bool operator==(const Parameter &a, const char *b)
{
    int count = a.count();
    int i;
    for(i = 0; (i < count) && (*b != 0); i++, b++) {
        if(*(a.cCharPtr()+i) != *b) return false;
    }
    return (*b == 0) && (i == count); //b reach end && a reach end
}

inline bool operator==(const char *b, const Parameter &a)
{
    int count = a.count();
    int i;
    for(i = 0; (i < count) && (*b != 0); i++, b++) {
        if(*(a.cCharPtr()+i) != *b) return false;
    }
    return (*b == 0) && (i == count); //b reach end && a reach end
}

inline bool operator==(const Parameter &a, const QByteArray &b)
{
    int count = a.count();
    return ((count == b.count()) && qstrncmp(a.cCharPtr(), b.constData(), count) == 0);
}

inline bool operator==(const QByteArray &b, const Parameter &a)
{
    int count = a.count();
    return ((count == b.count()) && qstrncmp(a.cCharPtr(), b.constData(), count) == 0);
}

inline bool operator<(const Parameter &a, int b)
{
    return *(a.cIntPtr()) < b;
}

inline bool operator<(int b, const Parameter &a)
{
    return b < *(a.cIntPtr());
}

inline bool operator<(const Parameter &a, char b)
{
    return *(a.cCharPtr()) < b;
}

inline bool operator<(char b, const Parameter &a)
{
    return b < *(a.cCharPtr());
}

Q_DECLARE_TYPEINFO(Parameter, Q_MOVABLE_TYPE);

typedef QList<Parameter> ParaList;

Q_DECLARE_METATYPE(Parameter)
Q_DECLARE_METATYPE(ParaList)

#endif // PARAMETER_HPP
