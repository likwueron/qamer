/*
 * File name: objectinfo.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_OBJECTINFO_HPP
#define QAMER_OBJECTINFO_HPP

#include "qamer_global.hpp"
#include <QString>

namespace Qamer {

struct QAMERSHARED_EXPORT ObjectInfo {
    ObjectInfo();
    ObjectInfo(const ObjectInfo &that);
    ~ObjectInfo();

    QString name;
    QString description;
    //about image(icon/avast)
    int avatarSpriteSheet;
};

} // end namespace Qamer

#endif // QAMER_OBJECTINFO_HPP
