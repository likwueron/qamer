/*
 * File name: qs_gameobjectiterator.cpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "qs_gameobjectiterator.hpp"
#include "../gameobjectiditerator.hpp"
#include "../gameobject.hpp"
#include <QScriptContext>
#include <QScriptEngine>

Q_DECLARE_METATYPE(Qamer::GameObjectIdIterator*)

namespace Qamer {
namespace QScript {

QS_GameObjectIterator::QS_GameObjectIterator(QObject *parent)
    : QObject(parent), QScriptable()
{

}

QS_GameObjectIterator::~QS_GameObjectIterator()
{

}

bool QS_GameObjectIterator::hasNext() const
{
    GameObjectIdIterator *i = qscriptvalue_cast<GameObjectIdIterator*>(thisObject());
    return i->hasNext();
}

QScriptValue QS_GameObjectIterator::next()
{
    GameObjectIdIterator *i = qscriptvalue_cast<GameObjectIdIterator*>(thisObject());
    return engine()->toScriptValue(i->next());
}

bool QS_GameObjectIterator::hasPrevious() const
{
    GameObjectIdIterator *i = qscriptvalue_cast<GameObjectIdIterator*>(thisObject());
    return i->hasPrevious();
}

QScriptValue QS_GameObjectIterator::previous()
{
    GameObjectIdIterator *i = qscriptvalue_cast<GameObjectIdIterator*>(thisObject());
    return engine()->toScriptValue(i->previous());
}

QScriptValue cf_GameObjectIterator_GameObjectIterator
(QScriptContext *context, QScriptEngine *engine)
{

    int argc = context->argumentCount();
    if(0 < argc) {
        QScriptValue arg0 = context->argument(0);
        if(arg0.isNumber()) {
            return engine->toScriptValue(
                        GameObjectIdIterator(arg0
                                             .toInt32()));
        }
        else if(arg0.isArray()) {
            QVariantList vList = arg0.toVariant().toList();
            QList<const GameObject*> list;
            list.reserve(vList.count());
            foreach(QVariant v, vList) {
                GameObject *o = nullptr;
                if(v.canConvert(QVariant::Int)) {
                    o = GameObject::findObject(v.toInt());
                }
                else if(v.canConvert<GameObjectId>()) {
                    o = v.value<GameObjectId>().object();
                }
                if(o) list << o;
            }
            return engine->toScriptValue(
                        GameObjectIdIterator(list));
        }
    }
    return engine->toScriptValue(
                GameObjectIdIterator());
}

} // end namespace QScript
} // end namespace Qamer
