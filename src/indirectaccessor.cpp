/*
 * File name: indirectaccessor.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "indirectaccessor.hpp"
#include "gameobject.hpp"
#include "gamescene.hpp"
#include "skill.hpp"
#include "_p/skill_p.hpp"
#include "unit.hpp"
#include "mics/parameter.hpp"
#include "pixmapeffect.hpp"
#include "diplomacy.hpp"
#include <QStack>
#include <QVariant>
#include <QDebug>

namespace Qamer {

IndirectAccessor::IndirectAccessor()
{
}

IndirectAccessor::~IndirectAccessor()
{

}

QList<ObjectInfo> IndirectAccessor::objectInfoList(QList<qObjectId> idList)
{
    QList<ObjectInfo> result;
    result.reserve(idList.count());
    foreach(qObjectId id, idList) {
        GameObject *obj = GameObject::findObject(id);
        result << obj->infomation();
    }
    return result;
}

QList<qObjectId> IndirectAccessor::skillIdListFromUnitId(qObjectId uid, int type)
{
    if(GameObject::objectTypeFromId(uid) & Enum::ObjectUnit) {
        Unit *u = (Unit*)(GameObject::findObject(uid));
        return u->skillIdList(type);
    }
    else return QList<qObjectId>();
}

QList<QPoint> IndirectAccessor::possibleTiles(const GameScene *scene,
                                              qObjectId sid, qObjectId uid)
{
    GameObject *obj = GameObject::findObject(uid);
    GameObject *s = GameObject::findObject(sid);
    if(!(s && obj && scene)) return QList<QPoint>();

    QVariant mappoint = obj->property("scenePoint");
    if(!mappoint.isValid()) {
        return QList<QPoint>();
    }

    ParaList paraList;
    QMetaObject::invokeMethod(s, "parameter", Q_RETURN_ARG(ParaList, paraList), Q_ARG(int, Enum::ParaTiles));
    int range = resolvePostorder(paraList, obj);

    QByteArray finderName = s->property("possibleTilesFinder").toByteArray();

    return scene->unitFindPath(finderName, mappoint.toPoint(), range, Enum::FaceNone);
}

QList<QPoint> IndirectAccessor::effectTiles(const GameScene *scene,
                                            qObjectId sid, qObjectId uid)
{
    //objectFromId
    GameObject *obj = GameObject::findObject(uid);
    GameObject *s = GameObject::findObject(sid);
    if(!(s && obj && scene)) return QList<QPoint>();
    //paralist
    ParaList paraList;
    QMetaObject::invokeMethod(s, "parameter", Q_RETURN_ARG(ParaList, paraList), Q_ARG(int, Enum::ParaArea));
    int range = resolvePostorder(paraList, obj);

    //get finder
    QByteArray finderName = s->property("effectTilesFinder").toByteArray();

    //check "faced"
    QPoint point = obj->property("scenePoint").toPoint();
    int faced;
    if(point.isNull()) faced = Enum::FaceNone;
    else faced = scene->unitDirection(point);

    return scene->unitFindPath(finderName, scene->cursorPoint(), range, faced);
}

Enum::UnitRelationship IndirectAccessor::relationBetween(qObjectId activeUnitId,
                                                         qObjectId passiveUnitId)
{
    GameObject *au = GameObject::findObject(activeUnitId);
    GameObject *pu = GameObject::findObject(passiveUnitId);
    if(!au || !pu) return Enum::RelateUnknown;

    int as = au->property("unitSide").toInt();
    int ps = pu->property("unitSide").toInt();

    return Diplomacy::diplomacy(as, ps);
}

QList<QPoint> IndirectAccessor::getValidTargetList(qObjectId sid, qObjectId uid, const QList<QPoint> &oriList)
{
    QList<QPoint> result;
    GameObject *skill = GameObject::findObject(sid);
    qUnitSide unitSide = getUnitSide(uid);
    if(skill && skill->isObjectType(Enum::ObjectSkill) &&
            unitSide != UINT_MAX) {
        int vTarget = skill->property("validTarget").toInt();
        int count = oriList.count();
        result.reserve(count);
        for(int i = 0; i < count; i++) {
            QPoint point = oriList.at(i);
            qObjectId tUid = GameScene::currentScene()->unitIdAt(point);
            qUnitSide tUnitSide = getUnitSide(tUid);
            Enum::UnitRelationship relate = Diplomacy::diplomacy(unitSide, tUnitSide);
            if(vTarget & int(relate)) {
                result << point;
            }
        }
    }
    return result;
}

qUnitSide IndirectAccessor::getUnitSide(qObjectId uid)
{
    GameObject *unit = GameObject::findObject(uid);
    if(unit && unit->isObjectType(Enum::ObjectUnit)) return unit->property("unitSide").toInt();
    else return UINT_MAX;
}

QList<PixmapEffect*> IndirectAccessor::pixmapEffects(qObjectId sid, const QList<qObjectId> &uids, QObject *admin)
{
    //0 as active, rest as passive
    QList<PixmapEffect*> result;

    Skill *skill = (Skill*)GameObject::findObject(sid, Enum::ObjectSkill);
    int count = uids.count();
    if(!count) ;
    else {
        //prepaired for animation
        GameObject *unit = GameObject::findObject(uids.at(0), Enum::ObjectUnit);
        GameScene *scene = nullptr;
        QRect unitGeo;
        QPoint unitAt;
        if(unit) {
            scene = (GameScene*)GameObject::findObject(unit->property("sceneId").toInt(), Enum::ObjectScene);
            if(scene) {
                unitGeo = unit->geometry();
                unitAt = unit->property("scenePoint").toPoint();
            }

        }

        //extract animation effect on active
        PixmapEffect *a_effect = new PixmapEffect(skill->d_func(), skill);
        a_effect->move(unitGeo, Enum::MoveHCenter | Enum::MoveBottom | Enum::MoveOn);
        qDebug() << "Unit:" << unitGeo;
        qDebug() << "effect:" << a_effect->geometry();
        a_effect->setFacing(Enum::SchemeDefault);
        a_effect->setFinishing();
        addPixmapEffect(result, a_effect, admin);
        addPixmapEffect(result, a_effect->backgroundEffect(), admin);

        for(int i = 1; i < count; i++) {
            PixmapEffect *b_effect = new PixmapEffect(skill->d_func(), skill);
            //get direction from unit 0 to unit n
            int facing = Enum::SchemeDefault;
            QRect unit2Geo;
            QPoint unit2At;
            if(scene) {
                GameObject *unit2 = GameObject::findObject(uids.at(i), Enum::ObjectUnit);
                if(unit2) {
                    unit2Geo = unit2->geometry();
                    unit2At = unit2->property("scenePoint").toPoint();
                    facing = schemeFromFacing(scene->objectOrientation(
                                                  unitAt.x(), unitAt.y(),
                                                  unit2At.x(), unit2At.y()));
                }
            }

            b_effect->setFacing(facing);
            b_effect->move(a_effect->geometry().topLeft());
            b_effect->setProgressing(5, unit2Geo, Enum::MoveOn | Enum::MoveBottom | Enum::MoveHCenter);
            b_effect->setFinishing();
            addPixmapEffect(result, b_effect, admin);
            addPixmapEffect(result, b_effect->backgroundEffect(), admin);
        }
    }

    return result;
}

int IndirectAccessor::resolvePostorder(const ParaList &list, GameObject *obj)
{
    int count = list.count();
    bool error = !count;
    QStack<int> numStack;

    for(int i = 0; i < count; i++) {
        Parameter cur = list.at(i);
        if(cur.isBaseOperator()) {
            if(numStack.count() >= 2) {
                int a2 = numStack.pop();
                int a1 = numStack.pop();
                char op = cur.toChar();
                switch(op) {
                case '+':
                    numStack.push(a1 + a2);
                    break;
                case '-':
                    numStack.push(a1 - a2);
                    break;
                case '*':
                    numStack.push(a1 * a2);
                    break;
                case '/':
                    numStack.push(a1 / a2);
                    break;
                }
            }
            else {
                qWarning() << "The formula for" << obj->objectName() << "is not correct, return 0 as result.";
                error = true;
                break;
            }
        } // end operator
        else if(cur.isInt()) numStack.push(cur.toInt());
        else {
            QByteArray name = cur.toByteArray();
            QVariant property = obj->property(name);
            if(property.isValid()) { //try property to int
                if(property.canConvert(QVariant::Int)) {
                    numStack.push(property.toInt());
                }
                else {
                    qWarning() << QString("Object %1\'s property \"%2\" is not Integer, return 0 as result.")
                                  .arg(obj->objectName())
                                  .arg(QString(name));
                    error = true;
                    break;
                }
            }
            else { // try statvalue
                int stat;
                QMetaObject::invokeMethod(obj,
                                          "statCurrentValue",
                                          Q_RETURN_ARG(int, stat),
                                          Q_ARG(QByteArray, name));
                if(stat != invalidStatValue) numStack.push(stat);
                else {
                    qWarning() << QString("Object %1 has no stat value \"%2\", return 0 as result.")
                                  .arg(obj->objectName())
                                  .arg(QString(name));
                    error = true;
                    break;
                }
            } // end try statvalue
        }
    }

    if(!error && numStack.isEmpty()) {
        qWarning() << "Stack is empty when formula end, return 0 as result.";
        error = true;
    }

    return error ? 0 : numStack.pop();
}

void IndirectAccessor::addPixmapEffect(QList<PixmapEffect *> &list, PixmapEffect *effect, QObject *admin)
{
    if(effect) {
        list << effect;
        QObject::connect(effect, SIGNAL(destroyed(QObject*)),
                admin, SLOT(when_animation_ended(QObject*)));
    }

}

} // end namespace Qamer
