/*
 * File name: messagehandler.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "messagehandler.hpp"
#include <QMessageBox>
#include <QVariant>
#include <cstdlib>
#include <cstdio>
#include <ctime>

FILE *p_file = nullptr;

namespace Qamer {

#if (QT_VERSION >= QT_VERSION_CHECK(5, 0, 0))
void handleV5(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QString fullMsg = QString("%1 (%2:%3, %4)")
                        .arg(msg)
                        .arg(context.file)
                        .arg(context.line)
                        .arg(context.function);
    switch (type) {
    case QtDebugMsg:
        MessageHandler::sendDebugMsg(msg);
        break;
    case QtWarningMsg:
        MessageHandler::warningBox(fullMsg);
        break;
    case QtCriticalMsg:
        MessageHandler::writeToFile("fatal.txt", fullMsg.toLocal8Bit().constData());
        MessageHandler::criticalBox();
        break;
    case QtFatalMsg:
        MessageHandler::writeToFile("fatal.txt", fullMsg.toLocal8Bit().constData());
        abort();
    }
}
#else //if Qt 4
void handleV4(QtMsgType type, const char *msg)
{
    switch(type) {
    case QtDebugMsg:
        MessageHandler::sendDebugMsg(QString::fromUtf8(msg));
        break;
    case QtWarningMsg:
        MessageHandler::warningBox(QString::fromUtf8(msg));
        break;
    case QtCriticalMsg:
        MessageHandler::writeToFile("stderr.txt", msg);
        MessageHandler::criticalBox();
        break;
    case QtFatalMsg:
        MessageHandler::writeToFile("fatal.txt", msg);
        abort();
        break;
    }
}
#endif //end Qt version 5/4

MessageHandler* MessageHandler::smp_instance = nullptr;

MessageHandler::MessageHandler(QWidget *window)
    :up_window(window)
{
    Q_ASSERT(!smp_instance);
    smp_instance = this;
#if (QT_VERSION >= QT_VERSION_CHECK(5, 0, 0))
    qInstallMessageHandler(&handleV5);
#else
    qInstallMsgHandler(&handleV4);
#endif
    p_file = fopen("qamer.log", "a");
    if(p_file) {
        time_t timer;
        time(&timer);
        fprintf(p_file, ctime(&timer));
    }
}

MessageHandler::~MessageHandler()
{
#if (QT_VERSION >= QT_VERSION_CHECK(5, 0, 0))
    qInstallMessageHandler(nullptr);
#else
    qInstallMsgHandler(nullptr);
#endif
    smp_instance = nullptr;
    if(p_file) fclose(p_file);
}

void MessageHandler::sendDebugMsg(const QString &msg)
{
    if(p_file) fprintf(p_file, msg.toLatin1().append('\n').constData());
#ifdef QT_DEBUG
    fprintf(stdout, "%s\n", msg.toLocal8Bit().constData());
    fflush(stdout);
#else
    MessageHandler::window()->setProperty("debug", msg);
#endif
}

void MessageHandler::warningBox(const QString &msg)
{
#ifdef QT_DEBUG
    fprintf(stdout, "%s\n", msg.toLocal8Bit().constData());
    fflush(stdout);
#else
    QMessageBox::warning(MessageHandler::window(), "Warning", msg);
#endif
}

void MessageHandler::criticalBox()
{
    QMessageBox::critical(MessageHandler::window(),
                          "Critical",
                          "Critical error occured. Suggest reboot your computer.\n"
                          "The detail information will be written in file \"stderr.txt\"");
}

void MessageHandler::writeToFile(const char *fileName, const char *msg)
{
    FILE *file;
    file = fopen(fileName, "a+");
    if(file != NULL) {
        fprintf(file, "%s\n", msg);
        fclose(file);
    }
    else {
        fprintf(stderr, "%s\n", msg);
        fflush(stderr);
    }
}

QWidget *MessageHandler::window()
{
    return smp_instance->up_window;
}

} //end namespace Qamer
