﻿==授權==
本函式庫採用LGPL version 3授權，請參閱GNU基金會網站上的文件或是內附的COPYRIGHT和COPYRIGHT.LESSER

==簡介==
Qamer 是一個基於 Qt 的 2D 遊戲框架。
目前本程式在不穩定開發階段，請不要期待下一版的用法、函式或類別名稱不會有所改變。
Bug之類的就更不要說了。

本專案目前專注於Windows平台，如果其它平台上上的編譯、執行上有問題，你可能要自行解決。

==額外二進制檔案==
Qt5依賴Microsoft Visual C++ 2012 可轉發送套件。

本專案使用Qt5的Multimedia模組播放多媒體檔，而在Windows底下它使用DirectShow做其後端。
一般來說它可以播放mp3, wmv等格式。
如果你需要Ogg框架下的格式，請到http://xiph.org/dshow/下載並安裝解碼器

==依賴==
你需要一個Qt開發環境，你可以在http://www.qt.io/download-open-source/下載。
目前最新版本是Qt 5.5，不過因為它才剛推出，所以我還是停在Qt 5.4.2

你還需要libQtLua，其原作者是Alexandre Becoulet
不過我對這函式庫做了些修改，源始碼：https://github.com/likwueron/libqtlua_fork

在這之前有做過一些針對Qt4的相容性處理，不過加入QtLua後就沒有特別調整了。
有些地方無法通過編譯，請參閱Qt的文件了解如何修改。

==編譯==
在Qamer編譯之前，請先編譯Plugin，它提供了讀檔時的物件載入支援


