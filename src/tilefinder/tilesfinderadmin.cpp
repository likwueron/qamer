/*
 * File name: tilesfinderadmin.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "tilesfinderadmin.hpp"
#include "finder.hpp"
#include "../gamescene.hpp"
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
#include "../script/qs_cfaccessor.hpp"
#include <QScriptValue>
#include <QScriptEngine>
#endif
#include "../script/luascriptengine.hpp"
#include <QtLua/Value>
#include <QVariant>
#include <QDebug>

namespace Qamer {

TilesFinderAdmin::FinderData::FinderData()
    : finderPtr(nullptr)
{

}

TilesFinderAdmin::FinderData::FinderData(const FinderData &that)
    : name(that.name), finderPtr(that.finderPtr),
      flags(that.flags)
{

}

TilesFinderAdmin::FinderData::~FinderData()
{

}

QList<TilesFinderAdmin::FinderData> TilesFinderAdmin::sm_finderList;

TilesFinderAdmin::TilesFinderAdmin(const GameScene *scene)
    : cup_scene(scene)
{

}

TilesFinderAdmin::~TilesFinderAdmin()
{
}

void TilesFinderAdmin::init()
{

}

void TilesFinderAdmin::quit()
{
    foreach(FinderData t, sm_finderList) {
        switch(t.flags) {
        case ScriptFinder:
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
            delete (QScriptValue*)(t.finderPtr);
#else
            t.finderPtr = nullptr;
#endif
            break;
        case LuaFinder:
            delete (QtLua::Value*)(t.finderPtr);
            break;
        case CppFinder:
            delete (TileFinder::Finder*)(t.finderPtr);
            break;
        }
    }
}

void TilesFinderAdmin::insertFinder(const QByteArray& name,
                                    TileFinder::Finder *finder)
{
    if(isExist(name)) return;

    FinderData t;
    t.name = name;
    t.finderPtr = (void*)finder;
    t.flags = CppFinder;

    sm_finderList << t;
}

void TilesFinderAdmin::insertFinder(const QByteArray &name, LuaScriptEngine *eng)
{
    if(isExist(name)) return;
    FinderData t;
    t.name = name;
    t.finderPtr = (void*)eng;
    t.flags = LuaFinder;
    sm_finderList << t;
}

void TilesFinderAdmin::insertFinder(const QByteArray& name,
                                    QScriptEngine *eng)
{
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    if(isExist(name)) return;

    FinderData t;
    t.name = name;
    t.finderPtr = (void*)QScript::getSfFinder(name, eng);
    t.flags = ScriptFinder;

    sm_finderList << t;
#else
    Q_UNUSED(name)
    Q_UNUSED(eng)
#endif
}

QList<QPoint> TilesFinderAdmin::find(const QByteArray &name, QPoint originalPoint, int movepoint, int direction)
{
    foreach(const FinderData &t, sm_finderList) {
        if(name.isEmpty()) break;
        if(t.name == name) {
            switch(t.flags) {
            case ScriptFinder:
                return findByScript(cup_scene,
                                    (QScriptValue*)(t.finderPtr),
                                    originalPoint, movepoint, direction);
                break;
            case LuaFinder:
                return findByLua(cup_scene,
                                 (QtLua::Value*)(t.finderPtr),
                                 originalPoint, movepoint, direction);
            case CppFinder:
            default:
                return findByCpp(cup_scene,
                                 (TileFinder::Finder*)(t.finderPtr),
                                 originalPoint, movepoint, direction);
                break;
            }
        }
    }

    return QList<QPoint>() << originalPoint;
}

bool TilesFinderAdmin::isExist(const QByteArray &name)
{
    foreach(const FinderData &t, sm_finderList) {
        if(t.name == name) return true;
    }
    return false;
}

QList<QPoint> TilesFinderAdmin::findByCpp(
        const GameScene *scene, TileFinder::Finder *finder,
        QPoint oPoint, int mp, int direction)
{
    TileFinder::Finder *p_f = finder->usableOne(scene);
    QList<QPoint> result = p_f->find(oPoint, mp, direction);
    delete p_f;
    return result;
}

QList<QPoint> TilesFinderAdmin::findByLua(const GameScene *scene, QtLua::Value *finder,
                                          QPoint oPoint, int mp, int direction)
{
    auto luaR = finder->call(QVariantList() << oPoint << mp << direction);
    QList<QPoint> result;
    foreach(const QtLua::Value &luaT, luaR) {
        result << QPoint(luaT.at(1), luaT.at(2));
    }
    return result;
}

QList<QPoint> TilesFinderAdmin::findByScript(
        const GameScene *scene, QScriptValue *finder,
        QPoint oPoint, int mp, int direction)
{
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    QScriptValue func = finder->property("find");
    if(!func.isFunction()) qCritical() << "Qamer::TilesFinderAdmin: assign variable has no funciton \"find\"";
    QScriptEngine *eng = func.engine();
    QScriptValue r = func.call(*finder,
                               QScriptValueList() <<
                               eng->toScriptValue(GameObjectId(scene->objectId())) <<
                               eng->toScriptValue(QPointF(oPoint)) <<
                               mp <<
                               direction);
    QVariantList list = r.toVariant().toList();

    QList<QPoint> result;
    foreach(const QVariant &v, list) {
        result << v.toPoint();
    }
    return result;
#else
    Q_UNUSED(scene)
    Q_UNUSED(finder)
    Q_UNUSED(oPoint)
    Q_UNUSED(mp)
    Q_UNUSED(direction)
    return QList<QPoint>();
#endif
}

} //end namespace Qamer

