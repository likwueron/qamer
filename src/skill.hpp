/*
 * File name: skill.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_SKILL_HPP
#define QAMER_SKILL_HPP

#include "qamer_global.hpp"
#include "gameobject.hpp"
#include "pixmapeffect.hpp"
#include "mics/parameter.hpp"
#include <QByteArray>
#include <QMap>

namespace Qamer {

class SkillPrivate;

class QAMERSHARED_EXPORT Skill : public GameObject
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(Skill)
    Q_PROPERTY(QByteArray possibleTilesFinder READ possibleTilesFinderName WRITE setPossibleTilesFinderName)
    Q_PROPERTY(QByteArray effectTilesFinder READ effectTilesFinderName WRITE setEffectTilesFinderName)
    Q_PROPERTY(int validTarget READ validTarget WRITE setValidTarget)
    Q_PROPERTY(int validPossibleTiles READ validPossibleTiles WRITE setValidPossibleTiles)
    
public:
    friend class IndirectAccessor;

    enum PixEffectMode {
        PixEffectSingle = 0x0,
        PixEffectFixed = 0x0,
        PixEffectMultiple = 0x1,
        PixEffectMoving = 0x2,
        //unit: centi-second, 7-bit for each
        PixEffectWaitMask = 0x7F00,
        PixEffectWaitPPMask = PixEffectWaitMask,
        PixEffectWaitAPMask = PixEffectWaitMask << 7
        //??
    };

    Q_INVOKABLE explicit Skill(QObject *parent = 0);
    ~Skill();

    Q_INVOKABLE ParaList parameter(int type) const;
    Q_INVOKABLE void setParameter(int type, const QString &para);
    //about scene/map
    void setPossibleTilesFinderName(const QByteArray &name);
    QByteArray possibleTilesFinderName() const;
    void setEffectTilesFinderName(const QByteArray &name);
    QByteArray effectTilesFinderName() const;

    int validTarget() const;
    void setValidTarget(int ur);
    int validPossibleTiles() const;
    void setValidPossibleTiles(int ur);

    int animationSpeed() const;
    void setAnimationSpeed(int speed);
    int animationWaitBetweenUnit();
    void setAnimationWaitBetweenUnit(int csecond);

    //paint
    void painted(QPainter *painter, const QRect &updateRect);
protected:
    Skill(SkillPrivate *dpp, QObject *parent);
    Skill(const Skill &that);
private:

};

} // end namespace Qamer

#endif //QAMER_SKILL_HPP
