/*
 *File name: controller.cpp
 *Author: kwueron
 *E-mail: likwueron@gmail.com
 *Last revised: 2014/1/11
 *Published under Public Domain
 */

#include "controller.hpp"
#include "joystick.hpp"
#include "controllerevent.hpp"
#include <QStringList>
#include <QCoreApplication>

QStringList Controller::sm_classNameList;
QList<const QMetaObject*> Controller::sm_classMetaObjectList;
QList<QString> Controller::sm_layoutList;

Controller::Controller(QObject *parent) :
    QObject(parent), mp_j(0x0)
{
}

Controller::~Controller()
{
}

void Controller::init()
{
    Joystick::init();
}

void Controller::quit()
{
    Joystick::quit();
}

void Controller::registerController(const QMetaObject &meta, const QString &name)
{
    if(sm_classNameList.contains(name)) {
        return;
    }
    const QMetaObject *p_meta = &meta;
    const QMetaObject *p_smeta = p_meta;
    while(p_smeta = p_smeta->superClass()) {
        if(QString(staticMetaObject.className()) == p_smeta->className()
                && p_meta->constructorCount()) {
        //is [superclass name] equal to "Controller"
        //AND is there controcutor
            sm_classNameList << (name.isEmpty() ? QString(p_meta->className()) : name);
            sm_classMetaObjectList << p_meta;
            QObject *p_o = p_meta->newInstance();
            sm_layoutList << qobject_cast<Controller*>(p_o)->layout();
            delete p_o;

            break;
        }
    }
}

QStringList Controller::registeredController()
{
    return sm_classNameList;
}

Controller *Controller::createController(int jid, const QString &controller, QObject *parent)
{
    Joystick *p_j = Joystick::createJoystick(jid);
    if(!p_j) {
        //fail to require Joystick
        return 0x0;
    }

    const QMetaObject *p_meta = 0x0;
    int num = sm_classMetaObjectList.count();
    if(controller.isEmpty()) {
        //try to find out most proper mapping
        for(int i = 0; i < num; i++) {
            if(compareLayout(sm_layoutList.at(i), p_j->layout())) {
                p_meta = sm_classMetaObjectList.at(i);
                break;
            }
        }

    }
    else {
        for(int i = 0; i < num; i++) {
            if(sm_classNameList.at(i) == controller) {
                p_meta = sm_classMetaObjectList.at(i);
                break;
            }
        }
    }

    if(p_meta) {
        QObject *p_o = p_meta->newInstance(Q_ARG(QObject*, parent));
        Controller *p_c = qobject_cast<Controller*>(p_o);
        if(p_c) {
            p_c->mp_j = p_j;
            connect(p_j, SIGNAL(hatValueChanged(int,qint8)),
                    p_c, SLOT(when_hatValue_changed(int,qint8)));
            connect(p_j, SIGNAL(axisValueChanged(int,qint16)),
                    p_c, SLOT(when_axisValue_changed(int,qint16)));
            connect(p_j, SIGNAL(buttonValueChanged(int,qint8)),
                    p_c, SLOT(when_buttonValue_changed(int,qint8)));
            connect(p_j, SIGNAL(trackballValueChanged(int,int,int)),
                    p_c, SLOT(when_trackballValue_changed(int,int,int)));
            return p_c;
        }
    }
    return 0x0;
}

void Controller::refreshControllerList()
{
    Joystick::refreshJoystickList();
}

QStringList Controller::controllerList()
{
    return Joystick::joystickNameList();
}

void Controller::setJoystickFocus(int jid, QObject *on_object)
{

}

Controller *Controller::newControllerMapping(const QString &controller)
{
    const QMetaObject *p_meta = 0x0;
    int num = sm_classMetaObjectList.count();
    for(int i = 0; i < num; i++) {
        if(sm_classNameList.at(i) == controller) {
            p_meta = sm_classMetaObjectList.at(i);
            break;
        }
    }

    if(p_meta) {
        QObject *p_o = p_meta->newInstance(Q_ARG(QObject*, parent()));
        Controller *p_c = qobject_cast<Controller*>(p_o);
        if(p_c) {
            p_c->mp_j = mp_j;
            connect(mp_j, SIGNAL(axisValueChanged(int,int)),
                    p_c, SLOT(when_hatValue_changed(int,int)));
            connect(mp_j, SIGNAL(buttonValueChanged(int,int)),
                    p_c, SLOT(when_buttonValue_changed(int,int)));
            connect(mp_j, SIGNAL(hatValueChanged(int,int)),
                    p_c, SLOT(when_axisValue_changed(int,int)));
            connect(mp_j, SIGNAL(trackballValueChanged(int,int,int)),
                    p_c, SLOT(when_trackballValue_changed(int,int,int)));
            deleteLater();
            return p_c;
        }
    }
    return 0x0;
}

bool Controller::compareLayout(const QString &a, const QString &b)
{
    int acount = a.count();
    int bcount = b.count();
    acount = qMin(acount, bcount);
    for(int i = 0; i < acount; i++) {
        QChar ca = a.at(i);
        QChar cb = b.at(i);
        if(ca != 'a' && cb != 'a' && ca != cb) {
            return false;
        }
    }
    return true;
}

QString Controller::joystickLayout() const
{
    if(mp_j) {
        return mp_j->layout();
    }
    return QString();
}


bool Controller::sendEvent(int role, const QString &name, qint64 value)
{
    if(!mp_focusObject) {
        return false;
    }
    Qamer::ControllerEvent cevent(mp_j->id(), role, name, value);
    QCoreApplication::sendEvent(mp_focusObject, &cevent);
    return true;
}


qint64 encodeQint32(qint32 a, qint32 b)
{
    qint64 bx = b;
    if(b < 0) {
        bx &= 0xffffffff;
    }
    return (qint64(a) << 32 | bx);
}


void decodeQint64(qint64 fusion, qint32 &a, qint32 &b)
{
    a = fusion >> 32;
    b = fusion;
}
