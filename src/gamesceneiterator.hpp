/*
 * File name: gamesceneiterator.hpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GAMESCENEITERATOR_HPP
#define GAMESCENEITERATOR_HPP

#include "qamer_export.hpp"
#include "gamescene.hpp"
#include <QList>

namespace Qamer {

class QAMERSHARED_EXPORT GameSceneIterator
{
public:
    GameSceneIterator(const GameScene &scene);
    ~GameSceneIterator();

    virtual bool hasNext() const = 0;
    virtual bool hasPrevious() const = 0;
    virtual int next() = 0;
    virtual int previous() = 0;

protected:
    int m_col;
    int m_row;

    int m_tIndex;
private:
};

} // end namespace Qamer

#endif // GAMESCENEITERATOR_HPP
