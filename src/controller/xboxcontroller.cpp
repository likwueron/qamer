#include "xboxcontroller.hpp"
#include <limits>

XBoxController::XBoxController(QObject *parent) :
    Controller(parent)
{
}

QString XBoxController::layout() const
{
#ifdef Q_OS_WIN32
    return "00061500";
#else
    return "01061100";
#endif
}

QString XBoxController::monitorUiFile() const
{
    return "./controllerMonitor/xboxmonitor.ui";
}

void XBoxController::when_hatValue_changed(int hatId, qint8 value)
{
    Q_UNUSED(hatId)
#ifndef Q_OS_WIN32
    #ifdef CONTROLLER_SIGNAL
    emit action_done(Hat1, "DPad", value);
    #else
    sendEvent(Hat1, "DPad", value);
    #endif
#else
    Q_UNUSED(value)
#endif
}

void XBoxController::when_buttonValue_changed(int buttonId, qint8 value)
{
    QString name;
    int role;
    qint8 _value = value;
    switch(buttonId) {
    case Select:
        name = "Select";
        role = ButtonSelect;
        break;
    case Start:
        name = "Start";
        role = ButtonStart;
        break;
    case RB:
        name = "RB";
        role = ButtonR1;
        break;
    case LB:
        name = "LB";
        role = ButtonL1;
        break;
    case RS:
        name = "RS";
        role = ButtonR3;
        break;
    case LS:
        name = "LS";
        role = ButtonL3;
        break;
    case A:
        name = "A";
        role = Button1;
        break;
    case B:
        name = "B";
        role = Button2;
        break;
    case X:
        name = "X";
        role = Button3;
        break;
    case Y:
        name = "Y";
        role = Button4;
        break;
    case Guide:
        name = "Guide";
        role = ButtonGuide;
        break;
#ifdef Q_OS_WIN32
    case HatUp:
        name = "DPad";
        role = Hat1;
        _value = value ? Controller::HatUp : Controller::HatCentered;
        break;
    case HatDown:
        name = "DPad";
        role = Hat1;
        _value = value ? Controller::HatDown : Controller::HatCentered;
        break;
    case HatLeft:
        name = "DPad";
        role = Hat1;
        _value = value ? Controller::HatLeft : Controller::HatCentered;
        break;
    case HatRight:
        name = "DPad";
        role = Hat1;
        _value = value ? Controller::HatRight : Controller::HatCentered;
        break;
#endif
    default:
        role = Invalid;
        break;
    }
#ifdef CONTROLLER_SIGNAL
    emit action_done(role, name, _value);
#else
    sendEvent(role, name, _value);
#endif
}

void XBoxController::when_axisValue_changed(int axisId, qint16 value)
{
    QString name;
    int role;
    qint64 _value = value;
    qint64 int16max = qint64(std::numeric_limits<qint16>::max()) + 1;
    switch(axisId) {
    case 0:
        name = "X_Move";
        role = AxisX1;
        break;
    case 1:
        name = "Y_Move";
        role = AxisY1;
        break;
    case 3:
#ifdef Q_OS_WIN32
        name = "Y_Rotation";
        role = AxisY2;
#else
        name = "X_Rotation";
        role = AxisX2;
#endif
        break;
    case 4:
#ifdef Q_OS_WIN32
        name = "LT";
        role = AxisZ1 | ButtonL2;
        _value += int16max;
#else
        name = "Y_Rotation";
        role = AxisY2;
#endif
        break;
    case 2:
#ifdef Q_OS_WIN32
        name = "X_Rotation";
        role = AxisX2;
#else
        name = "LT";
        role = AxisZ1 | ButtonL2;
        _value += int16max;
#endif
        break;
    case 5:
        name = "RT";
        role = AxisZ2 | ButtonR2;
        _value += int16max;
        break;
    default:
        role = Invalid;
        break;
    }
#ifdef CONTROLLER_SIGNAL
    emit action_done(role, name, _value);
#else
    sendEvent(role, name, _value);
#endif
}

void XBoxController::when_trackballValue_changed(int trackballId, int dx, int dy)
{
    Q_UNUSED(trackballId)
    Q_UNUSED(dx)
    Q_UNUSED(dy)
}

void XBoxController::when_dataValue_changed(const JoystickData *data)
{
    Q_UNUSED(data)
}
