/*
 * File name: qs_size.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_QSCRIPT_QSIZEF_HPP
#define QAMER_QSCRIPT_QSIZEF_HPP

#include "../qamer_export.hpp"
#include <QObject>
#include <QScriptable>
#include <QScriptValue>
#include <QSizeF>

namespace Qamer {
namespace QScript {

class QAMERSHARED_EXPORT QS_Size : public QObject, protected QScriptable
{
    Q_OBJECT
    Q_PROPERTY(qreal width READ width WRITE setWidth)
    Q_PROPERTY(qreal height READ height WRITE setHeight)
    Q_PROPERTY(qreal area READ area)
public:
    explicit QS_Size(QObject *parent = 0);

    qreal width() const;
    void setWidth(qreal width);
    qreal height() const;
    void setHeight(qreal height);

    qreal area() const;

    Q_INVOKABLE bool isValid() const;

    Q_INVOKABLE QSizeF boundedTo(const QSizeF &that);
    Q_INVOKABLE QSizeF expandedTo(const QSizeF &that);
    Q_INVOKABLE void scale(qreal width, qreal height, Qt::AspectRatioMode mode);
    Q_INVOKABLE void scale(const QSizeF &size, Qt::AspectRatioMode mode);

    Q_INVOKABLE void addBy(const QSizeF &that);
    Q_INVOKABLE void subtractBy(const QSizeF &that);
    Q_INVOKABLE void multiplyBy(qreal var);
    Q_INVOKABLE void divideBy(qreal var);
    Q_INVOKABLE bool equal(const QSizeF &that);

    Q_INVOKABLE QSizeF add(const QSizeF &that) const;
    Q_INVOKABLE QSizeF subtract(const QSizeF &that) const;
    Q_INVOKABLE QSizeF multiply(qreal var) const;
    Q_INVOKABLE QSizeF divide(qreal var) const;

signals:

public slots:
    void transpose();
    QString toString() const;
};

QAMERSHARED_EXPORT QScriptValue cf_Size_Size(QScriptContext *context, QScriptEngine *engine);

} // end namespace QScript
} // end namespace Qamer

#endif // QAMER_QSCRIPT_QSIZEF_HPP
