/*
 * File name: supportextension.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "supportextension.hpp"

namespace Qamer {

SupportExtension::SupportExtension()
{
}

SupportExtension *SupportExtension::images()
{
    SupportExtension *p = new SupportExtension();
    p->setAllFileLabel("All Images");
    p->add("Bitmap", QStringList() << "dib" << "bmp");
    p->add("GIF", "gif");
    p->add("JPEG", QStringList() << "jfif" << "jpe" << "jpeg" << "jpg");
    //p->add("JPEG 2000", "jp2");
    p->add("PNG", "png");
    p->add("TIFF", QStringList() << "tiff" << "tif");
    return p;
}

QString SupportExtension::allFileLabel() const
{
    return m_allFileLabel;
}

void SupportExtension::setAllFileLabel(const QString &label)
{
    m_allFileLabel = label;
}

void SupportExtension::add(const QString &label, const QString &extension)
{
    if(label.isEmpty() || extension.isEmpty()) {
        return;
    }
    insert(label, extension);
}

void SupportExtension::add(const QString &label, const QStringList &extensions)
{
    int num = extensions.size();
    if(label.isEmpty() || !num) {
        return;
    }

    for(int i = 0; i < num; i++) {
        //inserMulti() reverse input order
        insert(label, extensions.at(i));
    }
}

QStringList SupportExtension::listFilter() const
{
    QMapIterator<QString, QString> i(*this);
    QStringList fileter;
    fileter.reserve(size());
    while(i.hasNext()) {
        i.next();
        fileter << "*." + i.value();
    }

    return fileter;
}

QStringList SupportExtension::listFilterByLabel(const QString &label) const
{
    QStringList exts = values(label);
    int num = exts.size();
    if(!num) {
        return QStringList();
    }

    QStringList filter;
    for(int i = 0; i < num; i++) {
        filter << QString("*.%1").arg(exts.at(i));
    }
    return filter;
}

QString SupportExtension::openFileFilter() const
{
    if(isEmpty()) {
        return QString();
    }

    QString filter;
    QString allFileFilter = "";
    bool hasAllFileLbl = !m_allFileLabel.isEmpty();
    if(hasAllFileLbl) {
        allFileFilter += m_allFileLabel + " (";
    }

    QString curLbl;
    QMapIterator<QString, QString> i(*this);
    while(i.hasNext()) {
        i.next();
        if(curLbl.isEmpty()) {
            curLbl = i.key();
            filter += QString("%1 (*.%2").arg(curLbl).arg(i.value());
        }
        else {
            if(curLbl != i.key()) {
                //new lbl
                curLbl = i.key();
                filter += QString(");;%1 (*.%2").arg(curLbl).arg(i.value());
            }
            else {
                //new ext
                filter += QString(" *.%1").arg(i.value());
            }
        }
        if(hasAllFileLbl) {
            allFileFilter += QString("*.%1 ").arg(i.value());
        }
    }
    //add end
    filter += ")";
    if(hasAllFileLbl) {
        allFileFilter += ");;";
        filter = allFileFilter + filter;
    }

    return filter;
    //All Images (*.bmp *.gif *.jpg *.png *.tif)
}

QString SupportExtension::openFileFilterByLabel(const QString &label) const
{
    QStringList exts = values(label);
    int num = exts.size();
    if(!num) {
        return QString();
    }


    QString filter = label + " (";
    for(int i = 0; i < num; i++) {
        filter += QString("*.%1 ").arg(exts.at(i));
    }
    filter += ")";
    return filter;
}

QString SupportExtension::openFileFilterByExtension(const QString &extension) const
{
    QString lbl = key(extension);
    if(lbl.isEmpty()) {
        return QString();
    }
    return QString("%1 (*.%2)").arg(lbl, extension);
}

QString SupportExtension::label(const QString &extension)
{
    return key(extension);
}

QStringList SupportExtension::extensions(const QString &label)
{
    return values(label);
}

} // end namespace Qamer
