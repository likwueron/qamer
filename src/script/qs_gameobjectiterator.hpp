/*
 * File name: qs_gameobjectiterator.hpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_QSCRIPT_GAMEOBJECTITERATOR_HPP
#define QAMER_QSCRIPT_GAMEOBJECTITERATOR_HPP

#include "../qamer_export.hpp"
#include <QObject>
#include <QScriptable>
#include <QScriptValue>

class QScriptContext;
class QScriptEngine;

namespace Qamer {
namespace QScript {

class QAMERSHARED_EXPORT QS_GameObjectIterator : public QObject, protected QScriptable
{
    Q_OBJECT
public:
    explicit QS_GameObjectIterator(QObject *parent = 0);
    ~QS_GameObjectIterator();

    Q_INVOKABLE bool hasNext() const;
    Q_INVOKABLE QScriptValue next();
    Q_INVOKABLE bool hasPrevious() const;
    Q_INVOKABLE QScriptValue previous();
protected:

signals:

public slots:
private:
};

QAMERSHARED_EXPORT QScriptValue cf_GameObjectIterator_GameObjectIterator
(QScriptContext *context, QScriptEngine *engine);

} // end namespace QScript
} // end namespace Qamer

#endif // QAMER_QSCRIPT_GAMEOBJECTITERATOR_HPP
