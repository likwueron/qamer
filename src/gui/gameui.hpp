/*
 * File name:
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_GAMEUI_HPP
#define QAMER_GAMEUI_HPP

#include "../gameobject.hpp"
#include <QFont>

namespace Qamer {

class GameUIPrivate;

class GameUI : public GameObject
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(GameUI)

    Q_PROPERTY(QFont font READ font WRITE setFont RESET resetFont)
    Q_PROPERTY(Qt::Orientation orientation READ orientation WRITE setOrientation)
public:
    GameUI(QObject *parent);
    ~GameUI();

    QFont font() const;
    void setFont(const QFont &font);
    void resetFont();

    Qt::Orientation orientation() const;
    void setOrientation(Qt::Orientation orientation);

signals:
    void orientation_changed();

protected slots:
    virtual void when_orientation_changed();

protected:
    GameUI(const GameUI &that);

private:
    void setup();
};

} // namespace Qamer

#endif // QAMER_GAMEUI_HPP
