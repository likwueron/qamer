/*
 * File name: parameter.cpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "parameter.hpp"
#include "cstring"

const char Parameter::defaultIntsMax =
            (defaultSize <= CHAR_MAX) ? ((defaultSize-1) / sizeof(int)) : 0;
const int Parameter::defaultParameterMID =
        qRegisterMetaType<Parameter>("Parameter");
const int Parameter::defaultParaListMID =
        qRegisterMetaType<ParaList>("ParaList");

Parameter Parameter::fromRawData(const char *bytes)
{
    Parameter p;
    std::memcpy(p.mp_array, bytes, Parameter::defaultSize);
    return p;
}

Parameter::Parameter()
{
    setup();
}

Parameter::Parameter(int number)
{
    setup();
    setValue(number);
}

Parameter::Parameter(const int *numArray, char size)
{
    setup();
    setValue(numArray, size);
}

Parameter::Parameter(char c)
{
    setup();
    setValue(c);
}

Parameter::Parameter(const char *cstr)
{
    setup();
    setValue(cstr);
}

Parameter::Parameter(const QByteArray &text)
{
    setup();
    setValue(text);
}

Parameter::Parameter(const QVariant &that)
{
    setup();
    switch(that.type()) {
    case QVariant::Int:
    case QVariant::Double:
        setValue(that.toInt());
        break;
    case QVariant::Char:
        setValue(that.toChar().toLatin1());
        break;
    case QVariant::String:
    case QVariant::ByteArray:
        setValue(that.toByteArray());
        break;
    default:
        break;
    }
}

Parameter::Parameter(const QVariantList &that)
{
    setup();
    int count = that.count();
    count = defaultIntsMax < count ? defaultIntsMax : count;
    int size = 0;
    int *array = new int[count];
    for(int i = 0; i < count; i++) {
        QVariant v = that.at(i);
        if(v.canConvert(QVariant::Int)) {
            array[size++] = v.toInt();
        }
    }
    setValue(array, size);
    delete [] array;
}

Parameter::Parameter(const Parameter &that)
{
    setValue(that);
}

Parameter::Parameter(const Parameter &&that)
{
    std::memmove(mp_array, that.mp_array, defaultSize);
}

Parameter::~Parameter()
{
}

QVariant::Type Parameter::type() const
{
    char len = mp_array[0];
    if(len == 1) return QVariant::Char;
    if(defaultIntsMax) {
        if(len < 0) return QVariant::Int;
        else return QVariant::ByteArray;
    }
    else {
        if(len == 0) return QVariant::Int;
        else return QVariant::ByteArray;
    }
}

bool Parameter::isInt() const
{
    return defaultIntsMax ? (mp_array[0] == -4) : !count();
}

bool Parameter::isIntArray() const
{
    return defaultIntsMax && (mp_array[0] < -4);
}

bool Parameter::isChar() const
{
    return (mp_array[0] == 1);
}

bool Parameter::isByteArray() const
{
    return defaultIntsMax ? (1 < mp_array[0]) : (1 < count());
}

bool Parameter::isBaseOperator() const
{
    bool result = isChar();
    switch(*cCharPtr()) {
    case '+':
    case '-':
    case '*':
    case '/':
        break;
    default:
        result = false;
        break;
    }
    return result;
}

bool Parameter::isMathBracket() const
{
    bool result = isChar();
    switch(*cCharPtr()) {
    case '{':
    case '}':
    case '(':
    case ')':
    case '<':
    case '>':
        break;
    default:
        result = false;
        break;
    }
    return result;
}

bool Parameter::isBaseMathSign() const
{
    bool result = isChar();
    switch(*cCharPtr()) {
    case '+':
    case '-':
    case '*':
    case '/':
    case '{':
    case '}':
    case '(':
    case ')':
    case '<':
    case '>':
        break;
    default:
        result = false;
        break;
    }
    return result;
}

int Parameter::toInt(bool *ok) const
{
    if(ok) *ok = isInt();
    return *(cIntPtr());
}

int *Parameter::toIntArray(int &len) const
{
    if(defaultIntsMax) {
        len = count();
        if(len) {
            int *p = new int[len / sizeof(int)];
            std::memcpy(p, cIntPtr(), len);
            len /= sizeof(int);
            return p;
        }
    }
    return nullptr;
}

const int *Parameter::toCIntArray(int &len) const
{
    if(defaultIntsMax) {
        len = count() / sizeof(int);//unit: byte to sizeof(int)
        if(len) return cIntPtr();
    }
    return nullptr;
}

char Parameter::toChar() const
{
    return *(cCharPtr());
}

QByteArray Parameter::toByteArray() const
{
    return QByteArray(cCharPtr(), count());
}

void Parameter::setValue(int number)
{
    *(intPtr()) = number;
    //GCC-WARNNING: overflow
    setLength(-1 * sizeof(int));
}

void Parameter::setValue(const int *numArray, char size)
{
    setLength(-size * sizeof(int));
    unsigned char _size = count();
    if(_size) std::memcpy(intPtr(), numArray, _size);
    else if(0 < size) *intPtr() = *numArray;
}

void Parameter::setValue(char c)
{
    *(charPtr()) = c;
    setLength(1);
}

void Parameter::setValue(const char *cstr)
{
    int limit = defaultSize-1;
    int i;
    for(i = 0; (i < limit) && (*cstr != 0); i++, cstr++) {
        mp_array[i+1] = *cstr;
    }
    setLength(i);
}

void Parameter::setValue(const QByteArray &text)
{
    int count = text.count();
    int len = count < (defaultSize-1) ? count : (defaultSize-1);
    std::memcpy(charPtr(), text.constData(), len);
    setLength(len);
}

void Parameter::setValue(const Parameter &that)
{
    std::memcpy(mp_array, that.mp_array, defaultSize);
}

unsigned char Parameter::count() const
{
    if(defaultIntsMax) return qAbs(mp_array[0]);
    else return (unsigned char)mp_array[0];
}

Parameter &Parameter::operator =(int that)
{
    setValue(that);
    return *this;
}

Parameter &Parameter::operator =(char that)
{
    setValue(that);
    return *this;
}

Parameter &Parameter::operator =(const char *that)
{
    setValue(that);
    return *this;
}

Parameter &Parameter::operator =(const QByteArray &that)
{
    setValue(that);
    return *this;
}

Parameter &Parameter::operator =(const Parameter &that)
{
    setValue(that);
    return *this;
}

Parameter &Parameter::operator =(const Parameter &&that)
{
    std::memmove(mp_array, that.mp_array, defaultSize);
    return *this;
}

bool Parameter::operator ==(const Parameter &that) const
{
    int len = count();
    if(len != that.count()) return false;
    if(!len) { // number
        return *(cIntPtr()) == *(that.cIntPtr());
    }
    else return (qstrncmp(cCharPtr(), that.cCharPtr(), len) == 0);
}

void Parameter::setup()
{
    std::memset(mp_array, 0, defaultSize);
}

int *Parameter::intPtr()
{
    return (int*)(mp_array+1);
}

char *Parameter::charPtr()
{
    return (mp_array+1);
}

void Parameter::setLength(short len)
{
    if(0 <= len) {
        if(len < defaultSize) mp_array[0] = (unsigned char)len;
        else mp_array[0] = defaultSize-1;
    }
    else if(defaultIntsMax) {
        if(-defaultSize < len) mp_array[0] = (char)len;
        else mp_array[0] = defaultIntsMax * sizeof(int);
    }
    else mp_array[0] = 0;
}

QDebug operator<<(QDebug dbg, const Parameter &para)
{
    switch(para.type()) {
    case QVariant::Int: {
        int len = 0;
        const int *array = para.toCIntArray(len);
        for(int i = 0; i < len; i++) {
            dbg.nospace() << array[i] << ", ";
        }
        break; }
    case QVariant::Char:
        dbg.nospace() << para.toChar();
        break;
    case QVariant::ByteArray:
        dbg.nospace() << para.toByteArray();
        break;
    default:
        break;
    }
    return dbg.space();
}
