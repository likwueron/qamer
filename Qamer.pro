#-------------------------------------------------
#
# Project created by QtCreator 2014-02-07T01:06:50
#
#-------------------------------------------------

#QMAKE_CXXFLAGS_RELEASE += -g
#QMAKE_CFLAGS_RELEASE += -g
#QMAKE_LFLAGS_RELEASE =

#QT SETTING
lessThan(QT_MAJOR_VERSION, 5) {
    CONFIG += uitools
    QT += phonon
    QMAKE_CXXFLAGS += -std=c++11
}
greaterThan(QT_MAJOR_VERSION, 4) {
    QT += widgets uitools multimedia core-private
    CONFIG += c++11
    #CONFIG += c++14
}

lessThan(QT_MAJOR_VERSION, 5) {
    QT += script
    DEFINES += ENABLE_QSCRIPT
}

#3RD LIBRARY SETTING
    #SDL library
#win32 {
#    SDLDIR = D:/_lib/SDL2-2.0.1/i686-w64-mingw32
#    LIBS += -L$$SDLDIR/lib
#    INCLUDEPATH += $$SDLDIR/include/
#}
#LIBS += -lSDL2main -lSDL2
#    qtlua library
greaterThan(QT_MAJOR_VERSION, 4):win32 {
    QTLUA_DIR = D:/_lib/libqtlua-2.0
    CONFIG(debug, debug|release): LIBS += -L$$QTLUA_DIR/_debug -lqtlua.dll
    else: LIBS += -L$$QTLUA_DIR/_release -lqtlua.dll
    INCLUDEPATH += $$QTLUA_DIR/src
}

#LoadPlugin
LIBS += -L$${PWD}/qamerPlugin
CONFIG(debug, debug|release): LIBS += -lGeometryCtord -lDrawCtord -lImageCtord
else: LIBS += -lGeometryCtor -lDrawCtor -lImageCtor

#LIBRARY SETTING
LIBNAME = Qamer
TEMPLATE = lib
DEFINES += QAMER_LIBRARY
#CONFIG += staticlib #switch static OR dynamic

#OUTPUT SETTING
DESTDIR = $${PWD}/lib
DLLDESTDIR = $${PWD}/bin

contains(CONFIG, staticlib): LIBNAME = $${LIBNAME}s
win32 {
    CONFIG(debug, debug|release): TARGET = $${LIBNAME}d
    else: TARGET = $${LIBNAME}
}

unix {
    TARGET = $${LIBNAME}
    target.path = /usr/lib
    INSTALLS += target
}

#FILES
SOURCES += \
#    src/controller/controller.cpp \
    src/controller/controllerevent.cpp \
#    src/controller/joystick.cpp \
#    src/controller/ps2controller.cpp \
#    src/controller/rawcontroller.cpp \
#    src/controller/xboxcontroller.cpp \
#    src/controller/monitor/controllermonitor.cpp \
#    src/controller/monitor/hatbox.cpp \
#    src/controller/monitor/trackballbox.cpp \
    src/spritesheet.cpp \
    src/core/dependinjector.cpp \
    src/gameobject.cpp \
    src/core/supportextension.cpp \
    src/gamescene.cpp \
#    src/controller/controlleradministrator.cpp \
    src/qamer.cpp \
    src/unit.cpp \
    src/skill.cpp \
    src/mics/rhombus.cpp \
    src/indirectaccessor.cpp \
    src/core/inordertopostorder.cpp \
    src/diplomacy.cpp \
    src/objectinfo.cpp \
    src/core/messagehandler.cpp \
    src/core/fixedmemorypool.cpp \
    src/application.cpp \
    src/mics/parameter.cpp \
    src/terrain.cpp \
    src/sceneplacableobject.cpp \
    src/gamesceneiterator.cpp \
    src/qamer_types.cpp \
    src/gui/gameprogressbar.cpp \
    src/gui/unitmonitor.cpp \
    src/gui/gamelabel.cpp \
    src/_p/unit_p.cpp \
    src/_p/sceneplacableobject_p.cpp \
    src/_p/gameobject_p.cpp \
    src/_p/pixmapdata_p.cpp \
    src/statvalue.cpp \
    src/_p/skill_p.cpp \
    src/_p/terrain_p.cpp \
    src/_p/modifierdata_p.cpp \
    src/_p/gamescene_p.cpp \
    src/gui/gameui.cpp \
    src/gui/_p/gameui_p.cpp \
    src/pixmapeffect.cpp \
    src/_p/pixmapeffect_p.cpp \
    src/mics/platform.cpp \
    src/gameobjectiditerator.cpp \
    src/fileloader/basetextreader.cpp \
    src/fileloader/fileloader.cpp \
    src/fileloader/qmadmin.cpp \
    src/fileloader/loadingitem.cpp \
    src/tilefinder/tilesfinderadmin.cpp \
    src/tilefinder/finder.cpp \
    src/tilefinder/findertetr.cpp \
    src/plugin/loadhelper.cpp \
    src/plugin/bastardstringctor.cpp \
    src/widget/screen.cpp \
    src/script/luascreencoverter.cpp \
    src/widget/label.cpp \
    src/script/luaparameterconverter.cpp
greaterThan(QT_MAJOR_VERSION, 4) {
    SOURCES += src/script/luascriptengine.cpp \
    src/script/luagameobject.cpp \
    src/script/luagameobjectlist.cpp
}
lessThan(QT_MAJOR_VERSION, 5) {
SOURCES += src/script/qs_general.cpp \
    src/script/qs_cfaccessor.cpp \
    src/script/qs_point.cpp \
    src/script/qs_size.cpp \
    #src/script/qs_rect.cpp \
    #src/script/qs_painter.cpp \
    src/script/qs_gameobject.cpp \
    src/script/qs_gameobjectiterator.cpp
}
lessThan(QT_MAJOR_VERSION, 5) {
    #SOURCES += src/media/sound.cpp
}

HEADERS += src/qamer_global.hpp \
#    src/controller/controller.hpp \
    src/controller/controllerevent.hpp \
#    src/controller/joystick.hpp \
#    src/controller/ps2controller.hpp \
#    src/controller/rawcontroller.hpp \
#    src/controller/xboxcontroller.hpp \
#    src/controller/monitor/controllermonitor.hpp \
#    src/controller/monitor/hatbox.hpp \
#    src/controller/monitor/trackballbox.hpp \
    src/spritesheet.hpp \
    src/core/dependinjector.hpp \
    src/gameobject.hpp \
    src/core/supportextension.hpp \
    src/gamescene.hpp \
#    src/controller/controlleradministrator.hpp \
    src/qamer.hpp \
#    src/controller/joystickdata.hpp
    src/unit.hpp \
    src/skill.hpp \
    src/mics/rhombus.hpp \
    src/indirectaccessor.hpp \
    src/core/inordertopostorder.hpp \
    src/diplomacy.hpp \
    src/objectinfo.hpp \
    src/core/messagehandler.hpp \
    src/core/fixedmemorypool.hpp \
    src/application.hpp \
    src/qamer_export.hpp \
    src/core/pseudosingleton.hpp \
    src/mics/parameter.hpp \
    src/terrain.hpp \
    src/sceneplacableobject.hpp \
    src/gamesceneiterator.hpp \
    src/qamer_types.hpp \
    src/gui/gameprogressbar.hpp \
    src/gui/unitmonitor.hpp \
    src/gui/gamelabel.hpp \
    src/_p/QObjectPrivate \
    src/_p/unit_p.hpp \
    src/_p/sceneplacableobject_p.hpp \
    src/_p/gameobject_p.hpp \
    src/_p/pixmapdata_p.hpp \
    src/statvalue.hpp \
    src/_p/skill_p.hpp \
    src/_p/terrain_p.hpp \
    src/_p/modifierdata_p.hpp \
    src/_p/gamescene_p.hpp \
    src/gui/gameui.hpp \
    src/gui/_p/gameui_p.hpp \
    src/pixmapeffect.hpp \
    src/_p/pixmapeffect_p.hpp \
    src/mics/platform.hpp \
    src/gameobjectiditerator.hpp \
    src/fileloader/basetextreader.hpp \
    src/fileloader/fileloader.hpp \
    src/fileloader/qmadmin.hpp \
    src/fileloader/loadingitem.hpp \
    src/tilefinder/tilesfinderadmin.hpp \
    src/tilefinder/finder.hpp \
    src/tilefinder/findertetr.hpp \
    src/plugin/igeneral.hpp \
    src/plugin/loadhelper.hpp \
    src/plugin/bastardstringctor.hpp \
    src/plugin/iscreen.hpp \
    src/widget/screen.hpp \
    src/script/luascreencoverter.hpp \
    src/widget/label.hpp \
    src/script/luaparameterconverter.hpp
greaterThan(QT_MAJOR_VERSION, 4) {
    HEADERS += src/script/luascriptengine.hpp \
    src/script/luagameobject.hpp \
    src/script/luagameobjectlist.hpp
}
lessThan(QT_MAJOR_VERSION, 5) {
HEADERS += src/script/qs_general.hpp \
    src/script/qs_cfaccessor.hpp \
    src/script/qs_point.hpp \
    src/script/qs_size.hpp \
    #src/script/qs_rect.hpp \
    #src/script/qs_painter.hpp \
    src/script/qs_gameobject.hpp \
    src/script/qs_gameobjectiterator.hpp
}
lessThan(QT_MAJOR_VERSION, 5) {
    #HEADERS += src/media/sound.hpp
}
