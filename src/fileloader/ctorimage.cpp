/*
 * File name: ctorimage.cpp
 * Copyright 2015 kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "ctorimage.hpp"
#include <QImage>
#include <QBitmap>
#include <QIcon>
#include <QPixmap>

#include <QDebug>

namespace Qamer {
namespace LoadCtor {

QVariant Image(const QVariantList &values)
{
    switch(values.count()) {
    case 1:
        return QImage(values.at(0).toString());
        break;
    default:
        return QImage();
        break;
    }
}

QVariant Pixmap(const QVariantList &values)
{
    switch(values.count()) {
    case 1:
        return QPixmap(values.at(0).toString());
        break;
    default:
        return QPixmap();
        break;
    }
}

QVariant Icon(const QVariantList &values)
{
    switch(values.count()) {
    case 1:
        return QIcon(values.at(0).toString());
        break;
    default:
        return QIcon();
        break;
    }
}

QVariant Bitmap(const QVariantList &values)
{
    Q_UNUSED(values)
    qWarning() << "Qamer::LoadingCtor::Bitmap: function not implemented, return QVariant().";
    return QVariant();
}

} // end namespace LoadCtor
} // namespace Qamer
