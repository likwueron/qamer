/*
 * File name:
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "luagameobjectlist.hpp"
#include "luagameobject.hpp"
#include <internal/QObjectWrapper>

using namespace QtLua;

namespace Qamer {

LuaGameObjectListConverter::LuaGameObjectListConverter()
    : MetaType<LuaGameObjectList>("LuaGameObjectList")
{

}

Value LuaGameObjectListConverter::qt2lua(State *ls, const LuaGameObjectList *qtvalue)
{
    Value result(Value::new_table(ls));
    int count = qtvalue->count();
    for(int i = 0; i < count; i++) {
        result[i+1] = Value(ls, QObjectWrapper::get_wrapper(ls, qtvalue->at(i)));
    }
    return result;
}

bool LuaGameObjectListConverter::lua2qt(LuaGameObjectList *qtvalue, const Value &luavalue)
{
    Value::const_iterator i;
    for(i = luavalue.cbegin(); i != luavalue.cend(); ++i) {
        Value v(i.value());
        auto qow = v.to_userdata_cast<QObjectWrapper>();
        LuaGameObject *o = qobject_cast<LuaGameObject*>(&qow->get_object());
        if(o) qtvalue->append(o);
    }
    return (0 < qtvalue->count());
}

} // namespace Qamer

