/*
 * File name: qs_cfaccessor.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "qs_cfaccessor.hpp"
#include "qs_general.hpp"
#include "../indirectaccessor.hpp"
#include "../mics/parameter.hpp"
#include "../gameobject.hpp"
#include <QMetaObject>
#include <QScriptContext>
#include <QScriptEngine>

namespace Qamer {
namespace QScript {

QScriptValue cf_getDamage(QScriptContext *ctx, QScriptEngine *eng)
{
    Q_UNUSED(eng)

    if(ctx->argumentCount() < 2) {
        throwArgcError(ctx, "cf_getDamage", 2);
        return QScriptValue();
    }
    //get id from script
    int skillId = ctx->argument(0).toInt32();
    int atkId = ctx->argument(1).toInt32();
    GameObject *s = GameObject::findObject(skillId);
    GameObject *atk = GameObject::findObject(atkId);
    if(!s | !atk) return QScriptValue();

    ParaList list;
    QMetaObject::invokeMethod(s, "parameter", Q_RETURN_ARG(ParaList, list), Q_ARG(int, Enum::ParaDamage));

    return IndirectAccessor::resolvePostorder(list, atk);
}

QScriptValue *getSfFinder(const QByteArray &name, QScriptEngine *eng)
{
    QScriptValue *finder = new QScriptValue(eng->globalObject().property(QString(name)));
    return finder;
}

} // end namespace QScript
} // end namespace Qamer
