/*
 * File name: gameobjectiditerator.cpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "gameobjectiditerator.hpp"
#include "_p/gameobject_p.hpp"

namespace Qamer {

GameObjectIdIterator::GameObjectIdIterator(int type)
    : m_index(-1)
{
    m_list.reserve(GameObjectPrivate::sm_objectMap.count());
    foreach(const GameObject *o, GameObjectPrivate::sm_objectMap) {
        if(o->isObjectType(type)) m_list << o;
    }
}

GameObjectIdIterator::GameObjectIdIterator(const QList<qObjectId> &idList)
    : m_index(-1)
{
    m_list.reserve(idList.count());
    foreach(const int &id, idList) {
        GameObject *o = GameObject::findObject(id);
        if(o) m_list << o;
    }
}

GameObjectIdIterator::GameObjectIdIterator(const QList<GameObjectId> &idList)
    : m_index(-1)
{
    m_list.reserve(idList.count());
    foreach(const GameObjectId &id, idList) {
        GameObject *o = id.object();
        if(o) m_list << o;
    }
}

GameObjectIdIterator::GameObjectIdIterator(const QList<const GameObject *> &objList)
    : m_index(-1)
{
    m_list = objList;
}

GameObjectIdIterator::GameObjectIdIterator(const GameObjectIdIterator &that)
    : m_list(that.m_list), m_index(that.m_index)
{

}

GameObjectIdIterator::~GameObjectIdIterator()
{

}

bool GameObjectIdIterator::hasNext() const
{
    return (m_index+1 < m_list.count());
}

GameObjectId GameObjectIdIterator::next()
{
    return GameObjectId(m_list.at(++m_index)->objectId());
}

bool GameObjectIdIterator::hasPrevious() const
{
    return (0 <= m_index-1);
}

GameObjectId GameObjectIdIterator::previous()
{
    return GameObjectId(m_list.at(--m_index)->objectId());
}

GameObjectId GameObjectIdIterator::peekNext() const
{
    return GameObjectId(m_list.at(m_index+1)->objectId());
}

GameObjectId GameObjectIdIterator::peekPrevious() const
{
    return GameObjectId(m_list.at(m_index-1)->objectId());
}

void GameObjectIdIterator::toFront()
{
    m_index = -1;
}

void GameObjectIdIterator::toBack()
{
    m_index = m_list.count();
}

} // end namespace Qamer
