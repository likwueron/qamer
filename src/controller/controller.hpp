/*
 *File name: controller.hpp
 *Author: kwueron
 *E-mail: likwueron@gmail.com
 *Last revised: 2014/1/11
 *Published under Public Domain
 */

#ifndef CONTROLLER_HPP
#define CONTROLLER_HPP

#include "../qamer_global.hpp"
#include <QObject>
#include <QMap>

class Joystick;
class JoystickData;

class QAMERSHARED_EXPORT Controller : public QObject
{
    Q_OBJECT
public:
    enum ActionType {
        Hat,
        Button,
        Axis,
        Trackball,
        //Undefined
    };

    //Same as SDL
    enum HatDirection {
        HatCentered = 0x0,
        HatUp = 0x1,
        HatRight = 0x2,
        HatDown = 0x4,
        HatLeft = 0x8,
        HatRightUp = (HatRight|HatUp),
        HatRightDown = (HatRight|HatDown),
        HatLeftUp = (HatLeft|HatUp),
        HatLeftDown = (HatLeft|HatDown)
    };

    //Unused    Trackball   Button  Axis    Hat
    //0         000         00000   0000    000
    enum ActionRole {
        Invalid = 0,
    //Hats 3-bit: Total 7
        Hat1 = 0x1, Hat2 = 0x2, Hat3 = 0x3,
        HatMask = 0x7,
    //Axes 4-bit: Total 15
        AxisX1 = 0x1 << 3, AxisY1 = 0x2 << 3, AxisZ1 = 0x3 << 3,
        AxisX2 = 0x4 << 3, AxisY2 = 0x5 << 3, AxisZ2 = 0x6 << 3,
        AxisX3 = 0x7 << 3, AxisY3 = 0x8 << 3, AxisZ3 = 0x9 << 3,
        AxisMask = 0xf << 3,
    //Buttons 5-bit: Total 31
        //The first 4 buttons assumpt to be A, B, X and Y on XBOX,
        //                              or Cross, Circle, Square and Triangle on PS
        Button1 = 0x1 << 7, Button2 = 0x2 << 7,
        Button3 = 0x3 << 7, Button4 = 0x4 << 7,
        //The 5th and 6th buttons assumpt to be Select and Start
        Button5 = 0x5 << 7, Button6 = 0x6 << 7,
        //The 7th button assumpts to be Guide
        Button7 = 0x7 << 7, Button8 = 0x8 << 7,
        Button9 = 0x9 << 7, Button10 = 0xa << 7,
        //LB, LT and LS on XBOX
        //or L1, L2 and L3 on PS
        ButtonL1 = 0xb << 7, ButtonL2 = 0xc << 7,
        ButtonL3 = 0xd << 7, ButtonL4 = 0xe << 7,
        ButtonR1 = 0xf << 7, ButtonR2 = 0x10 << 7,
        ButtonR3 = 0x11 << 7, ButtonR4 = 0x12 << 7,
        ButtonMask = 0x1f << 7,

        ButtonSelect = Button5,
        ButtonStart = Button6,
        ButtonGuide = Button7,
    //Trackball 3-bit: Total 7
        Trackball1 = 0x1 << 12, Trackball2 = 0x2 << 12,
        TrackballMask = 0x7 << 12
    };

    Q_ENUMS(ActionRole)

    //Child class constructor must add Q_INVOKABLE in front of them, ex:
    //Q_INVOKABLE ClassName(QObject *parent = 0);
    //or you cannot use createController() to create that child class.
    explicit Controller(QObject *parent = 0);
    virtual ~Controller();
    //Admin
    static void init();
    static void quit();
    static void registerController(const QMetaObject &meta, const QString &name = QString());
    static QStringList registeredController();
    static Controller* createController(int jid, const QString &controller = QString(), QObject *parent = 0);
    static void refreshControllerList();
    static QStringList controllerList();

    static void setJoystickFocus(int jid, QObject *on_object);
    QObjectList targetObject() const;
    void setTargetObject(QObject *obj);
    void setTargetObject(QObjectList list);
    void addTargetObject(QObject *obj);

    Controller* newControllerMapping(const QString &controller);

    static bool compareLayout(const QString &a, const QString &b);
    /*
     * ex: return "[hat number][axis number][button number][trackball number]";
     * (base = 10, width = 2 for each)
     * For a joystick with 1 Hat, 6 Axes, 11 Buttons and 0 Trackball
     * ex: return "01061100";
     */
    QString joystickLayout() const;
    virtual QString layout() const = 0;

    virtual QString monitorUiFile() const = 0;
signals:

public slots:

    virtual void when_hatValue_changed(int hatId, qint8 value) = 0;
    virtual void when_buttonValue_changed(int buttonId, qint8 value) = 0;
    virtual void when_axisValue_changed(int axisId, qint16 value) = 0;
    virtual void when_trackballValue_changed(int trackballId, int dx, int dy) = 0;
    virtual void when_dataValue_changed(const JoystickData *data) = 0;
protected:
    bool sendEvent(int role, const QString &name, qint64 value);

    Joystick *mp_j;
    QObject *mp_focusObject;

private:
    static QStringList sm_classNameList;
    static QList<const QMetaObject*> sm_classMetaObjectList;
    static QList<QString> sm_layoutList;
};

QAMERSHARED_EXPORT qint64 encodeQint32(qint32 a, qint32 b);
QAMERSHARED_EXPORT void decodeQint64(qint64 fusion, qint32 &a, qint32 &b);

#endif // CONTROLLER_HPP
