/*
 * File name: unitmonitor.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_UNITMONITOR_HPP
#define QAMER_UNITMONITOR_HPP

#include "gameui.hpp"
#include <QMap>
#include <QPoint>

namespace Qamer {

class QAMERSHARED_EXPORT UnitMonitor : public GameUI
{
    Q_OBJECT
public:
    Q_INVOKABLE UnitMonitor(QObject *parent = 0);
    ~UnitMonitor();

    void setObject(GameObject *obj);
    //"at" is relative position
    Q_INVOKABLE void watchBar(const QString &label,
                              const QByteArray &propertyName,
                              const QColor &minColor,const QColor &maxColor,
                              QPoint at);
    Q_INVOKABLE void watchValue(const QString &label,
                                const QByteArray &propertyName,
                                QPoint at);

    void painted(QPainter *painter, const QRect &updateRect);
public slots:

protected slots:

protected:
    void watch_inner(GameUI *uiobj, const QString &label, const QByteArray &propertyName, QPoint at);

    virtual void setUi();

    GameObject *up_unit;

    QRect m_avasterRect;
private:
    QList<GameUI*> m_interfaceList;
};

} // end namespace Qamer

#endif // QAMER_UNITMONITOR_HPP
