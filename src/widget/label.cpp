/*
 * File name: label.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "label.hpp"
#include <QPixmap>
#include <QPixmapCache>
#include <QtLua/Value>

using namespace QtLua;

namespace Qamer {

Label::Label(QWidget *parent)
    : QLabel(parent), WrappedObject()
{

}

QByteArray Label::script() const
{
    return m_script;
}

void Label::setScript(const QByteArray &script)
{
    m_script = script;
    m_func = get_state()->at(script);
}

void Label::setPixmapPath(const QString &path)
{
    QPixmap pix;
    if(!QPixmapCache::find(path, pix)) {
        pix.load(path);
        if(!pix.isNull()) {
            QPixmapCache::insert(path, pix);
        }
        else return;
    }
    setPixmap(pix);
}

} // namespace Qamer

