/*
 * File name: qs_general.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef QAMER_QSCRIPT_GENERAL_HPP
#define QAMER_QSCRIPT_GENERAL_HPP

#include "../qamer_export.hpp"
#include <QString>
#include <QScriptValue>
#include <QScriptEngine>

class QScriptContext;

namespace Qamer {
namespace QScript {

QAMERSHARED_EXPORT QScriptValue throwTypeError(QScriptContext *context,
                                               const QString &typeName);
QAMERSHARED_EXPORT QScriptValue throwArgcError(QScriptContext *context,
                                               const QString &fullName,
                                               int leastArgc = 1);
QAMERSHARED_EXPORT QScriptValue throwArgvError(QScriptContext *context,
                                               const QString &fullName,
                                               const QString &typeName,
                                               int at = 0);

QAMERSHARED_EXPORT QScriptValue cf_print(QScriptContext *context, QScriptEngine *engine);

template<class T>
QScriptValue arrayFromList(QList<T> list, QScriptEngine *engine)
{
    if(!engine) {
        return QScriptValue();
    }
    QScriptValue sv_list = engine->newArray();
    for(int i = 0; i < list.count(); i++) {
        sv_list.setProperty(i, engine->newVariant(QVariant(list.at(i))));
    }

    return sv_list;
}

template<class T>
inline void registerScriptMetaObject(QScriptEngine *engine,
                                     QScriptEngine::FunctionSignature cf_ctor)
{
    QScriptValue meta = engine->newQMetaObject(&T::staticMetaObject,
                                               engine->newFunction(cf_ctor));
    engine->globalObject().setProperty(T::staticMetaObject.className(), meta);
}

template<class T, class Proto>
inline void registerScriptPrototype(QScriptEngine *engine)
{
    QScriptValue proto = engine->newQObject(new Proto(engine));
    engine->setDefaultPrototype(qMetaTypeId<T>(), proto);
}

template<class T, class Proto>
inline void registerScriptPrototypeCtor(QScriptEngine *engine,
                                        QScriptEngine::FunctionSignature cf_ctor, const char *name)
{
    QScriptValue proto = engine->newQObject(new Proto(engine));
    engine->setDefaultPrototype(qMetaTypeId<T>(), proto);

    QScriptValue ctor = engine->newFunction(cf_ctor, proto);
    engine->globalObject().setProperty(name, ctor);
}

inline void appendStaticMemberMethod(QScriptEngine *engine, const char* objName,
                                     QScriptEngine::FunctionSignature cf_method, const char* methodName)
{
    QScriptValue obj = engine->globalObject().property(objName);
    obj.setProperty(methodName, engine->newFunction(cf_method));
}

} // end namespace QScript
} // end namespace Qamer

#endif // QAMER_QSCRIPT_GENERAL_HPP
