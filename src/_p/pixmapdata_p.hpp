/*
 * File name: pixmapdata_p.hpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_PIXMAPDATAPRIVATE_HPP
#define QAMER_PIXMAPDATAPRIVATE_HPP

#include "../qamer_export.hpp"
#include "../spritesheet.hpp"
#include <QMap>
#include <QList>

//this file contain too many inline function which access protected member
//do not include it to public class

namespace Qamer {
//max sprite sheet index = 2^16 - 1 = 65535
class QAMERSHARED_EXPORT PixmapDataPrivate
{
public:
    PixmapDataPrivate();
    virtual ~PixmapDataPrivate();

    bool isSchemeIdValid(quint16 id) const;
    //animation scheme
    inline void addScheme_default(quint16 id, const QList<quint16> &list);
    inline quint16 currentPixmapIndex_default() const;
    inline quint16 currentSchemeId_default() const;
    inline quint16 currentSchemeIndex_default() const;
    inline quint16 setScheme_default(quint16 id, quint16 index = 0);
    //return schemeIndex
    inline quint16 progressScheme_default();
    //
    inline void addSpriteSheet_default(const SpriteSheet &sheet);
    inline void setCurrentPixmapIndex_default(quint16 index);

    bool hasSheet(int number) const;
protected:
    void setSheets_default(const QList<SpriteSheet> &sheets);
    const QList<SpriteSheet> &sheets_default() const;

    PixmapDataPrivate(const PixmapDataPrivate &that);
    PixmapDataPrivate &operator=(const PixmapDataPrivate &that);

    quint16 m_curSchemeId;
    quint16 m_curSchemeIndex;
    QMap<quint16, QList<quint16> > m_schemeMap;//id, indexs
private:
    QList<SpriteSheet> m_sheetList;
    quint16 m_sheetIndex;

    bool m_assignedSheetIndex;
};

void PixmapDataPrivate::addScheme_default(quint16 id, const QList<quint16> &list)
{
    m_schemeMap.insert(id, list);
}

quint16 PixmapDataPrivate::currentPixmapIndex_default() const
{
    if(m_assignedSheetIndex) return m_sheetIndex;
    if(isSchemeIdValid(m_curSchemeId)/* && m_curSchemeIndex != -1*/) {
        return m_schemeMap.value(m_curSchemeId).at(m_curSchemeIndex);
    }
    else return 0;//use first(0) index of spritesheet
}

quint16 PixmapDataPrivate::currentSchemeId_default() const
{
    return m_curSchemeId;
}

quint16 PixmapDataPrivate::currentSchemeIndex_default() const
{
    return m_curSchemeIndex;
}

quint16 PixmapDataPrivate::setScheme_default(quint16 id, quint16 index)
{
    if(m_schemeMap.contains(id)) {
        m_curSchemeId = id;
        QList<quint16> list = m_schemeMap.value(id);
        //GCC-WARNING: Compare between signed and unsigned
        if(index < list.count()) {
            m_curSchemeIndex = index;
            m_assignedSheetIndex = false;//reset
            return list.at(m_curSchemeIndex);
        }
        else m_curSchemeIndex = -1;
    }
    return 0;//use first(0) index of spritesheet
}

quint16 PixmapDataPrivate::progressScheme_default()
{
    if(!m_schemeMap.contains(m_curSchemeId)) return -1;

    QList<quint16> list = m_schemeMap.value(m_curSchemeId);
    int count = list.count();
    //GCC-WARNING: Compare between signed and unsigned
    if((++m_curSchemeIndex) < count) ;
    else if(count != 0) m_curSchemeIndex = 0;
    else {
        m_curSchemeIndex = -1;
    }
    return m_curSchemeIndex;
}

void PixmapDataPrivate::addSpriteSheet_default(const SpriteSheet &sheet)
{
    if(sheet.isValid()) m_sheetList << sheet;
}

void PixmapDataPrivate::setCurrentPixmapIndex_default(quint16 index)
{
    m_assignedSheetIndex = true;
    m_sheetIndex = index;
}

} // end namespace Qamer

#endif // QAMER_PIXMAPDATAPRIVATE_HPP
