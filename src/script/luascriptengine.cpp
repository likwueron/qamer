/*
 * File name: luascriptengine.cpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "luascriptengine.hpp"
#include <QtLua/State>
#include <QtLua/Console>
#include <QtLua/Function>
#include <QFile>
#include <QDebug>

using namespace QtLua;

namespace Qamer {

QList<LuaScriptEngine*> LuaScriptEngine::sm_instanceList;

LuaScriptEngine::LuaScriptEngine(QObject *parent)
    : QObject(parent),
      mp_state(new State), mp_console(nullptr)
{
    mp_state->enable_qdebug_print(true);

    mp_state->openlib(BaseLib | StringLib |
                      TableLib | MathLib |
                      PackageLib |
                      QtLib | QtLuaLib);

    sm_instanceList << this;
}

LuaScriptEngine::~LuaScriptEngine()
{
    delete mp_state;

    foreach(QtLua::Function *f, m_funcList) {
        delete f;
    }

    sm_instanceList.removeOne(this);
}

State *LuaScriptEngine::state() const
{
    return mp_state;
}

lua_State *LuaScriptEngine::luaState() const
{
    return mp_state->get_lua_state();
}

bool LuaScriptEngine::evaluate(const QString &script)
{
    try {
        mp_state->exec_statements(script);
    }
    catch(QtLua::String e) {
        if(mp_console) mp_console->print(e.to_qstring());
        else qDebug() << e;
        return false;
    }
    return true;
}

bool LuaScriptEngine::evaluateFile(const QString &filename)
{
    QFile file(filename);
    if(!file.open(QFile::ReadOnly | QFile::Text)) {
        qWarning() << QString("Cannot open file \"%1\"").arg(filename);
        return false;
    }

    try {
        mp_state->exec_chunk(file);
    }
    catch(QtLua::String e) {
        if(mp_console) mp_console->print(e.to_qstring());
        else qDebug() << e;
        return false;
    }
    return true;
}

Value LuaScriptEngine::luaValue(const String &name) const
{
    return mp_state->at(name);
}

ValueRef LuaScriptEngine::luaValue(const String &name)
{
    return ValueRef(QtLua::Value::new_global_env(mp_state), Value(mp_state, name));
}

ValueRef LuaScriptEngine::setLuaValue(const String &name,
                                      const Value &value)
{
    ValueRef vr = (*mp_state)[name];
    vr = value;
    return vr;
}

Value LuaScriptEngine::newQObject(QObject *object, bool takeOwnership, bool canReparent)
{
    return Value(mp_state, object, takeOwnership, canReparent);
}

Value LuaScriptEngine::newQVariant(const QVariant &value)
{
    return Value(mp_state, value);
}

Value LuaScriptEngine::newTable()
{
    return Value::new_table(mp_state);
}

void LuaScriptEngine::newFunction(const String &name, Function *func)
{
    m_funcList << func;
    mp_state->set_global(name, Value(mp_state, func));
}

//void LuaScriptEngine::newFunction(const String &name,
//                                   LuaFuncSignature func)
//{
//    mp_state->reg_c_function(name.constData(), func);
//}

void LuaScriptEngine::newQMetaObject(const QMetaObject &meta, bool autoSetProperty, const QMetaObject &supremeMeta)
{
    qtlib_register_meta(&meta, &supremeMeta, autoSetProperty, nullptr);
}

void LuaScriptEngine::enableAutoProperty(const QMetaObject &meta, bool set)
{
    qtlib_enable_meta_auto_property(&meta, set);
}

void LuaScriptEngine::newStaticFunction(const QMetaObject &meta,
                                        const String &name,
                                        FunctionSignature func,
                                        const QList<String> &argv)
{
    qtlib_register_static_method(&meta, name, func, argv);
}

Console *LuaScriptEngine::debugConsole()
{
    if(!mp_console) {
        mp_console = new Console;
        connect(mp_console, SIGNAL(line_validate(const QString&)),
                mp_state, SLOT(exec(const QString&)));
        connect(mp_console, SIGNAL(get_completion_list(const QString &,
                                                       QStringList&,
                                                       int&)),
                mp_state, SLOT(fill_completion_list(const QString &,
                                                    QStringList&,
                                                    int&))
                );
        connect(mp_state, SIGNAL(output(const QString&)),
                mp_console, SLOT(print(const QString&)));
    }
    return mp_console;
}

void LuaScriptEngine::messageToConsole(const String &msg)
{

}

void LuaScriptEngine::when_gc_required()
{
    mp_state->gc_collect();
}

LuaScriptEngine *LuaScriptEngine::instanceFromState(State *state)
{
    foreach(LuaScriptEngine *eng, sm_instanceList) {
        if(eng->mp_state == state) return eng;
    }
    return nullptr;
}

} // namespace Qamer

