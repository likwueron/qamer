/*
 * File name: indirectaccessor.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_INDIRECTACCESSOR_HPP
#define QAMER_INDIRECTACCESSOR_HPP

#include "qamer_global.hpp"
#include "objectinfo.hpp"
#include "mics/parameter.hpp"
#include <QList>
#include <QPoint>

namespace Qamer {

class GameObject;
class GameScene;
class PixmapEffect;
class ScenePlacableObject;

class QAMERSHARED_EXPORT IndirectAccessor
{
private:
    IndirectAccessor();
    virtual ~IndirectAccessor();

public:
    static QList<ObjectInfo> objectInfoList(QList<qObjectId> idList);

    static QList<qObjectId> skillIdListFromUnitId(qObjectId uid, int type);
    static QList<QPoint> possibleTiles(const GameScene *scene, qObjectId sid, qObjectId uid);
    static QList<QPoint> effectTiles(const GameScene *scene, qObjectId sid, qObjectId uid);

    static Enum::UnitRelationship relationBetween(qObjectId activeUnitId, qObjectId passiveUnitId);
    static QList<QPoint> getValidTargetList(qObjectId sid, qObjectId uid, const QList<QPoint> &oriList);
    static qUnitSide getUnitSide(qObjectId uid);

    static QList<PixmapEffect *> pixmapEffects(qObjectId sid, const QList<qObjectId> &uids, QObject *admin);

    //assume list is valid postorder
    static int resolvePostorder(const ParaList &list, GameObject *obj);
protected:
    static void addPixmapEffect(QList<PixmapEffect*> &list, PixmapEffect* effect, QObject *admin);
};

} // end namespace Qamer

#endif // QAMER_INDIRECTACCESSOR_HPP
