/*
 *File name: controllermonitor.cpp
 *Author: kwueron
 *E-mail: likwueron@gmail.com
 *Last revised: 2014/1/29
 *Published under Public Domain
 */

#include "controllermonitor.hpp"
#include "../controller.hpp"

#include "hatbox.hpp"
#include "trackballbox.hpp"
#include <limits>
#include <QVBoxLayout>
#include <QPushButton>
#include <QSpinBox>

#include <QtUiTools>
#include <QFile>
#include <QHBoxLayout>
#include <QProgressBar>
#include <QAbstractButton>
#include <QGroupBox>

ControllerMonitor::ControllerMonitor(QWidget *parent) :
    QWidget(parent), mp_ui(0x0)
{
    setMinimumSize(300, 350);
    setAttribute(Qt::WA_DeleteOnClose);
}

ControllerMonitor::~ControllerMonitor()
{
}

bool ControllerMonitor::loadUi(const QString &fileName)
{
    if(mp_ui) {
        delete mp_ui;
    }

    QFile file(fileName);
    if(!file.open(QFile::ReadOnly)) {
        return false;
    }

    QUiLoader loader;
    mp_ui = loader.load(&file, this);
    file.close();

    if(mp_ui) {
        QHBoxLayout *p_mainLayout = new QHBoxLayout;
        p_mainLayout->addWidget(mp_ui);
        setLayout(p_mainLayout);
        return true;
    }
    else {
        return false;
    }
}

void ControllerMonitor::constructRawMonitor(int hatNum, int axisNum, int buttonNum, int trackballNum)
{
    QVBoxLayout *p_mainLayout = new QVBoxLayout;

    if(hatNum) {
        QHBoxLayout *p_hatLayout = new QHBoxLayout;
        p_mainLayout->addLayout(p_hatLayout);
        for(int i = 0; i < hatNum; i++) {
            HatBox *p_hat = new HatBox;
            QString name = QString("DPad%1").arg(i+1);
            p_hat->setObjectName(name);
            p_hat->setTitle(name);
            p_hatLayout->addWidget(p_hat);
        }
    }

    if(axisNum) {
        QHBoxLayout *p_axisLayout = new QHBoxLayout;
        p_mainLayout->addLayout(p_axisLayout);
        for(int i = 0; i < axisNum; i++) {
            QProgressBar *p_pb = new QProgressBar;
            p_pb->setObjectName(QString("Axis%1").arg(i+1));
            p_pb->setMinimum(std::numeric_limits<qint16>::min());
            p_pb->setMaximum(std::numeric_limits<qint16>::max());
            p_pb->setValue(0);
            p_pb->setAlignment(Qt::AlignCenter | Qt::AlignVCenter);
            p_axisLayout->addWidget(p_pb);
        }
    }

    if(buttonNum) {
        QHBoxLayout *p_buttonLayout = new QHBoxLayout;
        p_mainLayout->addLayout(p_buttonLayout);
        for(int i = 0; i < buttonNum; i++) {
            QPushButton *p_b = new QPushButton;
            QString name = QString("Button%1").arg(i+1);
            p_b->setObjectName(name);
            p_b->setText(name);
            p_b->setCheckable(true);
            p_buttonLayout->addWidget(p_b);
        }
    }

    if(trackballNum) {
        QHBoxLayout *p_trackballLayout = new QHBoxLayout;
        p_mainLayout->addLayout(p_trackballLayout);
        for(int i = 0; i < trackballNum; i++) {
            TrackballBox *p_t = new TrackballBox;
            QString name = QString("Trackball%1").arg(i+1);
            p_t->setObjectName(name);
            p_t->setTitle(name);
            p_trackballLayout->addWidget(p_t);
        }
    }

    setLayout(p_mainLayout);
}

void ControllerMonitor::constructRawMonitor(const QString &layout)
{
    if(layout.count() != 8) {
        return;
    }
    int hatNum = layout.mid(0, 2).toInt();
    int axisNum = layout.mid(2, 2).toInt();
    int buttonNum = layout.mid(4, 2).toInt();
    int trackballNum = layout.mid(6, 2).toInt();

    constructRawMonitor(hatNum, axisNum, buttonNum, trackballNum);
}

void ControllerMonitor::when_action_done(int actionRole, const QString &actionName, qint64 value)
{
    if(actionRole & Controller::HatMask) {
        QWidget *p_w = findChild<QGroupBox*>(actionName);
        if(p_w) {
            QAbstractButton *p_b = p_w->findChild<QAbstractButton*>(QString("Hat%1").arg(value));
            if(p_b) {
                p_b->setChecked(true);
            }
        }
    }
    if(actionRole & Controller::AxisMask) {
        QProgressBar *p_pb = findChild<QProgressBar*>(actionName);
        if(p_pb) {
            p_pb->setValue(value);
        }
    }
    if(actionRole & Controller::ButtonMask) {
        QAbstractButton *p_b = findChild<QAbstractButton*>(actionName);
        if(p_b) {
            p_b->setChecked(value);
        }
    }
    if(actionRole & Controller::TrackballMask) {
        QWidget *p_t = findChild<QGroupBox*>(actionName);
        if(p_t) {
            QList<QSpinBox*> list = findChildren<QSpinBox*>();
            if(2 <= list.count()) {
                int a, b;
                decodeQint64(value, a, b);
                list.at(0)->setValue(a);
                list.at(1)->setValue(b);
            }
        }
    }
}

void ControllerMonitor::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        //ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
