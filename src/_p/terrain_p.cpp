/*
 * File name: terrain_p.cpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "terrain_p.hpp"

namespace Qamer {

TerrainPrivate::TerrainPrivate()
    : ScenePlacableObjectPrivate(), ModifierDataPrivate()
{

}

TerrainPrivate::~TerrainPrivate()
{

}

TerrainPrivate::TerrainPrivate(const TerrainPrivate &that)
    : ScenePlacableObjectPrivate(that), ModifierDataPrivate(that)
{

}

} // end namespace Qamer

