/*
 * File name: sceneplacableobject.cpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "sceneplacableobject.hpp"
#include "_p/sceneplacableobject_p.hpp"
#include "gamescene.hpp"
#include <QPainter>
#include <QDebug>

namespace Qamer {

ScenePlacableObject::ScenePlacableObject(int type, QObject *parent)
    : GameObject(new ScenePlacableObjectPrivate, type, parent)
{

}

ScenePlacableObject::~ScenePlacableObject()
{

}

int ScenePlacableObject::sceneId() const
{
    Q_D(const ScenePlacableObject);
    return d->m_sceneId;
}

void ScenePlacableObject::setSceneId(qObjectId scid)
{
    if(multiPlace()) return;

    Q_D(ScenePlacableObject);
    GameObject *scene = GameObject::findObject(scid, Enum::ObjectScene);
    if(scene) {
        d->m_sceneId = scid;
    }
    else {
        d->m_sceneId = invalidId;
        d->m_sceneIndex = -1;
    }
}

int ScenePlacableObject::sceneIndex() const
{
    Q_D(const ScenePlacableObject);
    return d->m_sceneIndex;
}

QPoint ScenePlacableObject::scenePoint() const
{
    Q_D(const ScenePlacableObject);
    if(d->m_sceneId != invalidId) {
        return ((GameScene*)findObject(d->m_sceneId))->pointFromIndex(d->m_sceneIndex);
    }
    else return QPoint();
}

void ScenePlacableObject::setScenePoint(QPoint point)
{
    Q_D(ScenePlacableObject);
    if(d->m_sceneId != invalidId) {
        int oldIndex = d->m_sceneIndex;
        d->m_sceneIndex = ((GameScene*)findObject(d->m_sceneId))->indexFromPoint(point);
        emit sceneMove_required(objectId(), d->m_sceneIndex, oldIndex);
    }
}

void ScenePlacableObject::movedByScene(int index)
{
    if(multiPlace()) return;

    Q_D(ScenePlacableObject);
    d->m_sceneIndex = index;
    emit scenePoint_changed();
    updatePosition();
}

bool ScenePlacableObject::multiPlace() const
{
    return false;
}

void ScenePlacableObject::painted(QPainter *painter, const QRect &updateRect)
{
    Q_UNUSED(updateRect)
    Q_D(const ScenePlacableObject);
    if(!d->hasSheet(0)) return;
    int index = d->currentPixmapIndex();
    d->sheets_default().at(0).draw(painter, position(), (index == -1) ? 0 : index);
}

void ScenePlacableObject::updatePosition()
{
    Q_D(ScenePlacableObject);
    if(!isIdValid(d->m_sceneId)) return;
    //id is valid because it checked when assigned
    GameScene* scene = (GameScene*)findObject(d->m_sceneId);
    if(d->hasSheet(0)) {
        int tH = scene->tileHeight();
        int tW = scene->tileWidth();
        QSize sheetSize = d->sheets_default().at(0).clipSize();
        int sH = sheetSize.height();
        int sW = sheetSize.width();

        move(scene->drawCoorFromSceneCoor(d->m_sceneIndex) -
             QPoint((sW-tW)/ 2, sH-tH)
             );
    }
    else {
        move(scene->drawCoorFromSceneCoor(d->m_sceneIndex));
    }
}

ScenePlacableObject::ScenePlacableObject(ScenePlacableObjectPrivate *pdd, int type, QObject *parent)
    : GameObject(pdd, type, parent)
{

}

ScenePlacableObject::ScenePlacableObject(const ScenePlacableObject &that)
    : GameObject(new ScenePlacableObjectPrivate(*(that.d_func())), Enum::ObjectCopy, that.parent())
{

}

void ScenePlacableObject::when_sheet_inserted(QSize clipSize)
{
    //Q_D(ScenePlacableObject);
    QSize temp = size();
    if(temp.width() < clipSize.width()) {
        temp.setWidth(clipSize.width());
    }
    if(temp.height() < clipSize.height()) {
        temp.setHeight(clipSize.height());
    }

    resize(temp);
}


} // namespace Qamer

