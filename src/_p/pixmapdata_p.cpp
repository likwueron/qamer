/*
 * File name: pixmapdata_p.cpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "pixmapdata_p.hpp"

namespace Qamer {

PixmapDataPrivate::PixmapDataPrivate()
    : m_curSchemeId(-1), m_curSchemeIndex(-1),
      m_sheetIndex(-1),
      m_assignedSheetIndex(false)
{

}

PixmapDataPrivate::~PixmapDataPrivate()
{

}

bool PixmapDataPrivate::isSchemeIdValid(quint16 id) const
{
    return m_schemeMap.contains(id);
}

bool PixmapDataPrivate::hasSheet(int index) const
{
    return (index < m_sheetList.count());
}

void PixmapDataPrivate::setSheets_default(const QList<SpriteSheet> &sheets)
{
    m_sheetList = sheets;
}

const QList<SpriteSheet> &PixmapDataPrivate::sheets_default() const
{
    return m_sheetList;
}

PixmapDataPrivate::PixmapDataPrivate(const PixmapDataPrivate &that)
    : m_curSchemeId(-1), m_curSchemeIndex(-1),
      m_schemeMap(that.m_schemeMap),
      m_sheetList(that.m_sheetList),
      m_sheetIndex(that.m_sheetIndex),
      m_assignedSheetIndex(that.m_assignedSheetIndex)
{

}

PixmapDataPrivate &PixmapDataPrivate::operator=(const PixmapDataPrivate &that)
{
    m_curSchemeId = -1;
    m_curSchemeIndex = -1;
    m_schemeMap = that.m_schemeMap;
    m_sheetList = that.m_sheetList;
    m_sheetIndex = -1;
    m_assignedSheetIndex = false;

    return *this;
}

} // namespace Qamer

