/*
 * File name: qmadmin.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_QMADMIN_HPP
#define QAMER_QMADMIN_HPP

#include "../qamer_export.hpp"
#include "../core/pseudosingleton.hpp"
#include "basetextreader.hpp"
#include <QMutex>
#include <QMap>
#include <QDir>
#include <QStringList>

class QTranslator;

namespace Qamer {

SINGLE(QmAdmin)

class QAMERSHARED_EXPORT QmAdmin : public BaseTextReader, public SQmAdmin
{
public:
    explicit QmAdmin();
    virtual ~QmAdmin();

    //Path that contains translate files(*.qm files)
    QString qmDir();
    void setQmDir(const QDir &dir);
    void setQmDir(const QString &path);
    //Make all relative path be absolute path accroding to path of application
    QString absPath(const QString &path);
    //read the configuration file
    void readConfig(const QString &fileName);
    QStringList avaliableLangs();
    //Set language the application used by locale
    QString lang();
    void setLang(const QString &loc);
    void rmLang();
    //Converse between label (ex: Ameriacan English) and locale (ex: en_US)
    QString lblFromLoc(const QString &loc);
    QString locFromLbl(const QString &lbl);
    //Return language without country (ex: find no "en_US", return "en")
    //Use config file to determine the return value
    QString fuzzyLoc(const QString &loc);

protected:
    bool isIgnoreChar(char c);

private:
    QMutex m_mutex;

    QString m_langPath;
    QMap<QString, QString> m_langMap;
    QString m_curLoc;
    QList<QTranslator*> m_curTrFiles;
};

} //end namespace Qamer

extern Qamer::QmAdmin *qLang;

#endif // QAMER_QMADMIN_HPP
