/*
 * File name: gameprogressbar.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_GAMEPROGRESSBAR_HPP
#define QAMER_GAMEPROGRESSBAR_HPP

#include "../qamer_global.hpp"
#include "gameui.hpp"
#include <QPolygon>
#include <QColor>
#include <QRect>
#include <QLinearGradient>

class QLinearGradient;

namespace Qamer {

class QAMERSHARED_EXPORT GameProgressBar : public GameUI
{
    Q_OBJECT
    Q_PROPERTY(int value READ value WRITE setValue)
    Q_PROPERTY(int maxValue READ maxValue WRITE setMaxValue)
    Q_PROPERTY(int minValue READ minValue WRITE setMinValue)
public:
    const int defaultShapePoints[8] = { 14, 0, 100, 0, 86, 14, 0, 14 };

    GameProgressBar(QObject *parent = 0);
    ~GameProgressBar();

    //if set, will emit resized()
    QPolygon shape() const;
    void setShape(const QPolygon &polygon);

    void setColors(QColor start, QColor end);

    int value() const;
    int maxValue() const;
    int minValue() const;

    void painted(QPainter *painter, const QRect &updateRect);
public slots:
    void setValue(int value);
    void setMaxValue(int max);
    void setMinValue(int min);
protected://virtual slots:
    void when_moved(QPoint old);
    void when_resized(QSize old);
    void when_orientation_changed();
protected:
    double fullPercentage() const;
    void resetGradientPos();
    void resetClipCache();
    void resetAll();
private:
    QPolygon m_shape;
    QLinearGradient m_g;

    int m_value;
    int m_max;
    int m_min;

    QRect m_clipCache;
};

} // end namespace Qamer

#endif // QAMER_GAMEPROGRESSBAR_HPP
