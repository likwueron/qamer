/*
 * File name: fixedmemorypool.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is marjorly based on Paper
 * "Fast Efficient Fixed-Size Memory Pool" by Ben Kenwright.
 * You can redistribute it and/or modify it freely under Public Domain.
 */

#include "fixedmemorypool.hpp"

namespace Qamer {

FixedMemoryPool::FixedMemoryPool(number blockNum, number blockSize,
                                 bool freelock)
    : mp_memHead(0x0),
      m_initBlockNum(0), m_freeBlockNum(0), m_blockNum(0)
{
    m_freelock = freelock ? Lock : Unlock;
    reset(blockNum, blockSize);
}

FixedMemoryPool::~FixedMemoryPool()
{
    freeAll();
}

bool FixedMemoryPool::reset(number blockNum, number blockSize)
{
    //refuse reset if pool is using
    if(m_freelock == Lock) {
        if(m_freeBlockNum != m_blockNum) return false;
    }

    freeAll();

    m_blockNum = blockNum;
    m_freeBlockNum = m_blockNum;
    m_blockSize = blockSize < staticBlockHeaderSize ? staticBlockHeaderSize : blockSize;

    m_pageSize = m_blockNum * m_blockSize;
    mp_memHead = new unsigned char[m_pageSize];
    mp_nextBlock = mp_memHead;

    return (bool)mp_memHead;
}

void FixedMemoryPool::freeAll()
{
    if(mp_memHead) {
        delete [] mp_memHead;
        mp_memHead = 0x0;
    }
}

void *FixedMemoryPool::allocate()
{
    if(!mp_memHead) return 0x0;

    //init block by give it nextblock's id
    if(m_initBlockNum < m_blockNum) {
        number* p = (number*)addrFromIndex(m_initBlockNum);
        *p = m_initBlockNum + 1;
        m_initBlockNum++;
    }

    void* result = 0x0;
    if(0 < m_freeBlockNum) {
        //get nextblock for use
        result = (void*)mp_nextBlock;
        --m_freeBlockNum;
        //get new nextblock
        if(m_freeBlockNum != 0) {
            mp_nextBlock = addrFromIndex(*((number*)mp_nextBlock));
        }
        else mp_nextBlock = 0x0;
    }
    return result;
}

void FixedMemoryPool::free(void *p)
{
    if(mp_nextBlock) {
        //free memory store nextblock's id
        (*(number*)p) = indexFromAddr(mp_nextBlock);
        //free memory become nextblock
        mp_nextBlock = (ptr)p;
    }
    else {
        (*(number*)p) = m_blockNum;
        mp_nextBlock = (ptr)p;
    }
    ++m_freeBlockNum;
}

FixedMemoryPool::number FixedMemoryPool::blockSize() const
{
    return mp_memHead ? m_blockSize : 0;
}

FixedMemoryPool::ptr FixedMemoryPool::addrFromIndex(number index) const
{
    return mp_memHead + index * m_blockSize;
}

FixedMemoryPool::number FixedMemoryPool::indexFromAddr(ptr addr) const
{
    return ((unsigned int)(addr - mp_memHead)) / m_blockSize;
}

} // end namespace Qamer
