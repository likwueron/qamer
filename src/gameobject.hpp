/*
 * File name: gameobject.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "qamer_global.hpp"
#include "objectinfo.hpp"
#include "spritesheet.hpp"
#include <QObject>
#include <QVariantList>
#include <QSize>
#include <QPoint>
#include <QRect>

#ifndef QAMER_GAMEOBJECT_HPP
#define QAMER_GAMEOBJECT_HPP

class QPainter;

namespace Qamer {

class GameObjectPrivate;
class ControllerEvent;
class PixmapEffect;

class QAMERSHARED_EXPORT GameObject : public QObject
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(GameObject)
    Q_PROPERTY(int objectId READ objectId CONSTANT)
    Q_PROPERTY(int objectType READ objectType)
    Q_PROPERTY(int subObjectType READ subObjectType WRITE setSubObjectType)
    Q_PROPERTY(QString name READ name WRITE setName)
    Q_PROPERTY(QString description READ description WRITE setDescription)
    Q_PROPERTY(QPoint position READ position WRITE move)
    Q_PROPERTY(QSize size READ size WRITE resize)
    Q_PROPERTY(QRect geometry READ geometry STORED false)
    Q_PROPERTY(bool visible READ isVisible WRITE setVisible)

    Q_PROPERTY(int spriteSheetIndex READ spriteSheetIndex WRITE setSpriteSheetIndex)
    
public:
    friend class IndirectAccessor;

    enum CF_ErrorType {
        CF_NoError,
        CF_TypeError, //incorrect object type
        CF_ParaTypeError, //incorrect parameter type
        CF_AccessError, //not accessible property
        CF_ScriptableError, //not scriptable property
        CF_DynamicError //not dynamic property
    };

    enum ScriptableTag {
        ScriptableCPP,
        ScriptableQS,
        ScriptableFile
    };

    explicit GameObject(int objType = Enum::ObjectUndefined, QObject *parent = 0);
    ~GameObject();

    static int objectNumber(int type = Enum::ObjectAll);

    static qObjectId findObjectId(const QString &name, int type = Enum::ObjectAll);
    static GameObject *findObject(const QString &name, int type = Enum::ObjectAll);
    static GameObject *findObject(qObjectId id, int type = Enum::ObjectAll);

    void setSystemName(const QString &name);

    static int objectTypeFromId(qObjectId id);
    int objectType() const;
    bool isObjectType(int objectType) const;
    int subObjectType() const;
    void setSubObjectType(int type);

    static bool isIdValid(qObjectId id);
    int objectId() const;
    //helper for both FileLoader AND QScriptable; return CF_ErrorType
    Q_INVOKABLE int cf_addProperty(const QByteArray &name, const QVariant &value);
    //infomation
    const ObjectInfo& infomation() const;
    QString name() const;
    void setName(const QString &name);
    QString description() const;
    void setDescription(const QString &des);
    //void setAvatarSpriteSheet(int code);
    //controller
    void setControllerFocus(int cid);
    //drawing info
    QPoint position() const;
    QRect geometry() const;
    static QPoint movePeek(const QRect &thisRect, QRect rect, Enum::MoveMode mode = Enum::MoveNormal);
    void move(QPoint position, Enum::MoveMode mode = Enum::MoveNormal);
    void move(QRect rect, Enum::MoveMode mode = Enum::MoveCenter);
    QSize size() const;
    void resize(QSize size);
    QSize parentSize() const;

    void addSpriteSheet(const SpriteSheet &sheet);
    Q_INVOKABLE void addSpriteSheet(const QString &file,
                                    int row = 1, int col = 1,
                                    bool applyMask = false,
                                    QColor maskColor = QColor());
    Q_INVOKABLE void addSpriteSheetFixsize(const QString &file,
                                           int w, int h,
                                           bool applyMask,
                                           QColor maskColor = QColor());
    bool isVisible() const;
    void setVisible(bool visible);
    void invertVisible();
    //Pixmap
        //animation control
    void addAnimationScheme(int id, const QList<quint16> &list);
    Q_INVOKABLE void addAnimationScheme(int id, const QVariantList &list);
    void setCurrentAnimationScheme(int id, int index = 0);
    void progressAnimation();
        //spritesheet control
    int spriteSheetIndex() const;
    void setSpriteSheetIndex(int index);

    //SCRIPTABLE
    bool scriptable() const;
    bool loadable() const;
    //paint
    virtual void painted(QPainter *painter, const QRect &updateRect) = 0;
    virtual void painted(QPainter *painter, int x, int y);
    //prototype pattern
    virtual GameObject* clone() const;
    virtual bool isClonable() const;

public slots:
protected slots: //virtual slots, won't set as slots in child class
    virtual void when_moved(QPoint old);
    virtual void when_resized(QSize old);
    virtual void when_sheet_inserted(QSize clipSize);
    virtual void when_schemeEnd_reached(int scheme);

signals:
    void moved(QPoint old);
    void resized(QSize old);
    void spriteSheet_inserted(QSize sheetClipSize);
    void schemeEnd_reached(int scheme);

protected:
    //helper for SCRITABLE
    static bool isScriptable(ScriptableTag tag);
    static ScriptableTag scriptableTag();
public:
    static void setScriptableTag(ScriptableTag tag);
protected:
    //event handle
    bool event(QEvent *e);
    virtual void controllerEvent(ControllerEvent *e);

    void appendToList();

    bool isStaticProperty(const QByteArray &name);
    QList<QByteArray> staticPropertyNames(PropertyAttr attr = All);

    //constructor for private class data
    GameObject(GameObjectPrivate *pdd, int objType, QObject *parent);
    //copy constructor for clone
    GameObject(const GameObject &that);
private:
    //initiation
    void setup(int objType);
    void setupConnect();
};

class QAMERSHARED_EXPORT GameObjectId
{
public:
    GameObjectId();
    GameObjectId(qObjectId id);
    GameObjectId(const GameObject *obj);
    GameObjectId(const GameObjectId &that);
    ~GameObjectId();

    qObjectId id() const;
    void setId(qObjectId id);
    void setId(const QString &name);

    GameObject *object() const;

    bool isValid() const;

private:
    qObjectId m_id;
};

template <class T>
inline T try_clone(GameObject *o)
{
    if(!o) return nullptr;

    GameObject *n = nullptr;
    if(!(n = o->clone())) n = o;
    return reinterpret_cast<T>(n);
}

inline GameObject* try_clone(GameObject *o)
{
    GameObject *n = nullptr;
    if(o) if(!(n = o->clone())) n = o;
    return n;
}

} // end namespace Qamer

Q_DECLARE_TYPEINFO(Qamer::GameObjectId, Q_MOVABLE_TYPE);
Q_DECLARE_METATYPE(Qamer::GameObjectId)

#endif // QAMER_GAMEOBJECT_HPP
