/*
 * File name: qs_gameobject.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef QAMER_QSCRIPT_GAMEOBJECT_HPP
#define QAMER_QSCRIPT_GAMEOBJECT_HPP

#include "../qamer_global.hpp"
#include <QObject>
#include <QScriptable>
#include <QMap>
//type
#include <QVariant>
#include <QSizeF>
#include <QPointF>

namespace Qamer {

class GameObject;

namespace QScript {

class QAMERSHARED_EXPORT QS_GameObject : public QObject, protected QScriptable
{
    Q_OBJECT
    Q_PROPERTY(int id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(QString systemName READ systemName WRITE setSystemName NOTIFY idChanged)
    Q_PROPERTY(int objectType READ objectType)
    Q_PROPERTY(int subObjectType READ subObjectType WRITE setSubObjectType)
    Q_PROPERTY(bool isValid READ isValid)

    Q_PROPERTY(QString name READ name WRITE setName)
    Q_PROPERTY(QString description READ description WRITE setDescription)

    Q_PROPERTY(QPointF position READ position WRITE move)
    Q_PROPERTY(QSizeF size READ size WRITE resize)
    Q_PROPERTY(bool visible READ isVisible WRITE setVisible)
    //Unit-only BEGIN
    Q_PROPERTY(QPointF scenePoint READ scenePoint WRITE setScenePoint)
    Q_PROPERTY(int facing READ facing WRITE setFacing)
    //Unit-only END
    //Skill-only BEGIN
    Q_PROPERTY(QString possibleTilesFinder READ possibleTilesFinderName WRITE setPossibleTilesFinderName)
    Q_PROPERTY(QString effectTilesFinder READ effectTilesFinderName WRITE setEffectTilesFinderName)
    //Skill-only END
public:
    explicit QS_GameObject(QObject *parent = 0);
    ~QS_GameObject();

    static void addTypeText(int type, const QString &text);
    static void addSubTypeText(int type, int subType, const QString &text);

    //because qt don't accept typedef for Q_PROPERTY, use int instead
    int id() const;
    void setId(int id);
    QString systemName() const;
    void setSystemName(const QString &name);
    int objectType() const;
    int subObjectType() const;
    void setSubObjectType(int subObjectType);
    bool isValid() const;

    Q_INVOKABLE QVariant propertyD(const QString &name);
    Q_INVOKABLE void setPropertyD(const QString &name, const QVariant &value);
    //common data
    QString name() const;
    void setName(const QString &name);
    QString description() const;
    void setDescription(const QString &des);
    //void setAvatarSpriteSheet(int code);
    //controller
    //void setControllerFocus(int cid);
    //drawing info
    QPointF position() const;
    Q_INVOKABLE void move(QPointF position, Enum::MoveMode mode = Enum::MoveNormal);
    QSizeF size() const;
    void resize(QSizeF size);
    QSizeF parentSize() const;
    //void addSpriteSheet(SpriteSheet *sheet);
    bool isVisible() const;
    void setVisible(bool visible);
    Q_INVOKABLE void invertVisible();
    //animation control
    Q_INVOKABLE void addAnimationScheme(int id, const QVariantList &list);
    Q_INVOKABLE void setCurrentAnimationScheme(int id, int index = 0);
    //void nextPix();
    //Unit-only BEGIN
    Q_INVOKABLE void addStatValue(const QString &name, int max = 255, int min = 0, int flags = Enum::StatValueZeroAtBegin, int val = 0);
    Q_INVOKABLE int statCurrentValue(const QString &name) const;
    Q_INVOKABLE int statMaxValue(const QString &name) const;
    Q_INVOKABLE int statMinValue(const QString &name) const;
    Q_INVOKABLE bool increaseStatValue(const QString &name, int val);
    Q_INVOKABLE void resetStatValue(const QString &name);
    Q_INVOKABLE void setStatCurrentValue(const QString &name, int cur);
    Q_INVOKABLE void setStatMaxValue(const QString &name, int max);
    Q_INVOKABLE void setStatMinValue(const QString &name, int min);

    QPointF scenePoint() const;
    void setScenePoint(QPointF point);

    int facing() const;
    void setFacing(int facing);
    //skill
    Q_INVOKABLE void addSkill();
    Q_INVOKABLE QVariantList skillList(int subType = Enum::ObjectSubTypeMask) const;
    //resistance to statuses and elements
    //int resistance(int id);
    //void setResistance(int id, int val);
    //Unit-only END

    //Scene-only BEGIN
    Q_INVOKABLE bool pointWithinScene(QPointF point) const;
    Q_INVOKABLE bool canPass(QPointF point) const;
    //Scene-only END

    //Skill-only BEGIN
    QString possibleTilesFinderName() const;
    void setPossibleTilesFinderName(const QString &name);
    QString effectTilesFinderName() const;
    void setEffectTilesFinderName(const QString &name);
    //Skill-only END

signals:
    void idChanged();
public slots:
    QString toString() const;

private:
    GameObject *accessObject() const;

    static QMap<int,QString> sm_typeTextMap;
};

QAMERSHARED_EXPORT QScriptValue cf_GameObject_GameObject
(QScriptContext *context, QScriptEngine *engine);
QAMERSHARED_EXPORT QScriptValue cf_GameObject_objectNumber
(QScriptContext *context, QScriptEngine *engine);

} // end namespace QScript
} // end namespace Qamer

#endif // QAMER_QSCRIPT_GAMEOBJECT_HPP
