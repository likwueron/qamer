/*
 * File name: gameprogressbar.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "gameprogressbar.hpp"
#include "../unit.hpp"
#include <QPainter>
#include <QBrush>
#include <QString>
#include <QFontMetrics>
//#include <QDebug>

namespace Qamer {

GameProgressBar::GameProgressBar(QObject *parent)
    : GameUI(parent),
      m_value(0), m_max(0), m_min(0)
{
    setSubObjectType(Enum::InterfaceProgressBar);

    move(QPoint(0, 0));

    int ptsNum = sizeof(defaultShapePoints) / (2 * sizeof(int));
    QPolygon poly(ptsNum, defaultShapePoints);
    setShape(poly);
}

GameProgressBar::~GameProgressBar()
{

}

QPolygon GameProgressBar::shape() const
{
    return m_shape;
}

void GameProgressBar::setShape(const QPolygon &polygon)
{
    m_shape = polygon;
    resize(polygon.boundingRect().size());
}

void GameProgressBar::setColors(QColor start, QColor end)
{
    m_g.setColorAt(0, start);
    m_g.setColorAt(1, end);
}

int GameProgressBar::value() const
{
    return m_value;
}

int GameProgressBar::maxValue() const
{
    return m_max;
}

int GameProgressBar::minValue() const
{
    return m_min;
}

void GameProgressBar::setValue(int value)
{
    m_value = value;
    resetAll();
}

void GameProgressBar::setMaxValue(int max)
{
    m_max = max;
    resetAll();
}

void GameProgressBar::setMinValue(int min)
{
    m_min = min;
    resetAll();
}

void GameProgressBar::when_moved(QPoint old)
{
    Q_UNUSED(old)
    resetGradientPos();
}

void GameProgressBar::when_resized(QSize old)
{
    Q_UNUSED(old)
    resetAll();
    //reset font size
    QFont _font = font();
    _font.setPixelSize(size().height());
    setFont(_font);
}

void GameProgressBar::when_orientation_changed()
{
    resetAll();
}

double GameProgressBar::fullPercentage() const
{
    int dValue = m_max - m_min;
    if(dValue) {
        return double(m_value - m_min)/dValue;
    }
    else return -1.0;
}

void GameProgressBar::resetGradientPos()
{
    QSize _size = size();
    QPoint _pos = position();
    if(orientation() == Qt::Horizontal) {
        m_g.setStart(_pos);
        m_g.setFinalStop(_pos + QPoint(_size.width(), 0));
    }
    else {
        double percent = fullPercentage();
        if(percent == -1.0) return;
        m_g.setStart(_pos + QPoint(0, _size.height()));
        m_g.setFinalStop(_pos.x(), _pos.y() + _size.height() * (1 - percent));
    }
}

void GameProgressBar::resetClipCache()
{
    QSize m_size = size();
    double percent = fullPercentage();
    if(percent == -1.0) m_clipCache = QRect(position(), m_size);
    else {
        if(orientation() == Qt::Horizontal) {
            m_clipCache.setTopLeft(position());
            m_clipCache.setWidth(m_size.width() * percent);
            m_clipCache.setHeight(m_size.height());
        }
        else {//vertical
            int newH = m_size.height() * percent;
            m_clipCache.setX(position().x());
            m_clipCache.setY(position().y() + m_size.height() - newH);
            m_clipCache.setWidth(m_size.width());
            m_clipCache.setHeight(newH);
        }
    }
}

void GameProgressBar::resetAll()
{
    double percent = fullPercentage();
    if(percent == -1.0) return;

    QSize m_size = size();
    QPoint m_pos = position();
    if(orientation() == Qt::Horizontal) {
        m_clipCache.setTopLeft(m_pos);
        m_clipCache.setWidth(m_size.width() * percent);
        m_clipCache.setHeight(m_size.height());

        m_g.setStart(m_pos);
        m_g.setFinalStop(m_pos + QPoint(m_size.width(), 0));
    }
    else {//vertical
        int newH = m_size.height() * percent;
        m_clipCache.setX(m_pos.x());
        m_clipCache.setY(m_pos.y() + m_size.height() - newH);
        m_clipCache.setWidth(m_size.width());
        m_clipCache.setHeight(newH);

        m_g.setStart(m_pos + QPoint(0, m_size.height()));
        m_g.setFinalStop(m_clipCache.topLeft());
    }
}

void GameProgressBar::painted(QPainter *painter, const QRect &updateRect)
{
    Q_UNUSED(updateRect)
    painter->save();
    painter->setFont(font());

    //draw label
    QFontMetrics fm(font());
    QSize labelSize = fm.size(Qt::TextExpandTabs, name());
    painter->drawText(QRect(position(), labelSize), name());

    //draw bar
    painter->translate(labelSize.width(), 0);

    QBrush brush(m_g);
    painter->setBrush(brush);

    QRect rect = m_clipCache;
    painter->setClipRect(rect);

    QPolygon shape = m_shape.translated(position());
    painter->drawPolygon(shape);

    painter->setClipping(false);
    painter->drawText(QRect(position(), size()+QSize(0, 30)), Qt::AlignRight, QString("%1/%2").arg(m_value).arg(m_max));

    painter->restore();
}

} // end namespace Qamer

