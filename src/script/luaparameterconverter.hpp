/*
 * File name: luaparameterconverter.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_LUAPARAMETERCONVERTER_HPP
#define QAMER_LUAPARAMETERCONVERTER_HPP

#include "../qamer_export.hpp"
#include "../mics/parameter.hpp"
#include <QtLua/MetaType>

namespace Qamer {

class QAMERSHARED_EXPORT LuaParameterConverter : public QtLua::MetaType<Parameter>
{
public:
    LuaParameterConverter();

    static QtLua::Value luaFromQt(QtLua::State *ls, Parameter const *qtvalue);
    static bool qtFromLua(Parameter *qtvalue, const QtLua::Value &luavalue);

    QtLua::Value qt2lua(QtLua::State *ls, Parameter const *qtvalue);
    bool lua2qt(Parameter *qtvalue, const QtLua::Value &luavalue);
};

class QAMERSHARED_EXPORT LuaParaListConverter : public QtLua::MetaType<ParaList>
{
public:
    LuaParaListConverter();

    static QtLua::Value luaFromQt(QtLua::State *ls, ParaList const *qtvalue);
    static bool qtFromLua(ParaList *qtvalue, const QtLua::Value &luavalue);

    QtLua::Value qt2lua(QtLua::State *ls, ParaList const *qtvalue);
    bool lua2qt(ParaList *qtvalue, const QtLua::Value &luavalue);
};

} // namespace Qamer

#endif // QAMER_LUAPARAMETERCONVERTER_HPP
