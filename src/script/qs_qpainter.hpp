/*
 * File name: qpainterprototype.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_QSCRIPT_QPAINTER_HPP
#define QAMER_QSCRIPT_QPAINTER_HPP

#include "../../qamer_global.hpp"
#include <QObject>
#include <QScriptable>
#include <QPainter>

namespace Qamer {

class QAMERSHARED_EXPORT QPainterPrototype : public QObject, protected QScriptable
{
    Q_OBJECT
public:
    explicit QPainterPrototype(QObject *parent = 0);

    Q_INVOKABLE void drawArc(const QRectF &rectangle, int startAngle, int spanAngle);
    Q_INVOKABLE void drawArc(const QRect &rectangle, int startAngle, int spanAngle);
    Q_INVOKABLE void drawArc(int x, int y, int width, int height, int startAngle, int spanAngle);
//    Q_INVOKABLE void drawChord(const QRectF &rectangle, int startAngle, int spanAngle);
//    Q_INVOKABLE void drawChord(const QRect &rectangle, int startAngle, int spanAngle);
//    Q_INVOKABLE void drawChord(int x, int y, int width, int height, int startAngle, int spanAngle);
//    Q_INVOKABLE void drawConvexPolygon(const QPointF *points, int pointCount);
//    Q_INVOKABLE void drawConvexPolygon(const QPoint *points, int pointCount);
//    Q_INVOKABLE void drawConvexPolygon(const QPolygonF &polygon);
//    Q_INVOKABLE void drawConvexPolygon(const QPolygon &polygon);
//    Q_INVOKABLE void drawEllipse(const QRectF &rectangle);
//    Q_INVOKABLE void drawEllipse(const QRect &rectangle);
//    Q_INVOKABLE void drawEllipse(int x, int y, int width, int height);
//    Q_INVOKABLE void drawEllipse(const QPointF &center, qreal rx, qreal ry);
//    Q_INVOKABLE void drawEllipse(const QPoint &center, int rx, int ry);
//    Q_INVOKABLE void drawGlyphRun(const QPointF &position, const QGlyphRun &glyphs);
//    Q_INVOKABLE void drawImage(const QRectF &target, const QImage &image, const QRectF &source, Qt::ImageConversionFlags flags = Qt::AutoColor);
//    Q_INVOKABLE void drawImage(const QRect &target, const QImage &image, const QRect &source, Qt::ImageConversionFlags flags = Qt::AutoColor);
//    Q_INVOKABLE void drawImage(const QPointF &point, const QImage &image);
//    Q_INVOKABLE void drawImage(const QPoint &point, const QImage &image);
//    Q_INVOKABLE void drawImage(const QPointF &point, const QImage &image, const QRectF &source, Qt::ImageConversionFlags flags = Qt::AutoColor);
//    Q_INVOKABLE void drawImage(const QPoint &point, const QImage &image, const QRect &source, Qt::ImageConversionFlags flags = Qt::AutoColor);
//    Q_INVOKABLE void drawImage(const QRectF &rectangle, const QImage &image);
//    Q_INVOKABLE void drawImage(const QRect &rectangle, const QImage &image);
//    Q_INVOKABLE void drawImage(int x, int y, const QImage &image, int sx = 0, int sy = 0, int sw = -1, int sh = -1, Qt::ImageConversionFlags flags = Qt::AutoColor);
//    Q_INVOKABLE void drawLine(const QLineF &line);
//    Q_INVOKABLE void drawLine(const QLine &line);
//    Q_INVOKABLE void drawLine(const QPoint &p1, const QPoint &p2);
//    Q_INVOKABLE void drawLine(const QPointF &p1, const QPointF &p2);
//    Q_INVOKABLE void drawLine(int x1, int y1, int x2, int y2);
//    Q_INVOKABLE void drawLines(const QLineF *lines, int lineCount);
//    Q_INVOKABLE void drawLines(const QLine *lines, int lineCount);
//    Q_INVOKABLE void drawLines(const QPointF *pointPairs, int lineCount);
//    Q_INVOKABLE void drawLines(const QPoint *pointPairs, int lineCount);
//    Q_INVOKABLE void drawLines(const QVector<QPointF> &pointPairs);
//    Q_INVOKABLE void drawLines(const QVector<QPoint> &pointPairs);
//    Q_INVOKABLE void drawLines(const QVector<QLineF> &lines);
//    Q_INVOKABLE void drawLines(const QVector<QLine> &lines);
//    Q_INVOKABLE void drawPath(const QPainterPath &path);
//    Q_INVOKABLE void drawPicture(const QPointF &point, const QPicture &picture);
//    Q_INVOKABLE void drawPicture(const QPoint &point, const QPicture &picture);
//    Q_INVOKABLE void drawPicture(int x, int y, const QPicture &picture);
//    Q_INVOKABLE void drawPie(const QRectF &rectangle, int startAngle, int spanAngle);
//    Q_INVOKABLE void drawPie(const QRect &rectangle, int startAngle, int spanAngle);
//    Q_INVOKABLE void drawPie(int x, int y, int width, int height, int startAngle, int spanAngle);
//    Q_INVOKABLE void drawPixmap(const QRectF &target, const QPixmap &pixmap, const QRectF &source);
//    Q_INVOKABLE void drawPixmap(const QRect &target, const QPixmap &pixmap, const QRect &source);
//    Q_INVOKABLE void drawPixmap(const QPointF &point, const QPixmap &pixmap, const QRectF &source);
//    Q_INVOKABLE void drawPixmap(const QPoint &point, const QPixmap &pixmap, const QRect &source);
//    Q_INVOKABLE void drawPixmap(const QPointF &point, const QPixmap &pixmap);
//    Q_INVOKABLE void drawPixmap(const QPoint &point, const QPixmap &pixmap);
//    Q_INVOKABLE void drawPixmap(int x, int y, const QPixmap &pixmap);
//    Q_INVOKABLE void drawPixmap(const QRect &rectangle, const QPixmap &pixmap);
//    Q_INVOKABLE void drawPixmap(int x, int y, int width, int height, const QPixmap &pixmap);
//    Q_INVOKABLE void drawPixmap(int x, int y, int w, int h, const QPixmap &pixmap, int sx, int sy, int sw, int sh);
//    Q_INVOKABLE void drawPixmap(int x, int y, const QPixmap &pixmap, int sx, int sy, int sw, int sh);
//    Q_INVOKABLE void drawPixmapFragments(const PixmapFragment *fragments, int fragmentCount, const QPixmap &pixmap, PixmapFragmentHints hints = 0);
//    Q_INVOKABLE void drawPixmapFragments(const QRectF *targetRects, const QRectF *sourceRects, int fragmentCount, const QPixmap &pixmap, PixmapFragmentHints hints = 0);
//    Q_INVOKABLE void drawPoint(const QPointF &position);
//    Q_INVOKABLE void drawPoint(const QPoint &position);
//    Q_INVOKABLE void drawPoint(int x, int y);
//    Q_INVOKABLE void drawPoints(const QPointF *points, int pointCount);
//    Q_INVOKABLE void drawPoints(const QPoint *points, int pointCount);
//    Q_INVOKABLE void drawPoints(const QPolygonF &points);
//    Q_INVOKABLE void drawPoints(const QPolygon &points);
//    Q_INVOKABLE void drawPolygon(const QPointF *points, int pointCount, Qt::FillRule fillRule = Qt::OddEvenFill);
//    Q_INVOKABLE void drawPolygon(const QPoint *points, int pointCount, Qt::FillRule fillRule = Qt::OddEvenFill);
//    Q_INVOKABLE void drawPolygon(const QPolygonF &points, Qt::FillRule fillRule = Qt::OddEvenFill);
//    Q_INVOKABLE void drawPolygon(const QPolygon &points, Qt::FillRule fillRule = Qt::OddEvenFill);
//    Q_INVOKABLE void drawPolyline(const QPointF *points, int pointCount);
//    Q_INVOKABLE void drawPolyline(const QPoint *points, int pointCount);
//    Q_INVOKABLE void drawPolyline(const QPolygonF &points);
//    Q_INVOKABLE void drawPolyline(const QPolygon &points);
//    Q_INVOKABLE void drawRect(const QRectF &rectangle);
//    Q_INVOKABLE void drawRect(const QRect &rectangle);
//    Q_INVOKABLE void drawRect(int x, int y, int width, int height);
//    Q_INVOKABLE void drawRects(const QRectF *rectangles, int rectCount);
//    Q_INVOKABLE void drawRects(const QRect *rectangles, int rectCount);
//    Q_INVOKABLE void drawRects(const QVector<QRectF> &rectangles);
//    Q_INVOKABLE void drawRects(const QVector<QRect> &rectangles);
//    Q_INVOKABLE void drawRoundedRect(const QRectF &rect, qreal xRadius, qreal yRadius, Qt::SizeMode mode = Qt::AbsoluteSize);
//    Q_INVOKABLE void drawRoundedRect(const QRect &rect, qreal xRadius, qreal yRadius, Qt::SizeMode mode = Qt::AbsoluteSize);
//    Q_INVOKABLE void drawRoundedRect(int x, int y, int w, int h, qreal xRadius, qreal yRadius, Qt::SizeMode mode = Qt::AbsoluteSize);
//    Q_INVOKABLE void drawStaticText(const QPointF &topLeftPosition, const QStaticText &staticText);
//    Q_INVOKABLE void drawStaticText(const QPoint &topLeftPosition, const QStaticText &staticText);
//    Q_INVOKABLE void drawStaticText(int left, int top, const QStaticText &staticText);
//    Q_INVOKABLE void drawText(const QPointF &position, const QString &text);
//    Q_INVOKABLE void drawText(const QPoint &position, const QString &text);
//    Q_INVOKABLE void drawText(const QRectF &rectangle, int flags, const QString &text, QRectF *boundingRect = 0);
//    Q_INVOKABLE void drawText(const QRect &rectangle, int flags, const QString &text, QRect *boundingRect = 0);
//    Q_INVOKABLE void drawText(int x, int y, const QString &text);
//    Q_INVOKABLE void drawText(int x, int y, int width, int height, int flags, const QString &text, QRect *boundingRect = 0);
//    Q_INVOKABLE void drawText(const QRectF &rectangle, const QString &text, const QTextOption &option = QTextOption());
//    Q_INVOKABLE void drawTiledPixmap(const QRectF &rectangle, const QPixmap &pixmap, const QPointF &position = QPointF());
//    Q_INVOKABLE void drawTiledPixmap(const QRect &rectangle, const QPixmap &pixmap, const QPoint &position = QPoint());
//    Q_INVOKABLE void drawTiledPixmap(int x, int y, int width, int height, const QPixmap &pixmap, int sx = 0, int sy = 0);
signals:

public slots:

};

QAMERSHARED_EXPORT QScriptValue cf_QPainter(QScriptContext *context, QScriptEngine *engine);

} // end namespace Qamer

#endif // QAMER_QSCRIPT_QPAINTER_HPP
