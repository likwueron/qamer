/*
 * File name: application.hpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "qamer_export.hpp"
#include <QApplication>

#ifndef QAMER_APPLICATION_HPP
#define QAMER_APPLICATION_HPP

#if defined(qApp)
#undef qApp
#endif
#define qApp (static_cast<Qamer::Application *>(QCoreApplication::instance()))

namespace Qamer {

class FixedMemoryPool;
class MessageHandler;

class QAMERSHARED_EXPORT Application : public QApplication
{
    Q_OBJECT
public:
    Application(int &argc, char **argv);
    ~Application();
    static void readTranslatorConfig(const QString &file);
    static void setTrLocale(const QString &locale);
    //use special message handler, see core/messagehandler for more information
    static void redirectMessage(QWidget &widget);
    static void directMessage();

    bool event(QEvent *event);

    static void* mainAlloc(size_t size);
    static void mainFree(void *p, size_t size);
    static void* sceneAlloc(size_t size);
    static void sceneFree(void *p, size_t size);

    Q_INVOKABLE void setMainMemBlk(int num, int size);
    Q_INVOKABLE void setSceneMemBlk(int num, int size);
private:
    void setMemBlk(FixedMemoryPool **pool, int num, int size);
    static void *alloc(FixedMemoryPool **pool, size_t size);
    static void free(FixedMemoryPool **pool, void *p, size_t size);

    static MessageHandler *smp_msgHandler;
    static FixedMemoryPool *smp_pool;
    static FixedMemoryPool *smp_scenePool;
};

} // end namespace Qamer

#endif // QAMER_APPLICATION_HPP
