/*
 * File name:
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "pixmapeffect.hpp"
#include "_p/pixmapeffect_p.hpp"
#include "_p/pixmapdata_p.hpp"
#include <QPainter>
#include <QDebug>

namespace Qamer {

PixmapEffect::PixmapEffect(const PixmapDataPrivate *pixdata, QObject *parent)
    : GameObject(new PixmapEffectPrivate,
                 Enum::ObjectUndefined, parent)
{
    Q_D(PixmapEffect);
    d->copyPixmapRsc(*pixdata);
    if(d->sheets_default().count()) {
        resize(d->sheets_default().at(0).clipSize());
    }
}

PixmapEffect::~PixmapEffect()
{

}

void PixmapEffect::setFacing(int faceScheme)
{
    Q_D(PixmapEffect);
    d->setFacing(faceScheme);
}

bool PixmapEffect::setProgressing(int speed, QRect toRect, Enum::MoveMode mode)
{
    //assume object has proper position as "from"
    Q_D(PixmapEffect);
    QRect geo = geometry();

    d->m_to = movePeek(geo, toRect, mode);

    QPoint delta = d->m_to - geo.topLeft();
    int sum = delta.manhattanLength();
    d->m_delta = QPointF(double(speed * delta.x()) / sum,
                         double(speed * delta.y()) / sum);

    d->m_canProgressing = true;

    return d->setScheme(d->progressId());
}

bool PixmapEffect::setFinishing()
{
    Q_D(PixmapEffect);
    d->m_canFinishing = true;
    return d->setScheme(d->finishId());
}

bool PixmapEffect::isBackground() const
{
    Q_D(const PixmapEffect);
    return d->m_ground == Background;
}

PixmapEffect *PixmapEffect::backgroundEffect()
{
    Q_D(PixmapEffect);
    d->generateId();
    if(d->m_ground == Foreground) {
        PixmapEffect *e = new PixmapEffect(*this);
        e->move(this->position());
        PixmapEffectPrivate *eP = e->d_func();
        eP->m_ground = Background;

        bool ok = false;

        //REWRITE:
        //has progressId => ok
        //or has finishId => ok
        if(eP->m_canProgressing) ok = eP->setScheme(eP->progressId());
        else ok = eP->setScheme(eP->finishId());

        qDebug() << "printing foreground";
        d->printInner();
        qDebug() << "";

        qDebug() << "printing background";
        e->d_func()->printInner();
        qDebug() << "";
        if(ok) return e;
        else delete e;
    }
    return nullptr;
}

void PixmapEffect::painted(QPainter *painter, const QRect &updateRect)
{
    Q_UNUSED(updateRect)
    Q_D(PixmapEffect);
    if(isVisible() && d->hasSheet(0)) {
        d->generateId();
        d->sheets_default().at(0).draw(painter, position(), d->currentPixmapIndex());
    }
}

void PixmapEffect::when_schemeEnd_reached(int scheme)
{
    Q_D(PixmapEffect);
    //this job is done by nextPhase currently
    Q_UNUSED(scheme)
    //go to next scheme or delete this object
    if(!d->nextPhase()) {
        setVisible(false);
        deleteLater();
    }
}

PixmapEffect::PixmapEffect(PixmapEffectPrivate *pdd, QObject *parent)
    : GameObject(pdd, Enum::ObjectCopy, parent)
{

}

PixmapEffect::PixmapEffect(const PixmapEffect &that)
    : GameObject(new PixmapEffectPrivate(*(that.d_func())),
                 Enum::ObjectCopy, that.parent())
{

}

} // namespace Qamer

