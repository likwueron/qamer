/*
 * File name: label.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_LABEL_HPP
#define QAMER_LABEL_HPP

#include "../qamer_export.hpp"
#include <QLabel>
#include <QtLua/WrappedObject>
#include <QtLua/Value>

namespace Qamer {

class SpriteSheet;

class QAMERSHARED_EXPORT Label : public QLabel, public QtLua::WrappedObject
{
    Q_OBJECT
    Q_CLASSINFO("LuaName", "Label")
    Q_PROPERTY(QByteArray script READ script WRITE setScript)
public:
    Q_INVOKABLE explicit Label(QWidget *parent = 0);

    QByteArray script() const;
    void setScript(const QByteArray &script);
signals:

public slots:
    void setPixmapPath(const QString &path);

private:
    QByteArray m_script;
    QtLua::Value m_func;
};

} // namespace Qamer

#endif // QAMER_LABEL_HPP
