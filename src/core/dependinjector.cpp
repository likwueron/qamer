/*
 * File name: dependinjector.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "dependinjector.hpp"

namespace Qamer {

DependInjector::DependInjector()
    :m_supremeClassName("QObject"), m_useDotNS(false)
{
}

DependInjector::~DependInjector()
{

}

void DependInjector::registerMetaObject(const QMetaObject &meta)
{
    QByteArray className = meta.className();
    registerMetaObject(className, meta);
}

void DependInjector::registerMetaObject(const QByteArray &className, const QMetaObject &meta)
{
    if(m_classMap.contains(className)) {
        return;
    }
    const QMetaObject *p_meta = &meta;
    const QMetaObject *p_smeta = p_meta;
    while((p_smeta = p_smeta->superClass())) {
        if(p_smeta->className() == m_supremeClassName) {
            m_classMap.insert(replaceColumn(className), p_meta);
        }
    }
}

QObject *DependInjector::createInstance(const QByteArray &className, QObject *parent)
{
    if(const QMetaObject *p_meta = m_classMap.value(className)) {
        QObject *obj = p_meta->newInstance(Q_ARG(QObject*, parent));
        Q_ASSERT_X(obj, "IDependencyInjection", "Required class MUST HAS public slots/Q_INVOKABLE constructor");
        return obj;
    }
    return 0x0;
}

QByteArray DependInjector::supremeClassName() const
{
    return m_supremeClassName;
}

void DependInjector::setSupremeClass(const QByteArray &className)
{
    m_supremeClassName = replaceColumn(className);
}

void DependInjector::setSupremeClass(const QMetaObject &meta)
{
    setSupremeClass(meta.className());
}

void DependInjector::useScriptNamespace(bool use)
{
    m_useDotNS = use;
}

QByteArray DependInjector::replaceColumn(const QByteArray &str)
{
    if(m_useDotNS) {
        bool hasColumn = false;

        int count = str.count();
        QByteArray result;
        result.reserve(count);
        for(int i = 0; i < count; i++) {
            char c = str.at(i);
            if(c != ':') {
                hasColumn = false;
                result += c;
            }
            else {
                if(!hasColumn) {
                    hasColumn = true;
                    result += '.';
                }
            }
        }
        return result;
    }
    else return str;
}

} // end namespace Qamer
