/*
 * File name: finder.hpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_TILEFINDER_FINDER_HPP
#define QAMER_TILEFINDER_FINDER_HPP

#include "../qamer_export.hpp"
#include <QList>
#include <QPoint>

namespace Qamer {

class GameScene;

namespace TileFinder {

class Finder
{
public:
    Finder();
    virtual ~Finder();

    virtual Finder *usableOne(const GameScene *scene) const = 0;

    virtual QList<QPoint> find(QPoint originPoint,
                               int movepoint, int direction) = 0;
protected:
    Finder(const Finder &that, const GameScene *scene);

    const GameScene *currentScene() const;
    bool isPointValid(const QPoint &point) const;
private:
    const GameScene *cup_scene;
};

} // namespace TileFinder
} // namespace Qamer

#endif // QAMER_TILEFINDER_FINDER_HPP
