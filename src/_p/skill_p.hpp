/*
 * File name: skill_p.hpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_SKILLPRIVATE_HPP
#define QAMER_SKILLPRIVATE_HPP

#include "../qamer_export.hpp"
#include "gameobject_p.hpp"
#include "pixmapdata_p.hpp"
#include "../skill.hpp"

namespace Qamer {

class QAMERSHARED_EXPORT SkillPrivate : public GameObjectPrivate, public PixmapDataPrivate
{
    Q_DECLARE_PUBLIC(Skill)
public:
    SkillPrivate();
    ~SkillPrivate();

    void addScheme(quint16 id, const QList<quint16> &list);
    //quint16 currentPixmapIndex() const;
    //quint16 currentSchemeId() const;
    //quint16 currentSchemeIndex() const;
    //quint16 setScheme(quint16 id, quint16 index = 0);
    //quint16 progressScheme();
    //
    void addSpriteSheet(const SpriteSheet &sheet);
    //void setCurrentPixmapIndex(quint16 index);

protected:
    const QList<SpriteSheet> &sheets() const;

    SkillPrivate(const SkillPrivate &that);

private:
    QMap<int,ParaList> m_parameterMap;

    QByteArray m_possibleTilesFinderName; //isEmpty mean effect self
    QByteArray m_effectTilesFinderName; //isEmpty mean effect 1 tile

    Enum::UnitRelationship m_vTarget;
    Enum::UnitRelationship m_vPossible;
    Enum::UnitRelationship m_vEffect;
    //all_target, single_target
    int m_passiveAnimationMode;

    int m_animationSpeed;
};

} // namespace Qamer

#endif // QAMER_SKILLPRIVATE_HPP
