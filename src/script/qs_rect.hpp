/*
 * File name: qrectf.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QRECTFPROTOTYPE_HPP
#define QRECTFPROTOTYPE_HPP

#include "../../qamer_global.hpp"
#include <QObject>
#include <QScriptable>
#include <QScriptValue>
#include <QMetaType>
#include <QRectF>

Q_DECLARE_METATYPE(QRectF*)

namespace Qamer {

class QAMERSHARED_EXPORT QRectFPrototype : public QObject, protected QScriptable
{
    Q_OBJECT
    Q_PROPERTY(qreal x READ x WRITE setX)
    Q_PROPERTY(qreal y READ y WRITE setY)
    Q_PROPERTY(qreal width READ width WRITE setWidth)
    Q_PROPERTY(qreal height READ height WRITE setHeight)
    Q_PROPERTY(qreal area READ area)
public:
    explicit QRectFPrototype(QObject *parent = 0);

    Q_INVOKABLE void adjust(qreal dx1, qreal dy1, qreal dx2, qreal dy2);
    Q_INVOKABLE QRectF adjusted(qreal dx1, qreal dy1, qreal dx2, qreal dy2) const;

    //    qreal top() const;
    //    QPointF topLeft() const;
    //    QPointF topRight() const;
    //    qreal left() const;
    //    void setLeft(qreal x);
    //    qreal right() const;
    Q_INVOKABLE qreal bottom() const;
    //    void setBottom(qreal y);
    Q_INVOKABLE QPointF bottomLeft() const;
    Q_INVOKABLE QPointF bottomRight() const;
    Q_INVOKABLE QPointF center() const;

    Q_INVOKABLE bool contains() const;
    Q_INVOKABLE void getCoords(qreal * x1, qreal * y1, qreal * x2, qreal * y2) const;
    Q_INVOKABLE void getRect(qreal * x, qreal * y, qreal * width, qreal * height) const;

//    QRectF intersected(const QRectF &rectangle) const;
//    bool intersects(const QRectF &rectangle) const;
      Q_INVOKABLE bool isValid() const;

//    void moveBottom(qreal y);
//    void moveBottomLeft(const QPointF &position);
//    void moveBottomRight(const QPointF &position);
//    void moveCenter(const QPointF &position);
//    void moveLeft(qreal x);
//    void moveRight(qreal x);
//    void moveTo(qreal x, qreal y);
//    void moveTo(const QPointF &position);
//    void moveTop(qreal y);
//    void moveTopLeft(const QPointF &position);
//    void moveTopRight(const QPointF &position);
//    QRectF normalized() const;

//    void setBottomLeft(const QPointF &position);
//    void setBottomRight(const QPointF &position);
//    void setCoords(qreal x1, qreal y1, qreal x2, qreal y2);

//    void setRect(qreal x, qreal y, qreal width, qreal height);
//    void setRight(qreal x);
    //    QSizeF size() const;
//    void setSize(const QSizeF &size);
//    void setTop(qreal y);
//    void setTopLeft(const QPointF &position);
//    void setTopRight(const QPointF &position);

//    void translate(qreal dx, qreal dy);
//    void translate(const QPointF &offset);
//    QRectF translated(qreal dx, qreal dy) const;
//    QRectF translated(const QPointF &offset) const;
//    QRectF united(const QRectF &rectangle) const;

    qreal x() const;
    void setX(qreal x);
    qreal y() const;
    void setY(qreal y);
    qreal width() const;
    void setWidth(qreal width);
    qreal height() const;
    void setHeight(qreal height);
    qreal area() const;
signals:

public slots:
    QString toString() const;
};

QAMERSHARED_EXPORT QScriptValue cf_QRectF(QScriptContext *context, QScriptEngine *engine);

} // end namespace Qamer

#endif // QAMER_QRECTFPROTOTYPE_HPP
