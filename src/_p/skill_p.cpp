/*
 * File name: skill_p.cpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "skill_p.hpp"

namespace Qamer {

SkillPrivate::SkillPrivate()
    : GameObjectPrivate(), PixmapDataPrivate(),
      m_vTarget(Enum::RelateEnemy),
      m_vPossible(Enum::RelateEnemy),
      m_vEffect(Enum::RelateEnemy),
      m_passiveAnimationMode(0), m_animationSpeed(0)
{

}

SkillPrivate::~SkillPrivate()
{

}

void SkillPrivate::addScheme(quint16 id, const QList<quint16> &list)
{
    addScheme_default(id, list);
}

void SkillPrivate::addSpriteSheet(const SpriteSheet &sheet)
{
    addSpriteSheet_default(sheet);
}

const QList<SpriteSheet> &SkillPrivate::sheets() const
{
    return sheets_default();
}

SkillPrivate::SkillPrivate(const SkillPrivate &that)
    : GameObjectPrivate(that), PixmapDataPrivate(that),
      m_vTarget(that.m_vTarget),
      m_vPossible(that.m_vPossible),
      m_vEffect(that.m_vEffect),
      m_passiveAnimationMode(that.m_passiveAnimationMode),
      m_animationSpeed(that.m_animationSpeed)
{

}

} // end namespace Qamer

