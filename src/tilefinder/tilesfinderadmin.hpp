/*
 * File name: tilesfinderadmin.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_TILESFINDERADMIN_HPP
#define QAMER_TILESFINDERADMIN_HPP

#include "../qamer_export.hpp"
#include <QPoint>
#include <QList>
#include <QByteArray>

class QScriptValue;
class QScriptEngine;

namespace QtLua {
class Value;
}

namespace Qamer {

class GameScene;
class LuaScriptEngine;

namespace TileFinder {
class Finder;
}

class QAMERSHARED_EXPORT TilesFinderAdmin
{
private:
    enum FinderFlags {
        CppFinder,
        ScriptFinder,
        LuaFinder
    };

    struct FinderData
    {
        FinderData();
        FinderData(const FinderData& that);
        ~FinderData();

        QByteArray name;
        void *finderPtr;
        FinderFlags flags;
    };
public:
    TilesFinderAdmin(const GameScene *scene);
    ~TilesFinderAdmin();

    static void init();
    static void quit();

    static void insertFinder(const QByteArray &name, TileFinder::Finder *finder);
    static void insertFinder(const QByteArray &name, LuaScriptEngine *eng);
    static void insertFinder(const QByteArray &name, QScriptEngine *eng);

    QList<QPoint> find(const QByteArray& name,
                       QPoint originalPoint, int movepoint, int direction);
protected:
    static bool isExist(const QByteArray& name);

    QList<QPoint> findByCpp(const GameScene *scene, TileFinder::Finder *finder,
                                   QPoint oPoint, int mp, int direction);
    QList<QPoint> findByLua(const GameScene *scene, QtLua::Value *finder,
                            QPoint oPoint, int mp, int direction);
    QList<QPoint> findByScript(const GameScene *scene, QScriptValue* finder,
                               QPoint oPoint, int mp, int direction);
private:
    static QList<FinderData> sm_finderList;

    const GameScene *cup_scene;
};

} // end namespace Qamer

#endif // QAMER_TILESFINDERADMIN_HPP
