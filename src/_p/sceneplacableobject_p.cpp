/*
 * File name: sceneplacableobject_p.cpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "sceneplacableobject_p.hpp"
#include "../sceneplacableobject.hpp"

namespace Qamer {

ScenePlacableObjectPrivate::ScenePlacableObjectPrivate()
    : GameObjectPrivate(), PixmapDataPrivate(),
      m_sceneId(-1), m_sceneIndex(-1)
{

}

ScenePlacableObjectPrivate::~ScenePlacableObjectPrivate()
{

}

void ScenePlacableObjectPrivate::addScheme(quint16 id, const QList<quint16> &list)
{
    addScheme_default(id, list);
}

quint16 ScenePlacableObjectPrivate::currentPixmapIndex() const
{
    return currentPixmapIndex_default();
}

quint16 ScenePlacableObjectPrivate::currentSchemeId() const
{
    return currentSchemeId_default();
}

quint16 ScenePlacableObjectPrivate::currentSchemeIndex() const
{
    return currentSchemeIndex_default();
}

quint16 ScenePlacableObjectPrivate::setScheme(quint16 id, quint16 index)
{
    return setScheme_default(id, index);
}

quint16 ScenePlacableObjectPrivate::progressScheme()
{
    return progressScheme_default();
}

void ScenePlacableObjectPrivate::addSpriteSheet(const SpriteSheet &sheet)
{
    Q_Q(ScenePlacableObject);
    if(sheet.isValid()) {
        addSpriteSheet_default(sheet);
        Q_EMIT q->spriteSheet_inserted(sheet.clipSize());
    }
}

void ScenePlacableObjectPrivate::setCurrentPixmapIndex(quint16 index)
{
    setCurrentPixmapIndex_default(index);
}

ScenePlacableObjectPrivate::ScenePlacableObjectPrivate(const ScenePlacableObjectPrivate &that)
    : GameObjectPrivate(that), PixmapDataPrivate(that),
      m_sceneId(-1), m_sceneIndex(-1)
{

}

} // end namespace Qamer

