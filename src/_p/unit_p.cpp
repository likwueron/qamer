/*
 * File name: unit_p.cpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "unit_p.hpp"
#include "../application.hpp"

namespace Qamer {

UnitPrivate::UnitPrivate()
    : ScenePlacableObjectPrivate(),
      m_side(invalidUnitSide),
      m_facing(Enum::FaceNorth),
      m_unique(false),
      m_movepoint(0)
{

}

UnitPrivate::~UnitPrivate()
{

}

void *UnitPrivate::operator new(size_t size) throw()
{
    return qApp->mainAlloc(size);
}

void UnitPrivate::operator delete(void *p)
{
    qApp->mainFree(p, sizeof(UnitPrivate));
}

StatValue UnitPrivate::statValue(const QByteArray &name) const
{
    return m_statValueMap.value(name);
}

UnitPrivate::UnitPrivate(const UnitPrivate &that)
    : ScenePlacableObjectPrivate(that),
      m_side(invalidUnitSide),
      m_statValueMap(that.m_statValueMap),
      m_facing(Enum::FaceNorth),
      m_unique(false),
      m_skillIdList(that.m_skillIdList),
      m_resisMap(that.m_resisMap),
      m_movepoint(that.m_movepoint)
{

}

} // namespace Qamer

