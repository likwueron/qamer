/*
 *File name: ps2controller.cpp
 *Author: kwueron
 *E-mail: likwueron@gmail.com
 *Last revised: 2014/1/30
 *Published under Public Domain
 */

#include "ps2controller.hpp"

PS2Controller::PS2Controller(QObject *parent) :
    Controller(parent)
{
}

QString PS2Controller::layout() const
{
    return "01041200";
}

QString PS2Controller::monitorUiFile() const
{
    return "./controllerMonitor/ps2monitor.ui";
}

void PS2Controller::when_hatValue_changed(int hatId, qint8 value)
{
    Q_UNUSED(hatId)
#ifdef CONTROLLER_SIGNAL
    emit action_done(Hat1, "DPad", value);
#else
    sendEvent(Hat1, "DPad", value);
#endif
}

void PS2Controller::when_buttonValue_changed(int buttonId, qint8 value)
{
    QString name;
    int role;
    switch(buttonId) {
    case Select:
        name = "Select";
        role = ButtonSelect;
        break;
    case Start:
        name = "Start";
        role = ButtonStart;
        break;
    case R1:
        name = "R1";
        role = ButtonR1;
        break;
    case L1:
        name = "L1";
        role = ButtonL1;
        break;
    case R2:
        name = "R2";
        role = ButtonR2;
        break;
    case L2:
        name = "L2";
        role = ButtonL2;
        break;
    case R3:
        name = "R3";
        role = ButtonR3;
        break;
    case L3:
        name = "L3";
        role = ButtonL3;
        break;
    case Cross:
        name = "Cross";
        role = Button1;
        break;
    case Circle:
        name = "Circle";
        role = Button2;
        break;
    case Square:
        name = "Square";
        role = Button3;
        break;
    case Triangle:
        name = "Triangle";
        role = Button4;
        break;
    default:
        role = Invalid;
        break;
    }
#ifdef CONTROLLER_SIGNAL
    emit action_done(role, name, value);
#else
    sendEvent(role, name, value);
#endif
}

void PS2Controller::when_axisValue_changed(int axisId, qint16 value)
{
    QString name;
    int role;
    switch(axisId) {
    case 0:
        name = "X_Move";
        role = AxisX1;
        break;
    case 1:
        name = "Y_Move";
        role = AxisY1;
        break;
    case 2:
        name = "X_Rotation";
        role = AxisX2;
        break;
    case 3:
        name = "Y_Rotation";
        role = AxisY2;
        break;
    default:
        role = Invalid;
        break;
    }
#ifdef CONTROLLER_SIGNAL
    emit action_done(role, name, value);
#else
    sendEvent(role, name, value);
#endif
}

void PS2Controller::when_trackballValue_changed(int trackballId, int dx, int dy)
{
    Q_UNUSED(trackballId)
    Q_UNUSED(dx)
    Q_UNUSED(dy)
}

void PS2Controller::when_dataValue_changed(const JoystickData *data)
{
    Q_UNUSED(data)
}
