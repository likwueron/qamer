/*
 * File name: ctorcoordinate.cpp
 * Copyright 2015 kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "ctorcoordinate.hpp"
#include <QPointF>
#include <QSizeF>
#include <QTransform>
#include <QMatrix4x4>
#include <QQuaternion>
#include <QVector2D>
#include <QVector3D>
#include <QVector4D>

namespace Qamer {
namespace LoadCtor {

QVariant Point(const QVariantList &values)
{
    switch(values.count()) {
    case 2:
        return QPointF(values.at(0).toReal(),
                       values.at(1).toReal());
        break;
    default:
        return QPointF();
        break;
    }
}

QVariant Size(const QVariantList &values)
{
    switch(values.count()) {
    case 2:
        return QSizeF(values.at(0).toReal(),
                      values.at(1).toReal());
        break;
    default:
        return QSizeF();
        break;
    }
}

QVariant Transform(const QVariantList &values)
{
    Q_UNUSED(values)
    qWarning() << "Qamer::LoadingCtor::Transform: function not implemented, return QVariant().";
    return QVariant();
}

QVariant Matric4x4(const QVariantList &values)
{
    Q_UNUSED(values)
    qWarning() << "Qamer::LoadingCtor::Matric4x4: function not implemented, return QVariant().";
    return QVariant();
}

QVariant Quaternion(const QVariantList &values)
{
    Q_UNUSED(values)
    qWarning() << "Qamer::LoadingCtor::Quaternion: function not implemented, return QVariant().";
    return QVariant();
}

QVariant Vector2D(const QVariantList &values)
{
    switch(values.count()) {
    case 1:
        return QVector2D(values.at(0).toPointF());
        break;
    case 2:
        return QVector2D(values.at(0).toReal(),
                         values.at(1).toReal());
        break;
    default:
        return QVector2D();
        break;
    }
}

QVariant Vector3D(const QVariantList &values)
{
    switch(values.count()) {
    case 1:
        return QVector3D(values.at(0).toPointF());
        break;
    case 3:
        return QVector3D(values.at(0).toReal(),
                         values.at(1).toReal(),
                         values.at(2).toReal());
        break;
    default:
        return QVector3D();
        break;
    }
}

QVariant Vector4D(const QVariantList &values)
{
    switch(values.count()) {
    case 1:
        return QVector4D(values.at(0).toPointF());
        break;
    case 4:
        return QVector4D(values.at(0).toReal(),
                         values.at(1).toReal(),
                         values.at(2).toReal(),
                         values.at(3).toReal());
        break;
    default:
        return QVector4D();
        break;
    }
}

} // end namespace LoadCtor
} // namespace Qamer
