/*
 * File name: gamelabel.hpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_GAMELABEL_HPP
#define QAMER_GAMELABEL_HPP

#include "gameui.hpp"

namespace Qamer {

class QAMERSHARED_EXPORT GameLabel : public GameUI
{
    Q_OBJECT
    Q_PROPERTY(QString text READ text WRITE setText)
public:
    GameLabel(QObject *parent = 0);
    ~GameLabel();

    QString text() const;//using description
    void setText(const QString &text);

    void painted(QPainter *painter, const QRect &updateRect);
private:
};

} // namespace Qamer

#endif // QAMER_GAMELABEL_HPP
