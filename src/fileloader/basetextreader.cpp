/*
 * File name: basetextreader.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "basetextreader.hpp"
#include <QFile>

namespace Qamer {

BaseTextReader::BaseTextReader()
    : mp_file(0x0),
      m_spaceAtTail(false),
      m_getError(false),
      m_curPos(1), m_curLine(1), m_willNextLine(false)

{
    m_trContext.reserve(64);
    m_buffer.reserve(256);
    m_errorMsg.reserve(4096);
}

BaseTextReader::~BaseTextReader()
{

}

QString BaseTextReader::errorMessage()
{
    QString result = m_errorMsg;
    m_getError = false;
    m_errorMsg.clear();
    return result;
}

void BaseTextReader::while_newText_reading()
{
    m_trContext.clear();
    //reader: buffer, space at end
    m_buffer.clear();
    m_spaceAtTail = false;
    //position tracker
    m_curPos = 1;
    m_curLine = 1;
    m_willNextLine = false;
    //error message
    m_getError = false;
    m_errorMsg.clear();
}

char BaseTextReader::readCharFromFile()
{
    char c;
    if(mp_file->getChar(&c)) {
        if(m_willNextLine) {
            m_curLine += 1;
            m_curPos = 0;
            m_willNextLine = false;
        }
        if(c == '\n') m_willNextLine = true;
        else m_curPos += 1;
    }
    else {} //error
    return c;
}

void BaseTextReader::ignoredUntil(char untilC)
{
    m_buffer.clear();
    m_spaceAtTail = false;
    while(BaseTextReader::readCharFromFile() != untilC);
}

QByteArray BaseTextReader::genTrContext(const QString &filename)
{
    //find last /
    int start = filename.lastIndexOf('/') + 1;
    //Qt4 lupdate should find first . after last /
    //Qt5 lupdate should find last . after last /
#if QT_VERSION < QT_VERSION_CHECK(5,0,0)
    //find first . after last /
    int end = filename.indexOf('.', start);
#else
    int end = filename.lastIndexOf('.', start);
#endif

    return filename.mid(start, end-start).toLocal8Bit();
}

void BaseTextReader::addErrorMessage(const QString &msg)
{
    m_getError = true;
    m_errorMsg += msg;
}

QString BaseTextReader::currentFileCursor() const
{
    return QString("Line: %1, Column: %2").arg(m_curLine).arg(m_curPos);
}

void BaseTextReader::bufferAppend(char c)
{
    m_buffer.append(c);
}

} //end namespace Qamer
