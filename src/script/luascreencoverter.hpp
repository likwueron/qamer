/*
 * File name: luascreenconverter.hpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LUASCREENCOVERTER_HPP
#define LUASCREENCOVERTER_HPP

#include "../qamer_export.hpp"
#include <QtLua/MetaType>

namespace Qamer {

class Screen;

class QAMERSHARED_EXPORT LuaScreenCoverter : public QtLua::MetaType<Screen*>
{
public:
    LuaScreenCoverter();

    QtLua::Value qt2lua(QtLua::State *ls, Screen* const *qtvalue);
    bool lua2qt(Screen **qtvalue, const QtLua::Value &luavalue);
};

} // end namespace Qamer

#endif // LUASCREENCOVERTER_HPP
