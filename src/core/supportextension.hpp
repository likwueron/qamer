/*
 * File name: supportextension.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_SUPPORTEXTENSION_HPP
#define QAMER_SUPPORTEXTENSION_HPP

#include "../qamer_export.hpp"
#include <QMultiMap>
#include <QStringList>

namespace Qamer {

typedef QMapIterator<QString, QString> SupportExtensionIterator;

class QAMERSHARED_EXPORT SupportExtension : public QMultiMap<QString, QString>
{
public:
    explicit SupportExtension();
    ~SupportExtension();

    static SupportExtension *fromFile(const QString &fileName);
    static SupportExtension *images();
    //static SupportExtension *audios();
    //static SupportExtension *videos();

    QString allFileLabel() const;
    void setAllFileLabel(const QString &label);

    void add(const QString &label, const QString &extension);
    void add(const QString &label, const QStringList &extensions);

    QStringList listFilter() const;
    QStringList listFilterByLabel(const QString &label) const;

    QString openFileFilter() const;
    QString openFileFilterByLabel(const QString &label) const;
    QString openFileFilterByExtension(const QString &extension) const;

    QString label(const QString &extension);
    QStringList extensions(const QString &label);
private:
    QString m_allFileLabel;
};

} // end namespace Qamer

#endif // QAMER_SUPPORTEXTENSION_HPP
