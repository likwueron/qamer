#ifndef RAWCONTROLLER_HPP
#define RAWCONTROLLER_HPP

#include "controller.hpp"

class RawController : public Controller
{
    Q_OBJECT
public:
    Q_INVOKABLE explicit RawController(QObject *parent = 0);
    
    QString layout() const;
    QString monitorUiFile() const;

    void when_hatValue_changed(int hatId, qint8 value);
    void when_buttonValue_changed(int buttonId, qint8 value);
    void when_axisValue_changed(int axisId, qint16 value);
    void when_trackballValue_changed(int trackballId, int dx, int dy);
    void when_dataValue_changed(const JoystickData *data);
signals:
    
public slots:
    
protected:
};

#endif // RAWCONTROLLER_HPP
