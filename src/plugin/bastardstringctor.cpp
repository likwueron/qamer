/*
 * File name:
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "bastardstringctor.hpp"
#include "../mics/parameter.hpp"
//#include <QtPlugin>

namespace Qamer {
namespace Plugin {

QVariant parameterCtor(const QVariantList &argv)
{
    int count = argv.count();
    switch(count) {
    case 1: {
        QVariant v = argv.at(0);
        Parameter p;
        switch(v.type()) {
        case QVariant::Int:
        case QVariant::Double:
            p.setValue(v.toInt());
            break;
        case QVariant::ByteArray:
        case QVariant::String:
            p.setValue(v.toByteArray());
            break;
        default:
            return QVariant();
        }
        return QVariant::fromValue(p);
        break; }
    default: {
        Parameter p(argv);
        return QVariant::fromValue(p);
        break; }
    }
}

QVariant paraListCtor(const QVariantList &argv)
{
    ParaList result;
    foreach(const QVariant &v, argv) {
        switch(v.type()) {
        case QVariant::Int:
        case QVariant::Double:
            result << v.toInt();
            break;
        case QVariant::ByteArray:
        case QVariant::String:
            result << v.toByteArray();
            break;
        default:
            break;
        }
    }
    return QVariant::fromValue(result);
}

const QList<QByteArray> BastardStringCtor::defaultNames =
{ "BastardString", "BastardList" };
const QList<VarCtorSignature> BastardStringCtor::defaultCtors =
{ parameterCtor, paraListCtor };

BastardStringCtor::BastardStringCtor(QObject *parent)
    : QObject(parent)
{

}

QByteArray BastardStringCtor::pluginName() const
{
    return "Qamer_BastardStringCtorPlugin";
}

Qamer::Plugin::Type BastardStringCtor::pluginType() const
{
    return Qamer::Plugin::PluginVariant;
}

QList<QByteArray> BastardStringCtor::supportedNames() const
{
    return defaultNames;
}

Qamer::Plugin::Type BastardStringCtor::pluginTypeFromName(const QByteArray &name) const
{
    Q_UNUSED(name)
    return Qamer::Plugin::PluginVariant;
}

QObject *BastardStringCtor::createObject(const QByteArray &name, const QVariantList &argv, QObject *parent) const
{
    Q_UNUSED(name)
    Q_UNUSED(argv)
    Q_UNUSED(parent)
    return nullptr;
}

QVariant BastardStringCtor::createVariant(const QByteArray &name, const QVariantList &argv) const
{
    int index = defaultNames.indexOf(name);
    if(index != -1) {
        Qamer::VarCtorSignature ctor = defaultCtors.at(index);
        return ctor(argv);
    }
    return QVariant();
}

Qamer::VarCtorSignature BastardStringCtor::variantCtor(const QByteArray &name) const
{
    int index = defaultNames.indexOf(name);
    if(index != -1) {
        Qamer::VarCtorSignature ctor = defaultCtors.at(index);
        return ctor;
    }
    else return nullptr;
}

} // end namespace Plugin
} // end namespace Qamer

#if QT_VERSION < 0x050000
//Q_EXPORT_PLUGIN2(Qamerds, Qamer::Plugin::BastardStringCtor)
#endif // QT_VERSION < 0x050000
