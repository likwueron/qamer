/*
 * File name: qamer.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_MAIN_HPP
#define QAMER_MAIN_HPP

#include "qamer_export.hpp"
#include <QString>

namespace Qamer {

QAMERSHARED_EXPORT const char *version();
QAMERSHARED_EXPORT int compilerVirsion();
QAMERSHARED_EXPORT const char *qtVersion();
QAMERSHARED_EXPORT void aboutQamer();
QAMERSHARED_EXPORT QString aboutQamerTitle();
QAMERSHARED_EXPORT QString aboutQamerText();

} // end namespace Qamer

#endif // QAMER_MAIN_HPP
