/*
 *File name: ps2controller.hpp
 *Author: kwueron
 *E-mail: likwueron@gmail.com
 *Last revised: 2014/1/30
 *Published under Public Domain
 */

#ifndef PS2CONTROLLER_HPP
#define PS2CONTROLLER_HPP

#include "controller.hpp"

class PS2Controller : public Controller
{
    Q_OBJECT
    enum ButtonBinding {
        Select = 8,
        Start = 9,
        Cross = 2,
        Circle = 1,
        Triangle = 0,
        Square = 3,
        L1 = 4,
        R1 = 5,
        L2 = 6,
        R2 = 7,
        L3 = 10,
        R3 = 11
    };
public:
    Q_INVOKABLE explicit PS2Controller(QObject *parent = 0);

    QString layout() const;
    QString monitorUiFile() const;

    void when_hatValue_changed(int hatId, qint8 value);
    void when_buttonValue_changed(int buttonId, qint8 value);
    void when_axisValue_changed(int axisId, qint16 value);
    void when_trackballValue_changed(int trackballId, int dx, int dy);
    void when_dataValue_changed(const JoystickData *data);
signals:

public slots:

protected:
};

#endif // PS2CONTROLLER_HPP
