/*
 * File name: basetextreader.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_BASETEXTREADER_HPP
#define QAMER_BASETEXTREADER_HPP

#include "../qamer_export.hpp"
#include <QString>
#include <QByteArray>

class QFile;

namespace Qamer {

class QAMERSHARED_EXPORT BaseTextReader
{
public:
    BaseTextReader();
    virtual ~BaseTextReader();

    QString errorMessage();
protected:
    virtual void while_newText_reading();

    virtual char readCharFromFile();
    void ignoredUntil(char untilC);

    QByteArray genTrContext(const QString &filename);

    void addErrorMessage(const QString& msg);
    QString currentFileCursor() const;
    //reading
    void bufferAppend(char c);
    inline QByteArray assignBuffer();
    inline bool replaceSpace(char c);
    virtual bool isIgnoreChar(char c) = 0;

    QFile *mp_file;
    QByteArray m_trContext;
private:
    bool m_spaceAtTail;
    QByteArray m_buffer;

    bool m_getError;
    QString m_errorMsg;
    quint32 m_curPos;
    quint32 m_curLine;
    bool m_willNextLine;
};

QByteArray BaseTextReader::assignBuffer()
{
    QByteArray result;
    if(m_spaceAtTail) {
        result = m_buffer.left(m_buffer.count()-1);
        m_spaceAtTail = false;
    }
    else result = m_buffer;

    m_buffer.clear();
    return result;
}

bool BaseTextReader::replaceSpace(char c)
{
    switch(c) {
    case '\t':
    case ' ':
    case '_':
        if(!m_spaceAtTail && m_buffer.count()) {
            m_buffer += '_';
            m_spaceAtTail = true;
        }
        return true;
        break;
    default:
        return false;
    }
}

} //end namespace Qamer

#endif // QAMER_BASETEXTREADER_HPP
