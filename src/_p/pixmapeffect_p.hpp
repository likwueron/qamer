/*
 * File name:
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_PIXMAPEFFECTPRIVATE_HPP
#define QAMER_PIXMAPEFFECTPRIVATE_HPP

#include "gameobject_p.hpp"
#include "pixmapdata_p.hpp"
#include "../pixmapeffect.hpp"
#include <QPoint>
#include <QPointF>

namespace Qamer {

class PixmapEffectPrivate : public GameObjectPrivate, public PixmapDataPrivate
{
    Q_DECLARE_PUBLIC(PixmapEffect)
public:
    PixmapEffectPrivate();
    ~PixmapEffectPrivate();

    void addScheme(quint16 id, const QList<quint16> &list);
    quint16 currentPixmapIndex() const;
    quint16 currentSchemeId() const;
    quint16 currentSchemeIndex() const;
    quint16 setScheme(quint16 id, quint16 index = 0);
    quint16 progressScheme();
    //
    void addSpriteSheet(const SpriteSheet &sheet);
    void setCurrentPixmapIndex(quint16 index);

    int facingId() const;
    void setFacing(int faceScheme);
protected:
    void generateId();

    int progressId() const;
    int finishId() const;

    bool nextPhase();

    void printInner() const;

    PixmapEffectPrivate(const PixmapEffectPrivate& that);

    void copyPixmapRsc(const PixmapDataPrivate &rsc);
private:
    int groundId() const;

    uchar m_canProgressing : 1;
    uchar m_canFinishing : 1;
    //0: undefined, 1 background, 2 foreground
    uchar m_ground : 2;
    uchar m_facing : 4;
    //for progressing phase
    QPointF m_delta;
    QPoint m_to;
};

} // namespace Qamer

#endif // QAMER_PIXMAPEFFECTPRIVATE_HPP
