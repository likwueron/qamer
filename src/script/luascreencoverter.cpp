/*
 * File name: luascreenconverter.cpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "luascreencoverter.hpp"
#include "../widget/screen.hpp"
#include <internal/QObjectWrapper>

using namespace QtLua;

namespace Qamer {

LuaScreenCoverter::LuaScreenCoverter()
    : MetaType<Screen*>("Screen")
{

}

Value LuaScreenCoverter::qt2lua(State *ls, Screen* const *qtvalue)
{
    return Value(ls, QObjectWrapper::get_wrapper(ls, *qtvalue));
}

bool LuaScreenCoverter::lua2qt(Screen **qtvalue, const Value &luavalue)
{
    auto qow = luavalue.to_userdata_cast<QObjectWrapper>();
    Screen *o = qobject_cast<Screen*>(&qow->get_object());
    if(o) *qtvalue = o;
    return o != nullptr;
}

} // end namespace Qamer

