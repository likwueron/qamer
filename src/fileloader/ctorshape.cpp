/*
 * File name: ctorshape.cpp
 * Copyright 2015 kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "ctorshape.hpp"
#include <QLineF>
#include <QPolygonF>
#include <QRectF>
#include <QRegion>

#include <QDebug>

namespace Qamer {
namespace LoadCtor {

QVariant Line(const QVariantList &values)
{
    switch(values.count()) {
    case 2:
        return QLineF(values.at(0).toPointF(),
                      values.at(1).toPointF());
        break;
    case 4:
        return QLineF(values.at(0).toReal(),
                      values.at(1).toReal(),
                      values.at(2).toReal(),
                      values.at(3).toReal()
                    );
        break;
    default:
        return QLineF();
    }
}

QVariant Polygon(const QVariantList &values)
{
    Q_UNUSED(values)
    qWarning() << "Qamer::LoadingCtor::Polygon: function not implemented, return QVariant().";
    return QVariant();
}

QVariant Rect(const QVariantList &values)
{
    switch(values.count()) {
    case 2:
        return QRectF(values.at(0).toPointF(),
                      values.at(1).toSizeF());
        break;
    case 4:
        return QRectF(values.at(0).toReal(),
                      values.at(1).toReal(),
                      values.at(2).toReal(),
                      values.at(3).toReal());
        break;
    default:
        return QRectF();
        break;
    }
}

QVariant Region(const QVariantList &values)
{
    Q_UNUSED(values)
    qWarning() << "Qamer::LoadingCtor::Region: function not implemented, return QVariant().";
    return QVariant();
}

} // end namespace LoadCtor
} // namespace Qamer

