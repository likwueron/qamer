/*
 * File name: terrain_p.hpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_TERRAINPRIVATE_HPP
#define QAMER_TERRAINPRIVATE_HPP

#include "sceneplacableobject_p.hpp"
#include "pixmapdata_p.hpp"
#include "modifierdata_p.hpp"
#include "../terrain.hpp"

namespace Qamer {

class QAMERSHARED_EXPORT TerrainPrivate : public ScenePlacableObjectPrivate, public ModifierDataPrivate
{
    Q_DECLARE_PUBLIC(Terrain)
public:
    TerrainPrivate();
    ~TerrainPrivate();

protected:
    TerrainPrivate(const TerrainPrivate &that);
};

} // end namespace Qamer

#endif // QAMER_TERRAINPRIVATE_HPP
