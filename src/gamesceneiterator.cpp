/*
 * File name: gamesceneiterator.cpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "gamesceneiterator.hpp"
#include <QDebug>

namespace Qamer {

GameSceneIterator::GameSceneIterator(const GameScene &scene)
    : m_col(scene.sceneCol()), m_row(scene.sceneRow()),
      m_tIndex(0)
{

}

GameSceneIterator::~GameSceneIterator()
{

}

// this is for isometric
//    m_indexList.reserve(m_row * m_col);
//    int tMax = m_row+m_col-2;
//    for(int sum = 0; sum <= tMax; sum++) {
//        for(int fir = sum >= m_row ? sum-m_row+1 : 0, sec = sum >= m_row ? m_row-1 : sum;
//            fir < m_col && 0 <= sec;
//            fir++, sec--) {
//            m_indexList << scene.indexFromPoint(fir, sec);
//        }
//    }

} // end namespcae Qamer
