/*
 * File name: qamer_global.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_GLOBAL_HPP
#define QAMER_GLOBAL_HPP

#include "qamer_export.hpp"
#include "qamer_types.hpp"
#include <QObject>
#include <QPoint>
#include <QSize>

namespace Qamer {

class QAMERSHARED_EXPORT Enum
{
    Q_GADGET
    Q_CLASSINFO("LuaName", "Qamer")
private:
    Enum() {}
    ~Enum() {}
public:
    static const int ObjectCopy = -1;//special value for ObjectType
//should same as qamer_types.cpp
enum Constant {
    InvalidId = -1,
    InvalidStatValue = INT_MIN,
};

enum UnitRelationshipFlags {
    RelateUnknown = 0,
    RelateAlly = 0x1,
    RelateEnemy = 0x2,
    RelateNature = 0x4,
};

enum StatValueConfig {
    StatValueZeroAtBegin,
    StatValueMaxAtBegin,
    StatValueMinAtBegin,
    StatValueSpecificAtBegin
};

enum StatValueChangedFlags {
    StatValueNone = 0x0,
    StatValueBegin = 0x1,
    StatValueCurrent = 0x2,
    StatValueMax = 0x4,
    StatValueMin = 0x8,
};
//BEGIN: GameObject type
enum ObjectType {
    ObjectUndefined = 0,
    ObjectUnit = 0x1,
    ObjectScene = 0x2,
    ObjectTerrain = 0x4,
    ObjectSkill = 0x8,
    ObjectModifier = 0x10, //modify other object especially Unit
    ObjectUserInterface = 0x20, //like menu, dialog...which invoke little about game logic
    ObjectEvent = 0x40,
    ObjectMainTypeMask = 0xfff,
    ObjectSubTypeMask = 0xffff000,
    ObjectAll = ObjectMainTypeMask | ObjectSubTypeMask
};
//subtype: cannot overlap with ObjectMainTypeMask: 0xfff
enum SkillType {
    SkillUndefined = 0,
    SkillMove = 0x1000,
    SkillAIOffenseMask = 0x2000,
    SkillHeal = 0x4000,
    SkillOffense = SkillHeal | SkillAIOffenseMask,
    SkillBuff = 0x8000,
    SkillDebuff = SkillBuff | SkillAIOffenseMask,
    SkillAura = 0x10000,
    SkillInterfere = SkillAura | SkillAIOffenseMask
};

enum InterfaceType {
    InterfaceUndefined = 0,
    InterfaceProgressBar = 0x1000,
    InterfaceLabel = 0x2000
};

enum ModifierType {
    ModifyUndefined = 0,
    ModifierUndefined = 0,
    ModifyUnit = 0x1000
};

//Should keep those thing?
enum TypeNumber {
    ObjectTypeNumber = 5,
    SkillTypeNumber = 7
};
//END: GameObject type

enum SpriteCode {
    //for unit
    SpriteFace = 0,
    SpriteBattle = 1,
    SpriteSpecial = 2,
    //for scene
    SpriteBuilding = 0,
    //for other use
    SpriteEmotion = 0,
};

enum ParameterType {
    ParaTiles,
    ParaDamage,
    ParaDefend,
    ParaHit,
    ParaEvade,
    ParaArea,
    ParameterTypeNumber
};

enum FaceDirectionFlags {
    FaceNone = 0x0,
    FaceNorth = 0x1,
    FaceSouth = 0x2,
    FaceEast = 0x4,
    FaceWest = 0x8
};

enum SchemeIdFlags {
//directions
    SchemeDefault = 0x0,
    SchemeNorth = 0x1,    //b 001
    SchemeEast = 0x2,     //b 010
    SchemeSouth = 0x3,    //b 011
    SchemeWest = 0x4,     //b 100
    SchemeNorthEast = 0x5,//b 101
    SchemeNorthWest = 0x6,//b 110
    SchemeSouthEast = 0x7,//b 111
    SchemeSouthWest = 0x8,//b1000
//actions
    SchemeIdle = 0x0,    //b0000,0000
    SchemeMove = 0x10,   //b0001,0000
    SchemeAttack = 0x20, //b0010,0000
    SchemeDefend = 0x30, //b0011,0000
    SchemeHit = 0x40,    //b0100,0000
    //
    SchemeMid = 0x0,
    SchemeFront = 0x100,
    SchemeBack = 0x200,
    SchemeProgress = 0x100,     //b0 actually
    SchemeFinish = 0x400,         //b0100,0000,0000

    SchemeDirectionMask = 0xf,

    SchemeUser = 1000,    //0x03E8
    SchemeUserMax = 65536 //0xffff
};

enum MoveModeFlags {
    MoveTop = 0x0,
    MoveVCenter = 0x1, //b01
    MoveBottom = 0x2, //b10
    MoveLeft = 0x0,
    MoveHCenter = 0x4, //b01 00
    MoveRight = 0x8, //b10 00
    //Position type
    MoveAbsolute = 0,
    MoveOn = 0x10,
    MoveAside = 0x20,
    //for convenience
    MoveNormal = MoveTop | MoveLeft,
    MoveCenter = MoveHCenter | MoveVCenter,
    MoveStandOn = MoveHCenter | MoveBottom | MoveOn,
    //Mask
    MoveVMask = MoveTop | MoveVCenter | MoveBottom,
    MoveHMask = MoveLeft | MoveHCenter | MoveRight,
    MovePosMask = MoveAbsolute | MoveOn | MoveAside
};

enum SceneLayer {
    LayerUndefined,
    LayerUpper,
    LayerUnit,
    LayerNUO, //non-unit object, ex: tree, rock...
    LayerTerrain,
    LayerEvent,
    LayerUnder = LayerNUO | LayerTerrain | LayerEvent,
};

Q_ENUMS(Constant)
Q_DECLARE_FLAGS(UnitRelationship, UnitRelationshipFlags)
Q_FLAGS(UnitRelationship)
Q_ENUMS(StatValueConfig)
Q_DECLARE_FLAGS(StatValueChanged, StatValueChangedFlags)
Q_FLAGS(StatValueChanged)
Q_ENUMS(ObjectType)
Q_ENUMS(SkillType)
Q_ENUMS(ParameterType)
Q_DECLARE_FLAGS(FaceDirection, FaceDirectionFlags)
Q_FLAGS(FaceDirection)
Q_DECLARE_FLAGS(SchemeId, SchemeIdFlags)
Q_FLAGS(SchemeId)
Q_DECLARE_FLAGS(MoveMode, MoveModeFlags)
Q_FLAGS(MoveMode)
Q_ENUMS(SceneLayer)

};//end class Enum
Q_DECLARE_OPERATORS_FOR_FLAGS(Enum::UnitRelationship)
Q_DECLARE_OPERATORS_FOR_FLAGS(Enum::StatValueChanged)
Q_DECLARE_OPERATORS_FOR_FLAGS(Enum::FaceDirection)
Q_DECLARE_OPERATORS_FOR_FLAGS(Enum::SchemeId)
Q_DECLARE_OPERATORS_FOR_FLAGS(Enum::MoveMode)

//enable QFont enum: weight
class EnumQtFont
{
    Q_GADGET
private:
    EnumQtFont() {}
    ~EnumQtFont() {}
public:
    enum FontWeight {
        Light = 25,
        Normal = 50,
        DemiBold = 63,
        Bold = 75,
        Black = 87
    };
    Q_ENUMS(FontWeight)
};

// won't be used by script
enum PropertyAttr {
    Readable = 0x1,
    Writable = 0x2,
    Resetable = 0x4,
    Designable = 0x8,
    Scriptable = 0x10,
    Stored = 0x20,
    Editable = 0x40,
    User = 0x80,
    Constant = 0x100,
    Final = 0x200,
    All = Readable | Writable | Resetable |
    Designable | Scriptable | Stored |
    Editable | User | Constant | Final
};

inline int schemeFromFacing(int facing)
{
    switch(facing) {
    case Enum::FaceNorth:
        return Enum::SchemeNorth;
        break;
    case Enum::FaceEast:
        return Enum::SchemeEast;
        break;
    case Enum::FaceSouth:
        return Enum::SchemeSouth;
        break;
    case Enum::FaceWest:
        return Enum::SchemeWest;
        break;
    case (Enum::FaceNorth + Enum::FaceWest):
        return Enum::SchemeNorthWest;
        break;
    case (Enum::FaceNorth + Enum::FaceEast):
        return Enum::SchemeNorthEast;
        break;
    case (Enum::FaceSouth + Enum::FaceWest):
        return Enum::SchemeSouthWest;
        break;
    case (Enum::FaceSouth + Enum::FaceEast):
        return Enum::SchemeSouthEast;
        break;
    default:
        return -1;
        break;
    }
}

} // end namespace Qamer
//QPoint
inline bool operator<(QPoint p1, QPoint p2)
{
    if(p1.x() < p2.x()) {
        return true;
    }
    else {
        return p1.y() < p2.y();
    }
}

inline QSize sizeFromPoint(QPoint point)
{
    return QSize(point.x(), point.y());
}

//QSize
inline QPoint pointFromSize(QSize size)
{
    return QPoint(size.width(), size.height());
}

#endif // QAMER_GLOBAL_HPP
