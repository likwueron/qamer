/*
 * File name: iscreen.hpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ISCREEN_HPP
#define ISCREEN_HPP

#include <QList>
#include <QByteArray>

class QObject;

namespace Qamer {

class Screen;

namespace Plugin {

#define IScreen_IDD "org.kwueron.Qamer.Plugin.IScreen/1.0"

class IScreen
{
public:
    virtual QByteArray pluginName() const = 0;
    virtual QList<QByteArray> supportedNames() const = 0;
    virtual Screen *createScreen(const QByteArray &name,
                                  QObject *parent = 0) const = 0;
};

} // end namespace Plugin
} // end namespace Qamer

#endif // ISCREEN_HPP

