/*
 *File name: controllermonitor.hpp
 *Author: kwueron
 *E-mail: likwueron@gmail.com
 *Last revised: 2014/1/29
 *Published under Public Domain
 */

#ifndef CONTROLLERMONITOR_HPP
#define CONTROLLERMONITOR_HPP

#include <QWidget>

class ControllerMonitor : public QWidget
{
    Q_OBJECT

public:
    explicit ControllerMonitor(QWidget *parent = 0);
    ~ControllerMonitor();

    bool loadUi(const QString &fileName);
    void constructRawMonitor(int hatNum, int axisNum, int buttonNum, int trackballNum);
    void constructRawMonitor(const QString &layout);

public slots:
    void when_action_done(int actionRole, const QString &actionName, qint64 value);
protected:
    void changeEvent(QEvent *e);


private:
    QWidget *mp_ui;
};

#endif // CONTROLLERMONITOR_HPP
