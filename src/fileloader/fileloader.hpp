/*
 * File name: fileloader.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_FILELOADER_HPP
#define QAMER_FILELOADER_HPP

#include "../qamer_export.hpp"
#include "../plugin/igeneral.hpp"
#include "basetextreader.hpp"
#include <QObject>
#include <QMap>
#include <QStack>
#include <QVariantList>
#include <QByteArray>

class QFile;

namespace Qamer {

namespace Plugin {
class IGeneral;
class LoadHelper;
}
class DependInjector;
class GameObject;
class LoadingItem;

class QAMERSHARED_EXPORT FileLoader : public QObject, public BaseTextReader
{
    Q_OBJECT
    Q_PROPERTY(QByteArray context READ context WRITE setContext)
    Q_PROPERTY(bool useDICreateObject READ useDICreateObject WRITE setUseDICreateObject RESET resetDICreateObject)
    Q_PROPERTY(bool useDICreateVariant READ useDICreateVariant WRITE setUseDICreateVariant RESET resetDICreateVariant)
    Q_PROPERTY(QByteArray objectPropertyName READ objectPropertyName WRITE setObjectPropertyName)
public:
    friend class Plugin::LoadHelper;

    explicit FileLoader(QObject *parent = 0);
    ~FileLoader();

    void addEnumByMetaObject(const QByteArray &nameSpace,
                             const QMetaObject* meta);

    void setDIAdmin(DependInjector *admin, bool asParent = false);
    static void addDefaultCtorPlugin(Plugin::IGeneral *ctorObj);

    void loadFile(const QString &filename);

    //properies
    QByteArray context() const;
    void setContext(const QByteArray &c);
    bool useDICreateObject() const;
    void setUseDICreateObject(bool set);
    void resetDICreateObject();
    bool useDICreateVariant() const;
    void setUseDICreateVariant(bool set);
    void resetDICreateVariant();
    QByteArray objectPropertyName() const;
    void setObjectPropertyName(const QByteArray &name);

    //methods
    Q_INVOKABLE void loadPlugin(const QByteArray &name);
signals:

public slots:
protected:
    void while_newText_reading();

    char readCharFromFile();

    void loadAsObject(const QByteArray &objName);
    void loadAsProperty(QObject *obj, const QByteArray &name);
    void loadAsVarOFunc(QObject *obj, const QByteArray &name);
    void loadAsString();
    void loadAsEnumODouble();

    //helper functions
    inline QVariant boolODouble(const QByteArray &bytes);
    inline QVariant enumODouble(const QByteArray &bytes);
    bool isIgnoreChar(char c);

    QObject *getObject(const QString &sysName, const QByteArray &className = QByteArray());
    QObject *createObject(const QByteArray &className, const QVariantList &argv);
    QVariant createVariant(const QByteArray &className, const QVariantList &argv);
    QVariant useCtorPlugin(const QList<Plugin::IGeneral*> plugins,
                           const QByteArray &name,
                           const QVariantList &argv,
                           bool *ok = nullptr) const;
    void callFunction(QObject *obj, const QByteArray &funName, QVariantList &paras);
    bool checkNConvert(QVariantList &vList, const QList<QByteArray> &tList);

    int valueFromEnum(const QByteArray &key);
private:
    int valueFromEnum_inner(const QByteArray &key, const QMetaObject *meta);
    QMap<QByteArray, const QMetaObject*> m_metaMap;
    //first, try VarCtorSignature
    //second, try constuct qobject then access property "m_objectPropertyName"
    static QList<Plugin::IGeneral*> sm_defaultCtorList;
    QList<Plugin::IGeneral*> m_ctorList;
    DependInjector *up_di;
    bool m_asDIParent;

    bool m_useDICreateObject;//default true
    bool m_useDICreateVariant;//default false
    QByteArray m_objectPropertyName;//use to convert object to variant

    QStack<LoadingItem*> m_loadingStack;
    char m_loadEnum;
};

QVariant FileLoader::boolODouble(const QByteArray &bytes)
{
    if(bytes == "true") return true;
    else if(bytes == "false") return false;
    else return bytes.toDouble();
}

QVariant FileLoader::enumODouble(const QByteArray &bytes)
{
    bool isDouble = false;
    double d = bytes.toDouble(&isDouble);
    if(isDouble) return d;
    else return valueFromEnum(bytes);
}

} // end namespace Qamer

#endif // QAMER_FILELOADER_HPP
