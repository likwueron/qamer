/*
 * File name: inordertopostorder.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "inordertopostorder.hpp"

namespace Qamer {

QStack<char> InorderToPostorder::sm_opStack;

ParaList InorderToPostorder::fromString(const QString &str)
{
    ParaList result;

    //bool minusSign = false;

    int operandS = -1;

    int count = str.count();
    for(int i = 0; i < count; i++) {
        char cur = str.at(i).toLatin1();
        if(cur == '(' ||
           cur == '+' || cur == '-' || cur == '*' || cur == '/' ||
           cur == ')') {
            if(operandS >= 0) {
                result << operandFromString(str, operandS, i);
                operandS = -1;
            }

            if(cur == '(') {
                //minusSign = true;
                sm_opStack.push(cur);
            }
            else if(cur == '+' || cur == '-' || cur == '*' || cur == '/') {
                //if(minusSign && cur == '-') minusSign = false;//-4 view as (0-4)
                //minusSign = true;
                if(!sm_opStack.isEmpty()) {
                    while(opPriority(cur) <= opPriority(sm_opStack.top())) {
                        result << sm_opStack.pop();
                    }
                }
                sm_opStack.push(cur);
            }
            else if(cur == ')') {
                while(sm_opStack.top() != '(') {
                    result << sm_opStack.pop();
                }
                sm_opStack.pop();
            }
        }
        else {
            if(operandS < 0) {
                operandS = i;
            }
        }
    }

    //ending
    if(operandS >= 0) {
        result << operandFromString(str, operandS, -1);
    }
    while(!sm_opStack.isEmpty()) {
        result << sm_opStack.pop();
    }

    return result;
}

InorderToPostorder::InorderToPostorder()
{
}

int InorderToPostorder::opPriority(const QChar &op)
{
    if(op == '+' || op == '-')  return 1;
    else if(op == '*' || op == '/')  return 2;
    else return 0;
}

Parameter InorderToPostorder::operandFromString(const QString &src, int start, int end)
{
    QString temp = src.mid(start, end-start);
    bool ok = false;
    int tempInt = temp.toInt(&ok);

    if(ok)  return tempInt;
    else return temp.toLatin1();
}

} // end namespace Qamer
