/*
 *File name: trackballbox.cpp
 *Author: kwueron
 *E-mail: %EMAIL%
 *Last revised: 2014/1/30
 *Published under Public Domain
 */

#include "trackballbox.hpp"
#include <limits>
#include <QSpinBox>
#include <QVBoxLayout>

TrackballBox::TrackballBox(QWidget *parent) :
    QGroupBox(parent)
{
    QVBoxLayout *p_mainLayout = new QVBoxLayout;

    QSpinBox *p_a = new QSpinBox;
    p_a->setObjectName("A");
    p_a->setMaximum(std::numeric_limits<qint32>::max());
    p_a->setMinimum(std::numeric_limits<qint32>::min());
    p_a->setValue(0);
    p_mainLayout->addWidget(p_a);

    QSpinBox *p_b = new QSpinBox;
    p_b->setObjectName("B");
    p_b->setMaximum(std::numeric_limits<qint32>::max());
    p_b->setMinimum(std::numeric_limits<qint32>::min());
    p_mainLayout->addWidget(p_b);

    setLayout(p_mainLayout);
}
