/*
 * File name: gameobjectiditerator.hpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GAMEOBJECTIDITERATOR_HPP
#define GAMEOBJECTIDITERATOR_HPP

#include "qamer_export.hpp"
#include "gameobject.hpp"

namespace Qamer {

class QAMERSHARED_EXPORT GameObjectIdIterator
{
public:
    GameObjectIdIterator(int type = Enum::ObjectAll);
    GameObjectIdIterator(const QList<qObjectId> &idList);
    GameObjectIdIterator(const QList<GameObjectId> &idList);
    GameObjectIdIterator(const QList<const GameObject*> &objList);
    GameObjectIdIterator(const GameObjectIdIterator &that);
    ~GameObjectIdIterator();

    bool hasNext() const;
    GameObjectId next();
    bool hasPrevious() const;
    GameObjectId previous();

    GameObjectId peekNext() const;
    GameObjectId peekPrevious() const;

    void toFront();
    void toBack();

protected:

private:
    QList<const GameObject*> m_list;
    int m_index;
};

} // end namespace Qamer

Q_DECLARE_METATYPE(Qamer::GameObjectIdIterator)

#endif // GAMEOBJECTIDITERATOR_HPP
