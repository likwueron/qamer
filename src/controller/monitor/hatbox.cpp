/*
 *File name: hatbox.cpp
 *Author: kwueron
 *E-mail: likwueron@gmail.com
 *Last revised: 2014/1/30
 *Published under Public Domain
 */

#include "hatbox.hpp"
#include <QGridLayout>
#include <QRadioButton>

HatBox::HatBox(QWidget *parent) :
    QGroupBox(parent)
{
    QRadioButton *p_hatLeftUp = new QRadioButton(this);
    p_hatLeftUp->setObjectName("Hat9");
    QRadioButton *p_hatUp = new QRadioButton(this);
    p_hatUp->setObjectName("Hat1");
    QRadioButton *p_hatRightUp = new QRadioButton(this);
    p_hatRightUp->setObjectName("Hat3");
    QRadioButton *p_hatLeft = new QRadioButton(this);
    p_hatLeft->setObjectName("Hat8");
    QRadioButton *p_hatCentered = new QRadioButton(this);
    p_hatCentered->setObjectName("Hat0");
    QRadioButton *p_hatRight = new QRadioButton(this);
    p_hatRight->setObjectName("Hat2");
    QRadioButton *p_hatLeftDown = new QRadioButton(this);
    p_hatLeftDown->setObjectName("Hat12");
    QRadioButton *p_hatDown = new QRadioButton(this);
    p_hatDown->setObjectName("Hat4");
    QRadioButton *p_hatRightDown = new QRadioButton(this);
    p_hatRightDown->setObjectName("Hat6");

    QGridLayout *p_mainlayout = new QGridLayout(this);
    p_mainlayout->addWidget(p_hatLeftUp, 0, 0);
    p_mainlayout->addWidget(p_hatUp, 0, 1);
    p_mainlayout->addWidget(p_hatRightUp, 0, 2);
    p_mainlayout->addWidget(p_hatLeft, 1, 0);
    p_mainlayout->addWidget(p_hatCentered, 1, 1);
    p_mainlayout->addWidget(p_hatRight, 1, 2);
    p_mainlayout->addWidget(p_hatLeftDown, 2, 0);
    p_mainlayout->addWidget(p_hatDown, 2, 1);
    p_mainlayout->addWidget(p_hatRightDown, 2, 2);

    setLayout(p_mainlayout);

    p_hatCentered->setChecked(true);
}
