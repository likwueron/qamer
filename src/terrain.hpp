/*
 * File name: terrain.hpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef QAMER_TERRAIN_HPP
#define QAMER_TERRAIN_HPP

#include "sceneplacableobject.hpp"

namespace Qamer {

class TerrainPrivate;

class QAMERSHARED_EXPORT Terrain : public ScenePlacableObject
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(Terrain)
public:
    Q_INVOKABLE explicit Terrain(QObject *parent = 0);
    ~Terrain();
    //give up sceneId and sceneIndex
    bool multiPlace() const;
    void painted(QPainter *painter, int x, int y);
protected:
    Terrain(const Terrain &that);
private:
};

} // namespace Qamer

#endif // QAMER_TERRAIN_HPP
