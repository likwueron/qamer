/*
 * File name:
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "gameui.hpp"
#include "_p/gameui_p.hpp"

namespace Qamer {

GameUI::GameUI(QObject *parent)
    : GameObject(new GameUIPrivate, Enum::ObjectUserInterface, parent)
{
    setup();
}

GameUI::~GameUI()
{

}

QFont GameUI::font() const
{
    Q_D(const GameUI);
    return d->m_font;
}

void GameUI::setFont(const QFont &font)
{
    Q_D(GameUI);
    d->m_font = font;
}

void GameUI::resetFont()
{
    Q_D(GameUI);
    d->resetFont();
}

Qt::Orientation GameUI::orientation() const
{
    Q_D(const GameUI);
    return d->m_orientation;
}

void GameUI::setOrientation(Qt::Orientation orientation)
{
    Q_D(GameUI);
    d->m_orientation = orientation;
    emit orientation_changed();
}

void GameUI::when_orientation_changed()
{

}

GameUI::GameUI(const GameUI &that)
    : GameObject(new GameUIPrivate(*(that.d_func())), Enum::ObjectCopy, that.parent())
{
    setup();
}

void GameUI::setup()
{
    connect(this, SIGNAL(orientation_changed()), SLOT(when_orientation_changed()));
}

} // namespace Qamer

