/*
 * File name: gameobject_p.hpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_GAMEOBJECTPRIVATE_HPP
#define QAMER_GAMEOBJECTPRIVATE_HPP

#include "../qamer_export.hpp"
#include "../objectinfo.hpp"
#include "../spritesheet.hpp"
#include "../gameobject.hpp"
#include "QObjectPrivate"
#include <QPointF>
#include <QSize>

namespace Qamer {

class GameObject;
class PixmapDataPrivate;

class QAMERSHARED_EXPORT GameObjectPrivate : public QObjectPrivate
{
    Q_DECLARE_PUBLIC(GameObject)
    friend class GameObjectIdIterator;
public:
    GameObjectPrivate();
    ~GameObjectPrivate();

    //animation scheme
    virtual void addScheme(quint16 id, const QList<quint16> &list);
    virtual quint16 currentPixmapIndex() const;
    virtual quint16 currentSchemeId() const;
    virtual quint16 currentSchemeIndex() const;
    virtual quint16 setScheme(quint16 id, quint16 index = 0);
    virtual quint16 progressScheme();
    //
    virtual void addSpriteSheet(const SpriteSheet &sheet);
    //force set index
    virtual void setCurrentPixmapIndex(quint16 index);

    virtual const QList<SpriteSheet> &sheets() const;

protected:
    bool objectAt(const QPoint &at) const;
    void translated(const QPoint &delta);
    void translated(const QPointF &delta, const QPoint &stop);
    //for clone
    GameObjectPrivate(const GameObjectPrivate &that);

private:
    static int generateId();
    static QMap<int, GameObject*> sm_objectMap;
    static GameObject::ScriptableTag sm_sTag;

    QPointF m_position;
    QSize m_size;

    ObjectInfo m_infomation;

    int m_objectType;
    qObjectId m_listId;

    bool m_isVisible;
};

} // end namespace Qamer

#endif // QAMER_GAMEOBJECTPRIVATE_HPP
