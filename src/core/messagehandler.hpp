/*
 * File name: messagehandler.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_MESSAGEHANDLER_HPP
#define QAMER_MESSAGEHANDLER_HPP

#include "../qamer_export.hpp"
#include <QApplication>
#include <QWidget>

namespace Qamer {

class QAMERSHARED_EXPORT MessageHandler
{
public:
#if (QT_VERSION >= QT_VERSION_CHECK(5, 0, 0))
    friend void handleV5(QtMsgType type, const QMessageLogContext &context, const QString &msg);
#else
    friend void handleV4(QtMsgType type, const char *msg);
#endif
    MessageHandler(QWidget *window);
    ~MessageHandler();

    static void sendDebugMsg(const QString &msg);
    static void warningBox(const QString &msg);
    static void criticalBox();
    static void writeToFile(const char *fileName, const char *msg);

protected:
    static QWidget *window();
private:
    static MessageHandler *smp_instance;
    QWidget *up_window;
};

} // end namespace Qamer

#endif // QAMER_MESSAGEHANDLER_HPP
