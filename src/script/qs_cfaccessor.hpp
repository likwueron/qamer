/*
 * File name: qs_cfaccessor.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "../qamer_export.hpp"
#include <QScriptValue>
#include <QByteArray>

#ifndef QAMER_QSCRIPT_CFACCESSOR_HPP
#define QAMER_QSCRIPT_CFACCESSOR_HPP

class QScriptContext;
class QScriptEngine;

namespace Qamer {
namespace QScript {
QAMERSHARED_EXPORT QScriptValue cf_getDamage(QScriptContext *ctx, QScriptEngine *eng);

QScriptValue* getSfFinder(const QByteArray& name, QScriptEngine *eng);
} // end namespace QScript
} // end namespace Qamer

#endif // QAMER_QSCRIPT_CFACCESSOR_HPP
