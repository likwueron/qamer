/*
 * File name: finder.cpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "finder.hpp"
#include "../gamescene.hpp"

namespace Qamer {
namespace TileFinder {

Finder::Finder()
    : cup_scene(nullptr)
{

}

Finder::~Finder()
{

}

Finder::Finder(const Finder &that, const GameScene *scene)
    : cup_scene(scene)
{
    Q_UNUSED(that)
}

const GameScene *Finder::currentScene() const
{
    return cup_scene;
}

bool Finder::isPointValid(const QPoint &point) const
{
    if(!cup_scene) return false;
    int x = point.x();
    int y = point.y();

    bool withinMap = (0 <= x) &&
            (x < cup_scene->sceneCol()) &&
            (0 <= y) &&
            (y < cup_scene->sceneRow());

    return withinMap && cup_scene->canPass(point);
}

} // namespace TileFinder
} // namespace Qamer

