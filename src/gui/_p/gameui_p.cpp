/*
 * File name: gameui_p.cpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "gameui_p.hpp"

namespace Qamer {

const QFont GameUIPrivate::defaultFont;

GameUIPrivate::GameUIPrivate()
    : GameObjectPrivate(),
      m_font(defaultFont),
      m_orientation(Qt::Horizontal)
{

}

GameUIPrivate::~GameUIPrivate()
{

}

void GameUIPrivate::resetFont()
{
    m_font = defaultFont;
}

GameUIPrivate::GameUIPrivate(const GameUIPrivate &that)
    : GameObjectPrivate(that),
      m_font(that.m_font),
      m_orientation(that.m_orientation)
{

}

} // namespace Qamer

