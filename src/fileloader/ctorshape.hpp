/*
 * File name: ctorshape.hpp
 * Copyright 2015 kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_LOADCTOR_CTORSHAPE_HPP
#define QAMER_LOADCTOR_CTORSHAPE_HPP

#include "../qamer_export.hpp"
#include <QVariant>
#include <QVariantList>

namespace Qamer {
namespace LoadCtor {

QAMERSHARED_EXPORT QVariant Line(const QVariantList &values);
QAMERSHARED_EXPORT QVariant Polygon(const QVariantList &values);
QAMERSHARED_EXPORT QVariant Rect(const QVariantList &values);
QAMERSHARED_EXPORT QVariant Region(const QVariantList &values);

} // end namespace LoadCtor
} // namespace Qamer

#endif // QAMER_LOADCTOR_CTORSHAPE_HPP
