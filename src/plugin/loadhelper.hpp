/*
 * File name:
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_PLUGIN_LOADHELPER_HPP
#define QAMER_PLUGIN_LOADHELPER_HPP

#include "../core/pseudosingleton.hpp"
#include "igeneral.hpp"
#include <QByteArray>
#include <QMap>

class QPluginLoader;

namespace Qamer {

typedef QMap<QByteArray, VarCtorSignature> VarCtorMap;

namespace Plugin {

SINGLE(LoadHelper)

class LoadHelper : private SLoadHelper
{
public:
    LoadHelper();
    ~LoadHelper();

    void init() const;
    IGeneral *generalPlugin(const QByteArray &pluginName);

private:
    QList<IGeneral*> m_dynamicList;
};

} // namespace Plugin
} // namespace Qamer

extern Qamer::Plugin::LoadHelper *qPHelp;

#endif // QAMER_PLUGIN_LOADHELPER_HPP
