/*
 * File name: qs_general.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "qs_general.hpp"
#include <QScriptContext>
#include <QScriptEngine>
#include <QDebug>

namespace Qamer {
namespace QScript {

QScriptValue throwTypeError(QScriptContext *context, const QString &typeName)
{
    return context->throwError(QScriptContext::TypeError,
                               QString("%1: this object is not a %1")
                               .arg(typeName));
}

QScriptValue throwArgcError(QScriptContext *context, const QString &fullName, int leastArgc)
{
    return context->throwError(QScriptContext::RangeError,
                               QString("%1: need at least %2 argument(s) to run.")
                               .arg(fullName)
                               .arg(leastArgc));
}

QScriptValue throwArgvError(QScriptContext *context, const QString &fullName, const QString &typeName, int at)
{
    return context->throwError(QScriptContext::TypeError,
                               QString("%1: argument %2 is not type %3.")
                               .arg(fullName)
                               .arg(at)
                               .arg(typeName));
}

QScriptValue cf_print(QScriptContext *context, QScriptEngine *engine)
{
    QString result;
    int count = context->argumentCount();
    for (int i = 0; i < count; ++i) {
        if (i > 0) {
            result.append(" ");
        }
        QScriptValue arg = context->argument(i);
        if(arg.isNumber()) {
            result.append(QString("%1").arg(arg.toNumber()));
        }
        else {
            result.append(context->argument(i).toString());
        }
    }
    qDebug() << result;

    return engine->undefinedValue();
}

} // end namespace QScript
} // end namespace Qamer
