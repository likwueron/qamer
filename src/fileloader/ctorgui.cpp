/*
 * File name: ctorgui.cpp
 * Copyright 2015 kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "ctorgui.hpp"
#include <QBrush>
#include <QColor>
#include <QCursor>
#include <QEasingCurve>
#include <QSizePolicy>
#include <QFont>
#include <QKeysequence>
#include <QTextFormat>
#include <QTextLength>
#include <QPen>
#include <QPalette>

#include <QDebug>

namespace Qamer {
namespace LoadCtor {

QVariant Brush(const QVariantList &values)
{
    Q_UNUSED(values)
    qWarning() << "Qamer::LoadingCtor::Brush: function not implemented, return QVariant().";
    return QVariant();
}

QVariant Color(const QVariantList &values)
{
    switch(values.count()) {
    case 1: {
        return QColor((Qt::GlobalColor)
                         values.at(0).toInt());
        break;
    }
    case 3:
        //QColor(r, g, b)
        return QColor(values.at(0).toInt(),
                      values.at(1).toInt(),
                      values.at(2).toInt());
        break;
    case 4:
        //QColor(r, g, b, a)
        return QColor(values.at(0).toInt(),
                      values.at(1).toInt(),
                      values.at(2).toInt(),
                      values.at(3).toInt());
        break;
    default:
        return QColor();
    }
}

QVariant Cursor(const QVariantList &values)
{
    Q_UNUSED(values)
    qWarning() << "Qamer::LoadingCtor::Cursor: function not implemented, return QVariant().";
    return QVariant();
}

QVariant EasingCurve(const QVariantList &values)
{
    Q_UNUSED(values)
    qWarning() << "Qamer::LoadingCtor::EasingCurve: function not implemented, return QVariant().";
    return QVariant();
}

QVariant SizePolicy(const QVariantList &values)
{
    switch(values.count()) {
    case 2: {
        return QSizePolicy(
                    (QSizePolicy::Policy)
                        values.at(0).toInt()
                    ,
                    (QSizePolicy::Policy)
                        values.at(1).toInt()
                    );
        break;
    }
    case 3:
        return QSizePolicy(
                    (QSizePolicy::Policy)
                        values.at(0).toInt()
                    ,
                    (QSizePolicy::Policy)
                        values.at(1).toInt()
                    ,
                    (QSizePolicy::ControlType)
                        values.at(2).toInt()
                    );
        break;
    default:
        return QSizePolicy();
        break;
    }
}

QVariant Font(const QVariantList &values)
{
    switch(values.count()) {
    case 1:
        return QFont(values.at(0).toString());
        break;
    case 2:
        return QFont(values.at(0).toString(),
                     values.at(1).toInt());
        break;
    case 3:
        return QFont(values.at(0).toString(),
                     values.at(1).toInt(),
                     values.at(2).toInt());
        break;
    case 4:
        return QFont(values.at(0).toString(),
                     values.at(1).toInt(),
                     values.at(2).toInt(),
                     values.at(3).toBool());
        break;
    default:
        return QFont();
        break;
    }
}

QVariant Keysequence(const QVariantList &values)
{
    Q_UNUSED(values)
    qWarning() << "Qamer::LoadingCtor::Keysequence: function not implemented, return QVariant().";
    return QVariant();
}

QVariant TextFormat(const QVariantList &values)
{
    Q_UNUSED(values)
    qWarning() << "Qamer::LoadingCtor::TextFormat: function not implemented, return QVariant().";
    return QVariant();
}

QVariant TextLength(const QVariantList &values)
{
    Q_UNUSED(values)
    qWarning() << "Qamer::LoadingCtor::TextLength: function not implemented, return QVariant().";
    return QVariant();
}

QVariant Pen(const QVariantList &values)
{
    switch(values.count()) {
    case 1:
    {
        QVariant v = values.at(0);
        if(v.canConvert<QColor>()) {
            return QPen(v.value<QColor>());
        }
        else {
            return QPen((Qt::PenStyle)
                            v.toByteArray().toInt()
                        );
        }
    }
        break;

    case 2:
        return QPen(
                    values.at(0).value<QBrush>(),
                    values.at(1).toDouble());
        break;
    case 3:
        return QPen(
                    values.at(0).value<QBrush>(),
                    values.at(1).toDouble(),
                    (Qt::PenStyle)
                        values.at(2).toInt()
                    );
        break;
    case 4:
        return QPen(
                    values.at(0).value<QBrush>(),
                    values.at(1).toDouble(),
                    (Qt::PenStyle)
                        values.at(2).toInt()
                    ,
                    (Qt::PenCapStyle)
                        values.at(3).toInt()
                    );
        break;
    case 5:
        //brush, width, style, cap, join
        return QPen(
                    values.at(0).value<QBrush>(),
                    values.at(1).toDouble(),
                    (Qt::PenStyle)
                        values.at(2).toInt()
                    ,
                    (Qt::PenCapStyle)
                        values.at(3).toInt()
                    ,
                    (Qt::PenJoinStyle)
                        values.at(4).toInt()
                    );
        break;
    default:
        return QPen();
    }
}

QVariant Palette(const QVariantList &values)
{
    Q_UNUSED(values)
    qWarning() << "Qamer::LoadingCtor::Palette: function not implemented, return QVariant().";
    return QVariant();
}

} // end namespace LoadCtor
} // namespace Qamer
