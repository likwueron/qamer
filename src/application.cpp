/*
 * File name: application.cpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "qamer_types.hpp"
#include "application.hpp"
#include "controller/controllerevent.hpp"
#include "core/fixedmemorypool.hpp"
#include "plugin/loadhelper.hpp"
#include "fileloader/qmadmin.hpp"
#include "core/messagehandler.hpp"
#include <QLocale>
#include <QTextCodec>
//remember use cout rather than qDebug
#include <iostream>

namespace Qamer {

MessageHandler *Application::smp_msgHandler = nullptr;
FixedMemoryPool *Application::smp_pool = nullptr;
FixedMemoryPool *Application::smp_scenePool = nullptr;

Application::Application(int &argc, char **argv)
    : QApplication(argc, argv)
{
#if (QT_VERSION <= QT_VERSION_CHECK(5, 0, 0))
    //confer that utf-8 string is correct from source files
    //must call before QmAdmin
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
#endif
    qLang = new QmAdmin();

    qPHelp = new Plugin::LoadHelper();
}

Application::~Application()
{
    if(smp_pool) {
        delete smp_pool;
        smp_pool = nullptr;
    }
    if(smp_scenePool) {
        //crash?
        delete smp_scenePool;
        smp_scenePool = nullptr;
    }

    directMessage();

    delete qLang;

    delete qPHelp;
}

void Application::readTranslatorConfig(const QString &file)
{
    qLang->readConfig(file);
    qLang->setLang(QLocale::system().name());
}

void Application::setTrLocale(const QString &locale)
{
    qLang->setLang(locale);
}

void Application::redirectMessage(QWidget &widget)
{
    smp_msgHandler = new MessageHandler(&widget);
}

void Application::directMessage()
{
    if(smp_msgHandler) {
        delete smp_msgHandler;
        smp_msgHandler = 0x0;
    }
}

bool Application::event(QEvent *event)
{
    return QApplication::event(event);
}

void *Application::mainAlloc(size_t size)
{
    if(!smp_pool) qApp->setMemBlk(&smp_pool, 1024, 256);
    return alloc(&smp_pool, size);
}

void Application::mainFree(void *p, size_t size)
{
    free(&smp_pool, p, size);
}

void *Application::sceneAlloc(size_t size)
{
    //up to 5 maps in same time(5 layers each),
    //with max tiles of 400(ex: 20x20, 10x40...)
    if(!smp_scenePool) {
        qApp->setMemBlk(&smp_scenePool, 5, 5 * 400 * sizeof(qObjectId));
    }
    return alloc(&smp_scenePool, size);
}

void Application::sceneFree(void *p, size_t size)
{
    free(&smp_scenePool, p, size);
}

void Application::setMainMemBlk(int num, int size)
{
    setMemBlk(&smp_pool, num, size);
}

void Application::setSceneMemBlk(int num, int size)
{
    setMemBlk(&smp_scenePool, num, size);
}

void Application::setMemBlk(FixedMemoryPool **pool, int num, int size)
{
    if(*pool) {
        if(!(*pool)->reset(num, size)) {
            std::cout << "Memory Pool refuse to reset because it is in used";
            std::fflush(stdout);
        }
    }
    else (*pool) = new FixedMemoryPool(num, size, true);
}

void *Application::alloc(FixedMemoryPool **pool, size_t size)
{
    if((*pool)->blockSize() < size) {
        return new char[size];
    }
    else return (*pool)->allocate();
}

void Application::free(FixedMemoryPool **pool, void *p, size_t size)
{
    if(!(*pool) || ((*pool)->blockSize() < size)) {
        delete [] (char*)(p);
    }
    else (*pool)->free(p);
}

} // namespace Qamer

