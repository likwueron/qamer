/*
 * File name: ctordatetime.cpp
 * Copyright 2015 kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "ctordatetime.hpp"
#include <QTime>
#include <QDate>
#include <QDateTime>

namespace Qamer {
namespace LoadCtor {

QVariant Time(const QVariantList &values)
{
    switch(values.count()) {
    case 2:
        return QTime(values.at(0).toInt(),
                     values.at(1).toInt());
        break;
    case 3:
        return QTime(values.at(0).toInt(),
                     values.at(1).toInt(),
                     values.at(2).toInt());
        break;
    case 4:
        return QTime(values.at(0).toInt(),
                     values.at(1).toInt(),
                     values.at(2).toInt(),
                     values.at(3).toInt());
        break;
    default:
        return QTime();
        break;
    }
}

QVariant Date(const QVariantList &values)
{
    switch(values.count()) {
    case 3:
        return QDate(values.at(0).toInt(),
                     values.at(1).toInt(),
                     values.at(2).toInt());
        break;
    default:
        return QDate();
        break;
    }
}

QVariant DateTime(const QVariantList &values)
{
    switch(values.count()) {
    case 1:
        return QDateTime(values.at(0).toDate());
        break;
    case 2:
        return QDateTime(values.at(0).toDate(),
                         values.at(1).toTime());
        break;
    case 3:
        return QDateTime(values.at(0).toDate(),
                         values.at(1).toTime(),
                         (Qt::TimeSpec)
                             values.at(2).toInt()
                         );
        break;
    default:
        return QDateTime();
        break;
    }
}

} // end namespace LoadCtor
} // namespace Qamer
