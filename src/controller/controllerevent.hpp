/*
 * File name: controllerevent.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_CONTROLLEREVENT_HPP
#define QAMER_CONTROLLEREVENT_HPP

#include "../qamer_export.hpp"
#include <QEvent>
#ifndef USE_QBYTEARRAY
#include <QString>
#else
#include <QByteArray>
#endif

namespace Qamer {

class QAMERSHARED_EXPORT ControllerEvent : public QEvent
{
public:
    static const QEvent::Type controllerEventType;
#ifndef USE_QBYTEARRAY
    ControllerEvent(int id, int role, const QString &name, qint64 value);
#else
    ControllerEvent(int id, int role, const QByteArray &name, qint64 value);
#endif
    ~ControllerEvent();

    int id() const;
    int role() const;
#ifndef USE_QBYTEARRAY
    QString name() const;
#else
    QByteArray name() const;
#endif
    qint64 value() const;

private:
    int m_id;
    int m_role;
#ifndef USE_QBYTEARRAY
    QString m_name;
#else
    QByteArray m_name;
#endif
    qint64 m_value;
};

} // end namespace Qamer

#endif // QAMER_CONTROLLEREVENT_HPP
