/*
 * File name:
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "loadhelper.hpp"
#include "igeneral.hpp"
#include "../fileloader/fileloader.hpp"
#include <QtPlugin>
#include <QPluginLoader>
#include <QDir>
#include <QCoreApplication>
#include "../mics/platform.hpp"
#include "bastardstringctor.hpp"
#include <QDebug>

#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
Q_IMPORT_PLUGIN(GeometryCtorPlugin)
Q_IMPORT_PLUGIN(DrawCtorPlugin)
Q_IMPORT_PLUGIN(ImageCtorPlugin)
#else
Q_IMPORT_PLUGIN(GeometryCtor)
Q_IMPORT_PLUGIN(DrawCtor)
Q_IMPORT_PLUGIN(ImageCtor)
#endif

Qamer::Plugin::LoadHelper *qPHelp = nullptr;

namespace Qamer {
namespace Plugin {

SINGLE_CPP(LoadHelper)

LoadHelper::LoadHelper()
    : SLoadHelper()
{
    init();
}

LoadHelper::~LoadHelper()
{

}

void LoadHelper::init() const
{
    BastardStringCtor *bsc = new BastardStringCtor(qApp);
    FileLoader::sm_defaultCtorList.append(bsc);
    foreach(QObject *plugin, QPluginLoader::staticInstances()) {
        auto ctorObj = qobject_cast<IGeneral*>(plugin);
        FileLoader::sm_defaultCtorList.append(ctorObj);
    }
}

IGeneral* LoadHelper::generalPlugin(const QByteArray &pluginName)
{
    foreach(IGeneral *g, m_dynamicList) {
        if(g->pluginName() == pluginName) {
            return g;
        }
    }

    //first time load
#ifndef QT_DEBUG
    QDir dir(qApp->applicationDirPath());
#else
    QDir dir(QDir::currentPath());
#endif
    dir.cd("qamerPlugin");
    QString _pluginName = pluginName + platform_librarySuffix();
    foreach(const QString &fileName, dir.entryList(QDir::Files)) {
        if(fileName == _pluginName) {
            QPluginLoader loader(dir.absoluteFilePath(fileName));
            IGeneral *plugin = qobject_cast<IGeneral*>(loader.instance());
            if(plugin) {
                m_dynamicList << plugin;
                return plugin;
            }
            else break;
        }
    }
    return nullptr;
}

} // namespace Plugin
} // namespace Qamer

