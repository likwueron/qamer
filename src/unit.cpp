/*
 * File name: unit.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "unit.hpp"
#include "_p/unit_p.hpp"
#include "statvalue.hpp"
#include "gamescene.hpp"
#include <QPainter>
#include <QDebug>

namespace Qamer {

Unit::Unit(QObject *parent)
    : ScenePlacableObject(new UnitPrivate, Enum::ObjectUnit, parent)
{

}

Unit::~Unit()
{

}

qUnitSide Unit::unitSide() const
{
    Q_D(const Unit);
    return d->m_side;
}

void Unit::setUnitSide(qUnitSide unitside)
{
    Q_D(Unit);
    d->m_side = unitside;
}

int Unit::movepoint() const
{
    Q_D(const Unit);
    return d->m_movepoint;
}

void Unit::setMovepoint(int movepoint)
{
    Q_D(Unit);
    d->m_movepoint = movepoint;
}

int Unit::statValueCount() const
{
    Q_D(const Unit);
    return d->m_statValueMap.count();
}

bool Unit::hasStatValue(const QByteArray &name) const
{
    Q_D(const Unit);
    return d->m_statValueMap.contains(name);
}

int Unit::addStatValue(const QByteArray &name, int max, int min, int flags, int val)
{
    //is static property?
    if(isStaticProperty(name)) return CF_DynamicError;
    //replace property
    setProperty(name, QVariant());

    StatValue b;
    b.maxValue = max;
    b.minValue = min;
    if(max < min) qSwap(b.maxValue, b.minValue);

    b.flags = flags;
    switch(flags) {
    case Enum::StatValueZeroAtBegin:
        if(b.maxValue < 0) b.beginValue = b.maxValue;
        else if(0 < b.minValue) b.beginValue = b.minValue;
        else b.beginValue = 0;
        break;
    case Enum::StatValueMaxAtBegin:
        b.beginValue = b.maxValue;
        break;
    case Enum::StatValueMinAtBegin:
        b.beginValue = b.minValue;
        break;
    case Enum::StatValueSpecificAtBegin:
    default:
        if(b.maxValue < val) b.beginValue = b.maxValue;
        else if(val < b.minValue) b.beginValue = b.minValue;
        else b.beginValue = val;
        break;
    }

    b.currentValue = b.beginValue;

    Q_D(Unit);
    d->m_statValueMap.insert(name, b);

    return CF_NoError;
}

int Unit::statCurrentValue(const QByteArray &name) const
{
    Q_D(const Unit);
    return d->statValue(name).currentValue;
}

int Unit::statMaxValue(const QByteArray &name) const
{
    Q_D(const Unit);
    return d->m_statValueMap.value(name).maxValue;
}

int Unit::statMinValue(const QByteArray &name) const
{
    Q_D(const Unit);
    return d->m_statValueMap.value(name).minValue;
}

bool Unit::increaseStatValue(const QByteArray &name, int val)
{
    Q_D(Unit);
    if(d->m_statValueMap.contains(name)) {
        int result = d->m_statValueMap[name].safeInc(val);
        emit statValue_changed(name, Enum::StatValueCurrent, result);
        return true;
    }
    return false;
}

void Unit::resetStatValue(const QByteArray &name)
{
    Q_D(Unit);
    if(d->m_statValueMap.contains(name)) {
        int result = d->m_statValueMap[name].reset();
        emit statValue_changed(name, Enum::StatValueCurrent, result);
    }
}

void Unit::setStatCurrentValue(const QByteArray &name, int cur)
{
    Q_D(Unit);
    if(d->m_statValueMap.contains(name)) {
        StatValue stat = d->m_statValueMap.value(name);
        if(stat.minValue <= cur && cur <= stat.maxValue) {
            d->m_statValueMap[name].currentValue = cur;
            emit statValue_changed(name, Enum::StatValueCurrent, cur);
        }
    }
}

void Unit::setStatMaxValue(const QByteArray &name, int max)
{
    Q_D(Unit);
    if(d->m_statValueMap.contains(name)) {
        //bool changed[3] = {0, 0, 0};
        StatValue v = d->m_statValueMap.value(name);

        if(max < v.beginValue) {
            v.beginValue = max;
            //changed[0] = true;
        }
        if(max < v.currentValue) {
            v.currentValue = max;
            //changed[1] = true;
        }
        if(max < v.minValue) {
            v.minValue = max;
            //changed[2] = true;
        }

        d->m_statValueMap[name].maxValue = max;
        emit statValue_changed(name, Enum::StatValueMax, max);
    }
}

void Unit::setStatMinValue(const QByteArray &name, int min)
{
    Q_D(Unit);
    if(d->m_statValueMap.contains(name)) {
        d->m_statValueMap[name].minValue = min;
        emit statValue_changed(name, Enum::StatValueMin, min);
    }
}

int Unit::facing() const
{
    Q_D(const Unit);
    return d->m_facing;
}

void Unit::setFacing(int facing)
{
    Q_D(Unit);
    d->m_facing = Enum::FaceDirection(facing);
    d->setScheme(schemeFromFacing(d->m_facing));

    emit facing_changed();
}

bool Unit::unique() const
{
    Q_D(const Unit);
    return d->m_unique;
}

void Unit::setUnique(bool yes)
{
    Q_D(Unit);
    d->m_unique = yes;
}

void Unit::addSkill(qObjectId id)
{
    if(objectTypeFromId(id) & int(Enum::ObjectSkill)) {
        Q_D(Unit);
        d->m_skillIdList << id;
        emit skill_added();
    }
}

QVariantList Unit::skillList(int filter) const
{
    Q_D(const Unit);
    QVariantList result;
    result.reserve(d->m_skillIdList.count());
    foreach(const qObjectId &id, d->m_skillIdList) {
        if(objectTypeFromId(id) & filter) {
            GameObjectId goid(id);
            result << QVariant::fromValue(goid);
        }
    }
    return result;
}

QList<qObjectId> Unit::skillIdList(int filter) const
{
    Q_D(const Unit);
    if(filter == Enum::ObjectSubTypeMask) return d->m_skillIdList;
    else {
        QList<qObjectId> result;
        result.reserve(d->m_skillIdList.count());
        foreach(const qObjectId &id, d->m_skillIdList) {
            if(objectTypeFromId(id) & filter) {
                result << id;
            }
        }
        return result;
    }
}

int Unit::resistance(int id) const
{
    Q_D(const Unit);
    return d->m_resisMap.value(id, 0);
}

void Unit::setResistance(int id, int val)
{
    Q_D(Unit);
    d->m_resisMap.insert(id, val);
}

void Unit::painted(QPainter *painter, const QRect &updateRect)
{
    Q_UNUSED(updateRect)
    ScenePlacableObject::painted(painter, updateRect);
}

GameObject *Unit::clone() const
{
    if(isClonable() && !unique()) return new Unit(*this);
    else return nullptr;
}

bool Unit::isClonable() const
{
    return true;
}

void Unit::updatePosition()
{
    ScenePlacableObject::updatePosition();
}

Unit::Unit(UnitPrivate *pdd, QObject *parent)
    : ScenePlacableObject(pdd, Enum::ObjectUnit, parent)
{

}

Unit::Unit(const Unit &that)
    : ScenePlacableObject(new UnitPrivate(*(that.d_func())), Enum::ObjectCopy, that.parent())
{

}
 
} // end namespace Qamer
