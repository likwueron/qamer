/*
 * File name: luagameobject.cpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "luagameobject.hpp"
#include "../gameobjectiditerator.hpp"

#include "../gamescene.hpp"
#include "../unit.hpp"
#include "../skill.hpp"

#include <QtLua/String>
#include <QtLua/State>

namespace Qamer {

QMap<int, QString> LuaGameObject::sm_typeTextMap;

LuaGameObject::LuaGameObject(QObject *parent)
    : QObject(parent)
{

}

LuaGameObject::LuaGameObject(int id)
    : QObject(nullptr)
{
    setId(id);
}

LuaGameObject::LuaGameObject(const QString &name)
    : QObject(nullptr)
{
    setId(name);
}

LuaGameObject::~LuaGameObject()
{

}

void LuaGameObject::addTypeText(int type, const QString &text)
{
    sm_typeTextMap.insert(type, text);
}

void LuaGameObject::addSubTypeText(int type, int subType, const QString &text)
{
    QString mainText = sm_typeTextMap.value(type);
    if(!mainText.isEmpty()) {
        sm_typeTextMap.insert(type | subType, mainText.append('-').append(text));
    }
}

int LuaGameObject::objectId()
{
    return id();
}

void LuaGameObject::setObjectId(int i)
{
    setId(i);
    emit idChanged();
}

QString LuaGameObject::systemName() const
{
    auto o = object();
    if(o) return o->objectName();
    else return QString();
}

void LuaGameObject::setSystemName(const QString &name)
{
    setId(name);
    emit idChanged();
}

int LuaGameObject::objectType() const
{
    auto o = object();
    if(o) return o->objectType();
    else {
        QTLUA_THROW(Qamer::GameObject, "Invalid GameObject.");
        return Enum::ObjectUndefined;
    }
}

int LuaGameObject::subObjectType() const
{
    auto o = object();
    if(o) return o->subObjectType();
    else {
        QTLUA_THROW(Qamer::GameObject, "Invalid GameObject.");
        return Enum::ObjectUndefined;
    }
}

void LuaGameObject::setSubObjectType(int subObjectType)
{
    auto o = object();
    if(o) o->setSubObjectType(subObjectType);
    else {
        QTLUA_THROW(Qamer::GameObject, "Invalid GameObject.");
    }
}

bool LuaGameObject::isObjectType(int type)
{
    auto o = object();
    if(o) return o->isObjectType(type);
    else {
        QTLUA_THROW(Qamer::GameObject, "Invalid GameObject.");
        return false;
    }
}

QString LuaGameObject::name() const
{
    auto o = object();
    if(o) return o->name();
    else {
        QTLUA_THROW(Qamer::GameObject, "Invalid GameObject.");
        return QString();
    }
}

void LuaGameObject::setName(const QString &name)
{
    auto o = object();
    if(o) o->setName(name);
    else {
        QTLUA_THROW(Qamer::GameObject, "Invalid GameObject.");
    }
}

QString LuaGameObject::description() const
{
    auto o = object();
    if(o) return o->description();
    else {
        QTLUA_THROW(Qamer::GameObject, "Invalid GameObject.");
        return QString();
    }
}

void LuaGameObject::setDescription(const QString &des)
{
    auto o = object();
    if(o) o->setDescription(des);
    else {
        QTLUA_THROW(Qamer::GameObject, "Invalid GameObject.");
    }
}

QPointF LuaGameObject::position() const
{
    auto o = object();
    if(o) return o->position();
    else {
        QTLUA_THROW(Qamer::GameObject, "Invalid GameObject.");
        return QPointF();
    }
}

void LuaGameObject::move(QPointF position, Enum::MoveMode mode)
{
    auto o = object();
    if(o) o->move(position.toPoint(), mode);
    else {
        QTLUA_THROW(Qamer::GameObject, "Invalid GameObject.");
    }
}

QSizeF LuaGameObject::size() const
{
    auto o = object();
    if(o) return o->size();
    else {
        QTLUA_THROW(Qamer::GameObject, "Invalid GameObject.");
        return QSizeF();
    }
}

void LuaGameObject::resize(QSizeF size)
{
    auto o = object();
    if(o) o->resize(size.toSize());
    else {
        QTLUA_THROW(Qamer::GameObject, "Invalid GameObject.");
    }
}

QSizeF LuaGameObject::parentSize() const
{
    auto o = object();
    if(o) return o->parentSize();
    else {
        QTLUA_THROW(Qamer::GameObject, "Invalid GameObject.");
        return QSizeF();
    }
}

bool LuaGameObject::isVisible() const
{
    auto o = object();
    if(o) return o->isVisible();
    else {
        QTLUA_THROW(Qamer::GameObject, "Invalid GameObject.");
        return false;
    }
}

void LuaGameObject::setVisible(bool visible)
{
    auto o = object();
    if(o) return o->setVisible(visible);
    else {
        QTLUA_THROW(Qamer::GameObject, "Invalid GameObject.");
    }
}

void LuaGameObject::invertVisible()
{
    auto o = object();
    if(o) o->invertVisible();
    else {
        QTLUA_THROW(Qamer::GameObject, "Invalid GameObject.");
    }
}

void LuaGameObject::addAnimationScheme(int id, const QVariantList &list)
{
    auto o = object();
    if(o) o->addAnimationScheme(id, list);
    else {
        QTLUA_THROW(Qamer::GameObject, "Invalid GameObject.");
    }
}

void LuaGameObject::setCurrentAnimationScheme(int id, int index)
{
    auto o = object();
    if(o) o->setCurrentAnimationScheme(id, index);
    else {
        QTLUA_THROW(Qamer::GameObject, "Invalid GameObject.");
    }
}

void LuaGameObject::addStatValue(const QByteArray &name, int max, int min, int flags, int val)
{
    auto o = object();
    if(o && o->isObjectType(Enum::ObjectUnit)) {
        auto u = reinterpret_cast<Unit*>(o);
        u->addStatValue(name, max, min, flags, val);
    }
    else {
        QTLUA_THROW(Qamer::Unit, "GameObject is not an Unit.");
    }
}

int LuaGameObject::statCurrentValue(const QByteArray &name) const
{
    auto o = object();
    if(o && o->isObjectType(Enum::ObjectUnit)) {
        auto u = reinterpret_cast<Unit*>(o);
        return u->statCurrentValue(name);
    }
    else {
        QTLUA_THROW(Qamer::Unit, "GameObject is not an Unit.");
        return invalidStatValue;
    }
}

int LuaGameObject::statMaxValue(const QByteArray &name) const
{
    auto o = object();
    if(o && o->isObjectType(Enum::ObjectUnit)) {
        auto u = reinterpret_cast<Unit*>(o);
        return u->statMaxValue(name);
    }
    else {
        QTLUA_THROW(Qamer::Unit, "GameObject is not an Unit.");
        return invalidStatValue;
    }
}

int LuaGameObject::statMinValue(const QByteArray &name) const
{
    auto o = object();
    if(o && o->isObjectType(Enum::ObjectUnit)) {
        auto u = reinterpret_cast<Unit*>(o);
        return u->statMinValue(name);
    }
    else {
        QTLUA_THROW(Qamer::Unit, "GameObject is not an Unit.");
        return invalidStatValue;
    }
}

bool LuaGameObject::increaseStatValue(const QByteArray &name, int val)
{
    auto o = object();
    if(o && o->isObjectType(Enum::ObjectUnit)) {
        auto u = reinterpret_cast<Unit*>(o);
        return u->increaseStatValue(name, val);
    }
    else {
        QTLUA_THROW(Qamer::Unit, "GameObject is not an Unit.");
        return false;
    }
}

void LuaGameObject::resetStatValue(const QByteArray &name)
{
    auto o = object();
    if(o && o->isObjectType(Enum::ObjectUnit)) {
        auto u = reinterpret_cast<Unit*>(o);
        u->resetStatValue(name);
    }
    else {
        QTLUA_THROW(Qamer::Unit, "GameObject is not an Unit.");
    }
}

void LuaGameObject::setStatCurrentValue(const QByteArray &name, int cur)
{
    auto o = object();
    if(o && o->isObjectType(Enum::ObjectUnit)) {
        auto u = reinterpret_cast<Unit*>(o);
        u->setStatCurrentValue(name, cur);
    }
    else {
        QTLUA_THROW(Qamer::Unit, "GameObject is not an Unit.");
    }
}

void LuaGameObject::setStatMaxValue(const QByteArray &name, int max)
{
    auto o = object();
    if(o && o->isObjectType(Enum::ObjectUnit)) {
        auto u = reinterpret_cast<Unit*>(o);
        u->setStatMaxValue(name, max);
    }
    else {
        QTLUA_THROW(Qamer::Unit, "GameObject is not an Unit.");
    }
}

void LuaGameObject::setStatMinValue(const QByteArray &name, int min)
{
    auto o = object();
    if(o && o->isObjectType(Enum::ObjectUnit)) {
        auto u = reinterpret_cast<Unit*>(o);
        u->setStatMinValue(name, min);
    }
    else {
        QTLUA_THROW(Qamer::Unit, "GameObject is not an Unit.");
    }
}

QPointF LuaGameObject::scenePoint() const
{
    auto o = object();
    if(o && o->isObjectType(Enum::ObjectUnit)) {
        auto u = reinterpret_cast<Unit*>(o);
        return u->scenePoint();
    }
    else {
        QTLUA_THROW(Qamer::Unit, "GameObject is not an Unit.");
        return QPointF();
    }
}

void LuaGameObject::setScenePoint(QPointF point)
{
    auto o = object();
    if(o && o->isObjectType(Enum::ObjectUnit)) {
        auto u = reinterpret_cast<Unit*>(o);
        u->setScenePoint(point.toPoint());
    }
    else {
        QTLUA_THROW(Qamer::Unit, "GameObject is not an Unit.");
    }
}

int LuaGameObject::facing() const
{
    auto o = object();
    if(o && o->isObjectType(Enum::ObjectUnit)) {
        auto u = reinterpret_cast<Unit*>(o);
        return u->facing();
    }
    else {
        QTLUA_THROW(Qamer::Unit, "GameObject is not an Unit.");
        return Enum::FaceNone;
    }
}

void LuaGameObject::setFacing(int facing)
{
    auto o = object();
    if(o && o->isObjectType(Enum::ObjectUnit)) {
        auto u = reinterpret_cast<Unit*>(o);
        u->setFacing(facing);
    }
    else {
        QTLUA_THROW(Qamer::Unit, "GameObject is not an Unit.");
    }
}

void LuaGameObject::addSkill(LuaGameObject *skill)
{
    auto o = object();
    if(o && o->isObjectType(Enum::ObjectUnit) &&
            skill->isObjectType(Enum::ObjectSkill)) {
        auto u = reinterpret_cast<Unit*>(o);
        u->addSkill(skill->id());
    }
    else {
        QTLUA_THROW(Qamer::Unit, "GameObject is not an Unit.");
    }
}

LuaGameObjectList LuaGameObject::skillList(int subType) const
{
    auto o = object();
    if(o && o->isObjectType(Enum::ObjectUnit)) {
        auto u = reinterpret_cast<Unit*>(o);
        QList<qObjectId> list = u->skillIdList(subType);
        LuaGameObjectList result;
        result.reserve(list.count());
        foreach(qObjectId id, list) {
            result << new LuaGameObject(id);
        }
        return result;
    }
    else {
        QTLUA_THROW(Qamer::Unit, "GameObject is not an Unit.");
    }
    return LuaGameObjectList();
}

bool LuaGameObject::pointWithinScene(QPointF point) const
{
    auto o = object();
    if(o && o->isObjectType(Enum::ObjectScene)) {
        auto s = reinterpret_cast<GameScene*>(o);
        return (0 <= point.x() && point.x() < s->sceneCol() &&
                 0 <= point.y() && point.y() < s->sceneRow());
    }
    else {
        QTLUA_THROW(Qamer::GameScene, "GameObject is not a GameScene.");
        return false;
    }
}

bool LuaGameObject::canPass(QPointF point) const
{
    auto o = object();
    if(o && o->isObjectType(Enum::ObjectScene)) {
        auto s = reinterpret_cast<GameScene*>(o);
        return s->canPass(point.toPoint());
    }
    else {
        QTLUA_THROW(Qamer::GameScene, "GameObject is not a GameScene.");
        return false;
    }
}

QByteArray LuaGameObject::possibleTilesFinderName() const
{
    auto o = object();
    if(o && o->isObjectType(Enum::ObjectSkill)) {
        auto s = reinterpret_cast<Skill*>(o);
        return s->possibleTilesFinderName();
    }
    else {
        QTLUA_THROW(Qamer::Skill, "GameObject is not a Skill.");
        return QByteArray();
    }
}

void LuaGameObject::setPossibleTilesFinderName(const QByteArray &name)
{
    auto o = object();
    if(o && o->isObjectType(Enum::ObjectSkill)) {
        auto s = reinterpret_cast<Skill*>(o);
        s->setPossibleTilesFinderName(name);
    }
    else {
        QTLUA_THROW(Qamer::Skill, "GameObject is not a Skill.");
    }
}

QByteArray LuaGameObject::effectTilesFinderName() const
{
    auto o = object();
    if(o && o->isObjectType(Enum::ObjectSkill)) {
        auto s = reinterpret_cast<Skill*>(o);
        return s->effectTilesFinderName();
    }
    else {
        QTLUA_THROW(Qamer::Skill, "GameObject is not a Skill.");
        return QByteArray();
    }
}

void LuaGameObject::setEffectTilesFinderName(const QByteArray &name)
{
    auto o = object();
    if(o && o->isObjectType(Enum::ObjectSkill)) {
        auto s = reinterpret_cast<Skill*>(o);
        s->setEffectTilesFinderName(name);
    }
    else {
        QTLUA_THROW(Qamer::Skill, "GameObject is not a Skill.");
    }
}

ParaList LuaGameObject::parameter(int type)
{
    auto o = object();
    if(o && o->isObjectType(Enum::ObjectSkill)) {
        auto s = reinterpret_cast<Skill*>(o);
        return s->parameter(type);
    }
    else {
        QTLUA_THROW(Qamer::Skill, "GameObject is not a Skill.");
        return ParaList();
    }
}

QString LuaGameObject::toString() const
{
    auto *o = object();
    if(o) {
        int type = o->objectType();
        int mainType = type & Enum::ObjectMainTypeMask;
        int subType = type & Enum::ObjectSubTypeMask;
        QString typeText = sm_typeTextMap.value(mainType);
        if(subType) {
            typeText.append('-')
                    .append(sm_typeTextMap.value(subType));
        }

        return QString("Qamer::GameObject - id: %1; type: %2; systemName: %3")
                .arg(o->objectId())
                .arg(typeText)
                .arg(o->objectName());
    }
    else return QString("Qamer::GameObject(nil)");
}

QVariant LuaGameObject::getDP(const QByteArray &name) const
{
    auto o = object();
    if(o) return o->property(name);
    else return QVariant();
}

void LuaGameObject::setDP(const QByteArray &name, const QVariant &value)
{
    auto o = object();
    if(o) o->cf_addProperty(name, value);
}

QtLua::Value::List GameObject_GameObject(QtLua::State *st, const QtLua::Value::List& argv)
{
    if(0 < argv.count()) {
        LuaGameObject *o = nullptr;
        QtLua::Value v = argv.at(0);
        switch(v.type()) {
        case QtLua::Value::TNumber:
            o = new LuaGameObject(v.to_integer());
            break;
        case QtLua::Value::TString:
            o = new LuaGameObject(v.to_qstring());
            break;
        default:
            break;
        }

        return QtLua::Value::List(QtLua::Value(st, o, true, true));
    }
    return QtLua::Value::List();
}

QtLua::Value::List GameObject_objectNumber(QtLua::State *st, const QtLua::Value::List& argv)
{
    int number;

    if(0 < argv.count()) {
        QtLua::Value v = argv.at(0);
        if(v.type() == QtLua::Value::TNumber) {
            int type = v.to_integer();
            number = GameObject::objectNumber(type);
        }
        else return QtLua::Value::List();
    }
    else number = GameObject::objectNumber();

    return QtLua::Value::List(QtLua::Value(st, number));
}

QtLua::Value::List GameObject_objectTable(QtLua::State *st, const QtLua::Value::List& argv)
{
    GameObjectIdIterator *p_i;

    if(0 < argv.count()) {
        QtLua::Value v = argv.at(0);
        if(v.type() == QtLua::Value::TNumber) {
            int type = v.to_integer();
            p_i = new GameObjectIdIterator(type);
        }
        else return QtLua::Value::List();
    }
    else p_i = new GameObjectIdIterator;

    QtLua::Value table = QtLua::Value::new_table(st);
    int i = 1;
    while(p_i->hasNext()) {
        auto oid = p_i->next();
        table[i++] = QtLua::Value(st, new LuaGameObject(oid.id()), true, true);
    }
    delete p_i;

    return QtLua::Value::List(table);
}

} // end namespace Qamer
