/*
 * File name: gamescene.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef QAMER_GAMESCENE_HPP
#define QAMER_GAMESCENE_HPP

#include "qamer_global.hpp"
#include "gameobject.hpp"
#include <QPoint>
#include <QMap>

namespace Qamer {

class GameScenePrivate;

class ScenePlacableObject;

class QAMERSHARED_EXPORT GameScene : public GameObject
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(GameScene)
    Q_PROPERTY(QSize sceneSize READ sceneSize WRITE setSceneSize SCRIPTABLE loadable)
    Q_PROPERTY(int sceneLength READ sceneLength STORED false)
    Q_PROPERTY(QSize tileSize READ tileSize WRITE setTileSize SCRIPTABLE loadable)
    Q_PROPERTY(QPoint cursorPoint READ cursorPoint SCRIPTABLE false)
public:
    friend class IndirectAccessor;

    static const int defaultTileWidth = 64;
    static const int defaultTileHeight = 32;
    static const int defaultCharTallPara = 3;

    explicit GameScene(QObject *parent = 0);
    ~GameScene();

    static GameScene *currentScene();
    static void setCurrentScene(GameScene *scene);

    static void setLayerOrder(const QList<int> &list);
    //map size
    int sceneCol() const;
    inline void setSceneCol(int col);
    int sceneRow() const;
    inline void setSceneRow(int row);
    QSize sceneSize() const;
    void setSceneSize(QSize size);
    int sceneLength() const;
    //tile size
    int tileWidth() const;
    void setTileWidth(int width);
    int tileHeight() const;
    void setTileHeight(int height);
    QSize tileSize() const;
    void setTileSize(QSize size);
    //map cursor
    QPoint cursorPoint() const;
    void setCursorPoint(int col, int row);
    inline void setCursorPoint(QPoint point);
    //CONVERSION
    //both side are about scene coordinate
    int indexFromPoint(int x, int y) const;
    inline int indexFromPoint(QPoint point) const;
    QPoint pointFromIndex(int i) const;
    //one for draw, one for scene
    inline QPoint sceneCoorFromDrawCoor(int x, int y) const;
    inline QPoint sceneCoorFromDrawCoor(QPoint point) const;
    inline QPoint drawCoorFromSceneCoor(int index) const;
    inline QPoint drawCoorFromSceneCoor(int col, int row) const;
    inline QPoint drawCoorFromSceneCoor(QPoint point) const;
    //LAYER
    virtual int layerNumber() const;
    int layerObjectCount(Enum::SceneLayer layerId) const;

    bool isObjectFacing(Enum::SceneLayer layerId, qObjectId id, int x, int y);
    inline bool isObjectFacing(Enum::SceneLayer layerId, qObjectId id, QPoint at);

    Q_INVOKABLE void setLayer(Enum::SceneLayer layerId, const QVariantList &list);
    const qObjectId *layer(Enum::SceneLayer layerId) const;
    Q_INVOKABLE inline bool placeObjectAt(Enum::SceneLayer layerId,
                                          qObjectId id, int x, int y);
    Q_INVOKABLE inline bool placeObjectAt(Enum::SceneLayer layerId,
                                          qObjectId id, QPoint at);
    Q_INVOKABLE inline bool moveObjectTo(Enum::SceneLayer layerId,
                                         qObjectId id, int x, int y);
    Q_INVOKABLE inline bool moveObjectTo(Enum::SceneLayer layerId,
                                         qObjectId id, QPoint to);
    Q_INVOKABLE inline bool removeObjectOn(Enum::SceneLayer layerId,
                                           int x, int y);
    Q_INVOKABLE inline bool removeObjectOn(Enum::SceneLayer layerId,
                                           QPoint at);
    //unit layer
    QList<qObjectId> unitIdList() const;
    qObjectId unitIdAt(int x, int y) const;
    inline qObjectId unitIdAt(QPoint point) const;
    inline qObjectId unitIdUnderCursor() const;
    //cursor's direction from unitPoint
    virtual Enum::FaceDirection unitDirection(QPoint unitPoint) const = 0;

    QList<QPoint> unitFindPath(const QByteArray& name,
                               QPoint originalPoint,
                               int movepoint,
                               int direction) const;
    virtual int canPass(QPoint point) const = 0;
    //end unit layer
    //under layer

    //end under layer

    virtual void painted(QPainter *painter, const QRect &updateRect) = 0;
public slots:
    void when_sceneMove_required(qObjectId id, int i, int oi);
signals:
    void object_added(int layerId, qObjectId id);
    void object_removed(int layerId, qObjectId id);
protected:
    GameScene(GameScenePrivate *pdd, QObject *parent);

    virtual QPoint sceneCoorFromDrawCoor_inner(int x, int y) const = 0;
    virtual QPoint drawCoorFromSceneCoor_inner(int col, int row) const = 0;
    //LAYER
    virtual qSceneLayer layerFromId(Enum::SceneLayer id) const;

    Enum::FaceDirection objectOrientation(int fromX, int fromY, int toX, int toY);
    Enum::FaceDirection objectOrientation(int fromI, int toI);

    bool placeObjectAt(Enum::SceneLayer layerId, qObjectId id, int i);
    //Untest
    bool replaceObjectAt(Enum::SceneLayer layerId, qObjectId id, int i);
    bool moveObjectTo(Enum::SceneLayer layerId, qObjectId id, int i);
    //Untest
    bool removeObjectOn(Enum::SceneLayer layerId, int i);

    virtual void allocateLayers(int count);
    virtual void freeLayers();

};

void GameScene::setSceneCol(int col)
{
    setSceneSize(QSize(col, 0));
}

void GameScene::setSceneRow(int row)
{
    setSceneSize(QSize(0, row));
}

void GameScene::setCursorPoint(QPoint point)
{
    setCursorPoint(point.x(), point.y());
}

int GameScene::indexFromPoint(QPoint point) const
{
    return indexFromPoint(point.x(), point.y());
}

QPoint GameScene::sceneCoorFromDrawCoor(int x, int y) const
{
    return sceneCoorFromDrawCoor_inner(x, y);
}

QPoint GameScene::sceneCoorFromDrawCoor(QPoint point) const
{
    return sceneCoorFromDrawCoor_inner(point.x(), point.y());
}

QPoint GameScene::drawCoorFromSceneCoor(int index) const
{
    QPoint p = pointFromIndex(index);
    return drawCoorFromSceneCoor_inner(p.x(), p.y());
}

QPoint GameScene::drawCoorFromSceneCoor(int col, int row) const
{
    return drawCoorFromSceneCoor_inner(col, row);
}

QPoint GameScene::drawCoorFromSceneCoor(QPoint point) const
{
    return drawCoorFromSceneCoor_inner(point.x(), point.y());
}

bool GameScene::isObjectFacing(Enum::SceneLayer layerId, qObjectId id, QPoint at)
{
    return isObjectFacing(layerId, id, at.x(), at.y());
}

bool GameScene::placeObjectAt(Enum::SceneLayer layerId, qObjectId id, int x, int y)
{
    return placeObjectAt(layerId, id, indexFromPoint(x, y));
}

bool GameScene::placeObjectAt(Enum::SceneLayer layerId, qObjectId id, QPoint at)
{
    return placeObjectAt(layerId, id, indexFromPoint(at));
}

bool GameScene::moveObjectTo(Enum::SceneLayer layerId, qObjectId id, int x, int y)
{
    return moveObjectTo(layerId, id, indexFromPoint(x, y));
}

bool GameScene::moveObjectTo(Enum::SceneLayer layerId, qObjectId id, QPoint to)
{
    return moveObjectTo(layerId, id, indexFromPoint(to));
}

bool GameScene::removeObjectOn(Enum::SceneLayer layerId, int x, int y)
{
    return removeObjectOn(layerId, indexFromPoint(x, y));
}

bool GameScene::removeObjectOn(Enum::SceneLayer layerId, QPoint at)
{
    return removeObjectOn(layerId, indexFromPoint(at));
}

qObjectId GameScene::unitIdAt(QPoint point) const
{
    return unitIdAt(point.x(), point.y());
}

qObjectId GameScene::unitIdUnderCursor() const
{
    return unitIdAt(cursorPoint());
}

} // end namespace Qamer

#endif // QAMER_GAMESCENE_HPP
