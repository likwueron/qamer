/*
 * File name: findertetr.cpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "../qamer_global.hpp"
#include "findertetr.hpp"
#include "../gamescene.hpp"
#include <QQueue>

namespace Qamer {
namespace TileFinder {

enum Direction {
    Up = 0,
    Right = 1,
    Down = 2,
    Left = 3
};

const QPoint direct[] = {
    QPoint(0, -1),
    QPoint(1, 0),
    QPoint(0, 1),
    QPoint(-1, 0)
};

FinderTetrStd::FinderTetrStd()
    : Finder()
{

}

FinderTetrStd::~FinderTetrStd()
{

}

Finder *FinderTetrStd::usableOne(const GameScene *scene) const
{
    return new FinderTetrStd(*this, scene);
}

QList<QPoint> FinderTetrStd::find(QPoint originPoint,
                                  int movepoint, int direction)
{
    Q_UNUSED(direction)

    QList<QPoint> result;
    result.reserve(2*movepoint*(1+movepoint)+1);
    result << originPoint;

    QQueue<QPoint> queue;
    queue.reserve(4*movepoint+1);
    queue.enqueue(originPoint);
    while(0 < movepoint--) {
        for(int i = queue.count(); 0 < i; i--) {
            QPoint curPoint = queue.dequeue();
            for(int d = 0; d < 4; d++) {
                //try direction: up right down left
                QPoint newPoint = curPoint + direct[d];
                if(!result.contains(newPoint) &&
                        isPointValid(newPoint)) {
                    queue.enqueue(newPoint);
                    result.append(newPoint);
                }
            }
        }
    }
    return result;
}

FinderTetrStd::FinderTetrStd(const FinderTetrStd &that,
                             const GameScene *scene)
    : Finder(that, scene)
{

}

FinderTetrLine::FinderTetrLine()
    : Finder()
{

}

FinderTetrLine::~FinderTetrLine()
{

}

Finder *FinderTetrLine::usableOne(const GameScene *scene) const
{
    return new FinderTetrLine(*this, scene);
}

QList<QPoint> FinderTetrLine::find(QPoint originPoint,
                                   int movepoint, int direction)
{
    int dx = 0;
    int dy = 0;
    switch(direction) {
    case Enum::FaceNorth:
        dy = -1;
        break;
    case Enum::FaceWest:
        dx = -1;
        break;
    case Enum::FaceSouth:
        dy = 1;
        break;
    case Enum::FaceEast:
        dx = 1;
        break;
    default:
        break;
    }

    QList<QPoint> result;
    result.reserve(movepoint);
    for(int i = 0; i < movepoint; ++i) {
        QPoint newPoint = originPoint + QPoint(dx * i, dy * i);
        if(isPointValid(newPoint) ) {
            result << newPoint;
        }
    }
    return result;
}

FinderTetrLine::FinderTetrLine(const FinderTetrLine &that,
                               const GameScene *scene)
    : Finder(that, scene)
{

}

FinderTetrCycle::FinderTetrCycle()
    : Finder()
{

}

FinderTetrCycle::~FinderTetrCycle()
{

}

Finder *FinderTetrCycle::usableOne(const GameScene *scene) const
{
    return new FinderTetrCycle(*this, scene);
}

QList<QPoint> FinderTetrCycle::find(QPoint originPoint,
                                    int movepoint, int direction)
{
    Q_UNUSED(direction)

    QList<QPoint> result;
    int r2 = movepoint * movepoint;
    result.reserve((int)(3.14 * r2));
    findInner(originPoint, originPoint, r2, result);
    return result;
}

FinderTetrCycle::FinderTetrCycle(const FinderTetrCycle &that,
                                 const GameScene *scene)
    : Finder(that, scene)
{

}

bool FinderTetrCycle::withinCycle(const QPoint dp, int r2)
{
    return (dp.x() * dp.x() + dp.y() * dp.y()) <= r2;
}

void FinderTetrCycle::findInner(const QPoint &op, const QPoint &p, int &r2, QList<QPoint> &list)
{
    if(withinCycle(op - p, r2) &&
            !list.contains(p) &&
            isPointValid(p)) {
        list.append(p);
        for(int d = 0; d < 4; d++) {
            //try direction: up right down left
            QPoint newPoint = p + direct[d];
            findInner(op, newPoint, r2, list);
        }
    }
}

} // namespace TileFinder
} // namespace Qamer
