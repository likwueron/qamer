/*
 * File name: qrectf.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "qrectf.hpp"
#include "general.hpp"
#include <QScriptContext>
#include <QScriptEngine>

namespace Qamer {

QRectFPrototype::QRectFPrototype(QObject *parent) :
    QObject(parent), QScriptable()
{
}

void QRectFPrototype::adjust(qreal dx1, qreal dy1, qreal dx2, qreal dy2)
{
    QRectF *p = qscriptvalue_cast<QRectF*>(thisObject());
    if(p) {
        p->adjust(dx1, dy1, dx2, dy2);
    }
    else {
        throwTypeError(context(), "QRect");
    }
}

QRectF QRectFPrototype::adjusted(qreal dx1, qreal dy1, qreal dx2, qreal dy2) const
{
    QRectF *p = qscriptvalue_cast<QRectF*>(thisObject());
    if(p) {
        return p->adjusted(dx1, dy1, dx2, dy2);
    }
    else {
        return QRectF();
    }
}

qreal QRectFPrototype::bottom() const
{
    QRectF *p = qscriptvalue_cast<QRectF*>(thisObject());
    if(p) {
        return p->bottom();
    }
    else {
        return -1.0;
    }
}

QPointF QRectFPrototype::bottomLeft() const
{
    QRectF *p = qscriptvalue_cast<QRectF*>(thisObject());
    if(p) {
        return p->bottomLeft();
    }
    else {
        return QPointF();
    }
}

QPointF QRectFPrototype::bottomRight() const
{
    QRectF *p = qscriptvalue_cast<QRectF*>(thisObject());
    if(p) {
        return p->bottomRight();
    }
    else {
        return QPointF();
    }
}

QPointF QRectFPrototype::center() const
{
    QRectF *p = qscriptvalue_cast<QRectF*>(thisObject());
    if(p) {
        return p->center();
    }
    else {
        return QPointF();
    }
}

bool QRectFPrototype::contains() const
{
    int argc = argumentCount();
    QRectF *p = qscriptvalue_cast<QRectF*>(thisObject());
    if(!p) {
        return false;
    }
    switch(argc) {
    case 1:
    {
        QVariant var = argument(0).toVariant();
        if(var.type() == QVariant::RectF) {
            return p->contains(var.toRectF());
        }
        else if(var.type() == QVariant::PointF){
            return p->contains(var.toPointF());
        }
    }
        break;
    case 2:
    {
        qreal x = context()->argument(0).toNumber();
        qreal y = context()->argument(1).toNumber();
        return p->contains(QPointF(x, y));
    }
        break;
    default:
        break;
    }
    return false;
}

void QRectFPrototype::getCoords(qreal *x1, qreal *y1, qreal *x2, qreal *y2) const
{
    QRectF *p = qscriptvalue_cast<QRectF*>(thisObject());
    if(p) {
        p->getCoords(x1, y1, x2, y2);
    }
    else {
        throwTypeError(context(), "QRect");
    }
}

void QRectFPrototype::getRect(qreal *x, qreal *y, qreal *width, qreal *height) const
{
    QRectF *p = qscriptvalue_cast<QRectF*>(thisObject());
    if(p) {
        p->getRect(x, y, width, height);
    }
    else {
        throwTypeError(context(), "QRect");
    }
}

bool QRectFPrototype::isValid() const
{
    QRectF *p = qscriptvalue_cast<QRectF*>(thisObject());
    if(p) {
        return p->isValid();
    }
    else {
        return false;
    }
}

qreal QRectFPrototype::x() const
{
    QRectF *p = qscriptvalue_cast<QRectF*>(thisObject());
    if(p) {
        return p->x();
    }
    else {
        return -1.0;
    }
}

void QRectFPrototype::setX(qreal x)
{
    QRectF *p = qscriptvalue_cast<QRectF*>(thisObject());
    if(p) {
        p->setX(x);
    }
    else {
        throwTypeError(context(), "QRect");
    }
}

qreal QRectFPrototype::y() const
{
    QRectF *p = qscriptvalue_cast<QRectF*>(thisObject());
    if(p) {
        return p->y();
    }
    else {
        return -1.0;
    }
}

void QRectFPrototype::setY(qreal y)
{
    QRectF *p = qscriptvalue_cast<QRectF*>(thisObject());
    if(p) {
        p->setY(y);
    }
    else {
        throwTypeError(context(), "QRect");
    }
}

qreal QRectFPrototype::width() const
{
    QRectF *p = qscriptvalue_cast<QRectF*>(thisObject());
    if(p) {
        return p->width();
    }
    else {
        return -1.0;
    }
}

void QRectFPrototype::setWidth(qreal width)
{
    QRectF *p = qscriptvalue_cast<QRectF*>(thisObject());
    if(p) {
        p->setWidth(width);
    }
    else {
        throwTypeError(context(), "QRect");
    }
}

qreal QRectFPrototype::height() const
{
    QRectF *p = qscriptvalue_cast<QRectF*>(thisObject());
    if(p) {
        return p->height();
    }
    else {
        return -1.0;
    }
}

void QRectFPrototype::setHeight(qreal height)
{
    QRectF *p = qscriptvalue_cast<QRectF*>(thisObject());
    if(p) {
        p->setHeight(height);
    }
    else {
        throwTypeError(context(), "QRect");
    }
}

qreal QRectFPrototype::area() const
{
    QRectF *p = qscriptvalue_cast<QRectF*>(thisObject());
    if(p) {
        return p->width() * p->height();
    }
    else {
        return -1.0;
    }
}

QString QRectFPrototype::toString() const
{
    QRectF *p = qscriptvalue_cast<QRectF*>(thisObject());
    if(p) {
        return QString("QRect(%1, %2, %3, %4)")
                .arg(p->x())
                .arg(p->y())
                .arg(p->width())
                .arg(p->height());
    }
    else {
        return QString();
    }
}

QScriptValue cf_QRectF(QScriptContext *context, QScriptEngine *engine)
{
    int argc = context->argumentCount();
    switch(argc) {
    case 2:
    {
        QVariant var1 = context->argument(0).toVariant();
        if(var1.type() != QVariant::PointF) {
            return engine->toScriptValue(QRectF());
        }

        QVariant var2 = context->argument(1).toVariant();
        if(var2.type() == QVariant::SizeF) {
            return engine->toScriptValue(QRectF(var1.toPointF(), var2.toSizeF()));
        }
        else if(var2.type() == QVariant::PointF) {
            return engine->toScriptValue(QRectF(var1.toPointF(), var2.toPointF()));
        }
    }
        break;
    case 4:
    {
        qreal x = context->argument(0).toNumber();
        qreal y = context->argument(1).toNumber();
        qreal w = context->argument(2).toNumber();
        qreal h = context->argument(3).toNumber();
        return engine->toScriptValue(QRectF(x, y, w, h));
    }
        break;
    default:
        break;
    }
    return engine->toScriptValue(QRectF());
}

} // end namespace Qamer
