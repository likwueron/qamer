/*
 * File name:
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_PIXMAPEFFECT_HPP
#define QAMER_PIXMAPEFFECT_HPP

#include "gameobject.hpp"
#include <QPoint>

namespace Qamer {

class PixmapEffectPrivate;
class PixmapDataPrivate;

class QAMERSHARED_EXPORT PixmapEffect : public GameObject
{
    Q_DECLARE_PRIVATE(PixmapEffect)
    Q_OBJECT
public:
    enum Ground {
        Undefined = 0,
        Background = 1,
        Foreground = 2
    };

    PixmapEffect(const PixmapDataPrivate *pixdata, QObject *parent = 0);
    ~PixmapEffect();

    void setFacing(int faceScheme);
    bool setProgressing(int speed, QRect toRect, Enum::MoveMode mode);
    bool setFinishing();

    bool isBackground() const;

    PixmapEffect* backgroundEffect();

    void painted(QPainter *painter, const QRect &updateRect);

protected://virtual slot
    void when_schemeEnd_reached(int scheme);
protected:
    PixmapEffect(PixmapEffectPrivate *pdd, QObject *parent);
    PixmapEffect(const PixmapEffect& that);
};

} // namespace Qamer

#endif // QAMER_PIXMAPEFFECT_HPP
