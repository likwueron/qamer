/*
 *File name: hatbox.hpp
 *Author: kwueron
 *E-mail: likwueron@gmail.com
 *Last revised: 2014/1/30
 *Published under Public Domain
 */

#ifndef HATBOX_HPP
#define HATBOX_HPP

#include <QGroupBox>

class HatBox : public QGroupBox
{
    Q_OBJECT
public:
    explicit HatBox(QWidget *parent = 0);

signals:

public slots:

};

#endif // HATBOX_HPP
