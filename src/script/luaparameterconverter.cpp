/*
 * File name: luaparameterconverter.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "luaparameterconverter.hpp"

using namespace QtLua;

namespace Qamer {

LuaParameterConverter::LuaParameterConverter()
    : MetaType<Parameter>(Parameter::defaultParameterMID)
{

}

Value LuaParameterConverter::luaFromQt(State *ls, const Parameter *qtvalue)
{
    Value result(Value::new_table(ls));

    const char *bytes = qtvalue->cCharPtr()-1;
    result[1] = Value(ls, *bytes);

    int count = qtvalue->count() + 2;
    //lua table start from 1 AND
    //we use 1st element stored "length"
    for(int i = 2; i < count; i++) {
        result[i] = Value(ls, (int)(*(++bytes)));
    }

    return result;
}

bool LuaParameterConverter::qtFromLua(Parameter *qtvalue, const Value &luavalue)
{
    if(luavalue.type() == Value::TTable) {
        //int count = Parameter::defaultSize;
        char array[Parameter::defaultSize] = {0};

        Value::const_iterator i;
        int j = 0;
        for(i = luavalue.cbegin(); i != luavalue.cend(); ++i, j++) {
            Value v(i.value());
            try {
                array[j] = (char)(v.to_integer());
            }
            catch(String) {
                return false;
            }
        }
        *qtvalue = Parameter::fromRawData(array);
    }
    return qtvalue->count();
}

Value LuaParameterConverter::qt2lua(State *ls, const Parameter *qtvalue)
{
    return luaFromQt(ls, qtvalue);
}

bool LuaParameterConverter::lua2qt(Parameter *qtvalue, const Value &luavalue)
{
    return qtFromLua(qtvalue, luavalue);
}

LuaParaListConverter::LuaParaListConverter()
    : MetaType<ParaList>(Parameter::defaultParaListMID)
{

}

Value LuaParaListConverter::luaFromQt(State *ls, const ParaList *qtvalue)
{
    Value result(Value::new_table(ls));
    int count = qtvalue->count();
    for(int i = 0; i < count; i++) {
        result[i+1] = LuaParameterConverter::luaFromQt(ls, &(qtvalue->at(i)));
    }
    return result;
}

bool LuaParaListConverter::qtFromLua(ParaList *qtvalue, const Value &luavalue)
{
    if(luavalue.type() == Value::TTable) {
        Value::const_iterator i;
        for(i = luavalue.cbegin(); i != luavalue.cend(); ++i) {
            Value v(i.value());
            qtvalue->append(Parameter());
            if(!LuaParameterConverter::
                    qtFromLua(&(qtvalue->last()),
                              v)) return false;
        }
    }
    return (0 < qtvalue->count());
}

Value LuaParaListConverter::qt2lua(State *ls, const ParaList *qtvalue)
{
    return luaFromQt(ls, qtvalue);
}

bool LuaParaListConverter::lua2qt(ParaList *qtvalue, const Value &luavalue)
{
    return qtFromLua(qtvalue, luavalue);
}

} // namespace Qamer

