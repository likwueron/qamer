/*
 * File name:
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "../qamer_global.hpp"
#include "pixmapeffect_p.hpp"
#include <QDebug>

namespace Qamer {

PixmapEffectPrivate::PixmapEffectPrivate()
    : GameObjectPrivate(), PixmapDataPrivate(),
      m_canProgressing(0), m_canFinishing(0),
      m_ground(0), m_facing(0)
{

}

PixmapEffectPrivate::~PixmapEffectPrivate()
{

}

void PixmapEffectPrivate::addScheme(quint16 id, const QList<quint16> &list)
{
    addScheme_default(id, list);
}

quint16 PixmapEffectPrivate::currentPixmapIndex() const
{
    return currentPixmapIndex_default();
}

quint16 PixmapEffectPrivate::currentSchemeId() const
{
    return currentSchemeId_default();
}

quint16 PixmapEffectPrivate::currentSchemeIndex() const
{
    return currentSchemeIndex_default();
}

quint16 PixmapEffectPrivate::setScheme(quint16 id, quint16 index)
{
    return setScheme_default(id, index);
}

quint16 PixmapEffectPrivate::progressScheme()
{
    //move while animation progressing
    if(m_curSchemeId == progressId()) {
        translated(m_delta, m_to);
    }
    return progressScheme_default();
}

void PixmapEffectPrivate::addSpriteSheet(const SpriteSheet &sheet)
{
    return addSpriteSheet_default(sheet);
}

void PixmapEffectPrivate::setCurrentPixmapIndex(quint16 index)
{
    setCurrentPixmapIndex_default(index);
}

int PixmapEffectPrivate::facingId() const
{
    return m_facing;
}

void PixmapEffectPrivate::setFacing(int faceScheme)
{
    m_facing = faceScheme;
}

void PixmapEffectPrivate::generateId()
{
    if(m_ground) return;
    //test if has foreground scheme
    int id = m_canProgressing ?
                (Enum::SchemeProgress | Enum::SchemeFront) :
                (Enum::SchemeFinish | Enum::SchemeFront);
    bool hasDefault = false;//default
    bool faceFit = false;
    for(int i = 0; i <= 8; i++) {
        if(m_schemeMap.contains(i + id)) {
            if(i == 0) hasDefault = true;
            m_ground = PixmapEffect::Foreground;
        }
        else m_ground = PixmapEffect::Background;

        if(i == facingId()) {
            faceFit = true;
            break;
        }
    }

    if(!faceFit) {
        if(hasDefault) {
            setFacing(Enum::SchemeDefault);
            m_ground = PixmapEffect::Foreground;
        }
        else m_ground = PixmapEffect::Background;
    }

    //after generate, set current scheme
    if(m_canProgressing) setScheme(progressId());
    else setScheme(finishId());
}

int PixmapEffectPrivate::progressId() const
{
    if(!m_canProgressing) return invalidId;
    return groundId();
}

int PixmapEffectPrivate::finishId() const
{
    if(!m_canFinishing) return invalidId;
    return groundId() | Enum::SchemeFinish;
}

bool PixmapEffectPrivate::nextPhase()
{
    if(m_curSchemeId == progressId()) {
        if(objectAt(m_to)) return setScheme(finishId());
        else return true;
    }
    else return false;
}

void PixmapEffectPrivate::printInner() const
{
    qDebug() << "progressing:" << m_canProgressing << m_delta << m_to;
    qDebug() << "finishing" << m_canFinishing;
    qDebug() << "ground:" << m_ground;
    qDebug() << "facing" << m_facing;
    qDebug() << "scheme" << m_curSchemeId;
    qDebug() << "schemes" << m_schemeMap.value(m_curSchemeId);
}

PixmapEffectPrivate::PixmapEffectPrivate(const PixmapEffectPrivate &that)
    : GameObjectPrivate(that), PixmapDataPrivate(that),
      m_canProgressing(that.m_canProgressing),
      m_canFinishing(that.m_canFinishing),
      m_ground(that.m_ground), m_facing(that.m_facing),
      m_delta(that.m_delta), m_to(that.m_to)

{
    if(m_canProgressing) setScheme(progressId());
    else setScheme(finishId());
}

void PixmapEffectPrivate::copyPixmapRsc(const PixmapDataPrivate &rsc)
{
    PixmapDataPrivate::operator =(rsc);
}

int PixmapEffectPrivate::groundId() const
{
    switch(m_ground) {
    case 1://background
        return Enum::SchemeBack | facingId();
    case 2://foreground
        return Enum::SchemeFront | facingId();
    default:
        return 0;
    }
}

} // namespace Qamer

