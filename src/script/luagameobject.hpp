/*
 * File name: luagameobject.hpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_LUAGAMEOBJECT_HPP
#define QAMER_LUAGAMEOBJECT_HPP

#include "../qamer_global.hpp"
#include "../gameobject.hpp"
#include "luagameobjectlist.hpp"

#include <QObject>
#include <QString>
#include <QPointF>
#include <QSizeF>
#include <QVariant>
#include <QVariantList>
#include "../mics/parameter.hpp"

#include <QtLua/Value>

namespace QtLua {
class State;
}

namespace Qamer {

class QAMERSHARED_EXPORT LuaGameObject : public QObject, public GameObjectId
{
    Q_OBJECT
    Q_CLASSINFO("LuaName", "GameObject")
    Q_PROPERTY(int objectId READ objectId WRITE setObjectId NOTIFY idChanged)
    Q_PROPERTY(QString systemName READ systemName WRITE setSystemName NOTIFY idChanged)
    Q_PROPERTY(int objectType READ objectType)
    Q_PROPERTY(int subObjectType READ subObjectType WRITE setSubObjectType)
    Q_PROPERTY(bool isValid READ isValid)

    Q_PROPERTY(QString name READ name WRITE setName)
    Q_PROPERTY(QString description READ description WRITE setDescription)

    Q_PROPERTY(QPointF position READ position WRITE move)
    Q_PROPERTY(QSizeF size READ size WRITE resize)
    Q_PROPERTY(bool visible READ isVisible WRITE setVisible)
    //Unit-only BEGIN
    Q_PROPERTY(QPointF scenePoint READ scenePoint WRITE setScenePoint)
    Q_PROPERTY(int facing READ facing WRITE setFacing)
    //Unit-only END
    //Skill-only BEGIN
    Q_PROPERTY(QByteArray possibleTilesFinder READ possibleTilesFinderName WRITE setPossibleTilesFinderName)
    Q_PROPERTY(QByteArray effectTilesFinder READ effectTilesFinderName WRITE setEffectTilesFinderName)
    //Skill-only END
public:
    explicit LuaGameObject(QObject *parent = 0);
    LuaGameObject(int id);
    LuaGameObject(const QString &name);
    ~LuaGameObject();

    static void addTypeText(int type, const QString &text);
    static void addSubTypeText(int type, int subType, const QString &text);
    int objectId();
    void setObjectId(int i);
    QString systemName() const;
    void setSystemName(const QString &name);
    int objectType() const;
    int subObjectType() const;
    void setSubObjectType(int subObjectType);
    Q_INVOKABLE bool isObjectType(int type);

    //common data
    QString name() const;
    void setName(const QString &name);
    QString description() const;
    void setDescription(const QString &des);
    //void setAvatarSpriteSheet(int code);
    //controller
    //void setControllerFocus(int cid);
    //drawing info
    QPointF position() const;
    Q_INVOKABLE void move(QPointF position, Enum::MoveMode mode = Enum::MoveNormal);
    QSizeF size() const;
    void resize(QSizeF size);
    QSizeF parentSize() const;
    //void addSpriteSheet(SpriteSheet *sheet);
    bool isVisible() const;
    void setVisible(bool visible);
    Q_INVOKABLE void invertVisible();
    //animation control
    Q_INVOKABLE void addAnimationScheme(int id, const QVariantList &list);
    Q_INVOKABLE void setCurrentAnimationScheme(int id, int index = 0);
    //void nextPix();
    //Unit-only BEGIN
    Q_INVOKABLE void addStatValue(const QByteArray &name, int max = 255, int min = 0, int flags = Enum::StatValueZeroAtBegin, int val = 0);
    Q_INVOKABLE int statCurrentValue(const QByteArray &name) const;
    Q_INVOKABLE int statMaxValue(const QByteArray &name) const;
    Q_INVOKABLE int statMinValue(const QByteArray &name) const;
    Q_INVOKABLE bool increaseStatValue(const QByteArray &name, int val);
    Q_INVOKABLE void resetStatValue(const QByteArray &name);
    Q_INVOKABLE void setStatCurrentValue(const QByteArray &name, int cur);
    Q_INVOKABLE void setStatMaxValue(const QByteArray &name, int max);
    Q_INVOKABLE void setStatMinValue(const QByteArray &name, int min);

    QPointF scenePoint() const;
    void setScenePoint(QPointF point);

    int facing() const;
    void setFacing(int facing);
    //skill
    Q_INVOKABLE void addSkill(LuaGameObject *skill);
    Q_INVOKABLE LuaGameObjectList skillList(int subType = Enum::ObjectSubTypeMask) const;
    //resistance to statuses and elements
    //int resistance(int id);
    //void setResistance(int id, int val);
    //Unit-only END

    //Scene-only BEGIN
    Q_INVOKABLE bool pointWithinScene(QPointF point) const;
    Q_INVOKABLE bool canPass(QPointF point) const;
    //Scene-only END

    //Skill-only BEGIN
    QByteArray possibleTilesFinderName() const;
    void setPossibleTilesFinderName(const QByteArray &name);
    QByteArray effectTilesFinderName() const;
    void setEffectTilesFinderName(const QByteArray &name);
    Q_INVOKABLE ParaList parameter(int type);
    //Skill-only END

signals:
    void idChanged();

public slots:
    QString toString() const;
    QVariant getDP(const QByteArray &name) const;
    void setDP(const QByteArray &name, const QVariant &value);

private:
    static QMap<int, QString> sm_typeTextMap;
};

QAMERSHARED_EXPORT QtLua::Value::List GameObject_GameObject(QtLua::State *, const QtLua::Value::List&);
//get number of object with specific type
QAMERSHARED_EXPORT QtLua::Value::List GameObject_objectNumber(QtLua::State *, const QtLua::Value::List&);
//get a table about object with specific type
QAMERSHARED_EXPORT QtLua::Value::List GameObject_objectTable(QtLua::State *, const QtLua::Value::List&);

} // end namespcae Qamer

#endif // QAMER_LUAGAMEOBJECT_HPP
