/*
 * File name: dependinjector.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_DEPENDINJECTOR_HPP
#define QAMER_DEPENDINJECTOR_HPP

#include "../qamer_export.hpp"
#include <QObject>
#include <QMetaObject>
#include <QByteArray>
#include <QMap>

namespace Qamer {

class QAMERSHARED_EXPORT DependInjector
{
public:
    friend class FileLoader;

    DependInjector();
    ~DependInjector();
    void registerMetaObject(const QMetaObject &meta);
    void registerMetaObject(const QByteArray &className,
                            const QMetaObject &meta);
    QObject *createInstance(const QByteArray &className,
                            QObject *parent = 0);

    QByteArray supremeClassName() const;
    void setSupremeClass(const QByteArray &className);
    void setSupremeClass(const QMetaObject &meta);
    // Qamer::GameObject become Qamer.GameObject
    void useScriptNamespace(bool use = true);
private:
    QByteArray replaceColumn(const QByteArray &str);

    QMap<QByteArray, const QMetaObject*> m_classMap;
    QByteArray m_supremeClassName;

    bool m_useDotNS;
};

} // end namespace Qamer

#endif // QAMER_DEPENDINJECTOR_HPP
