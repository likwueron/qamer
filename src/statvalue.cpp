/*
 * File name: statvalue.cpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "statvalue.hpp"
#include "qamer_global.hpp"

namespace Qamer {

StatValue::StatValue()
    : beginValue(invalidStatValue),
      currentValue(invalidStatValue),
      maxValue(invalidStatValue), minValue(invalidStatValue)
{

}

StatValue::StatValue(const StatValue &that)
    : flags(that.flags),
      beginValue(that.beginValue),
      currentValue(that.currentValue),
      maxValue(that.maxValue), minValue(that.minValue)
{

}

StatValue::~StatValue()
{

}

int StatValue::safeInc(int val)
{//just simple check because i don't think anyone will use large number
    currentValue += val;
    if(currentValue < minValue) currentValue = minValue;
    if(maxValue < currentValue) currentValue = maxValue;
    return currentValue;
}

int StatValue::reset()
{
    switch(flags) {
    case Enum::StatValueMaxAtBegin:
        currentValue = maxValue;
        break;
    case Enum::StatValueMinAtBegin:
        currentValue = minValue;
        break;
    case Enum::StatValueZeroAtBegin:
    case Enum::StatValueSpecificAtBegin:
        currentValue = beginValue;
        break;
    }
    return currentValue;
}

} // namespace Qamer

