/*
 * File name: rhombus.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "rhombus.hpp"

Rhombus::Rhombus()
{

}

Rhombus::Rhombus(QPoint at, QSize size)
{
    setSize(size);
    moveTo(at);
}

bool Rhombus::isEmpty() const
{
    return m_size.isEmpty() | m_up.isNull();
}

void Rhombus::setSize(const QSize size)
{
    m_size = size;
}

void Rhombus::setSize(int w, int h)
{
    setSize(QSize(w, h));
}

void Rhombus::moveTo(const QPoint point)
{
    if(m_size.isEmpty()) {
        return;
    }

    int halfW = m_size.width() / 2;
    int halfH = m_size.height() / 2;

    m_up = point + QPoint(halfW, 0);
    m_left = point + QPoint(0, halfH);
    m_right = point + QPoint(m_size.width(), halfH);
    m_down = point + QPoint(halfW, m_size.height());
}

void Rhombus::move(const QPoint point)
{
    m_up + point;
    m_left + point;
    m_right + point;
    m_down + point;
}

QPolygon Rhombus::toPolygon() const
{
    return QPolygon() << m_up << m_left << m_down << m_right;
}
