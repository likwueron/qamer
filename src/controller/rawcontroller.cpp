#include "rawcontroller.hpp"
#include <QMetaEnum>

RawController::RawController(QObject *parent) :
    Controller(parent)
{
}

QString RawController::layout() const
{
    return "aaaaaaaa";
}

QString RawController::monitorUiFile() const
{
    return QString();
}

void RawController::when_hatValue_changed(int hatId, qint8 value)
{
    int role = hatId + 1;
#ifdef CONTROLLER_SIGNAL
    emit action_done(role, QString("DPad%1").arg(role), value);
#else
    sendEvent(role, QString("DPad%1").arg(role), value);
#endif
}

void RawController::when_buttonValue_changed(int buttonId, qint8 value)
{
    int role = buttonId + 1;
#ifdef CONTROLLER_SIGNAL
    emit action_done(role << 7, QString("Button%1").arg(role), value);
#else
    sendEvent(role << 7, QString("Button%1").arg(role), value);
#endif
}

void RawController::when_axisValue_changed(int axisId, qint16 value)
{
    int role = axisId + 1;
#ifdef CONTROLLER_SIGNAL
    emit action_done(role << 3, QString("Axis%1").arg(role), value);
#else
    sendEvent(role << 3, QString("Axis%1").arg(role), value);
#endif
}

void RawController::when_trackballValue_changed(int trackballId, int dx, int dy)
{
    int role = trackballId + 1;
#ifdef CONTROLLER_SIGNAL
    emit action_done(role << 12, QString("Trackball%1").arg(role), encodeQint32(dx, dy));
#else
    sendEvent(role, QString("Trackball%1").arg(role), encodeQint32(dx, dy));
#endif
}

void RawController::when_dataValue_changed(const JoystickData *data)
{
    Q_UNUSED(data)
}
