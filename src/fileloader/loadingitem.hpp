/*
 * File name: loadingitem.hpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_LOADINGITEM_HPP
#define QAMER_LOADINGITEM_HPP

#include <QStack>
#include <QVariantList>

namespace Qamer {

class LoadingItem;
typedef QStack<LoadingItem*> LoadingStack_t;

class LoadingItem
{
public:
    enum LoadingType {
        L_None,
        L_Obj,
        L_Func,
        L_Pro,
        L_Var,
        L_Enum,
        L_Str,
        L_Num
    };

    LoadingItem(LoadingType type, LoadingStack_t *stack);
    ~LoadingItem();

    LoadingType loadingType() const;
    void setLoadingType(LoadingType type);
    //add variant to list,
    //discard variant "one time" if has upper item previously
    //it will act normally when second try
    void appendVariant(const QVariant &v, bool ignoreUpper = false);
    //return all it's variant and clear itself
    QVariant exportVariant();
    QVariantList exportVarList();
    //
    bool deeperThan(int depth);
private:
    LoadingStack_t *up_stack;

    LoadingType m_type;
    QVariantList m_parameters;
    bool m_hasUpper;
};

} // namespace Qamer

#endif // QAMER_LOADINGITEM_HPP
