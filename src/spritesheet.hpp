/*
 * File name: spritesheet.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */


#include "qamer_global.hpp"
#include <QPoint>
#include <QPixmap>
#include <QColor>

#ifndef QAMER_SPRITESHEET_HPP
#define QAMER_SPRITESHEET_HPP

class QPainter;

namespace Qamer {

class AnimationScheme;

class QAMERSHARED_EXPORT SpriteSheet
{
public:
    SpriteSheet();
    //shallow clone
    SpriteSheet(const SpriteSheet &that);
    ~SpriteSheet();
    //if pixmap has been loaded, it won't apply new mask
    bool load(const QString &fileName, int row = 1, int col = 1,
              bool applyMask = false, const QColor &maskColor = QColor());
    bool loadFixsize(const QString &fileName, int w, int h,
                     bool applyMask = false, const QColor &maskColor = QColor());
    void resetClip(int row, int col);

    bool isValid() const;
    QSize clipSize() const;
    //Waring: may not change pixmap in cache
    void setMask(const QColor &color);

    void draw(QPainter *p_painter, int atX, int atY, int col, int row) const;
    inline void draw(QPainter *p_painter, int atX, int atY, int index) const;
    inline void draw(QPainter *p_painter, QPoint at, int index) const;
    inline void draw(QPainter *p_painter, QPoint at, QPoint src) const;

protected:
    bool loading(const QString &fileName, bool applyMask, const QColor &maskColor);
    void indexToPoint(int index, int &col, int &row) const;

private:
    QPixmap m_pix;
    int m_rowMax;
    int m_colMax;
    int m_w;
    int m_h;
};

void SpriteSheet::draw(QPainter *p_painter, int atX, int atY, int index) const
{
    int col, row;
    indexToPoint(index, col, row);
    draw(p_painter, atX, atY, col, row);
}

void SpriteSheet::draw(QPainter *p_painter, QPoint at, int index) const
{
    int col, row;
    indexToPoint(index, col, row);
    draw(p_painter, at.x(), at.y(), col, row);
}

void SpriteSheet::draw(QPainter *p_painter, QPoint at, QPoint src) const
{
    draw(p_painter, at.x(), at.y(), src.x(), src.y());
}

} // end namespace Qamer

Q_DECLARE_TYPEINFO(Qamer::SpriteSheet, Q_MOVABLE_TYPE);

#endif // QAMER_SPRITESHEET_HPP
