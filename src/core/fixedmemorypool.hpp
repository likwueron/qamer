/*
 * File name: fixedmemorypool.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is marjorly based on Paper
 * "Fast Efficient Fixed-Size Memory Pool" by Ben Kenwright.
 * You can redistribute it and/or modify it freely under Public Domain.
 */

#ifndef QAMER_FIXEDMEMORYPOOL_HPP
#define QAMER_FIXEDMEMORYPOOL_HPP

#include "../qamer_export.hpp"

namespace Qamer {

class QAMERSHARED_EXPORT FixedMemoryPool
{
    typedef unsigned char* ptr;
    typedef unsigned int number;
public:
    static const number defaultBlockSize = sizeof(void*);
    static const number defaultBlockNum = 1024;
    static const number staticBlockHeaderSize = sizeof(number);

    FixedMemoryPool(number blockNum = defaultBlockNum,
                    number blockSize = defaultBlockSize,
                    bool freelock = false);
    ~FixedMemoryPool();

    bool reset(number blockNum, number blockSize);
    void freeAll();

    void* allocate();
    void free(void* p);

    number blockSize() const;
protected:
    ptr addrFromIndex(number index) const;
    number indexFromAddr(ptr addr) const;

private:
    ptr mp_memHead;
    ptr mp_nextBlock;
    enum FreeLock {
        Unlock,
        Lock
    } m_freelock;
    // size-related data
    number m_pageSize;
    number m_blockSize;
    // statistics
    number m_initBlockNum;
    number m_freeBlockNum;
    number m_blockNum;

    // disable copy & assignment
    FixedMemoryPool(const FixedMemoryPool &that);
    FixedMemoryPool &operator=(const FixedMemoryPool &that);
};

//template <typename T>
//class FixedAllocator
//{
//public:
//    typedef T value_type;
//    typedef T* pointer;
//    typedef T& reference;
//    typedef const T* const_pointer;
//    typedef const T& const_reference;
//    typedef size_t size_type;
//    typedef ptrdiff_t difference_type;
//    template <typename U>
//    struct rebind
//    {
//        typedef allocator<U> other;
//    };

//    pointer address(reference x) const;
//    const_pointer address(const_reference x) const;
//    pointer allocate(size_type sizeType, std::allocator<void>::const_pointer hint = 0);
//    deallocate();
//    max_size();
//    contruct();
//    template<typename U>
//    void destroy(U *p);
//};

} // end namespace Qamer

#endif // QAMER_FIXEDMEMORYPOOL_HPP
