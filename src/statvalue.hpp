/*
 * File name: statvalue.hpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_STATVALUE_HPP
#define QAMER_STATVALUE_HPP

#include "qamer_export.hpp"

namespace Qamer {

struct QAMERSHARED_EXPORT StatValue
{
    StatValue();
    StatValue(const StatValue &that);
    ~StatValue();

    int safeInc(int val);
    int reset();

    int flags;
    int beginValue;
    int currentValue;
    int maxValue;
    int minValue;
};

} // namespace Qamer

Q_DECLARE_TYPEINFO(Qamer::StatValue, Q_MOVABLE_TYPE);

#endif // QAMER_STATVALUE_HPP
