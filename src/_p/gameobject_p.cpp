/*
 * File name: gameobject_p.cpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "gameobject_p.hpp"
#include <QDebug>

namespace Qamer {

QMap<int, GameObject*> GameObjectPrivate::sm_objectMap;
GameObject::ScriptableTag GameObjectPrivate::sm_sTag = GameObject::ScriptableCPP;

GameObjectPrivate::GameObjectPrivate()
    : QObjectPrivate(),
      m_position(QPoint(0, 0)), m_size(QSize(0, 0)),
      m_objectType(Enum::ObjectUndefined), m_listId(-1),
      m_isVisible(true)
{
    m_infomation.avatarSpriteSheet = -1;
}

GameObjectPrivate::~GameObjectPrivate()
{

}

void GameObjectPrivate::addScheme(quint16 id, const QList<quint16> &list)
{
    Q_UNUSED(id)
    Q_UNUSED(list)
}

quint16 GameObjectPrivate::currentPixmapIndex() const
{
    return -1;
}

quint16 GameObjectPrivate::currentSchemeId() const
{
    return -1;
}

quint16 GameObjectPrivate::currentSchemeIndex() const
{
    return -1;
}

quint16 GameObjectPrivate::setScheme(quint16 id, quint16 index)
{
    Q_UNUSED(id)
    Q_UNUSED(index)
    return -1;
}

quint16 GameObjectPrivate::progressScheme()
{
    return -1;
}

void GameObjectPrivate::addSpriteSheet(const SpriteSheet &sheet)
{
    Q_UNUSED(sheet)
}

void GameObjectPrivate::setCurrentPixmapIndex(quint16 index)
{
    Q_UNUSED(index)
}

const QList<SpriteSheet> &GameObjectPrivate::sheets() const
{
    //GCC-Warning temporary return value
    return QList<SpriteSheet>();
}

bool GameObjectPrivate::objectAt(const QPoint &at) const
{
    return m_position == at;
}

void GameObjectPrivate::translated(const QPoint &delta)
{
    Q_Q(GameObject);
    QPoint old = m_position.toPoint();
    m_position += delta;
    emit q->moved(old);
}

void GameObjectPrivate::translated(const QPointF &delta, const QPoint &stop)
{
    QPointF newPoint = m_position + delta;
    if(delta.x() < 0) {
        if(newPoint.x() < stop.x()) newPoint.rx() = stop.x();
    }
    else if(0 < delta.x()) {
        if(stop.x() < newPoint.x()) newPoint.rx() = stop.x();
    }

    if(delta.y() < 0) {
        if(newPoint.y() < stop.y()) newPoint.ry() = stop.y();
    }
    else if(0 < delta.y()) {
        if(stop.y() < newPoint.y()) newPoint.ry() = stop.y();
    }

    Q_Q(GameObject);
    QPoint old = m_position.toPoint();
    m_position = newPoint;
    emit q->moved(old);
}

GameObjectPrivate::GameObjectPrivate(const GameObjectPrivate &that)
    : QObjectPrivate(),
      m_position(QPoint(0, 0)), m_size(that.m_size),
      m_infomation(that.m_infomation),
      m_objectType(that.m_objectType), m_listId(-1),
      m_isVisible(true)
{

    if(that.extraData) {
        if(!extraData) extraData = new QObjectPrivate::ExtraData;
        //should check with metaproperty?
        extraData->propertyNames = that.extraData->propertyNames;
        extraData->propertyValues = that.extraData->propertyValues;
#if (QT_VERSION >= QT_VERSION_CHECK(5,3,0))
        extraData->objectName = that.extraData->objectName;
    }
#else
    }
    objectName = that.objectName;
#endif
}

int GameObjectPrivate::generateId()
{
    int count = sm_objectMap.count();
    int temp = count;
    while(count != INT_MAX) {
        if(sm_objectMap.contains(temp)) {
            if(temp == INT_MAX) temp = 0;
            else temp++;
        }
        else return temp;
    }
    qFatal() << "GameObject: Number of GameObject exceed maximum limit!";
    return invalidId;
}

} // end namespace Qamer
