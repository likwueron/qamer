/*
 * File name: loadingitem.cpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "loadingitem.hpp"
#include <QDebug>

namespace Qamer {

LoadingItem::LoadingItem(LoadingType type, LoadingStack_t *stack)
    : up_stack(stack), m_type(type), m_hasUpper(false)
{
    if(up_stack->count()) {
        up_stack->top()->m_hasUpper = true;
    }
    up_stack->push(this);
}

LoadingItem::~LoadingItem()
{
    if(!deeperThan(1)) return; //normal end
    if(this != up_stack->top()) {
        qCritical() << "Qamer::FileLoader: loading stack error";
        return;
    }
    //discard "this"
    up_stack->pop();

    LoadingItem *top = up_stack->top();
    if( (L_Enum == top->m_type) &&
            (L_Enum == m_type) ) {
        if(m_parameters.count() != 1) { //error
            qCritical() << "Qamer::FileLoader: loading enum error";
        }
        QVariant &r_v = top->m_parameters[0];
        r_v.setValue(m_parameters.at(0).toInt() |
                     r_v.toInt());
    }
    else top->appendVariant(exportVariant(), true);
}

LoadingItem::LoadingType LoadingItem::loadingType() const
{
    return m_type;
}

void LoadingItem::setLoadingType(LoadingItem::LoadingType type)
{
    m_type = type;
}

void LoadingItem::appendVariant(const QVariant &v, bool ignoreUpper)
{
    if(m_hasUpper && !ignoreUpper) {
        m_hasUpper = false;
        return;
    }
    if(!v.isNull() || v.type() == QVariant::PointF) {
        m_parameters << v;
    }
}

QVariant LoadingItem::exportVariant()
{
    int count = m_parameters.count();
    QVariant result;
    switch(count) {
    case 0:
        break;
    case 1:
        result =  m_parameters.at(0);
        break;
    default:
        result = m_parameters;
        break;
    }
    m_parameters.clear();
    return result;
}

QVariantList LoadingItem::exportVarList()
{
    QVariantList list = m_parameters;
    m_parameters.clear();
    return list;
}

bool LoadingItem::deeperThan(int depth)
{
    return depth < up_stack->count();
}

} // namespace Qamer

