/*
 * File name: gamescene.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "gamescene.hpp"
#include "_p/gamescene_p.hpp"
//#include "core/fixedmemorypool.hpp"
#include "application.hpp"
#include "tilefinder/tilesfinderadmin.hpp"
#include <QPainter>
#include <QVariant>
#include <QDebug>

namespace Qamer {

GameScene::GameScene(QObject *parent) :
    GameObject(new GameScenePrivate, Enum::ObjectScene, parent)
{
    Q_D(GameScene);
    d->mp_finders = new TilesFinderAdmin(this);
}

GameScene::~GameScene()
{
    freeLayers();
    Q_D(GameScene);
    delete d->mp_finders;
}

GameScene *GameScene::currentScene()
{
    return GameScenePrivate::smp_curScene;
}

void GameScene::setCurrentScene(GameScene *scene)
{
    GameScenePrivate::smp_curScene = scene;
}

void GameScene::setLayerOrder(const QList<int> &list)
{
    GameScenePrivate::sm_layerOrder = list;
}

int GameScene::sceneCol() const
{
    Q_D(const GameScene);
    return d->m_sceneCol;
}

int GameScene::sceneRow() const
{
    Q_D(const GameScene);
    return d->m_sceneRow;
}

QSize GameScene::sceneSize() const
{
    Q_D(const GameScene);
    return QSize(d->m_sceneCol, d->m_sceneRow);
}

void GameScene::setSceneSize(QSize size)
{
    Q_D(GameScene);
    bool reset = false;
    if((size.width() != d->m_sceneCol) && size.width()) {
        d->m_sceneCol = size.width();
        reset = true;
    }
    if((size.height() != d->m_sceneRow) && size.height()) {
        d->m_sceneRow = size.height();
        reset = true;
    }

    if(reset) {
        //unit place on it will be discard, need add units again
        int axisSum = sceneLength();
        allocateLayers(d->m_sceneCol * d->m_sceneRow);
        resize(QSize(d->m_hTileW * axisSum, d->m_hTileH * axisSum));
    }
}

int GameScene::sceneLength() const
{
    Q_D(const GameScene);
    return d->m_sceneCol * d->m_sceneRow;
}

int GameScene::tileWidth() const
{
    Q_D(const GameScene);
    return d->m_hTileW * 2;
}

void GameScene::setTileWidth(int width)
{
    Q_D(GameScene);
    d->m_hTileW = width / 2;
}

int GameScene::tileHeight() const
{
    Q_D(const GameScene);
    return d->m_hTileH * 2;
}

void GameScene::setTileHeight(int height)
{
    Q_D(GameScene);
    d->m_hTileH = height / 2;
}

QSize GameScene::tileSize() const
{
    Q_D(const GameScene);
    return QSize(d->m_hTileW * 2, d->m_hTileH * 2);
}

void GameScene::setTileSize(QSize size)
{
    Q_D(GameScene);
    d->m_hTileW = size.width() / 2;
    d->m_hTileH = size.height() / 2;
}

QPoint GameScene::cursorPoint() const
{
    Q_D(const GameScene);
    return QPoint(d->m_cursorCol, d->m_cursorRow);
}

void GameScene::setCursorPoint(int col, int row)
{
    Q_D(GameScene);
    if((0 <= col) | (col < d->m_sceneCol) |
            (0 <= row) | (row < d->m_sceneRow)) {
        d->m_cursorCol = col;
        d->m_cursorRow = row;
    }
}

int GameScene::indexFromPoint(int x, int y) const
{
    Q_D(const GameScene);
    return x + y * d->m_sceneCol;
}

QPoint GameScene::pointFromIndex(int i) const
{
    Q_D(const GameScene);
    int x = i % d->m_sceneCol;
    int y = (i - x) / d->m_sceneCol;
    return QPoint(x, y);
}

int GameScene::layerNumber() const
{
    return 4;
}

int GameScene::layerObjectCount(Enum::SceneLayer layerId) const
{
    int result = 0;
    int count = sceneLength();
    qObjectId *layer = layerFromId(layerId);
    for(int i = 0; i < count; i++) {
        if(isIdValid(*(layer+i))) result++;
    }
    return result;
}

bool GameScene::isObjectFacing(Enum::SceneLayer layerId, qObjectId id, int x, int y)
{
    GameObject *obj = nullptr;
    int count = sceneLength();
    qObjectId *layer = layerFromId(layerId);
    for(int i = 0; i < count; i++) {
        if(id == *(layer+i)) {
            obj = findObject(id);
            break;
        }
    }
    //extrac point
    QPoint objAt = obj->property("scenePoint").toPoint();
    Enum::FaceDirection dirAns = objectOrientation(objAt.x(), objAt.y(), x, y);
    int facing = obj->property("facing").toInt();
    return facing == dirAns;
}

void GameScene::setLayer(Enum::SceneLayer layerId, const QVariantList &list)
{
    Q_D(GameScene);
    int count = qMin(d->m_sceneCol * d->m_sceneRow, list.count());
    for(int i = 0; i < count; i++) {
        bool ok;
        qObjectId id = list.at(i).toInt(&ok);
        if(ok && isIdValid(id)) {
            placeObjectAt(layerId, id, i);
        }
    }
}

const qObjectId* GameScene::layer(Enum::SceneLayer layerId) const
{
    return layerFromId(layerId);
}

QList<qObjectId> GameScene::unitIdList() const
{
    int count = sceneLength();
    QList<qObjectId> result;
    result.reserve(count);
    qObjectId *layer = layerFromId(Enum::LayerUnit);
    for(int i = 0; i < count; i++) {
        int id = *(layer+i);
        if(id != invalidId) result << id;
    }
    return result;
}

qObjectId GameScene::unitIdAt(int x, int y) const
{
    Q_D(const GameScene);
    if(0 <= x && x < d->m_sceneCol &&
            0 <= y && y < d->m_sceneRow) return *(layer(Enum::LayerUnit)+indexFromPoint(x,y));
    else return invalidId;
}

QList<QPoint> GameScene::unitFindPath(const QByteArray &name, QPoint originalPoint, int movepoint, int direction) const
{
    Q_D(const GameScene);
    return d->mp_finders->find(name, originalPoint, movepoint, direction);
}

void GameScene::when_sceneMove_required(qObjectId id, int i, int oi)
{
    if(i == oi) return;
    if(0 <= i && i < sceneLength()) {
        moveObjectTo(Enum::LayerUnit, id, i);
    }
}

GameScene::GameScene(GameScenePrivate *pdd, QObject *parent)
    : GameObject(pdd, Enum::ObjectScene, parent)
{
    pdd->mp_finders = new TilesFinderAdmin(this);
}

qSceneLayer GameScene::layerFromId(Enum::SceneLayer id) const
{
    Q_D(const GameScene);
    qSceneLayer result = d->mp_layerArray;
    int len;
    switch(id) {
    case Enum::LayerUnit:
        len = 0;
    case Enum::LayerNUO:
        len = sceneLength();
        break;
    case Enum::LayerTerrain:
        len = sceneLength()*2;
        break;
    case Enum::LayerEvent:
        //may cause crash, alloc mem not enough?
        len = sceneLength()*3;
        break;
    default:
        return nullptr;
    }
    return result+len;
}

Enum::FaceDirection GameScene::objectOrientation(int fromX, int fromY, int toX, int toY)
{
    Enum::FaceDirection result;
    //North 0, 1; South 0, -1; West 1, 0; East -1, 0
    //NorthWest -> North; NorthEast -> East; SouthEast -> South; SouthWest -> West
    int dx = fromX - toX;
    int dy = fromY - toY;
    int ax = qAbs(dx);
    int ay = qAbs(dy);
    if((0 < dy) && (ax < dy || dx == dy)) result = Enum::FaceNorth;
    else if((0 < dx) && (ay <= dx)) result = Enum::FaceWest;
    else if((dy < 0) && (ax <= ay)) result = Enum::FaceSouth;
    else if((dx < 0) && (ay <= ax)) result = Enum::FaceEast;
    else result = Enum::FaceNone;
    return result;
}

Enum::FaceDirection GameScene::objectOrientation(int fromI, int toI)
{
    QPoint fromP = pointFromIndex(fromI);
    QPoint toP = pointFromIndex(toI);
    return objectOrientation(fromP.x(), fromP.y(), toP.x(), toP.y());
}

bool GameScene::placeObjectAt(Enum::SceneLayer layerId, qObjectId id, int i)
{
    GameObject *obj = GameObject::findObject(id, Enum::ObjectUnit | Enum::ObjectTerrain);
    if(!obj) return false;
    qSceneLayer layer = layerFromId(layerId);
    if(!layer) return false;
    if(i < 0 || sceneLength() <= i) return false;

    qObjectId *p_id = layer+i;
    if(*p_id == invalidId) {
        obj->setProperty("sceneId", objectId());
        QMetaObject::invokeMethod(obj, "movedByScene", Q_ARG(int, i));
        *p_id = id;

        connect(obj, SIGNAL(sceneMove_required(qObjectId,int,int)),
                this, SLOT(when_sceneMove_required(qObjectId,int,int)));
        emit object_added(layerId, id);
        return true;
    }
    else return false;
}

bool GameScene::replaceObjectAt(Enum::SceneLayer layerId, qObjectId id, int i)
{
    if(removeObjectOn(layerId, i)) {
        return placeObjectAt(layerId, id, i);
    }
    else return false;
}

bool GameScene::moveObjectTo(Enum::SceneLayer layerId, qObjectId id, int i)
{
    GameObject *obj = GameObject::findObject(id);
    if(!obj) return false;
    qSceneLayer layer = layerFromId(layerId);
    if(!layer) return false;
    if(i < 0 || sceneLength() <= i) return false;

    qObjectId *p_oid = layer+obj->property("sceneIndex").toInt();
    if(*p_oid == id) {
        qObjectId *p_id = layer+i;
        if(*p_id == invalidId) {
            *p_id = id;
            QMetaObject::invokeMethod(obj, "movedByScene", Q_ARG(int, i));
            *p_oid = invalidId;
            return true;
        }
    }
    return false;
}

bool GameScene::removeObjectOn(Enum::SceneLayer layerId, int i)
{
    qSceneLayer layer = layerFromId(layerId);
    if(!layer) return false;

    int *p_id = layer+i;
    GameObject *obj = GameObject::findObject(*p_id);

    obj->setProperty("sceneId", invalidId);
    *p_id = invalidId;
    disconnect(obj, SIGNAL(sceneMove_required(qObjectId,int,int)),
            this, SLOT(when_sceneMove_required(qObjectId,int,int)));
    emit object_removed(layerId, *p_id);
    return true;
}

void GameScene::allocateLayers(int count)
{
    Q_D(GameScene);
    if(d->mp_layerArray) freeLayers();

    int layerNum = layerNumber();
    int totalSize = layerNum * count;
//    if(sizeof(qObjectId) * totalSize <= qMapMem->blockSize()) {
//        d->m_useMempool = true;
//        d->mp_layerArray = (qSceneLayer)qMapMem->allocate();
//    }
//    else {
//        d->m_useMempool = false;
//        d->mp_layerArray = new qObjectId[totalSize];
//    }
    d->mp_layerArray = (qSceneLayer)qApp->sceneAlloc(totalSize * sizeof(qObjectId));
    //set each tile as invalidId
    memset(d->mp_layerArray, 0xff, sizeof(qObjectId) * totalSize);
}

void GameScene::freeLayers()
{
    Q_D(GameScene);
    qApp->sceneFree(d->mp_layerArray,
                    d->m_sceneCol * d->m_sceneRow * layerNumber());
//    if(d->m_useMempool && qMapMem) {
//        qMapMem->free(d->mp_layerArray);
//    }
//    else {
//        delete [] d->mp_layerArray;
//    }
    d->mp_layerArray = nullptr;
}

} // end namespace Qamer
