/*
 * File name: controllerevent.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "controllerevent.hpp"

namespace Qamer {

const QEvent::Type ControllerEvent::controllerEventType = (QEvent::Type)QEvent::registerEventType(QEvent::User+100);

#ifndef USE_QBYTEARRAY
ControllerEvent::ControllerEvent(int id, int role, const QString &name, qint64 value)
    :QEvent(controllerEventType),
      m_id(id), m_role(role), m_name(name), m_value(value)
{

}
#else
ControllerEvent::ControllerEvent(int id, int role, const QByteArray &name, qint64 value)
    :QEvent(controllerEventType),
      m_id(id), m_role(role), m_name(name), m_value(value)
{

}
#endif

ControllerEvent::~ControllerEvent()
{

}

int ControllerEvent::id() const
{
    return m_id;
}

int ControllerEvent::role() const
{
    return m_role;
}

#ifndef USE_QBYTEARRAY
QString ControllerEvent::name() const
{
    return m_name;
}
#else
QByteArray ControllerEvent::name() const
{
    return m_name;
}
#endif

qint64 ControllerEvent::value() const
{
    return m_value;
}

} // end namespace Qamer
