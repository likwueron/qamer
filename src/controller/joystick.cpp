/*
 *File name: joystick.cpp
 *Author: kwueron
 *E-mail: likwueron@gmail.com
 *Last revised: 2013/12/28
 *Published under Public Domain
 */

#include "joystick.hpp"
#include <SDL2/SDL.h>
#include <QStringList>
#include <QTimer>
#include <limits>
#include <QDebug>

namespace Qamer {

const int Joystick::defaultEventTimeout = 32; //about 30 actions per second
const int Joystick::defaultAutoRepeatDelay = 250;
const qint16 Joystick::defaultAxisDeadzone = 2000;
const qint16 Joystick::defaultAxisSensitivity = 200;

const qint16 Joystick::axisMax = std::numeric_limits<qint16>::max();
const qint16 Joystick::axisMin = std::numeric_limits<qint16>::min();

Joystick::Joystick(QObject *parent) :
    QObject(parent),
    m_state(NotOpened), mp_js(0x0)
{
}

Joystick::~Joystick()
{
    close();
}

Joystick *Joystick::createJoystick(int jid)
{
    if(!sm_doesSDLwork) {
        return 0x0;
    }

    int jsNum = sm_joystickList.count();
    if(!jsNum) {
        return 0x0;
    }
    if((jsNum <= jid) || (jid < 0)) {
        jid = 0;
    }

    Joystick *p_j = sm_joystickList.at(jid);
    int state = p_j->state();
    switch(state) {
    case HasOpened:
    {
        if(p_j->startUse()) {
            return p_j;
        }
        return 0x0;
    }
    case InUsed:
        return p_j;
        break;
    case CannotUsed:
    default:
        return 0x0;
        break;
    }
}

int Joystick::id()
{
    return sm_joystickList.indexOf(this);
}

bool Joystick::isValid() const
{
    return bool(mp_js);
}

void Joystick::stopUse()
{
    m_data.axisPosition.clear();
    m_data.buttonStatus.clear();
    m_data.hatDirection.clear();
    m_data.trackballDelta.clear();

    m_state = HasOpened;
}

SDL_JoystickGUID Joystick::guid() const
{
    return m_guid;
}

QString Joystick::layout() const
{
    if(!inState(HasOpened | InUsed)) {
        return QString();
    }
    return m_layoutCache;
}

QString Joystick::name() const
{
    return m_name;
}

int Joystick::state() const
{
    return m_state;
}

bool Joystick::inState(int state) const
{
    return (m_state & state);
}

qint16 Joystick::sensitivity(int axisId)
{
    return m_sensitivity.value(axisId, defaultAxisSensitivity);
}

qint16 Joystick::deadzone(int axisId)
{
    return m_deadzone.value(axisId, defaultAxisDeadzone);
}

void Joystick::setSensitivities(qint16 sensitivity)
{
    for(int i = 0; i < m_axisNum; i++) {
        m_sensitivity.replace(i, sensitivity);
    }
}

void Joystick::setSensitivity(int axisId, qint16 sensitivity)
{
    if(m_axisNum <= axisId || axisId < 0) {
        return;
    }
    m_sensitivity.replace(axisId, sensitivity);
}

void Joystick::setDeadzones(qint16 deadzone)
{
    for(int i = 0; i < m_axisNum; i++) {
        m_deadzone.replace(i, deadzone);
    }
}

void Joystick::setDeadzone(int axisId, qint16 deadzone)
{
    if(m_axisNum <= axisId || axisId < 0) {
        return;
    }
    m_deadzone.replace(axisId, deadzone);
}

bool Joystick::open(int id)
{
    //try to open
    mp_js = SDL_JoystickOpen(id);
    if(!isValid()) {
        qCritical() << "Joystick:" << sm_joystickList.at(id)->name() << "open failure!";
        m_state = CannotUsed;
        return false;
    }

    //collect information about joystick layout
    m_axisNum = SDL_JoystickNumAxes(mp_js);
    m_btnNum = SDL_JoystickNumButtons(mp_js);
    m_hatNum = SDL_JoystickNumHats(mp_js);
    m_trackballNum = SDL_JoystickNumBalls(mp_js);
    //generate layout string
    m_layoutCache = QString("%1%2%3%4")
            .arg(m_hatNum, 2, 10, QChar('0'))
            .arg(m_axisNum, 2, 10, QChar('0'))
            .arg(m_btnNum, 2, 10, QChar('0'))
            .arg(m_trackballNum, 2, 10, QChar('0'))
            /*.toLatin1()*/;

    m_state = HasOpened;
    return true;
}

bool Joystick::startUse()
{
    //Init data
    for(int i = 0; i < m_axisNum; i++) {
        qint16 originalPosition = SDL_JoystickGetAxis(mp_js, i);
        if(originalPosition == axisMin
                || originalPosition == axisMax) {
            //for RT/LT
            m_data.axisPosition << originalPosition;
        }
        else {
            m_data.axisPosition << 0;
        }
        m_sensitivity << defaultAxisSensitivity;
        m_deadzone << defaultAxisDeadzone;
    }
    m_axisReleasePosition = m_data.axisPosition;
    for(int i = 0; i < m_btnNum; i++) {
        m_data.buttonStatus << 0;
    }
    for(int i = 0; i < m_hatNum; i++) {
        m_data.hatDirection << SDL_HAT_CENTERED;
    }
    for(int i = 0; i < m_trackballNum; i++) {
        m_data.trackballDelta << 0 << 0;
    }
    //start timer
    if(!inUsedNum()) {
        connect(smp_commonTimer, SIGNAL(timeout()), this, SLOT(sdlUpdate()));
        smp_commonTimer->start(defaultEventTimeout);
    }
    connect(smp_commonTimer, SIGNAL(timeout()), this, SLOT(update()));

    m_state = InUsed;
    return true;
}

void Joystick::close()
{
    if(inState(InUsed | HasOpened)) {
        SDL_JoystickClose(mp_js);
        mp_js = 0x0;
        m_state = NotOpened;
    }
}

int Joystick::inUsedNum()
{
    int count = 0;
    QListIterator<Joystick*> i(sm_joystickList);
    while(i.hasNext()) {
        if(i.next()->inState(InUsed)) {
            count += 1;
        }
    }
    return count;
}

bool Joystick::isAxisPositionChange(qint16 curPos, int axisId) const
{
    qint32 prePos = axisPositions.at(axisId);
    qint32 delta = qAbs(qint32(curPos) - prePos);
    return m_sensitivity.at(axisId) < delta;
}

bool Joystick::isAxisInDeadzone(qint16 pos, int axisId) const
{
    qint16 original = m_axisReleasePosition.at(axisId);
    qint16 deadzone = m_deadzone.at(axisId);
    qint16 upperLimit = original + deadzone;
    if(upperLimit < original) {
        upperLimit = axisMax;
    }
    qint16 lowerLimit = original - deadzone;
    if(original < lowerLimit) {
        lowerLimit = axisMin;
    }
    return (lowerLimit <= pos && pos <= upperLimit);
}

bool Joystick::isButtonStatusChange(qint8 value, int buttonId) const
{
    return value != buttonStatus.at(buttonId);
}

bool Joystick::isHatDirectionChange(qint8 value, int hatId) const
{
    return value != m_data.hatDirection.at(hatId);
}

void Joystick::sdlUpdate()
{
    SDL_JoystickUpdate();
}

void Joystick::update()
{
    if(!SDL_JoystickGetAttached(mp_js)) {
        return;//CannotUsed
    }
    //Axes
    for(int i = 0; i < m_axisNum; i++) {
        qint16 pos = SDL_JoystickGetAxis(mp_js, i);
        if(isAxisPositionChange(pos, i)) {
            if(!isAxisInDeadzone(pos, i)) {
                //new pos not in deadzone
                m_data.axisPosition.replace(i, pos);
                emit axisValueChanged(i, pos);
            }
            else if(!isAxisInDeadzone(m_data.axisPosition.at(i), i)) {
                //old pos not in deadzone
                m_data.axisPosition.replace(i, m_axisReleasePosition.at(i));
                emit axisValueChanged(i, m_axisReleasePosition.at(i));
            }
        }
    }
    //Buttons
    for(int i = 0; i < m_btnNum; i++) {
        qint8 status = SDL_JoystickGetButton(mp_js, i);
        if(isButtonStatusChange(status, i)) {
            m_data.buttonStatus.replace(i, status);
            emit buttonValueChanged(i, status);
        }
    }
    //Hats
    for(int i = 0; i < m_hatNum; i++) {
        qint8 direction = SDL_JoystickGetHat(mp_js, i);
        if(isHatDirectionChange(direction, i)) {
            m_data.hatDirection.replace(i, direction);
            emit hatValueChanged(i, direction);
        }
    }
    //Trackball
    int _trackballNum = m_trackballNum * 2;
    for(int i = 0; i < _trackballNum; i+=2) {
        int dx, dy;
        if(SDL_JoystickGetBall(mp_js, i, &dx, &dy) == 0) {
            //m_data.trackballDelta.replace(i, dx);
            //m_data.trackballDelta.replace(i+1, dy);
            emit trackballValueChanged(i, dx, dy);
        }
    }
    //emit dataValueChanged(&m_data);
}

bool isSDL_JoystickGUIDEqual(const SDL_JoystickGUID &a, const SDL_JoystickGUID &b)
{
    //check SDL2/SDL_Joystick.h line 68 to line 70
    int count = sizeof(SDL_JoystickGUID);
    for(int i = 0; i < count; i++) {
        if(a.data[i] != b.data[i]) {
            return false;
        }
    }
    return true;
}

} // namespace Qamer
