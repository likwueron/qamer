/*
 * File name:
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_PLUGIN_BASTARDSTRINGCTOR_HPP
#define QAMER_PLUGIN_BASTARDSTRINGCTOR_HPP

#include "igeneral.hpp"
#include <QObject>

namespace Qamer {
namespace Plugin {

class BastardStringCtor : public QObject, public IGeneral
{
    Q_OBJECT
    Q_INTERFACES(Qamer::Plugin::IGeneral)
public:
    explicit BastardStringCtor(QObject *parent = 0);
    QByteArray pluginName() const;
    Qamer::Plugin::Type pluginType() const;
    QList<QByteArray> supportedNames() const;
    Qamer::Plugin::Type pluginTypeFromName(const QByteArray &name) const;
    QObject *createObject(const QByteArray &name,
                          const QVariantList &argv,
                          QObject *parent = 0) const;
    QVariant createVariant(const QByteArray &name,
                           const QVariantList &argv) const;
    VarCtorSignature variantCtor(const QByteArray &name) const;

private:
    static const QList<QByteArray> defaultNames;
    static const QList<Qamer::VarCtorSignature> defaultCtors;
};

} // end namespace Plugin
} // end namespace Qamer

#endif // QAMER_PLUGIN_BASTARDSTRINGCTOR_HPP
