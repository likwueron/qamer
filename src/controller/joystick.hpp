/*
 *File name: joystick.hpp
 *Author: kwueron
 *E-mail: likwueron@gmail.com
 *Last revised: 2013/12/28
 *Published under Public Domain
 */

#ifndef JOYSTICK_HPP
#define JOYSTICK_HPP

#include "../qamer_global.hpp"
#include "joystickdata.hpp"
#include <SDL2/SDL_joystick.h>
#include <QString>
#include <QMap>

namespace Qamer {

class Joystick : public JoystickData
{
public:
    enum State {
        NotOpened = 0x1,    //This Object has not open any SDL_Joystick
        HasOpened = 0x2,
        InUsed = 0x4,       //This Object has open a SDL_Joystick successfully
        CannotUsed = 0x10,  //SDL_Joystick open failure
        WillKeeped = 0xf000 //This Object will be keeped during refreshJoystickList()
    };

private:
    explicit Joystick();
    virtual ~Joystick();
public:
    static Joystick* createJoystick(int jid);

    int id();
    bool isValid() const;
    void stopUse();

    SDL_JoystickGUID guid() const;
    QString layout() const;
    QString name() const;
    int state() const;
    bool inState(int state) const;

    qint16 sensitivity(int axisId);
    qint16 deadzone(int axisId);

    //default value
    static const int defaultEventTimeout;
    static const int defaultAutoRepeatDelay;
    static const qint16 defaultAxisDeadzone;
    static const qint16 defaultAxisSensitivity;

    static const qint16 axisMax;
    static const qint16 axisMin;
public:
    void setSensitivities(qint16 sensitivity);
    void setSensitivity(int axisId, qint16 sensitivity);
    void setDeadzones(qint16 deadzone);
    void setDeadzone(int axisId, qint16 deadzone);

protected:
    bool open(int id);
    bool startUse();
    void close();

    static int inUsedNum();

    bool isAxisPositionChange(qint16 curPos, int axisId) const;
    bool isAxisInDeadzone(qint16 pos, int axisId) const;
    bool isButtonStatusChange(qint8 value, int buttonId) const;
    bool isHatDirectionChange(qint8 value, int hatId) const;

    SDL_JoystickGUID m_guid;
    int m_id;
    QString m_name;
    int m_state;

    SDL_Joystick *mp_js;

    qint8 m_axisNum;
    qint8 m_btnNum;
    qint8 m_hatNum;
    qint8 m_trackballNum;
    QString m_layoutCache;
    //
    QList<qint16> m_axisReleasePosition;
    //for axes calibration
    QList<qint32> m_sensitivity;
    QList<qint16> m_deadzone;

    static void sdlUpdate();
    void update();
};

bool isSDL_JoystickGUIDEqual(const SDL_JoystickGUID &a, const SDL_JoystickGUID &b);

} //namaspace Qamer

#endif // QAMER_JOYSTICK_HPP
