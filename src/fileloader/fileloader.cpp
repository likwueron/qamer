/*
 * File name: fileloader.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "fileloader.hpp"
#include "loadingitem.hpp"
#include "../core/dependinjector.hpp"
#include "../gameobject.hpp"
#include "../mics/platform.hpp"
#include "../plugin/loadhelper.hpp"

#include <QCoreApplication>
#include <QMetaObject>
#include <QMetaEnum>
#include <QFile>
#include <QDebug>
#include <QVariant>

namespace Qamer {

QList<Plugin::IGeneral*> FileLoader::sm_defaultCtorList;

FileLoader::FileLoader(QObject *parent)
    : QObject(parent), BaseTextReader(),
      up_di(0x0), m_asDIParent(false),
      m_loadEnum(false)

{
    resetDICreateObject();
    resetDICreateVariant();
    //load Qt default enums...
    addEnumByMetaObject("Qt", &staticQtMetaObject);
    addEnumByMetaObject("Qt.Font", &EnumQtFont::staticMetaObject);
}

FileLoader::~FileLoader()
{
    if(up_di && m_asDIParent) delete up_di;
}

void FileLoader::addEnumByMetaObject(const QByteArray &nameSpace, const QMetaObject *meta)
{
    m_metaMap.insert(nameSpace, meta);
}

void FileLoader::setDIAdmin(DependInjector *admin, bool asParent)
{
    if(up_di && m_asDIParent) delete up_di;
    m_asDIParent = asParent;
    up_di = admin;
}

void FileLoader::addDefaultCtorPlugin(Plugin::IGeneral *ctorObj)
{
    sm_defaultCtorList.prepend(ctorObj);
}

void FileLoader::loadFile(const QString &filename)
{
    while_newText_reading();
    if(!parent()) qCritical() << "Qamer::FileLoader: No parent to assign! This will cause memory leak.";

    mp_file = new QFile(filename);
    if(!mp_file->open(QIODevice::ReadOnly | QIODevice::Text)) {
        qCritical() << "Qamer::FileLoader: Open file failed";
        return;
    }
    m_trContext = genTrContext(filename);

    while(!mp_file->atEnd()) {
        char c = readCharFromFile();
        if(isIgnoreChar(c)) continue;
        if(replaceSpace(c)) continue;
        switch(c) {
        //those ignored in object section
        case ',':
        case ';':
        case ')':
        case '\n':
        case '}':
        case ':':
            break;
        case '{': //creat without parameter
        case '(': //with
            loadAsObject(assignBuffer());
            break;
        //case '.':
        default:
            bufferAppend(c);
            break;
        }
    }

    delete mp_file;
}

QByteArray FileLoader::context() const
{
    return m_trContext;
}

void FileLoader::setContext(const QByteArray &c)
{
    m_trContext = c;
}

bool FileLoader::useDICreateObject() const
{
    return m_useDICreateObject;
}

void FileLoader::setUseDICreateObject(bool set)
{
    m_useDICreateObject = set;
}

void FileLoader::resetDICreateObject()
{
    m_useDICreateObject = true;
}

bool FileLoader::useDICreateVariant() const
{
    return m_useDICreateVariant;
}

void FileLoader::setUseDICreateVariant(bool set)
{
    m_useDICreateVariant = set;
}

void FileLoader::resetDICreateVariant()
{
    m_useDICreateVariant = false;
    m_objectPropertyName.clear();
}

QByteArray FileLoader::objectPropertyName() const
{
    return m_objectPropertyName;
}

void FileLoader::setObjectPropertyName(const QByteArray &name)
{
    m_objectPropertyName = name;
}

void FileLoader::loadPlugin(const QByteArray &name)
{
    auto plugin = qPHelp->generalPlugin(name);
    if(plugin) m_ctorList.prepend(plugin);
}

void FileLoader::while_newText_reading()
{
    BaseTextReader::while_newText_reading();
    resetDICreateObject();
    resetDICreateVariant();
    m_ctorList = sm_defaultCtorList;
}

char FileLoader::readCharFromFile()
{
    if(m_loadEnum) {//force ending for property hold enum
        char temp = m_loadEnum;
        m_loadEnum = 0;
        switch(temp) {
        case '\n':
        case ';':
            return '\n';
            break;
        default:
            return temp;
        }
    }

    return BaseTextReader::readCharFromFile();
}

void FileLoader::loadAsObject(const QByteArray &objName)
{
    LoadingItem _d(LoadingItem::L_Obj, &m_loadingStack);
    QObject *o = nullptr;
    bool loadData = false;

    if(objName == "FILE") o = this;
    else if(objName == "APP") o = qApp;
    if(o) loadData = true;

    while(!mp_file->atEnd()) {
        char c = readCharFromFile();
        if(isIgnoreChar(c)) continue;
        if(replaceSpace(c)) continue;
        switch(c) {
        //extra ignore
        case '\n':
        case '.':
        case ';':
            break;
        //start collect data
        case '{':
            if(loadData) break;//error: multiple {
            if(!o) {
                qWarning() << "No object name assigned!" << mp_file->fileName() << "\n\t" << currentFileCursor();
                assignBuffer();
                ignoredUntil('}');
                return;
            }
            loadData = true;
            break;
        case ':':
            if(!loadData) break; //error: no object
            loadAsProperty(o, assignBuffer());
            break;
        case '(':
            if(!loadData) break;
            loadAsVarOFunc(o, assignBuffer());
            break;
        //end collect data
        case '}':
            return;
            break;
        //load string
        case '\"':
            if(loadData) break;
            loadAsString();
            break;
        //creat object
        case ')':
            if(o) break; //error multiple )
            //WILL BE load multi argv in future...
            o = createObject(objName, _d.exportVarList());
            if(!o) {
                qWarning() << "Cannot load object in file" << mp_file->fileName() << "\n\t" << currentFileCursor();
                assignBuffer();
                ignoredUntil('}');
                return;
            }
            break;
        default:
            if(!loadData) {} //error not correct char
            bufferAppend(c);
            break;
        }

    }
    qWarning() << QString("Qamer::FileLoader: Unknown Error while creating object \"%1\".")
                  .arg(QString(objName));
    if(o) delete o;
}

void FileLoader::loadAsProperty(QObject *obj, const QByteArray &name)
{
    LoadingItem _d(LoadingItem::L_Pro, &m_loadingStack);

    while(!mp_file->atEnd()) {
        char c = readCharFromFile();
        if(isIgnoreChar(c)) continue;
        if(replaceSpace(c)) continue;

        switch(c) {
        //those ignored in property section
        case '{':
        case '}':
        case ')':
            break;
        //load string
        case '\"':
            loadAsString();
            break;
        //load enum
        case '.':
            bufferAppend(c);
            loadAsEnumODouble();
            break;
        //load variant
        case '(':
            loadAsVarOFunc(nullptr, assignBuffer());
            break;
        //load another parameter
        case ',':
            //variant, string and enum already loaded
            _d.appendVariant(boolODouble(assignBuffer()));
            break;
        //end property section
        case ';':
        case '\n': {
            //variant, string and enum already loaded
            _d.appendVariant(boolODouble(assignBuffer()));
            QVariant v = _d.exportVariant();
            if(v.isValid()) {
                if(qobject_cast<GameObject*>(obj)) {
                    QMetaObject::invokeMethod(obj, "cf_addProperty",
                                              Q_ARG(QByteArray, name),
                                              Q_ARG(QVariant, v));
                }
                else obj->setProperty(name, v);
            }
            return;
            break; }
        default:
            bufferAppend(c);
            break;
        }
    }
}

void FileLoader::loadAsVarOFunc(QObject *obj, const QByteArray &name)
{
    LoadingItem _d( (obj) ? LoadingItem::L_Func : LoadingItem::L_Var, &m_loadingStack);

    while(!mp_file->atEnd()) {
        char c = readCharFromFile();
        if(isIgnoreChar(c)) continue;
        if(replaceSpace(c)) continue;

        switch(c) {
        //those ignored in variant section
        case '{':
        case '}':
        case ':':
        case ';':
        case '\n':
            break;
        //load string
        case '\"':
            loadAsString();
            break;
        //load enum
        case '.':
            bufferAppend(c);
            loadAsEnumODouble();
            break;
        //load another variant as parameter
        case '(':
            loadAsVarOFunc(nullptr, assignBuffer());
            break;
        //end section
        case ')':
            //variant, string and enum already loaded
            _d.appendVariant(boolODouble(assignBuffer()));
            switch(_d.loadingType()) {
            case LoadingItem::L_Func: {
                QVariantList vl = _d.exportVarList();
                callFunction(obj, name, vl);
                break; }
            case LoadingItem::L_Var:
                _d.appendVariant(createVariant(name, _d.exportVarList()));
                break;
            default:
                //error
                break;
            }
            return;
            break;
        //load another parameter
        case ',':
            //variant, string and enum already loaded
            _d.appendVariant(boolODouble(assignBuffer()));
            break;
        default:
            bufferAppend(c);
            break;
        }
    }
}

void FileLoader::loadAsString()
{
    LoadingItem _d(LoadingItem::L_Str, &m_loadingStack);
    //should append/return only 1 string
    while(!mp_file->atEnd()) {
        char c = readCharFromFile();
        switch(c) {
        case '?':
        case '|':
        case '\\':
        case '$':
        case '&':
        case '<':
        case '=':
        case '>':
            break;
        case '\"':
            _d.appendVariant(
                        platform_path(
                            QString::fromUtf8(assignBuffer())));
            return;
            break;
        default:
            bufferAppend(c);
            break;
        }
    }
}

void FileLoader::loadAsEnumODouble()
{
    LoadingItem _d(LoadingItem::L_Enum, &m_loadingStack);
    while(!mp_file->atEnd()) {
        char c = readCharFromFile();
        switch(c) {
        //combine enum
        case '|':
            _d.appendVariant(enumODouble(assignBuffer()));
            loadAsEnumODouble();
            return;
            break;
        case ' ':
            break;
        case '_':
        case '.':
            bufferAppend(c);
            break;
        //load as variant
        case '(':
            // this may cause error
            _d.setLoadingType(LoadingItem::L_Var);
            loadAsVarOFunc(nullptr, assignBuffer());
            return;
            break;
        //end
        case ')':
        case '\n':
        case ';':
        case ',': {//dex 44
            m_loadEnum = c;
            QVariant v = enumODouble(assignBuffer());
            if(v.type() == QVariant::Double) {
                _d.setLoadingType(LoadingItem::L_Num);
            }
            _d.appendVariant(v);
            return;
            break; }
        default:
            if((0 <= c && c < 48) ||
                    (58 <= c && c < 65) ||
                    (91 <= c && c < 97) ||
                    (123 <= c /*&& c < 128*/)) {
                //loading fail
                assignBuffer();
                qCritical() << "Qamer::FileLoader::loadAsEnumODouble: Unknown error";
                return;
            }
            else bufferAppend(c);
            break;
        }
    }
    qCritical() << "Qamer::FileLoader::loadAsEnumODouble: Unknown error";
}

void FileLoader::callFunction(QObject *obj, const QByteArray &funName, QVariantList &paras)
{
    const QMetaObject* meta = obj->metaObject();
    int count = meta->methodCount();
    bool success = false;
    for(int i = 0; i < count; i++) {
        QMetaMethod mm = meta->method(i);
        if(mm.methodType() == QMetaMethod::Signal) continue;
        //get function name
#if (QT_VERSION >= QT_VERSION_CHECK(5, 0, 0))
        QByteArray tFunName = mm.name();
#else
        QByteArray signature = mm.signature();
        //signature to function name...
        int index = signature.indexOf('(');
        QByteArray tFunName = signature.left(index);
#endif
        if(funName == tFunName) { //is function name match
            QList<QByteArray> tParasType = mm.parameterTypes();
            int pCount = paras.count();
            if(pCount == tParasType.count()) { // is parameters number match
                if(checkNConvert(paras, tParasType)) { // is parameters type match
                    QGenericArgument garg[10];
                    for(int i = 0; i < 10; i++) {
                        if(i < pCount) garg[i] =
                                QGenericArgument(tParasType.at(i).data(),
                                                 paras.at(i).data());
                    }
                    success = QMetaObject::invokeMethod(obj, funName, garg[0], garg[1],
                            garg[2], garg[3], garg[4], garg[5],
                            garg[6], garg[7], garg[8], garg[9]);
                    break;
                }
                else continue; // parameters type
            }
            else continue; // parameters number
        }
        else continue; // function name
    }

    if(!success) {
        qWarning() << "Cannot find method" << funName
                   << "with arguments" << paras
                   << "for object:" << obj;
    }
}

bool FileLoader::checkNConvert(QVariantList &vList, const QList<QByteArray> &tList)
{
    int count = vList.count();
    if(!count) return true;

    //name to type
    QList<int> tIdList;
    //check type
    for(int i = 0; i < count; i++) {
        int pId = vList.at(i).type();
        int tpId = QMetaType::type(tList.at(i));
        tIdList << tpId;
        if(pId != tpId) {
            //default number set as double which allow convert to int
            if(tpId == QMetaType::Int &&
                    pId == QMetaType::Double) ;
            else if(tpId == QMetaType::QPoint &&
                    pId == QMetaType::QPointF) ;
            else if(tpId == QMetaType::QRect &&
                    pId == QMetaType::QRectF) ;
            else if(tpId == QMetaType::QSize &&
                    pId == QMetaType::QSizeF) ;
            //default string set as qstring which allow convert to qbytearray
            else if(tpId == QMetaType::QByteArray &&
                    pId == QMetaType::QString) ;
            //enum will be void/Unknown which allow convert to int
#if (QT_VERSION >= QT_VERSION_CHECK(5, 0, 0))
            else if(tpId == QMetaType::UnknownType &&
                    pId == QMetaType::Int) ;
#else
            else if(tpId == QMetaType::Void &&
                    pId == QMetaType::Int) ;
#endif
            else return false;
        }
    }
    //convert
    for(int i = 0; i < count; i++) {
        vList[i].convert((QVariant::Type)tIdList.at(i));
    }

    return true;
}

bool FileLoader::isIgnoreChar(char c)
{
    switch(c) {
    //case '\n':
    case '$':
    case '%':
    case '&':
    case '\'': // '

    case '*':
    case '+':

    case '/':

    case '<':
    case '=':
    case '>':
    case '?':

    case '[':
    case '\\':
    case ']':

    case '|':

    case '~':
        return true;
        break;
    case '#':
        ignoredUntil('\n');
        return true;
        break;
    default:
        return false;
    }
}

QObject *FileLoader::getObject(const QString &sysName, const QByteArray &className)
{
    //try to find
    QObject *obj = GameObject::findObject(sysName);
    if(!obj) {
        //try di
        if(up_di && !className.isEmpty()) {
            obj = up_di->createInstance(className, parent());
            if(obj) obj->setObjectName(sysName);
        }
    }
    else obj = try_clone(qobject_cast<GameObject*>(obj));
    return obj;
}

QObject *FileLoader::createObject(const QByteArray &className, const QVariantList &argv)
{
    QObject *o = nullptr;
    //try plugin
    foreach(const Plugin::IGeneral* ctorObj, m_ctorList) {
        //try until we get object
        if((o = ctorObj->createObject(className,
                                      argv,
                                      parent()))
                ) {
            break;
        }
    }
    if(m_useDICreateObject &&
            !o && argv.count()) {
        o = getObject(argv.at(0).toString(), className);
        if(o) o->setParent(parent());
    }
    return o;
}

QVariant FileLoader::createVariant(const QByteArray &className,
                                   const QVariantList &argv)
{
    QVariant result;
    bool ok = false;
    //try di if enable
    if(m_useDICreateVariant &&
            up_di->m_classMap.contains(className) &&
            argv.count() == 1) {
        QString sysName = argv.at(0).toString();
        QObject *obj = getObject(sysName, className);
        if(obj) {
            if(!m_objectPropertyName.isEmpty()) {
                result = obj->property(m_objectPropertyName);
            }
            else if(GameObject *o = qobject_cast<GameObject*>(obj)) {
                result.setValue(o->objectId());
            }
            else result.setValue((int)(obj));
            ok = true;
        }
    }
    //try variant constructor
    if(!ok) {
        foreach(const Plugin::IGeneral* ctorObj, m_ctorList) {
            if(VarCtorSignature ctor = ctorObj->variantCtor(className)) {
                ok = true;
                result = ctor(argv);
            }
        }
    }
    //tr
    if(!ok && "qsTr" == className) {
        switch(argv.count()) {
        case 1:
            result = qApp->translate(m_trContext,
                                     argv.at(0).toByteArray());
            break;
        case 2:
            result = qApp->translate(m_trContext,
                                     argv.at(0).toByteArray(),
                                     argv.at(1).toByteArray());
            break;
        case 3: {
            QByteArray v1 = argv.at(1).toByteArray();
            const char* v1c = (v1.count()) ? v1.constData() : 0x0;
    #if (QT_VERSION >= QT_VERSION_CHECK(5, 0, 0))
            result = qApp->translate(m_trContext,
                                     argv.at(0).toByteArray(),
                                     v1c,
                                     argv.at(2).toInt());
    #else
            result = qApp->translate(m_trContext,
                                     argv.at(0).toByteArray(),
                                     v1c,
                                     QCoreApplication::UnicodeUTF8,
                                     argv.at(2).toInt());
    #endif
            break; }
        default:
            result = QString();
            break;
        }
    }
    else if(!ok && "List" == className) result = argv;

    if(result.isNull() && result.type() != QVariant::PointF) {
        qWarning() << "Cannot construct/find variant" << className << "with parameters" << argv;
    }

    return result;
}

QVariant FileLoader::useCtorPlugin(const QList<Plugin::IGeneral *> plugins,
                                   const QByteArray &name,
                                   const QVariantList &argv,
                                   bool *ok) const
{
    foreach(const Plugin::IGeneral* ctorObj, plugins) {
        if(VarCtorSignature ctor =
                ctorObj->variantCtor(name)) {
            if(ok) *ok = true;
            return ctor(argv);
        }
    }
    if(ok) *ok = false;
    return QVariant();
}

int FileLoader::valueFromEnum(const QByteArray &key)
{
    QByteArray truekey;
    QByteArray nameSpace;
    int dotPos = key.lastIndexOf('.');
    if(dotPos != -1) {
        nameSpace = key.left(dotPos);
        truekey = key.right(key.count() - dotPos - 1);
    }

    if(nameSpace.isEmpty()) {
        foreach(const QMetaObject* meta, m_metaMap) {
            int result = valueFromEnum_inner(key, meta);
            if(result != -1) return result;
        }
    }
    else {
        const QMetaObject* meta = m_metaMap.value(nameSpace, 0x0);
        if(meta) {
            return valueFromEnum_inner(truekey, meta);
        }
    }
    return -1;
}

int FileLoader::valueFromEnum_inner(const QByteArray &key, const QMetaObject* meta)
{
    int enumsCount = meta->enumeratorCount();
    for(int i = 0; i < enumsCount; i++) {
        QMetaEnum mEnum = meta->enumerator(i);
        int result = mEnum.keyToValue(key);
        if(result != -1) return result;
    }
    return -1;
}

} // end namespace Qamer
