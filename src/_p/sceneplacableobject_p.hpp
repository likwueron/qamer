/*
 * File name: sceneplacableobject_p.hpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_SCENEPLACABLEOBJECTPRIVATE_HPP
#define QAMER_SCENEPLACABLEOBJECTPRIVATE_HPP

#include "gameobject_p.hpp"
#include "pixmapdata_p.hpp"
#include "../sceneplacableobject.hpp"

namespace Qamer {

class QAMERSHARED_EXPORT ScenePlacableObjectPrivate : public GameObjectPrivate, public PixmapDataPrivate
{
    Q_DECLARE_PUBLIC(ScenePlacableObject)
public:
    ScenePlacableObjectPrivate();
    ~ScenePlacableObjectPrivate();

    //animation scheme
    void addScheme(quint16 id, const QList<quint16> &list);
    quint16 currentPixmapIndex() const;
    quint16 currentSchemeId() const;
    quint16 currentSchemeIndex() const;
    quint16 setScheme(quint16 id, quint16 index = 0);
    quint16 progressScheme();
    //
    void addSpriteSheet(const SpriteSheet &sheet);
    void setCurrentPixmapIndex(quint16 index);

protected:
    //clone
    ScenePlacableObjectPrivate(const ScenePlacableObjectPrivate &that);

//private:
    int m_sceneId;
    int m_sceneIndex;
};

} // end namespace Qamer

#endif // QAMER_SCENEPLACABLEOBJECTPRIVATE_HPP
