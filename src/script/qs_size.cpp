/*
 * File name: qs_size.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "qs_size.hpp"
#include "qs_general.hpp"
#include <QScriptEngine>
#include <QScriptContext>
#include <QScriptValue>

Q_DECLARE_METATYPE(QSizeF*)

namespace Qamer {
namespace QScript {

QS_Size::QS_Size(QObject *parent) :
    QObject(parent), QScriptable()
{
}

qreal QS_Size::width() const
{
    QSizeF s = qscriptvalue_cast<QSizeF>(thisObject());
    return s.width();
}

void QS_Size::setWidth(qreal width)
{
    QSizeF *s = qscriptvalue_cast<QSizeF*>(thisObject());
    if(s) s->setWidth(width);
    else throwTypeError(context(), "Size");
}

qreal QS_Size::height() const
{
    QSizeF s = qscriptvalue_cast<QSizeF>(thisObject());
    return s.height();
}

void QS_Size::setHeight(qreal height)
{
    QSizeF *s = qscriptvalue_cast<QSizeF*>(thisObject());
    if(s) s->setHeight(height);
    else throwTypeError(context(), "Size");
}

qreal QS_Size::area() const
{
    QSizeF s = qscriptvalue_cast<QSizeF>(thisObject());
    return s.height() * s.width();
}

bool QS_Size::isValid() const
{
    QSizeF s = qscriptvalue_cast<QSizeF>(thisObject());
    return s.isValid();
}

QSizeF QS_Size::boundedTo(const QSizeF &that)
{
    QSizeF s = qscriptvalue_cast<QSizeF>(thisObject());
    return s.boundedTo(that);
}

QSizeF QS_Size::expandedTo(const QSizeF &that)
{
    QSizeF s = qscriptvalue_cast<QSizeF>(thisObject());
    return s.expandedTo(that);
}

void QS_Size::scale(qreal width, qreal height, Qt::AspectRatioMode mode)
{
    QSizeF *s = qscriptvalue_cast<QSizeF*>(thisObject());
    if(s) s->scale(width, height, mode);
    else throwTypeError(context(), "Size");
}

void QS_Size::scale(const QSizeF &size, Qt::AspectRatioMode mode)
{
    QSizeF *s = qscriptvalue_cast<QSizeF*>(thisObject());
    if(s) s->scale(size, mode);
    else throwTypeError(context(), "Size");
}

void QS_Size::addBy(const QSizeF &that)
{
    QSizeF *s = qscriptvalue_cast<QSizeF*>(thisObject());
    if(s) *s += that;
    else throwTypeError(context(), "Size");
}

void QS_Size::subtractBy(const QSizeF &that)
{
    QSizeF *s = qscriptvalue_cast<QSizeF*>(thisObject());
    if(s) *s -= that;
    else throwTypeError(context(), "Size");
}

void QS_Size::multiplyBy(qreal var)
{
    QSizeF *s = qscriptvalue_cast<QSizeF*>(thisObject());
    if(s) *s *= var;
    else throwTypeError(context(), "Size");
}

void QS_Size::divideBy(qreal var)
{
    QSizeF *s = qscriptvalue_cast<QSizeF*>(thisObject());
    if(s) *s /= var;
    else throwTypeError(context(), "Size");
}

bool QS_Size::equal(const QSizeF &that)
{
    QSizeF s = qscriptvalue_cast<QSizeF>(thisObject());
    return s == that;
}

QSizeF QS_Size::add(const QSizeF &that) const
{
    QSizeF s = qscriptvalue_cast<QSizeF>(thisObject());
    return s + that;
}

QSizeF QS_Size::subtract(const QSizeF &that) const
{
    QSizeF s = qscriptvalue_cast<QSizeF>(thisObject());
    return s - that;
}

QSizeF QS_Size::multiply(qreal var) const
{
    QSizeF s = qscriptvalue_cast<QSizeF>(thisObject());
    return s * var;
}

QSizeF QS_Size::divide(qreal var) const
{
    QSizeF s = qscriptvalue_cast<QSizeF>(thisObject());
    return s / var;
}

QString QS_Size::toString() const
{
    QSizeF s = qscriptvalue_cast<QSizeF>(thisObject());
    return QString("Size(%1, %2)").arg(s.width()).arg(s.height());

}

void QS_Size::transpose()
{
    QSizeF *s = qscriptvalue_cast<QSizeF*>(thisObject());
    if(s) s->transpose();
    else throwTypeError(context(), "Size");
}

QScriptValue cf_Size_Size(QScriptContext *context, QScriptEngine *engine)
{
    int argc = context->argumentCount();
    if(argc < 2) {
        return engine->toScriptValue(QSizeF());
    }
    else {
        qreal w = context->argument(0).toNumber();
        qreal h = context->argument(1).toNumber();
        return engine->toScriptValue(QSizeF(w, h));
    }
}

} // end namespace QScript
} // end namespace Qamer
