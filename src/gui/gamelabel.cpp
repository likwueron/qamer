/*
 * File name: gamelabel.cpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "gamelabel.hpp"
#include <QFontMetrics>
#include <QPainter>
#include <QDebug>

namespace Qamer {

GameLabel::GameLabel(QObject *parent)
    : GameUI(parent)
{
    setSubObjectType(Enum::InterfaceLabel);
}

GameLabel::~GameLabel()
{

}

QString GameLabel::text() const
{
    return description();
}

void GameLabel::setText(const QString &text)
{
    setDescription(text);
    QFontMetrics fm(font());
    resize(fm.size(Qt::TextExpandTabs, text));
}

void GameLabel::painted(QPainter *painter, const QRect &updateRect)
{
    Q_UNUSED(updateRect)
    painter->save();
    painter->setFont(font());
    //draw label and content;
    QString allText = QString("%1: %2").arg(name(), text());
    QFontMetrics fm(font());
    QSize labelSize = fm.size(Qt::TextExpandTabs, allText);
    painter->drawText(QRect(position(), labelSize), allText);
    painter->restore();
}

} // namespace Qamer

