/*
 * File name: unit_p.hpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_UNITPRIVATE_HPP
#define QAMER_UNITPRIVATE_HPP

#include "sceneplacableobject_p.hpp"
#include "../unit.hpp"
#include "../statvalue.hpp"

namespace Qamer {

class QAMERSHARED_EXPORT UnitPrivate : public ScenePlacableObjectPrivate
{
    Q_DECLARE_PUBLIC(Unit)
public:
    UnitPrivate();
    ~UnitPrivate();

    static void* operator new(size_t size) throw();
    static void operator delete(void *p);

    StatValue statValue(const QByteArray &name) const;

protected:
    UnitPrivate(const UnitPrivate &that);

private:
    //owner
    qUnitSide m_side;
    //stat value
    QMap<QByteArray, StatValue> m_statValueMap;
    //facing
    Enum::FaceDirection m_facing;
    //is unique unit? like heros; a unique unit isnt clonable
    bool m_unique;
    //contains skill
    QList<qObjectId> m_skillIdList;
    //resistance to Status, Terrain and, etc
    QMap<int, int> m_resisMap; //id, resistance

    int m_movepoint;//move to statValue?
};

} // namespace Qamer

#endif // QAMER_UNITPRIVATE_HPP
