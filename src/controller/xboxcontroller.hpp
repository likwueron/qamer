#ifndef XBOXCONTROLLER_HPP
#define XBOXCONTROLLER_HPP

#include "controller.hpp"

class XBoxController : public Controller
{
    Q_OBJECT
public:
    enum ButtonBinding {
#ifndef Q_OS_WIN32
        Select = 6,
        Start = 7,
        A = 0,
        B = 1,
        X = 2,
        Y = 3,
        LB = 4,
        RB = 5,
        LS = 9,
        RS = 10,
        Guide = 8,
#else
        HatUp = 0,
        HatDown = 1,
        HatLeft = 2,
        HatRight = 3,
        Select = 5,
        Start = 4,
        A = 10,
        B = 11,
        X = 12,
        Y = 13,
        LB = 8,
        RB = 9,
        LS = 6,
        RS = 7,
        Guide = 14,
#endif
    };

    Q_INVOKABLE explicit XBoxController(QObject *parent = 0);
    
    QString layout() const;
    QString monitorUiFile() const;

    void when_hatValue_changed(int hatId, qint8 value);
    void when_buttonValue_changed(int buttonId, qint8 value);
    void when_axisValue_changed(int axisId, qint16 value);
    void when_trackballValue_changed(int trackballId, int dx, int dy);
    void when_dataValue_changed(const JoystickData *data);
signals:
    
public slots:
    
protected:
};

#endif // XBOXCONTROLLER_HPP
