/*
 * File name: qs_gameobject.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "qs_gameobject.hpp"
#include "../gameobject.hpp"
#include "qs_general.hpp"
#include "../gamescene.hpp"
#include <QScriptContext>
#include <QScriptEngine>
#include <QMetaObject>
#include <QMetaProperty>

Q_DECLARE_METATYPE(Qamer::GameObjectId*)

namespace Qamer {
namespace QScript {

QMap<int,QString> QS_GameObject::sm_typeTextMap;

QS_GameObject::QS_GameObject(QObject *parent)
    : QObject(parent), QScriptable()
{

}

QS_GameObject::~QS_GameObject()
{

}

void QS_GameObject::addTypeText(int type, const QString &text)
{
    sm_typeTextMap.insert(type, text);
}

void QS_GameObject::addSubTypeText(int type, int subType, const QString &text)
{
    QString mainText = sm_typeTextMap.value(type);
    if(!mainText.isEmpty()) {
        sm_typeTextMap.insert(type | subType, mainText.append('-').append(text));
    }
}

int QS_GameObject::id() const
{
    GameObjectId i = qscriptvalue_cast<GameObjectId>(thisObject());
    return i.id();
}

void QS_GameObject::setId(int id)
{
    GameObjectId *i = qscriptvalue_cast<GameObjectId*>(thisObject());
    if(i) i->setId(id);
}

QString QS_GameObject::systemName() const
{
    GameObject* obj = accessObject();
    if(obj) return obj->objectName();
    else return QString();
}

void QS_GameObject::setSystemName(const QString &name)
{
    GameObjectId *i = qscriptvalue_cast<GameObjectId*>(thisObject());
    if(i) i->setId(name);
}

int QS_GameObject::objectType() const
{
    GameObject *obj = accessObject();
    if(obj) return obj->objectType();
    else return Enum::ObjectUndefined;
}

int QS_GameObject::subObjectType() const
{
    GameObject *obj = accessObject();
    if(obj) return obj->subObjectType();
    else return Enum::ObjectUndefined;
}

void QS_GameObject::setSubObjectType(int subType)
{
    GameObject *obj = accessObject();
    if(obj) obj->setSubObjectType(subType);
}

bool QS_GameObject::isValid() const
{
    GameObjectId i = qscriptvalue_cast<GameObjectId>(thisObject());
    return i.isValid();
}

QVariant QS_GameObject::propertyD(const QString &name)
{
    GameObject* obj = accessObject();
    if(obj) {
        QByteArray array = name.toLatin1();
        QVariant v = obj->property(array);
        if(!v.isValid() && obj->isObjectType(Enum::ObjectUnit)) {
            //try statValue
            int val;
            bool ok = QMetaObject::invokeMethod(obj, "statCurrentValue", Q_RETURN_ARG(int, val), Q_ARG(QByteArray, array));
            if(ok) return QVariant(val);
        }
        else return v;
    }
    return QVariant();
}

void QS_GameObject::setPropertyD(const QString &name, const QVariant &value)
{
    GameObject* obj = accessObject();
    if(!obj) return;

    int error = obj->cf_addProperty(name.toLatin1(), value);
    switch(error) {
    case GameObject::CF_NoError:
        break;
    case GameObject::CF_ParaTypeError:
        context()->throwError(QScriptContext::UnknownError,
                              QString("\"%1\" is statValue, can only assign integer value.")
                              .arg(name));
        break;
    case GameObject::CF_AccessError:
    case GameObject::CF_ScriptableError:
        context()->throwError(QScriptContext::UnknownError,
                              QString("\"%1\" can not be changed in script.")
                              .arg(name));
        break;
    default:
        context()->throwError(QScriptContext::UnknownError,
                              "An unknown error occured while adding property.");
        break;
    }
}

QString QS_GameObject::name() const
{
    GameObject *obj = accessObject();
    if(obj) return obj->name();
    else return QString();
}

void QS_GameObject::setName(const QString &name)
{
    GameObject *obj = accessObject();
    if(obj) obj->setName(name);
}

QString QS_GameObject::description() const
{
    GameObject *obj = accessObject();
    if(obj) return obj->description();
    else return QString();
}

void QS_GameObject::setDescription(const QString &des)
{
    GameObject *obj = accessObject();
    if(obj) obj->setDescription(des);
}

QPointF QS_GameObject::position() const
{
    GameObject *obj = accessObject();
    if(obj) return QPointF(obj->position());
    else return QPointF();
}

void QS_GameObject::move(QPointF position, Enum::MoveMode mode)
{
    GameObject *obj = accessObject();
    if(obj) obj->move(position.toPoint(), mode);
}

QSizeF QS_GameObject::size() const
{
    GameObject *obj = accessObject();
    if(obj) return QSizeF(obj->size());
    else return QSizeF();
}

void QS_GameObject::resize(QSizeF size)
{
    GameObject *obj = accessObject();
    if(obj) return obj->resize(size.toSize());
}

QSizeF QS_GameObject::parentSize() const
{
    GameObject *obj = accessObject();
    if(obj) return obj->parentSize();
    else return QSizeF();
}

bool QS_GameObject::isVisible() const
{
    GameObject *obj = accessObject();
    if(obj) return obj->isVisible();
    else return false;
}

void QS_GameObject::setVisible(bool visible)
{
    GameObject *obj = accessObject();
    if(obj) obj->setVisible(visible);
}

void QS_GameObject::invertVisible()
{
    GameObject *obj = accessObject();
    if(obj) obj->invertVisible();
}

void QS_GameObject::addAnimationScheme(int id, const QVariantList &list)
{
    GameObject *obj = accessObject();
    if(obj) obj->addAnimationScheme(id, list);
}

void QS_GameObject::setCurrentAnimationScheme(int id, int index)
{
    GameObject *obj = accessObject();
    if(obj) obj->setCurrentAnimationScheme(id, index);
}

void QS_GameObject::addStatValue(const QString &name, int max, int min, int flags, int val)
{
    GameObject *obj = accessObject();
    if(!obj) return;

    int error = obj->isObjectType(Enum::ObjectUnit) ?
                GameObject::CF_NoError : GameObject::CF_TypeError;
    QMetaObject::invokeMethod(obj, "addStatValue", Q_RETURN_ARG(int, error),
                              Q_ARG(int, max), Q_ARG(int, min), Q_ARG(int, flags), Q_ARG(int, val));
    switch(error) {
    case GameObject::CF_NoError:
        break;
    case GameObject::CF_DynamicError:
        context()->throwError(QScriptContext::UnknownError,
                              QString("\"%1\" is static property, rewrite denial.")
                              .arg(name));
        break;
    case GameObject::CF_TypeError:
        throwTypeError(context(), "Unit");
        break;
    default:
        context()->throwError(QScriptContext::UnknownError,
                              "An unknown error occured while adding statValue.");
        break;
    }
}

int QS_GameObject::statCurrentValue(const QString &name) const
{
    GameObject *obj = accessObject();
    if(obj && obj->isObjectType(Enum::ObjectUnit)) {
        int result;
        QMetaObject::invokeMethod(obj, "statCurrentValue", Q_RETURN_ARG(int, result), Q_ARG(QByteArray, name.toLatin1()));
        return result;
    }
    else {
        throwTypeError(context(), "Unit");
        return INT_MIN;
    }
}

int QS_GameObject::statMaxValue(const QString &name) const
{
    GameObject *obj = accessObject();
    if(obj && obj->isObjectType(Enum::ObjectUnit)) {
        int result;
        QMetaObject::invokeMethod(obj, "statMaxValue", Q_RETURN_ARG(int, result), Q_ARG(QByteArray, name.toLatin1()));
        return result;
    }
    else {
        throwTypeError(context(), "Unit");
        return INT_MIN;
    }
}

int QS_GameObject::statMinValue(const QString &name) const
{
    GameObject *obj = accessObject();
    if(obj && obj->isObjectType(Enum::ObjectUnit)) {
        int result;
        QMetaObject::invokeMethod(obj, "statMinValue", Q_RETURN_ARG(int, result), Q_ARG(QByteArray, name.toLatin1()));
        return result;
    }
    else {
        throwTypeError(context(), "Unit");
        return INT_MIN;
    }
}

bool QS_GameObject::increaseStatValue(const QString &name, int val)
{
    GameObject *obj = accessObject();
    if(obj && obj->isObjectType(Enum::ObjectUnit)) {
        bool result;
        QMetaObject::invokeMethod(obj, "increaseStatValue", Q_RETURN_ARG(bool, result), Q_ARG(QByteArray, name.toLatin1()), Q_ARG(int, val));
        return result;
    }
    else {
        throwTypeError(context(), "Unit");
        return false;
    }
}

void QS_GameObject::resetStatValue(const QString &name)
{
    GameObject *obj = accessObject();
    if(obj && obj->isObjectType(Enum::ObjectUnit)) {
        QMetaObject::invokeMethod(obj, "resetStatValue", Q_ARG(QByteArray, name.toLatin1()));
    }
    else throwTypeError(context(), "Unit");
}

void QS_GameObject::setStatCurrentValue(const QString &name, int cur)
{
    GameObject *obj = accessObject();
    if(obj && obj->isObjectType(Enum::ObjectUnit)) {
        QMetaObject::invokeMethod(obj, "setStatCurrentValue", Q_ARG(QByteArray, name.toLatin1()), Q_ARG(int, cur));
    }
    else throwTypeError(context(), "Unit");
}

void QS_GameObject::setStatMaxValue(const QString &name, int max)
{
    GameObject *obj = accessObject();
    if(obj && obj->isObjectType(Enum::ObjectUnit)) {
        QMetaObject::invokeMethod(obj, "setStatMaxValue", Q_ARG(QByteArray, name.toLatin1()), Q_ARG(int, max));
    }
    else throwTypeError(context(), "Unit");
}

void QS_GameObject::setStatMinValue(const QString &name, int min)
{
    GameObject *obj = accessObject();
    if(obj && obj->isObjectType(Enum::ObjectUnit)) {
        QMetaObject::invokeMethod(obj, "setStatMinValue", Q_ARG(QByteArray, name.toLatin1()), Q_ARG(int, min));
    }
    else throwTypeError(context(), "Unit");
}

QPointF QS_GameObject::scenePoint() const
{
    GameObject *obj = accessObject();
    if(obj && obj->isObjectType(Enum::ObjectUnit)) {
        return obj->property("scenePoint").toPointF();
    }
    else {
        throwTypeError(context(), "Unit");
        return QPointF();
    }
}

void QS_GameObject::setScenePoint(QPointF point)
{
    GameObject *obj = accessObject();
    if(obj && obj->isObjectType(Enum::ObjectUnit)) {
        obj->setProperty("scenePoint", point.toPoint());
    }
    else throwTypeError(context(), "Unit");
}

int QS_GameObject::facing() const
{
    GameObject *obj = accessObject();
    if(obj && obj->isObjectType(Enum::ObjectUnit)) {
        return obj->property("facing").toInt();
    }
    else {
        throwTypeError(context(), "Unit");
        return -1;
    }
}

void QS_GameObject::setFacing(int facing)
{
    GameObject *obj = accessObject();
    if(obj && obj->isObjectType(Enum::ObjectUnit)) {
        obj->setProperty("facing", facing);
    }
    else throwTypeError(context(), "Unit");
}

void QS_GameObject::addSkill()
{
    GameObject *obj = accessObject();
    if(obj && obj->isObjectType(Enum::ObjectUnit)) {
        int argc = context()->argumentCount();
        if(0 < argc) {
            QScriptValue argv0 = context()->argument(0);
            GameObjectId i = qscriptvalue_cast<GameObjectId>(argv0);
            if(i.isValid()) {
                //is it safe to use typedef here?
                qObjectId sid = i.id();
                QMetaObject::invokeMethod(obj, "addSkill", Q_ARG(qObjectId, sid));
            }
            else throwArgvError(context(), "Unit.addSkill", "GameObject");
        }
        else throwArgcError(context(), "Unit.addSkill");
    }
    else throwTypeError(context(), "Unit");
}

QVariantList QS_GameObject::skillList(int subType) const
{
    GameObject *obj = accessObject();
    if(obj && obj->isObjectType(Enum::ObjectUnit)) {
        QVariantList list;
        QMetaObject::invokeMethod(obj, "skillList",
                                  Q_RETURN_ARG(QVariantList, list), Q_ARG(int, subType));
        return list;
    }
    else {
        throwTypeError(context(), "Unit");
        return QVariantList();
    }
}

bool QS_GameObject::pointWithinScene(QPointF point) const
{
    GameObject *obj = accessObject();
    if(obj && obj->isObjectType(Enum::ObjectScene)) {
        GameScene *scene = (GameScene*)obj;
        QPoint p = point.toPoint();
        return (0 <= p.x()) &&
                    (p.x() < scene->sceneCol()) &&
                    (0 <= p.y()) &&
                    (p.y() < scene->sceneRow());
    }
    else {
        throwTypeError(context(), "GameScene");
        return false;
    }
}

bool QS_GameObject::canPass(QPointF point) const
{
    GameObject *obj = accessObject();
    if(obj && obj->isObjectType(Enum::ObjectScene)) {
        GameScene *scene = (GameScene*)obj;
        return scene->canPass(point.toPoint());
    }
    else {
        throwTypeError(context(), "GameScene");
        return false;
    }
}

QString QS_GameObject::possibleTilesFinderName() const
{
    GameObject *obj = accessObject();
    if(obj && obj->isObjectType(Enum::ObjectSkill)) {
        return obj->property("possibleTilesFinder").toString();
    }
    else {
        throwTypeError(context(), "Skill");
        return QString();
    }
}

void QS_GameObject::setPossibleTilesFinderName(const QString &name)
{
    GameObject *obj = accessObject();
    if(obj && obj->isObjectType(Enum::ObjectSkill)) {
        obj->setProperty("possibleTilesFinder", name.toLatin1());
    }
    else throwTypeError(context(), "Skill");
}

QString QS_GameObject::effectTilesFinderName() const
{
    GameObject *obj = accessObject();
    if(obj && obj->isObjectType(Enum::ObjectSkill)) {
        return obj->property("effectTilesFinder").toString();
    }
    else {
        throwTypeError(context(), "Skill");
        return QString();
    }
}

void QS_GameObject::setEffectTilesFinderName(const QString &name)
{
    GameObject *obj = accessObject();
    if(obj && obj->isObjectType(Enum::ObjectSkill)) {
        obj->setProperty("effectTilesFinder", name.toLatin1());
    }
    else throwTypeError(context(), "Skill");
}

QString QS_GameObject::toString() const
{
    GameObject *obj = accessObject();
    if(obj) {
        int type = obj->objectType();
        QString typeText = sm_typeTextMap.value(type);
        if(typeText.isEmpty()) {
            typeText = sm_typeTextMap.value(type & Enum::ObjectMainTypeMask);
            if(!typeText.isEmpty()) {
                typeText.append(QString("-%1")
                                .arg(type & Enum::ObjectSubTypeMask));
            }
            else {
                typeText = QString("%1-%2")
                        .arg(type & Enum::ObjectMainTypeMask)
                        .arg(type & Enum::ObjectSubTypeMask);
            }
        }

        return QString("GameObject - id: %1; type: %2; systemName: %3")
                .arg(obj->objectId())
                .arg(typeText)
                .arg(obj->objectName());
    }
    else return QString("Invalid GameObject");
}

GameObject *QS_GameObject::accessObject() const
{
    GameObjectId i = qscriptvalue_cast<GameObjectId>(thisObject());
    if(i.isValid()) return GameObject::findObject(i.id());
    else return nullptr;
}

QScriptValue cf_GameObject_GameObject(QScriptContext *context, QScriptEngine *engine)
{
    int argc = context->argumentCount();
    if(argc < 1) {
        return engine->toScriptValue(GameObjectId());
    }
    else {
        int id = context->argument(0).toInt32();
        return engine->toScriptValue(GameObjectId(id));
    }
}
//static method
QScriptValue cf_GameObject_objectNumber
(QScriptContext *context, QScriptEngine *engine)
{
    Q_UNUSED(engine)
    int argc = context->argumentCount();
    Q_UNUSED(argc)
    return GameObject::objectNumber();
}

} // end namespace QScript
} // end namespace Qamer
