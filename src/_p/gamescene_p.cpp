/*
 * File name: gamescene_p.cpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "gamescene_p.hpp"

namespace Qamer {

GameScene *GameScenePrivate::smp_curScene = nullptr;

QList<int> GameScenePrivate::sm_layerOrder = {Enum::LayerUpper, Enum::LayerUnit, Enum::LayerNUO, Enum::LayerTerrain, Enum::LayerEvent};

GameScenePrivate::GameScenePrivate()
    : m_sceneCol(0), m_sceneRow(0),
      m_cursorCol(-1), m_cursorRow(-1),
      m_hTileW(GameScene::defaultTileWidth / 2), m_hTileH(GameScene::defaultTileHeight / 2),
      m_useMempool(false), mp_layerArray(nullptr),
      mp_finders(nullptr)
{

}

GameScenePrivate::~GameScenePrivate()
{

}

} // namespace Qamer

