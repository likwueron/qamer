/*
 * File name: diplomacy.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_DIPLOMACY_HPP
#define QAMER_DIPLOMACY_HPP

#include "qamer_global.hpp"

namespace Qamer {

class QAMERSHARED_EXPORT Diplomacy
{
private:
    Diplomacy();
    ~Diplomacy();
public:
    static bool init(int playerNumber);
    static bool isInit();
    static void quit();
    static Enum::UnitRelationship diplomacy(int sideId1, int sideId2);
    static void setDiplomacy(int sideId1, int sideId2, Enum::UnitRelationship dip);
private:
    void setId(int id);
    int indexFromId(int id) const;
    int idFromIndex(int index) const;

    static int sm_playerNumebr;
    static Diplomacy* smp_diplomacyArray;

    int m_playerId;
    Enum::UnitRelationship* mp_relationArray;
};

} // end namespace Qamer

#endif // QAMER_DIPLOMACY_HPP
