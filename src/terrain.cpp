/*
 * File name: terrain.cpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "terrain.hpp"
#include "_p/terrain_p.hpp"
#include <QPainter>
#include <QDebug>

namespace Qamer {

Terrain::Terrain(QObject *parent)
    : ScenePlacableObject(new TerrainPrivate, Enum::ObjectTerrain, parent)
{

}

Terrain::~Terrain()
{

}

bool Terrain::multiPlace() const
{
    return true;
}

void Terrain::painted(QPainter *painter, int x, int y)
{
    Q_D(Terrain);
    if(d->hasSheet(0)) {
        d->sheets_default().at(0).draw(painter, x, y, d->currentPixmapIndex());
    }
}

Terrain::Terrain(const Terrain &that)
    : ScenePlacableObject(new TerrainPrivate(*(that.d_func())), Enum::ObjectCopy, that.parent())
{

}

} // namespace Qamer

