#include "../qamer_global.hpp"
#include <QList>

#ifndef QAMER_JOYSTICKDATA_HPP
#define QAMER_JOYSTICKDATA_HPP

namespace Qamer {

struct QAMERSHARED_EXPORT JoystickData
{
    QList<qint16> axisPositions;
    QList<qint8> buttonValues;
    QList<qint8> hatDirections;
    QList<int> trackballDxes;
    QList<int> trackballDys;
};

} //namespace Qamer

#endif //QAMER_JOYSTICKDATA_HPP
