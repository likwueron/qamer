/*
 * File name: diplomacy.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "diplomacy.hpp"

namespace Qamer {

int Diplomacy::sm_playerNumebr = 0;
Diplomacy* Diplomacy::smp_diplomacyArray = 0x0;

Diplomacy::Diplomacy()
    :mp_relationArray(0x0)
{
}

Diplomacy::~Diplomacy()
{
    delete [] mp_relationArray;
    mp_relationArray = 0x0;
}

bool Diplomacy::init(int playerNumber)
{
    if(playerNumber <= 0) return false;

    if(sm_playerNumebr) quit();

    sm_playerNumebr = playerNumber;

    smp_diplomacyArray = new Diplomacy[sm_playerNumebr];
    for(int i = 0; i < sm_playerNumebr; i++) {
        (smp_diplomacyArray+i)->setId(i);
    }
    return true;
}

bool Diplomacy::isInit()
{
    if(sm_playerNumebr) return true;
    else return false;
}

void Diplomacy::quit()
{
    delete [] smp_diplomacyArray;
    sm_playerNumebr = 0;
    smp_diplomacyArray = 0x0;
}

Enum::UnitRelationship Diplomacy::diplomacy(int sideId1, int sideId2)
{
    //just check p1 to p2
    if(sm_playerNumebr <= sideId1 || sideId1 < 0 ||
            sm_playerNumebr <= sideId2 || sideId2 < 0 ||
            sideId1 == sideId2) return Enum::RelateUnknown;

    Diplomacy *p1 = smp_diplomacyArray+sideId1;
    return *(p1->mp_relationArray+p1->indexFromId(sideId2));
}

void Diplomacy::setDiplomacy(int sideId1, int sideId2, Enum::UnitRelationship dip)
{
    if(sideId1 < sm_playerNumebr || sideId1 < 0 ||
            sideId2 < sm_playerNumebr || sideId2 < 0 ||
            sideId1 == sideId2) return;

    Diplomacy *p1 = smp_diplomacyArray+sideId1;
    Diplomacy *p2 = smp_diplomacyArray+sideId2;
    //currently just set for both side
    *(p1->mp_relationArray+p1->indexFromId(sideId2)) = dip;
    *(p2->mp_relationArray+p2->indexFromId(sideId1)) = dip;
}

void Diplomacy::setId(int id)
{
    m_playerId = id;

    int count = sm_playerNumebr - 1;
    mp_relationArray = new Enum::UnitRelationship[count];
    for(int i = 0; i < count; i++) {
        *(mp_relationArray+i) = Enum::RelateEnemy;
    }
}

int Diplomacy::indexFromId(int id) const
{
    //for player 2
    //id:   0 1 3 4 5...
    //index:0 1 2 3 4...
    int index = id;
    if(m_playerId <= index) index -= 1;
    if(index < 0) index = sm_playerNumebr - 1;

    return index;
}

int Diplomacy::idFromIndex(int index) const
{
    int id = index;
    if(id <= m_playerId) id += 1;
    if(sm_playerNumebr-1 < id) id = 0;

    return id;
}

} // end namespace Qamer
