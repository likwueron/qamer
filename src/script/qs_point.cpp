/*
 * File name: qs_point.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "qs_point.hpp"
#include "qs_general.hpp"
#include <QScriptContext>
#include <QScriptEngine>

Q_DECLARE_METATYPE(QPointF*)

namespace Qamer {
namespace QScript {

QS_Point::QS_Point(QObject *parent) :
    QObject(parent), QScriptable()
{
}

qreal QS_Point::x() const
{
    QPointF p = qscriptvalue_cast<QPointF>(thisObject());
    return p.x();
}

void QS_Point::setX(qreal num) const
{
    QPointF *p = qscriptvalue_cast<QPointF*>(thisObject());
    if(p) p->setX(num);
    else throwTypeError(context(), "Point");
}

qreal QS_Point::y() const
{
    QPointF p = qscriptvalue_cast<QPointF>(thisObject());
    return p.y();
}

void QS_Point::setY(qreal num) const
{
    QPointF *p = qscriptvalue_cast<QPointF*>(thisObject());
    if(p) p->setY(num);
    else throwTypeError(context(), "Point");
}

bool QS_Point::isValid() const
{
    QPointF p = qscriptvalue_cast<QPointF>(thisObject());
    return !p.isNull();
}

qreal QS_Point::manhattanLength() const
{
    QPointF p = qscriptvalue_cast<QPointF>(thisObject());
    return p.manhattanLength();
}

void QS_Point::addBy(const QPointF &that)
{
    QPointF *p = qscriptvalue_cast<QPointF*>(thisObject());
    if(p) *p += that.toPoint();
    else throwTypeError(context(), "Point");
}

void QS_Point::subtractBy(const QPointF &that)
{
    QPointF *p = qscriptvalue_cast<QPointF*>(thisObject());
    if(p) *p -= that.toPoint();
    else throwTypeError(context(), "Point");
}

void QS_Point::multiplyBy(qreal var)
{
    QPointF *p = qscriptvalue_cast<QPointF*>(thisObject());
    if(p) *p *= var;
    else throwTypeError(context(), "Point");
}

void QS_Point::divideBy(qreal var)
{
    QPointF *p = qscriptvalue_cast<QPointF*>(thisObject());
    if(p) *p /= var;
    else throwTypeError(context(), "Point");
}

QPointF QS_Point::add(const QPointF &that) const
{
    QPointF p = qscriptvalue_cast<QPointF>(thisObject());
    return p + that;
}

QPointF QS_Point::subtract(const QPointF &that) const
{
    QPointF p = qscriptvalue_cast<QPointF>(thisObject());
    return p - that;
}

QPointF QS_Point::multiply(qreal var) const
{
    QPointF p = qscriptvalue_cast<QPointF>(thisObject());
    return p * var;
}

QPointF QS_Point::divide(qreal var) const
{
    QPointF p = qscriptvalue_cast<QPointF>(thisObject());
    return p / var;
}

QString QS_Point::toString() const
{
    QPointF p = qscriptvalue_cast<QPointF>(thisObject());
    return QString("Point(%1, %2)").arg(p.x()).arg(p.y());
}

QScriptValue cf_Point_Point(QScriptContext *context, QScriptEngine *engine)
{
    int argc = context->argumentCount();
    if(argc < 2) {
        return engine->toScriptValue(QPointF());
    }
    else {
        qreal x = context->argument(0).toNumber();
        qreal y = context->argument(1).toNumber();
        return engine->toScriptValue(QPointF(x, y));
    }
}

} // end namespace QScript
} // end namespace Qamer
