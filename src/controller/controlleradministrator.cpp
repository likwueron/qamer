/*
 *File name: controlleradministrator.cpp
 *Author: kwueron
 *E-mail: %EMAIL%
 *Last revised: 2014/4/11
 *Published under Public Domain
 */

#include "controlleradministrator.hpp"
#include "joystick.hpp"
#include <QDebug>

namespace Qamer {

ControllerAdministrator *ControllerAdministrator::smp_instance = 0x0;

ControllerAdministrator::ControllerAdministrator(QObject *parent)
    : QObject(parent), m_timerId(-1)
{
}

ControllerAdministrator::~ControllerAdministrator()
{
    smp_instance = 0x0;
    quit();
}

ControllerAdministrator *ControllerAdministrator::instance(QObject *parent)
{
    if(!smp_instance) {
        smp_instance = new ControllerAdministrator(parent);
        smp_instance->init();
    }
    return smp_instance;
}

void ControllerAdministrator::init()
{
    //should be thread-safe but NOT!
    if(!SDL_WasInit(SDL_INIT_JOYSTICK)) {
        if(SDL_Init(SDL_INIT_JOYSTICK)) {
            m_sdlWork = false;
            qCritical() << "Error while initializing SDL Joystick function: " << SDL_GetError();
            return;
        }
        m_sdlWork = true;
        refresh();
    }
}

void ControllerAdministrator::quit()
{
    //should be thread-safe but NOT!
    if(SDL_WasInit(SDL_INIT_JOYSTICK)) {
        foreach(SDL_Joystick *p_j, m_joystickList) {
            SDL_JoystickClose(p_j);
        }
        m_joystickList.clear();

        m_commonTimer.stop();
        SDL_Quit();
        m_sdlWork = false;
    }
}

void ControllerAdministrator::refresh()
{
    //should be thread-safe but NOT!
    if(!m_sdlWork) {
        return;
    }

//    SDL_JoystickUpdate();
//    int jsNum = SDL_NumJoysticks();
//    QList<Joystick*> tempList;
//    int oldNum = sm_joystickList.count();
//    for(int i = 0; i < jsNum; i++) {
//        Joystick *p_j = 0x0;
//        SDL_JoystickGUID newGuid = SDL_JoystickGetDeviceGUID(i);
//        //compare with old one...
//        if(oldNum && (p_j = findJoystickByGuid(newGuid))) {
//            p_j->m_state |= WillKeeped;
//            if(p_j->inState(CannotUsed)) {
//                p_j->open(i);
//            }
//        }
//        else {
//            p_j = new Joystick;
//            p_j->m_name = SDL_JoystickNameForIndex(i);
//            p_j->m_guid = newGuid;
//            p_j->open(i);
//        }
//        tempList << p_j;
//    }
//    //Delete old joystick
//    foreach(Joystick *p_j, sm_joystickList) {
//        if(p_j->inState(WillKeeped)) {
//            p_j->m_state ^= WillKeeped;//remove flags
//        }
//        else {
//            delete p_j;
//        }
//    }
    //    m_joystickList = tempList;
}

void ControllerAdministrator::setUpdateTime(int millisecond)
{
    if(m_timerId != -1) {
        killTimer(m_timerId);
    }
    m_timerId = startTimer(millisecond);
}

void ControllerAdministrator::timerEvent(QTimerEvent *)
{
    Joystick::sdlUpdate();
    int jnum = m_joystickList.count();
    for(int i = 0; i < jnum; i++) {
        m_joystickList.at(i)->update();
    }


}

ControllerAdministrator::Joystick::Joystick()
{

}

ControllerAdministrator::Joystick::~Joystick()
{

}

} //namespace Qamer
