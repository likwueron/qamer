/*
 * File name: qs_point.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_QSCRIPT_POINT_HPP
#define QAMER_QSCRIPT_POINT_HPP

#include "../qamer_export.hpp"
#include <QObject>
#include <QScriptable>
#include <QPointF>

class QScriptEngine;

namespace Qamer {
namespace QScript {

class QAMERSHARED_EXPORT QS_Point : public QObject, protected QScriptable
{
    Q_OBJECT
    Q_PROPERTY(qreal x READ x WRITE setX)
    Q_PROPERTY(qreal y READ y WRITE setY)
public:
    explicit QS_Point(QObject *parent = 0);

    qreal x() const;
    void setX(qreal num) const;
    qreal y() const;
    void setY(qreal num) const;

    Q_INVOKABLE bool isValid() const;
    Q_INVOKABLE qreal manhattanLength() const;

    Q_INVOKABLE void addBy(const QPointF &that);
    Q_INVOKABLE void subtractBy(const QPointF &that);
    Q_INVOKABLE void multiplyBy(qreal var);
    Q_INVOKABLE void divideBy(qreal var);

    Q_INVOKABLE QPointF add(const QPointF &that) const;
    Q_INVOKABLE QPointF subtract(const QPointF &that) const;
    Q_INVOKABLE QPointF multiply(qreal var) const;
    Q_INVOKABLE QPointF divide(qreal var) const;
signals:

public slots:
    QString toString() const;
};

QAMERSHARED_EXPORT QScriptValue cf_Point_Point(QScriptContext *context, QScriptEngine *engine);

} // end namespace QScript
} // end namespace Qamer

#endif // QAMER_QSCRIPT_POINT_HPP
