/*
 * File name: luascriptengine.hpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_LUASCRIPTENGINE_HPP
#define QAMER_LUASCRIPTENGINE_HPP

#include "../qamer_export.hpp"
#include <QObject>
#include <QtLua/Value>
#include <QtLua/String>

namespace QtLua {
class State;
class Console;
class Function;
}

namespace Qamer {

typedef int (*LuaFuncSignature)(lua_State*);
//typedef QtLua::Value LuaValue;
//typedef QtLua::ValueBase::List LuaValueList;

class QAMERSHARED_EXPORT LuaScriptEngine : public QObject
{
    Q_OBJECT
public:
    LuaScriptEngine(QObject *parent = 0);
    ~LuaScriptEngine();

    QtLua::State* state() const;
    lua_State* luaState() const;

    bool evaluate(const QString &script);
    bool evaluateFile(const QString &filename);

    QtLua::Value luaValue(const QtLua::String &name) const;
    QtLua::ValueRef luaValue(const QtLua::String &name);
    QtLua::ValueRef setLuaValue(const QtLua::String &name,
                                const QtLua::Value &value);

    QtLua::Value newQObject(QObject *object,
                            bool takeOwnership = false,
                            bool canReparent = true);
    inline QtLua::ValueRef newQObject(const QtLua::String &name,
                                      QObject *object,
                                      bool takeOwnership = false,
                                      bool canReparent = true);

    QtLua::Value newQVariant(const QVariant &value);
    inline QtLua::ValueRef newQVariant(const QtLua::String &name,
                                       const QVariant &value);

    QtLua::Value newTable();

    void newFunction(const QtLua::String &name,
                     QtLua::Function *func);
//    void newFunction(const QtLua::String &name,
//                     LuaFuncSignature func);

    void newQMetaObject(const QMetaObject &meta,
                        bool autoSetProperty = false,
                        const QMetaObject &supremeMeta = QObject::staticMetaObject);
    void enableAutoProperty(const QMetaObject &meta, bool set);
    void newStaticFunction(const QMetaObject &meta,
                           const QtLua::String &name,
                           QtLua::FunctionSignature func,
                           const QList<QtLua::String> &argv = QList<QtLua::String>());
    inline void newNewOperator(const QMetaObject &meta,
                               QtLua::FunctionSignature func,
                               const QList<QtLua::String> &argv = QList<QtLua::String>());

    QtLua::Console *debugConsole();
    static void messageToConsole(const QtLua::String &msg);

public slots:
    void when_gc_required();

protected:
    static LuaScriptEngine *instanceFromState(QtLua::State *state);

private:
    static QList<LuaScriptEngine*> sm_instanceList;

    QtLua::State *mp_state;
    QtLua::Console *mp_console;

    QList<QtLua::Function*> m_funcList;
};

QtLua::ValueRef LuaScriptEngine::newQObject(const QtLua::String &name,
                                            QObject *object,
                                            bool takeOwnership,
                                            bool canReparent)
{
    return setLuaValue(name,
                       newQObject(object,
                                  takeOwnership,
                                  canReparent)
                       );
}

QtLua::ValueRef LuaScriptEngine::newQVariant(const QtLua::String &name,
                                             const QVariant &value)
{
    return setLuaValue(name, newQVariant(value));
}

void LuaScriptEngine::newNewOperator(const QMetaObject &meta,
                                     QtLua::FunctionSignature func,
                                     const QList<QtLua::String> &argv)
{
    newStaticFunction(meta, "new", func, argv);
}

} // end namespace Qamer

#endif // QAMER_LUASCRIPTENGINE_HPP
