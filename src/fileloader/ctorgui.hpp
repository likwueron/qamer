/*
 * File name: ctorgui.hpp
 * Copyright 2015 kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_LOADCTOR_CTORGUI_HPP
#define QAMER_LOADCTOR_CTORGUI_HPP

#include "../qamer_export.hpp"
#include <QVariant>
#include <QVariantList>

namespace Qamer {
namespace LoadCtor {

QAMERSHARED_EXPORT QVariant Brush(const QVariantList &values);
QAMERSHARED_EXPORT QVariant Color(const QVariantList &values);
QAMERSHARED_EXPORT QVariant Cursor(const QVariantList &values);
QAMERSHARED_EXPORT QVariant EasingCurve(const QVariantList &values);
QAMERSHARED_EXPORT QVariant SizePolicy(const QVariantList &values);
QAMERSHARED_EXPORT QVariant Font(const QVariantList &values);
QAMERSHARED_EXPORT QVariant Keysequence(const QVariantList &values);
QAMERSHARED_EXPORT QVariant TextFormat(const QVariantList &values);
QAMERSHARED_EXPORT QVariant TextLength(const QVariantList &values);
QAMERSHARED_EXPORT QVariant Pen(const QVariantList &values);
QAMERSHARED_EXPORT QVariant Palette(const QVariantList &values);

} // end namespace LoadCtor
} // namespace Qamer

#endif // QAMER_LOADCTOR_CTORGUI_HPP
