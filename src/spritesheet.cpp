/*
 * File name: spritesheet.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "spritesheet.hpp"
#include <QImage>
#include <QBitmap>
#include <QPainter>
#include <QPixmapCache>
#include <QDebug>

namespace Qamer {

SpriteSheet::SpriteSheet()
    : m_rowMax(1), m_colMax(1), m_w(-1), m_h(-1)
{

}

SpriteSheet::SpriteSheet(const SpriteSheet &that)
    : m_pix(that.m_pix),
      m_rowMax(that.m_rowMax), m_colMax(that.m_colMax),
      m_w(that.m_w), m_h(that.m_h)
{

}

SpriteSheet::~SpriteSheet()
{

}

bool SpriteSheet::load(const QString &fileName, int row, int col,
                             bool applyMask, const QColor &maskColor)
{
    if(!loading(fileName, applyMask, maskColor)) return false;
    //use it's own clip setting
    resetClip(row, col);
    return true;
}

bool SpriteSheet::loadFixsize(const QString &fileName, int w, int h,
                                    bool applyMask, const QColor &maskColor)
{
    if(!loading(fileName, applyMask, maskColor)) return false;
    if(0 < w && 0 < h) {
        m_colMax = m_pix.width() / w;
        m_rowMax = m_pix.height() / h;

        m_w = w - (m_pix.width() % w);
        m_h = h - (m_pix.height() % h);
    }
    return true;
}

void SpriteSheet::resetClip(int row, int col)
{
    if(m_pix.isNull()) return;
    if(0 < row && 0 < col) {
        m_rowMax = row;
        m_colMax = col;
        m_w = m_pix.width() / m_colMax;
        m_h = m_pix.height() / m_rowMax;
    }
    else {
        m_rowMax = 1;
        m_colMax = 1;
        m_w = m_pix.width();
        m_h = m_pix.height();
    }
}

bool SpriteSheet::isValid() const
{
    return !m_pix.isNull();
}

QSize SpriteSheet::clipSize() const
{
    return QSize(m_w, m_h);
}

void SpriteSheet::setMask(const QColor &color)
{
    if(m_pix.isNull()) return;
    //depend on image type, you should use another function to get pixel data
    QColor _maskColor = color.isValid() ? color : m_pix.toImage().pixel(0, 0);
    QBitmap mask = m_pix.createMaskFromColor(_maskColor, Qt::MaskInColor);
    m_pix.setMask(mask);
}

void SpriteSheet::draw(QPainter *p_painter, int atX, int atY, int col, int row) const
{
    if(isValid()) {
        p_painter->drawPixmap(atX, atY, m_pix, col * m_w, row * m_h, m_w, m_h);
    }
}

bool SpriteSheet::loading(const QString &fileName, bool applyMask, const QColor &maskColor)
{
    if(!QPixmapCache::find(fileName, &m_pix)) {
        m_pix.load(fileName);
        if(m_pix.isNull()) {
            m_rowMax = m_colMax = 1;
            m_w = m_h = -1;
            qWarning() << QString("Qamer::SpriteSheet: Cannot load image \"%1\", not supported format or not existed")
                         .arg(fileName);
            return false;
        }
        if(applyMask) setMask(maskColor);
        QPixmapCache::insert(fileName, m_pix);
    }
    return true;
}

void SpriteSheet::indexToPoint(int index, int &col, int &row) const
{
    col = index % m_colMax;
    row = (index - col) / m_colMax;
}

} //end namespace Qamer
