/*
 * File name: rhombus.hpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "../qamer_global.hpp"
#include <QPolygon>
#include <QPoint>
#include <QColor>

#ifndef RHOMBUS_HPP
#define RHOMBUS_HPP

class QAMERSHARED_EXPORT Rhombus
{
public:
    Rhombus();
    Rhombus(QPoint at, QSize size);

    bool isEmpty() const;

    void setSize(const QSize size);
    void setSize(int w, int h);
    void moveTo(const QPoint point);
    void move(const QPoint point);

    QPolygon toPolygon() const;
private:
    QSize m_size;

    QPoint m_up;
    QPoint m_left;
    QPoint m_right;
    QPoint m_down;
};

#endif // RHOMBUS_HPP
