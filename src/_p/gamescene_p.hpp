/*
 * File name: gamescene_p.hpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_GAMESCENEPRIVATE_HPP
#define QAMER_GAMESCENEPRIVATE_HPP

#include "../qamer_export.hpp"
#include "gameobject_p.hpp"
#include "../gamescene.hpp"

namespace Qamer {

class TilesFinderAdmin;

class QAMERSHARED_EXPORT GameScenePrivate : public GameObjectPrivate
{
    Q_DECLARE_PUBLIC(GameScene)
public:
    GameScenePrivate();
    ~GameScenePrivate();

protected:
    int m_sceneCol;
    int m_sceneRow;

    int m_cursorCol;
    int m_cursorRow;

    int m_hTileW;
    int m_hTileH;

    bool m_useMempool;
    qSceneLayer mp_layerArray;
private:
    static GameScene *smp_curScene;
    static QList<int> sm_layerOrder;

    TilesFinderAdmin *mp_finders;
};

} // namespace Qamer

#endif // QAMER_GAMESCENEPRIVATE_HPP
