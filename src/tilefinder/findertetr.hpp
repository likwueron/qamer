/*
 * File name: findertetr.hpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_TILEFINDER_FINDERTETR_HPP
#define QAMER_TILEFINDER_FINDERTETR_HPP

#include "../qamer_export.hpp"
#include "finder.hpp"

namespace Qamer {
namespace TileFinder {

class QAMERSHARED_EXPORT FinderTetrStd : public Finder
{
public:
    FinderTetrStd();
    ~FinderTetrStd();

    Finder *usableOne(const GameScene *scene) const;

    QList<QPoint> find(QPoint originPoint,
                       int movepoint, int direction);
protected:
    FinderTetrStd(const FinderTetrStd &that, const GameScene *scene);
};

class QAMERSHARED_EXPORT FinderTetrLine : public Finder
{
public:
    FinderTetrLine();
    ~FinderTetrLine();

    Finder *usableOne(const GameScene *scene) const;

    QList<QPoint> find(QPoint originPoint,
                       int movepoint, int direction);
protected:
    FinderTetrLine(const FinderTetrLine &that, const GameScene *scene);
};

class QAMERSHARED_EXPORT FinderTetrCycle : public Finder
{
public:
    FinderTetrCycle();
    ~FinderTetrCycle();

    Finder *usableOne(const GameScene *scene) const;

    QList<QPoint> find(QPoint originPoint,
                       int movepoint, int direction);

protected:
    FinderTetrCycle(const FinderTetrCycle &that, const GameScene *scene);

    bool withinCycle(const QPoint dp, int r2);
    void findInner(const QPoint &op, const QPoint &p, int &r2, QList<QPoint> &list);
};

} // namespace TileFinder
} // namespace Qamer

#endif // QAMER_TILEFINDER_FINDERTETR_HPP
