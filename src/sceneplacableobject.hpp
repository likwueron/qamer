/*
 * File name: sceneplacableobject.hpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_SCENEPLACABLEOBJECT_HPP
#define QAMER_SCENEPLACABLEOBJECT_HPP

#include "qamer_export.hpp"
#include "gameobject.hpp"

namespace Qamer {

class ScenePlacableObjectPrivate;

class QAMERSHARED_EXPORT ScenePlacableObject : public GameObject
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(ScenePlacableObject)
    Q_PROPERTY(int sceneId READ sceneId WRITE setSceneId SCRIPTABLE false)
    Q_PROPERTY(int sceneIndex READ sceneIndex SCRIPTABLE false)
    Q_PROPERTY(QPoint scenePoint READ scenePoint WRITE setScenePoint NOTIFY scenePoint_changed SCRIPTABLE scriptable STORED false)
    Q_PROPERTY(bool multiPlace READ multiPlace CONSTANT)
public:
    explicit ScenePlacableObject(int type = Enum::ObjectUndefined, QObject *parent = 0);
    ~ScenePlacableObject();

    //scene
        //sceneId will be changed by GameScene, it's passive property
    int sceneId() const;
    void setSceneId(qObjectId scid);
    int sceneIndex() const;
    QPoint scenePoint() const;
    void setScenePoint(QPoint point); //send required to GameScene
    //invoke while Unit is moved by GameScene
    Q_INVOKABLE void movedByScene(int index);

    virtual bool multiPlace() const;
    void painted(QPainter *painter, const QRect &updateRect);
signals:
    void sceneMove_required(qObjectId uid, int index, int oldIndex);
    void scenePoint_changed();
protected:
    //when scenePoint changed or apply new sprite sheet
    virtual void updatePosition();
    //inheriting
    ScenePlacableObject(ScenePlacableObjectPrivate *pdd, int type, QObject *parent);
    //clone
    ScenePlacableObject(const ScenePlacableObject &that);
protected://virtual slot defined in parent
    void when_sheet_inserted(QSize clipSize);
private:

};

} // end namespace Qamer

#endif // QAMER_SCENEPLACABLEOBJECT_HPP
