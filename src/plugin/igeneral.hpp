/*
 * File name: ivariantctor.hpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_PLUGIN_IGENERAL_HPP
#define QAMER_PLUGIN_IGENERAL_HPP

#include <QByteArray>
#include <QVariant>
#include <QVariantList>

namespace Qamer {

typedef QVariant (*VarCtorSignature)(const QVariantList &argv);

namespace Plugin {

enum Type {
    PluginVariant,
    PluginObject,
    PluginMixed
};

#define IGeneral_IDD "org.kwueron.Qamer.Plugin.IGeneral/1.0"

class IGeneral
{
public:
    virtual QByteArray pluginName() const = 0;
    virtual Type pluginType() const = 0;
    virtual QList<QByteArray> supportedNames() const = 0;
    virtual Type pluginTypeFromName(const QByteArray &name) const = 0;
    virtual QObject *createObject(const QByteArray &name,
                                  const QVariantList &argv,
                                  QObject *parent = 0) const = 0;
    virtual QVariant createVariant(const QByteArray &name,
                                   const QVariantList &argv) const = 0;
    virtual VarCtorSignature variantCtor(const QByteArray &name) const = 0;
};

} // end namespace Plugin
} // end namespace Qamer

Q_DECLARE_INTERFACE(Qamer::Plugin::IGeneral,
                    IGeneral_IDD)

#endif // QAMER_PLUIGIN_IGENERAL_HPP

