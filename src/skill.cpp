/*
 * File name: skill.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "skill.hpp"
#include "_p/skill_p.hpp"
#include "core/inordertopostorder.hpp"
#include <QDebug>

namespace Qamer {

Skill::Skill(QObject *parent)
    : GameObject(new SkillPrivate, Enum::ObjectSkill, parent)
{

}

Skill::~Skill()
{

}

ParaList Skill::parameter(int type) const
{
    Q_D(const Skill);
    return d->m_parameterMap.value(type);
}

void Skill::setParameter(int type, const QString &para)
{
    Q_D(Skill);
    d->m_parameterMap.insert(type, InorderToPostorder::fromString(para));
}

void Skill::setPossibleTilesFinderName(const QByteArray &name)
{
    Q_D(Skill);
    d->m_possibleTilesFinderName = name;
}

QByteArray Skill::possibleTilesFinderName() const
{
    Q_D(const Skill);
    return d->m_possibleTilesFinderName;
}

void Skill::setEffectTilesFinderName(const QByteArray &name)
{
    Q_D(Skill);
    if(!(objectType() & Enum::SkillMove)) {
        d->m_effectTilesFinderName = name;
    }
}

QByteArray Skill::effectTilesFinderName() const
{
    Q_D(const Skill);
    return d->m_effectTilesFinderName;
}

int Skill::validTarget() const
{
    Q_D(const Skill);
    return d->m_vTarget;
}

void Skill::setValidTarget(int ur)
{
    Q_D(Skill);
    d->m_vTarget = QFlag(ur);
}

int Skill::validPossibleTiles() const
{
    Q_D(const Skill);
    return d->m_vPossible;
}

void Skill::setValidPossibleTiles(int ur)
{
    Q_D(Skill);
    d->m_vPossible = QFlag(ur);
}

int Skill::animationSpeed() const
{
    Q_D(const Skill);
    return d->m_animationSpeed;
}

void Skill::setAnimationSpeed(int speed)
{
    Q_D(Skill);
    d->m_animationSpeed = speed;
}

void Skill::painted(QPainter *painter, const QRect &updateRect)
{
    Q_UNUSED(updateRect)
    Q_D(const Skill);
    if(!d->hasSheet(0)) return;
    d->sheets_default().at(0).draw(painter, position(), 0);
}

Skill::Skill(SkillPrivate *dpp, QObject *parent)
    : GameObject(dpp, Enum::ObjectSkill, parent)
{

}

Skill::Skill(const Skill &that)
    : GameObject(new SkillPrivate(*that.d_func()), Enum::ObjectCopy, that.parent())
{

}

} // end namespace Qamer
