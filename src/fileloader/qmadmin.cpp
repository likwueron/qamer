/*
 * File name: qmadmin.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "qmadmin.hpp"
#include "../mics/platform.hpp"
#include <QCoreApplication>
#include <QTranslator>
#include <QMutexLocker>
#include <QDir>
#include <QFile>

Qamer::QmAdmin *qLang = 0x0;

namespace Qamer {

SINGLE_CPP(QmAdmin)

QmAdmin::QmAdmin()
    : BaseTextReader()
{
}

QmAdmin::~QmAdmin()
{
    rmLang();
}

QString QmAdmin::qmDir()
{
    return m_langPath;
}

void QmAdmin::setQmDir(const QDir &dir)
{
    setQmDir(dir.path());
}

void QmAdmin::setQmDir(const QString &path)
{
    m_langPath = absPath(path);
}

QString QmAdmin::absPath(const QString &path)
{
    QDir dir(path);
    if(dir.isAbsolute()) {
        return path;
    }
    else {
        QString pathFromDir = dir.path();
        if(pathFromDir.startsWith("./")) {
            pathFromDir = pathFromDir.right(pathFromDir.count() - 1);
        }
        else {
            pathFromDir.prepend('/');
        }
        return QString(platform_abspath() + pathFromDir);
    }
}

void QmAdmin::readConfig(const QString &fileName)
{
    while_newText_reading();
    if(m_langMap.count()) {
        m_langMap.clear();
        m_langPath.clear();
    }

    QString srcFileName = absPath(fileName);

    mp_file = new QFile(srcFileName);
    if(!mp_file->open(QIODevice::ReadOnly)) {
        return;
    }

    QStringList paras;
    //bool commentMode = false;
    while(!mp_file->atEnd()) {
        char c = readCharFromFile();
        if(replaceSpace(c)) continue;

        switch(c) {
        //comment, also end a line reading
        case '#':
            ignoredUntil('\n');
            break;
        //end of line
        case '\n':
            paras << QString::fromUtf8(assignBuffer());
            //set locale-language label map
            if(paras.count() >= 2) {
                QString name = paras.at(0);
                if(name == "LANGDIR") setQmDir(platform_path(paras.at(1)));
                else m_langMap.insert(name, paras.at(1));
            }
            paras.clear();
            break;
        //next parameter
        case '\t':
            paras << QString::fromUtf8(assignBuffer());
            break;
        default:
            bufferAppend(c);
            break;
        }
    }

    //auto set m_langPath
    if(m_langPath.isEmpty()) {
        setQmDir(platform_path("./language"));
    }
}

QStringList QmAdmin::avaliableLangs()
{
    QDir langDir(m_langPath);
    QStringList list = langDir.entryList(QDir::Dirs | QDir::Readable |
                                         QDir::NoDotAndDotDot);
    QStringList result;
    foreach(QString loc, list) {
        QDir dir = langDir;
        dir.cd(loc);
        //whether this dir contains *.qm files
        if(dir.entryList(QStringList() << "*.qm").count()) {
            result << loc;
        }
    }

    return result;
}

QString QmAdmin::lang()
{
    return m_curLoc;
}

void QmAdmin::setLang(const QString &loc)
{
    if(m_curLoc == loc) {
        return;
    }
    //remove old translators
    if(m_curTrFiles.count()) {
        rmLang();
        m_curTrFiles.clear();
    }

    QDir dir(m_langPath);
    if(dir.cd(loc)) {
        QFileInfoList entries = dir.entryInfoList(QStringList() << "*.qm",
                                                  QDir::Files | QDir::Readable);
        //load new translators
        foreach(QFileInfo file, entries) {
            QTranslator *p_tr = new QTranslator;
            if(p_tr->load(file.absoluteFilePath())) {
                QCoreApplication::installTranslator(p_tr);
                m_curTrFiles << p_tr;
            }
            else delete p_tr;
        }
        m_curLoc = loc;
    }
}

void QmAdmin::rmLang()
{
    foreach(QTranslator *p_tr, m_curTrFiles) {
        QCoreApplication::removeTranslator(p_tr);
        delete p_tr;
    }
    m_curTrFiles.clear();
}

QString QmAdmin::lblFromLoc(const QString &loc)
{
    return m_langMap.value(loc, loc);
}

QString QmAdmin::locFromLbl(const QString &lbl)
{
    return m_langMap.key(lbl, lbl);
}

QString QmAdmin::fuzzyLoc(const QString &loc)
{
    QDir dir(m_langPath);
    if(dir.cd(loc)) {
        if(dir.entryList(QStringList() << "*.qm").count()) {
            return loc;
        }
    }
    int dashPos = loc.indexOf('_');
    return loc.left(dashPos);
    /*
    if(m_langMap.contains(loc)) {
        return loc;
    }
    else {
        int dashPos = loc.indexOf('_');
        return loc.left(dashPos);
    }
    */
}

bool Qamer::QmAdmin::isIgnoreChar(char c)
{
    Q_UNUSED(c)
    return false;
}

} // end namespace Qamer
