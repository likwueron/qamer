/*
 *File name: controlleradministrator.hpp
 *Author: kwueron
 *E-mail: %EMAIL%
 *Last revised: 2014/4/11
 *Published under Public Domain
 */

#ifndef CONTROLLERADMINISTRATOR_HPP
#define CONTROLLERADMINISTRATOR_HPP

#include "../qamer_global.hpp"
#include <QObject>
#include <QList>
#include <QTimer>

namespace Qamer {

class Joystick;

class QAMERSHARED_EXPORT ControllerAdministrator : public QObject
{
public:
    enum State {
        JoyNotOpened = 0x1,    //This Object has not open any SDL_Joystick
        JoyHasOpened = 0x2,
        JoyInUsed = 0x4,       //This Object has open a SDL_Joystick successfully
        JoyCannotUsed = 0x10,  //SDL_Joystick open failure
        JoyWillKeeped = 0xf000 //This Object will be keeped during refresh()
    };

    ~ControllerAdministrator();
    //admin
    static ControllerAdministrator *instance(QOjbect *parent);
    void init();
    void quit();
    void refresh();
    void setUpdateTime(int millisecond);
protected:
    void timerEvent(QTimerEvent *);
    int m_timerId;
private:
    explicit ControllerAdministrator(QObject *parent);

    static ControllerAdministrator *smp_instance;

    //admin
    bool m_sdlWork;

    QList<Joystick*> m_joystickList;
};

} //namespace Qamer

#endif // QAMER_CONTROLLERADMINISTRATOR_HPP
