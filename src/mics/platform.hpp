/*
 * File name: platform.hpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_PLATFORM_HPP
#define QAMER_PLATFORM_HPP

#include "../qamer_export.hpp"
#include <QString>
#include <QDir>

namespace Qamer {

inline QString platform_path(const QString &path)
{
    QString result = path;
#ifdef __ANDROID__
    if(result.startsWith('.')) {
        result.replace(0, 1, ':');//if use qrc
        //path.replace(0, 1, "assert:"); //if use android assert
    }
#else
#endif
    return result;
}

inline QString platform_abspath()
{
#ifdef __ANDROID__
    return QLatin1String(":");
#else
    return QDir::currentPath();
#endif
}

inline QString platform_librarySuffix()
{
#ifdef Q_OS_WIN
    return QString(".dll");
#elif defined(Q_OS_UNIX)
    return QString(".so");
#elif defined(Q_OS_MAC)
    return QString(".dylib");
#endif
}

} // namespace Qamer

#endif // QAMER_PLATFORM_HPP
