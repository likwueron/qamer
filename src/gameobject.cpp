/*
 * File name: gameobject.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "gameobject.hpp"
#include "_p/gameobject_p.hpp"
#include "controller/controllerevent.hpp"
//#include "controller/controlleradministrator.hpp"
#include <QWidget>
#include <QPainter>
#include <QVariant>
#include <QMetaProperty>
#include <QDebug>

namespace Qamer {

GameObject::GameObject(int objType, QObject *parent)
    : QObject(*new GameObjectPrivate, parent)
{
    setup(objType);
}

GameObject::~GameObject()
{
    Q_D(const GameObject);
    if(d->m_listId != invalidId) {
        GameObjectPrivate::sm_objectMap[d->m_listId] = nullptr;
    }
}

int GameObject::objectNumber(int type)
{
    if(type == Enum::ObjectAll) {
        return GameObjectPrivate::sm_objectMap.count();
    }
    else {
        int sum = 0;
        foreach(const GameObject* o, GameObjectPrivate::sm_objectMap) {
            if(o->objectType() & type) {
                sum++;
            }
        }
        return sum;
    }
}

qObjectId GameObject::findObjectId(const QString &name, int type)
{
    QMap<int, GameObject*>::const_iterator i =
            GameObjectPrivate::sm_objectMap.constBegin();
    while(i != GameObjectPrivate::sm_objectMap.constEnd()) {
        GameObject *obj = i.value();
        if((obj->objectType() & type) && obj->objectName() == name) {
            return i.key();
        }
    }
    return invalidId;
}

GameObject *GameObject::findObject(const QString &name, int type)
{
    foreach(GameObject *o, GameObjectPrivate::sm_objectMap) {
        if((o->objectType() & type) && o->objectName() == name) {
            return o;
        }
    }
    return nullptr;
}

GameObject *GameObject::findObject(qObjectId id, int type)
{
    GameObject *obj = GameObjectPrivate::sm_objectMap.value(id, nullptr);
    if(obj && obj->isObjectType(type)) return obj;
    else return nullptr;
}

void GameObject::setSystemName(const QString &name)
{
    setObjectName(name);
}

int GameObject::objectTypeFromId(qObjectId id)
{
//    if(isIdValid(id)) {
//        return GameObjectPrivate::sm_objectList.at(id)->objectType();
//    }
    if(isIdValid(id)) {
        return GameObjectPrivate::sm_objectMap.value(id)->objectType();
    }
    else return Enum::ObjectUndefined;
}

int GameObject::objectType() const
{
    Q_D(const GameObject);
    return d->m_objectType;
}

bool GameObject::isObjectType(int objectType) const
{
    Q_D(const GameObject);
    return d->m_objectType & objectType;
}

int GameObject::subObjectType() const
{
    Q_D(const GameObject);
    return d->m_objectType & Enum::ObjectSubTypeMask;
}

void GameObject::setSubObjectType(int type)
{
    Q_D(GameObject);
    //remove subtype in current type
    d->m_objectType &= Enum::ObjectMainTypeMask;
    //remove maintype(you cannot change that!)
    type &= Enum::ObjectSubTypeMask;
    //apply new subtype
    d->m_objectType |= type;
}

bool GameObject::isIdValid(qObjectId id)
{
    //return (0 <= id) && (id < objectNumber());
    return GameObjectPrivate::sm_objectMap.contains(id);
}

int GameObject::objectId() const
{
    Q_D(const GameObject);
    return d->m_listId;
}

int GameObject::cf_addProperty(const QByteArray &name, const QVariant &value)
{
    //is statValue
    bool isStatValue = false;
    if(isObjectType(Enum::ObjectUnit)) {
        QMetaObject::invokeMethod(this, "hasStatValue",
                                  Q_RETURN_ARG(bool, isStatValue), Q_ARG(QByteArray, name));
    }
    //try set as statValue
    if(isStatValue) {
        bool ok = false;
        int val = value.toInt(&ok);
        if(ok) {
            QMetaObject::invokeMethod(this, "setStatCurrentValue",
                                      Q_ARG(QByteArray, name), Q_ARG(int, val));
        }
        else return CF_ParaTypeError;
    }
    //try set as property
    else {
        const QMetaObject *meta = metaObject();
        int start = GameObject::staticMetaObject.propertyOffset();
        int at = meta->indexOfProperty(name);
        //set as dynamic property
        if(at < 0) {
            setProperty(name, value);
        }
        //reject QObject's property
        else if(at < start) return CF_AccessError;
        //set static property
        else {
            QMetaProperty mp = meta->property(at);
            //check SCRITABLE
            if(mp.isScriptable()) mp.write(this, value);
            else return CF_ScriptableError;
        }
    }
    return CF_NoError;
}

const ObjectInfo &GameObject::infomation() const
{
    Q_D(const GameObject);
    return d->m_infomation;
}

QString GameObject::name() const
{
    Q_D(const GameObject);
    return d->m_infomation.name;
}

void GameObject::setName(const QString &name)
{
    Q_D(GameObject);
    d->m_infomation.name = name;
}

QString GameObject::description() const
{
    Q_D(const GameObject);
    return d->m_infomation.description;
}

void GameObject::setDescription(const QString &des)
{
    Q_D(GameObject);
    d->m_infomation.description = des;
}

void GameObject::setControllerFocus(int cid)
{
    Q_UNUSED(cid)
    //ControllerAdministrator
}

QPoint GameObject::position() const
{
    Q_D(const GameObject);
    return d->m_position.toPoint();
}

QRect GameObject::geometry() const
{
    Q_D(const GameObject);
    return QRect(d->m_position.toPoint(), d->m_size);
}

QPoint GameObject::movePeek(const QRect &thisRect, QRect rect, Enum::MoveMode mode)
{
    QPoint nPos = rect.topLeft();
    QSize _size;
    switch(mode & Enum::MovePosMask) {
    case Enum::MoveOn:
        _size = thisRect.size();
        break;
    case Enum::MoveAside:
        _size = QSize(0, 0);
        break;
    default:
        rect.setSize(QSize(0, 0));
        _size = thisRect.size() * -1;
    }

    int dw = rect.width() - _size.width();
    switch(mode & Enum::MoveHMask) {
    case Enum::MoveLeft:
        if(mode & Enum::MoveAside) nPos.rx() -= dw;
        break;
    case Enum::MoveHCenter:
        nPos.rx() += dw / 2;
        break;
    case Enum::MoveRight:
        nPos.rx() += dw;
        break;
    default:
        break;
    }

    int dh = rect.height() - _size.height();
    switch(mode & Enum::MoveVMask) {
    case Enum::MoveTop:
        if(mode & Enum::MoveAside) nPos.ry() -= dh;
        break;
    case Enum::MoveVCenter:
        nPos.ry() += dh / 2;
        break;
    case Enum::MoveBottom:
        nPos.ry() += dh;
        break;
    default:
        break;
    }

    return nPos;
}

void GameObject::move(QPoint position, Enum::MoveMode mode)
{
    Q_D(GameObject);
    QPoint delta = QPoint(0, 0);
    switch(mode & Enum::MoveHMask) {
    case Enum::MoveHCenter:
        delta.setX(d->m_size.width() / 2);
        break;
    case Enum::MoveRight:
        delta.setX(d->m_size.width());
        break;
    default:
        break;
    }
    switch(mode & Enum::MoveVMask) {
    case Enum::MoveVCenter:
        delta.setY(d->m_size.height() / 2);
        break;
    case Enum::MoveBottom:
        delta.setY(d->m_size.height());
        break;
    default:
        break;
    }

    QPoint old = d->m_position.toPoint();
    d->m_position = position + delta;
    if(d->m_position != old) emit moved(old);
}

void GameObject::move(QRect rect, Enum::MoveMode mode)
{
    Q_D(GameObject);
    QPoint old = d->m_position.toPoint();
    d->m_position = movePeek(QRect(old, d->m_size), rect, mode);
    if(d->m_position != old) emit moved(old);
}

QSize GameObject::size() const
{
    Q_D(const GameObject);
    return d->m_size;
}

void GameObject::resize(QSize size)
{
    Q_D(GameObject);
    QSize old = d->m_size;
    d->m_size = size;
    emit resized(old);
}

QSize GameObject::parentSize() const
{
    QObject *p_o = parent();
    while(p_o) {
        QWidget *p_p = qobject_cast<QWidget*>(p_o);
        if(p_p) {
            return p_p->size();
        }
        p_o = p_o->parent();
    }

    return QSize();
}

void GameObject::addSpriteSheet(const SpriteSheet &sheet)
{
    Q_D(GameObject);
    d->addSpriteSheet(sheet);
}

void GameObject::addSpriteSheet(const QString &file, int row, int col, bool applyMask, QColor maskColor)
{
    SpriteSheet sheet;
    sheet.load(file, row, col, applyMask, maskColor);
    addSpriteSheet(sheet);
}

void GameObject::addSpriteSheetFixsize(const QString &file, int w, int h, bool applyMask, QColor maskColor)
{
    SpriteSheet sheet;
    sheet.loadFixsize(file, w, h, applyMask, maskColor);
    addSpriteSheet(sheet);
}

bool GameObject::isVisible() const
{
    Q_D(const GameObject);
    return d->m_isVisible;
}

void GameObject::setVisible(bool visible)
{
    Q_D(GameObject);
    d->m_isVisible = visible;
}

void GameObject::invertVisible()
{
    Q_D(GameObject);
    d->m_isVisible = !d->m_isVisible;
}

void GameObject::addAnimationScheme(int id, const QList<quint16> &list)
{
    Q_D(GameObject);
    d->addScheme(qMin(id, (int)USHRT_MAX), list);
}

void GameObject::addAnimationScheme(int id, const QVariantList &list)
{
    QList<quint16> result;
    int count = qMin(list.count(), (int)USHRT_MAX+1);
    result.reserve(count);
    for(int i = 0; i < count; i++) {
        result << qMin(list.at(i).toUInt(), uint(USHRT_MAX));
    }
    Q_D(GameObject);
    d->addScheme(qMin(id, (int)USHRT_MAX), result);
}

void GameObject::setCurrentAnimationScheme(int id, int index)
{
    Q_D(GameObject);
    d->setScheme(id, index);
}

void GameObject::progressAnimation()
{
    Q_D(GameObject);
    if(d->progressScheme() == 0) {
        emit schemeEnd_reached(d->currentSchemeId());
    }
}

int GameObject::spriteSheetIndex() const
{
    Q_D(const GameObject);
    return d->currentPixmapIndex();
}

void GameObject::setSpriteSheetIndex(int index)
{
    Q_D(GameObject);
    d->setCurrentPixmapIndex(qMin(index, (int)USHRT_MAX));
}

bool GameObject::scriptable() const
{
    return isScriptable(ScriptableQS);
}

bool GameObject::loadable() const
{
    return isScriptable(ScriptableFile);
}

void GameObject::painted(QPainter *painter, int x, int y)
{
    Q_UNUSED(painter)
    Q_UNUSED(x)
    Q_UNUSED(y)
}

GameObject *GameObject::clone() const
{
    return nullptr;
}

bool GameObject::isClonable() const
{
    return false;
}

void GameObject::when_moved(QPoint old)
{
    Q_UNUSED(old)
}

void GameObject::when_resized(QSize old)
{
    Q_UNUSED(old)
}

void GameObject::when_sheet_inserted(QSize clipSize)
{
    Q_UNUSED(clipSize)
}

void GameObject::when_schemeEnd_reached(int scheme)
{
    Q_UNUSED(scheme)
}

bool GameObject::isScriptable(GameObject::ScriptableTag tag)
{
    return GameObjectPrivate::sm_sTag == tag;
}

void GameObject::setScriptableTag(GameObject::ScriptableTag tag)
{
    GameObjectPrivate::sm_sTag = tag;
}

bool GameObject::event(QEvent *e)
{
    if(e->type() == ControllerEvent::controllerEventType) {
        controllerEvent((ControllerEvent *)e);
        return true;
    }
    return QObject::event(e);
}

void GameObject::controllerEvent(ControllerEvent *e)
{
    if(!e->isAccepted()) {
        GameObject *p = qobject_cast<GameObject*>(parent());
        if(p) {
            p->controllerEvent(e);
        }
    }
}

void GameObject::appendToList()
{
    Q_D(GameObject);
    d->m_listId = GameObjectPrivate::generateId();
    GameObjectPrivate::sm_objectMap.insert(d->m_listId, this);
}

bool GameObject::isStaticProperty(const QByteArray &name)
{
    const QMetaObject *meta = metaObject();
    int count = meta->propertyCount();
    for(int i = 0; i < count; i++) {
        QMetaProperty mp = meta->property(i);
        if(qstrcmp(mp.name(), name.constData()) == 0) {
            return true;
        }
    }
    return false;
}

QList<QByteArray> GameObject::staticPropertyNames(PropertyAttr attr)
{
    QList<QByteArray> result;

    const QMetaObject *meta = metaObject();
    int count = meta->propertyCount();
    int start = staticMetaObject.propertyOffset();
    for(int i = start; i < count; i++) {
        QMetaProperty mp = meta->property(i);
        if( ((attr & Readable) && mp.isReadable()) ||
                ((attr & Writable) && mp.isWritable()) ||
                ((attr & Resetable) && mp.isResettable()) ||
                ((attr & Designable) && mp.isDesignable(this)) ||
                ((attr & Scriptable) && mp.isScriptable(this)) ||
                ((attr & Stored) && mp.isStored(this)) ||
                ((attr & Editable) && mp.isEditable(this)) ||
                ((attr & User) && mp.isUser(this)) ||
                ((attr & Constant) && mp.isConstant()) ||
                ((attr & Final) && mp.isFinal())) {
            result << mp.name();
        }
    }
    return result;
}

GameObject::GameObject(GameObjectPrivate *pdd, int objType, QObject *parent)
    : QObject(*pdd, parent)
{
    setup(objType);
}

GameObject::GameObject(const GameObject &that)
    : QObject(*new GameObjectPrivate(*(that.d_func())), that.parent())
{
    setupConnect();
}

void GameObject::setup(int objType)
{
    Q_D(GameObject);
    if(objType != Enum::ObjectCopy) {
        d->m_objectType = objType;
    }

    if(d->m_objectType != Enum::ObjectUndefined) appendToList();
    else d->m_listId = invalidId;

    setupConnect();
}

void GameObject::setupConnect()
{
    connect(this, SIGNAL(moved(QPoint)),
            SLOT(when_moved(QPoint)));
    connect(this, SIGNAL(resized(QSize)),
            SLOT(when_resized(QSize)));
    connect(this, SIGNAL(spriteSheet_inserted(QSize)),
            SLOT(when_sheet_inserted(QSize)));
    connect(this, SIGNAL(schemeEnd_reached(int)),
            SLOT(when_schemeEnd_reached(int)));
}

GameObjectId::GameObjectId()
    :m_id(-1)
{

}

GameObjectId::GameObjectId(qObjectId id)
{
    setId(id);
}

GameObjectId::GameObjectId(const GameObject *obj)
{
    if(obj) setId(obj->objectId());
}

GameObjectId::GameObjectId(const GameObjectId &that)
    : m_id(that.m_id)
{

}

GameObjectId::~GameObjectId()
{

}

qObjectId GameObjectId::id() const
{
    return m_id;
}

void GameObjectId::setId(qObjectId id)
{
    m_id = id;
}

void GameObjectId::setId(const QString &name)
{
    GameObject *obj = GameObject::findObject(name);
    if(obj) m_id = obj->objectId();
    else m_id = invalidId;
}

GameObject *GameObjectId::object() const
{
    return GameObject::findObject(m_id);
}

bool GameObjectId::isValid() const
{
    return GameObject::isIdValid(m_id);
}

} // end namespace Qamer
