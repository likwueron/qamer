/*
 * File name: pseudosingleton.hpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_PSEUDOSINGLETON_HPP
#define QAMER_PSEUDOSINGLETON_HPP

#include <QtGlobal>

namespace Qamer {

#define SINGLE(CLASS) \
class S ## CLASS { \
protected: \
    S ## CLASS() { \
        Q_ASSERT_X(!sm_hasInstance, #CLASS, \
        "Cannot creat more than one instance."); \
        sm_hasInstance = true; } \
    virtual ~S ## CLASS() { sm_hasInstance = false; } \
private: \
    static bool sm_hasInstance;\
};

#define SINGLE_CPP(CLASS) \
    bool S ## CLASS::sm_hasInstance = false;

} // end namespace Qamer

#endif // QAMER_PSEUDOSINGLETON_HPP
