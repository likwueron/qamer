/*
 * File name: gameui_p.hpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_GAMEUIPRIVATE_HPP
#define QAMER_GAMEUIPRIVATE_HPP

#include "../../_p/gameobject_p.hpp"
#include "../gameui.hpp"
#include <QFont>

namespace Qamer {

class QAMERSHARED_EXPORT GameUIPrivate : public GameObjectPrivate
{
    Q_DECLARE_PUBLIC(GameUI)
public:
    static const QFont defaultFont;

    GameUIPrivate();
    ~GameUIPrivate();

    void resetFont();

protected:
    GameUIPrivate(const GameUIPrivate &that);

    QFont m_font;
    Qt::Orientation m_orientation;
};

} // namespace Qamer

#endif // QAMER_GAMEUIPRIVATE_HPP
