/*
 * File name: screen.hpp
 * Copyright 2015 Li, Kwueron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QAMER_SCREEN_HPP
#define QAMER_SCREEN_HPP

#include "../qamer_export.hpp"
#include <QWidget>

namespace Qamer {

class QAMERSHARED_EXPORT Screen : public QWidget
{
    Q_OBJECT
    Q_CLASSINFO("LuaName", "Screen")

public:
    Q_INVOKABLE explicit Screen(QWidget *parent = 0);

signals:
    void changeScreen_required(int id);
public slots:
    void when_widgetToScreen_required();
    void when_widgetPerformScript_required();


};

} // namespace Qamer

#endif // QAMER_SCREEN_HPP
