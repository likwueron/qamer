/*
 * File name: qamer.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "qamer.hpp"
#include <QApplication>
#include <QMessageBox>

namespace Qamer {

const char *version()
{
    return "0.5.1";
}

int compilerVirsion()
{
#ifdef __GNUC__
    return (__GNUC__ * 10000 \
            + __GNUC_MINOR__ * 100 \
            + __GNUC_PATCHLEVEL__);
#endif
#ifdef _MSC_VER
    return _MSC_VER;
#endif
}

const char *qtVersion()
{
    return QT_VERSION_STR;
}

void aboutQamer()
{
    QMessageBox::about(QApplication::activeWindow(),
                       aboutQamerTitle(),
                       aboutQamerText());
}

QString aboutQamerTitle()
{
    return QCoreApplication::translate("Qamer", "About Qamer");
}

QString aboutQamerText()
{
    return QCoreApplication::translate("Qamer", "Qamer is a game engine designed by Qt library.\n\nVersion: %1\nQt Version: %2")
            .arg(version())
            .arg(qtVersion());
}

} // end namespace Qamer
