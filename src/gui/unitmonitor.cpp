/*
 * File name: unitmonitor.cpp
 * Copyright 2015 Li, Kwue-Ron
 * E-mail: likwueron@gmail.com
 */
/*
 * This file is part of Qamer.
 * Qamer is free software: you can redistribute it and/or modify
 * it under the terms of the Lesser GNU General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.

 * Qamer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.
 * See the Lesser GNU General Public License for more details.

 * You should have received a copy of
 * the Lesser GNU General Public License along with Qamer.
 * File named "COPYRIGHT.LESSER".
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "unitmonitor.hpp"
#include "gameprogressbar.hpp"
#include "gamelabel.hpp"
#include <QPainter>
#include <QDebug>

namespace Qamer
{

UnitMonitor::UnitMonitor(QObject *parent)
    : GameUI(parent),
      up_unit(nullptr)
{

}

UnitMonitor::~UnitMonitor()
{

}

void UnitMonitor::setObject(GameObject *obj)
{
    up_unit = obj;

    setUi();
}

void UnitMonitor::watchBar(const QString &label, const QByteArray &propertyName, const QColor &minColor, const QColor &maxColor, QPoint at)
{
    GameProgressBar *bar = new GameProgressBar(this);
    watch_inner(bar, label, propertyName, at);
    bar->setColors(minColor, maxColor);
}

void UnitMonitor::watchValue(const QString &label, const QByteArray &propertyName, QPoint at)
{
    GameLabel *lbl = new GameLabel(this);
    watch_inner(lbl, label, propertyName, at);
}

void UnitMonitor::painted(QPainter *painter, const QRect &updateRect)
{
    if(!up_unit || !isVisible()) return;
    painter->save();
    painter->translate(position());//use relative position
    foreach(GameUI *uiobj, m_interfaceList) {
        uiobj->painted(painter, updateRect);
    }
    painter->restore();
}

void UnitMonitor::watch_inner(GameUI *uiobj, const QString &label, const QByteArray &propertyName, QPoint at)
{
    uiobj->setFont(font());

    uiobj->setName(label);
    uiobj->setProperty("watch", propertyName);
    uiobj->move(at);
    m_interfaceList << uiobj;
}

void UnitMonitor::setUi()
{
    if(!up_unit) return;

    foreach(GameUI *uiobj, m_interfaceList) {
        QByteArray propertyName = uiobj->property("watch").toByteArray();
        bool ok = false;
        switch(uiobj->subObjectType()) {
        case Enum::InterfaceProgressBar: {
            GameProgressBar *bar = reinterpret_cast<GameProgressBar*>(uiobj);
            int curVal = -1;
            ok = QMetaObject::invokeMethod(up_unit, "statCurrentValue",
                                           Q_RETURN_ARG(int, curVal),
                                           Q_ARG(QByteArray, propertyName));
            if(ok) bar->setValue(curVal);

            int maxVal = -1;
            ok = QMetaObject::invokeMethod(up_unit, "statMaxValue",
                                           Q_RETURN_ARG(int, maxVal),
                                           Q_ARG(QByteArray, propertyName));
            if(ok) bar->setMaxValue(maxVal);
        }
            break;
        case Enum::InterfaceLabel: {
            GameLabel *lbl = reinterpret_cast<GameLabel*>(uiobj);
            int val = 0;
            QMetaObject::invokeMethod(up_unit, "statCurrentValue",
                                      Q_RETURN_ARG(int, val),
                                      Q_ARG(QByteArray, propertyName));
            if(val != invalidStatValue) lbl->setText(QString::number(val));
            else {
                QVariant var = up_unit->property(propertyName);
                //only support String, Bool, Date and Time or Number
                //but i think that's enough
                lbl->setText(var.toString());
            }
        }
            break;
        default:
            break;
        }
    }
}

} // end namespace Qamer
